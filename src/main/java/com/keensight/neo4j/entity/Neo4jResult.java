package com.keensight.neo4j.entity;

import org.neo4j.ogm.annotation.NodeEntity;

/**
 * @author 46106
 */
@NodeEntity
public class Neo4jResult {

    private Neo4jBasicNode start;
    private Neo4jBasicRelationship relationship;
    private Neo4jBasicNode end;

    /**
     * Gets the value of start.
     *
     * @return the value of start
     */
    public Neo4jBasicNode getStart() {
        return start;
    }

    /**
     * Sets the start.
     *
     * <p>You can use getStart() to get the value of start</p>
     *
     * @param start start
     */
    public void setStart(Neo4jBasicNode start) {
        this.start = start;
    }

    /**
     * Gets the value of relationship.
     *
     * @return the value of relationship
     */
    public Neo4jBasicRelationship getRelationship() {
        return relationship;
    }

    /**
     * Sets the relationship.
     *
     * <p>You can use getRelationship() to get the value of relationship</p>
     *
     * @param relationship relationship
     */
    public void setRelationship(Neo4jBasicRelationship relationship) {
        this.relationship = relationship;
    }

    /**
     * Gets the value of end.
     *
     * @return the value of end
     */
    public Neo4jBasicNode getEnd() {
        return end;
    }

    /**
     * Sets the end.
     *
     * <p>You can use getEnd() to get the value of end</p>
     *
     * @param end end
     */
    public void setEnd(Neo4jBasicNode end) {
        this.end = end;
    }
}
