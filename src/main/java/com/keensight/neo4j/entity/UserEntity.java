package com.keensight.neo4j.entity;

public class UserEntity {
    private String display_name;
    private Long user_id;
    /**
     * Gets the value of display_name.
     *
     * @return the value of display_name
     */
    public String getDisplay_name() {
        return display_name;
    }

    /**
     * Sets the display_name.
     *
     * <p>You can use getDisplay_name() to get the value of display_name</p>
     *
     * @param display_name display_name
     */
    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    /**
     * Gets the value of user_id.
     *
     * @return the value of user_id
     */
    public Long getUser_id() {
        return user_id;
    }

    /**
     * Sets the user_id.
     *
     * <p>You can use getUser_id() to get the value of user_id</p>
     *
     * @param user_id user_id
     */
    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }
}
