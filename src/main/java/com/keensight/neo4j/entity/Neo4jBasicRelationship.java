package com.keensight.neo4j.entity;

import org.neo4j.ogm.model.Property;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author 46106
 */
public class Neo4jBasicRelationship {
    private Long id;
    private String type;
    private Long start;
    private Long end;
    private Map<String, Object> property;

    /**
     * Gets the value of id.
     *
     * @return the value of id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * <p>You can use getId() to get the value of id</p>
     *
     * @param id id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the value of type.
     *
     * @return the value of type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type.
     *
     * <p>You can use getType() to get the value of type</p>
     *
     * @param type type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets the value of start.
     *
     * @return the value of start
     */
    public Long getStart() {
        return start;
    }

    /**
     * Sets the start.
     *
     * <p>You can use getStart() to get the value of start</p>
     *
     * @param start start
     */
    public void setStart(Long start) {
        this.start = start;
    }

    /**
     * Gets the value of end.
     *
     * @return the value of end
     */
    public Long getEnd() {
        return end;
    }

    /**
     * Sets the end.
     *
     * <p>You can use getEnd() to get the value of end</p>
     *
     * @param end end
     */
    public void setEnd(Long end) {
        this.end = end;
    }


    /**
     * Gets the value of property.
     *
     * @return the value of property
     */
    public Map<String, Object> getProperty() {
        return property;
    }

    /**
     * Sets the property.
     *
     * <p>You can use getProperty() to get the value of property</p>
     *
     * @param property property
     */
    public void setProperty(Map<String, Object> property) {
        this.property = property;
    }
}
