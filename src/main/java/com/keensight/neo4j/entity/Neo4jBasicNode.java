package com.keensight.neo4j.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
/**
 * neo4j 节点实体类
 * @author YangBM
 */
public class Neo4jBasicNode implements Serializable {
    /**
     * id
     */
    private Long id;
    /**
     * 标签
     */
    private List<String> labels;
    /**
     * 标签属性
     */
    private Map<String, Object> property;


    /**
     * Gets the value of id.
     *
     * @return the value of id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * <p>You can use getId() to get the value of id</p>
     *
     * @param id id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the value of labels.
     *
     * @return the value of labels
     */
    public List<String> getLabels() {
        return labels;
    }

    /**
     * Sets the labels.
     *
     * <p>You can use getLabels() to get the value of labels</p>
     *
     * @param labels labels
     */
    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    /**
     * Gets the value of property.
     *
     * @return the value of property
     */
    public Map<String, Object> getProperty() {
        return property;
    }

    /**
     * Sets the property.
     *
     * <p>You can use getProperty() to get the value of property</p>
     *
     * @param property property
     */
    public void setProperty(Map<String, Object> property) {
        this.property = property;
    }
}
