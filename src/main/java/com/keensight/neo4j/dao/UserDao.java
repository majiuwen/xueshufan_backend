package com.keensight.neo4j.dao;

import com.keensight.neo4j.entity.UserEntity;
import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface  UserDao extends Neo4jRepository<UserEntity, Long>{
}
