package com.keensight.solr.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "solr.server")
public class SolrConfig {
    private String autocompleteUrl;

    private String publicationSearchUrl;

    private String userSearchUrl;

    private String fieldSearchUrl;

    private String institutionSearchUrl;

    private int connectionTimeout;

    private int socketTimeout;

    public String getAutocompleteUrl() {
        return autocompleteUrl;
    }

    public void setAutocompleteUrl(String autocompleteUrl) {
        this.autocompleteUrl = autocompleteUrl;
    }

    public String getPublicationSearchUrl() {
        return publicationSearchUrl;
    }

    public void setPublicationSearchUrl(String publicationSearchUrl) {
        this.publicationSearchUrl = publicationSearchUrl;
    }

    public String getUserSearchUrl() {
        return userSearchUrl;
    }

    public void setUserSearchUrl(String userSearchUrl) {
        this.userSearchUrl = userSearchUrl;
    }

    public String getFieldSearchUrl() {
        return fieldSearchUrl;
    }

    public void setFieldSearchUrl(String fieldSearchUrl) {
        this.fieldSearchUrl = fieldSearchUrl;
    }

    public String getInstitutionSearchUrl() {
        return institutionSearchUrl;
    }

    public void setInstitutionSearchUrl(String institutionSearchUrl) {
        this.institutionSearchUrl = institutionSearchUrl;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public int getSocketTimeout() {
        return socketTimeout;
    }

    public void setSocketTimeout(int socketTimeout) {
        this.socketTimeout = socketTimeout;
    }
}
