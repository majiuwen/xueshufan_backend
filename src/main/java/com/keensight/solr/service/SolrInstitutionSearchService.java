package com.keensight.solr.service;

import cn.newtouch.dl.solr.model.SolrResultModel;
import cn.newtouch.dl.solr.model.SolrSortSearchItem;
import cn.newtouch.dl.solr.service.SolrSearchService;
import com.keensight.solr.config.SolrConfig;
import com.keensight.solr.model.SolrInstitutionSearchModel;
import com.keensight.solr.utils.AutocompleteUtil;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class SolrInstitutionSearchService extends SolrSearchService {


    public SolrInstitutionSearchService(SolrConfig solrConfig) {
        super();

        SolrClient client = new HttpSolrClient.Builder(solrConfig.getInstitutionSearchUrl())
                .withConnectionTimeout(solrConfig.getConnectionTimeout())
                .withSocketTimeout(solrConfig.getSocketTimeout())
                .build();
        super.setSolrClient(client);
    }

    /**
     * keyword检索，获取研究机构ID和内容
     *
     * @param keyword 检索keyword
     * @param autocompleteKeyword 画面自动补全keyword
     * @param index 开始页数
     * @param rows 检索件数
     *
     * @return SolrResultModel.rows 总件数
     * @return SolrResultModel.customVal 研究机构
     */
    public SolrResultModel getResultsInstitutionByKeyword(String keyword, String autocompleteKeyword,int index, int rows) {
        SolrInstitutionSearchModel searchModel = new SolrInstitutionSearchModel();
        SolrResultModel solrResultModel = null;

        // 显示字段设定
        String[] outputCols = new String[]{"id", "institution"};
        searchModel.setOutputCols(outputCols);

        List<SolrSortSearchItem> sort = new ArrayList<SolrSortSearchItem>();

        sort.add(new SolrSortSearchItem("rank"));

        searchModel.setSortInfoList(sort);

        // keyword
        if (StringUtils.isEmpty(autocompleteKeyword)) {
            searchModel.setKeyword(keyword);
        } else {
            // TODO 自动补全要进一步处理
            AutocompleteUtil.setKeyword(keyword, autocompleteKeyword, searchModel);
        }

        // 检索件数
        if (rows > 0) {
            searchModel.setRows(rows);
        }

        if (index <= 0) {
            index = 1;
        }
        searchModel.setStart((index - 1) * searchModel.getRows());

        try {
            solrResultModel = this.search(searchModel, SolrInstitutionSearchModel.class);
        } catch (Exception e) {

        }
        return solrResultModel;
    }
}
