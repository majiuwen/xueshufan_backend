package com.keensight.solr.service;

import cn.newtouch.dl.solr.model.SolrResultModel;
import cn.newtouch.dl.solr.model.SolrSortSearchItem;
import cn.newtouch.dl.solr.service.SolrSearchService;
import com.keensight.solr.config.SolrConfig;
import com.keensight.solr.model.SolrUserSearchModel;
import com.keensight.solr.utils.AutocompleteUtil;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SolrUserSearchService extends SolrSearchService {


    public SolrUserSearchService(SolrConfig solrConfig) {
        super();

        SolrClient client = new HttpSolrClient.Builder(solrConfig.getUserSearchUrl())
                .withConnectionTimeout(solrConfig.getConnectionTimeout())
                .withSocketTimeout(solrConfig.getSocketTimeout())
                .build();
        super.setSolrClient(client);
    }

    /**
     * keyword检索，获取用户ID
     *
     * @param keyword 检索keyword
     * @param autocompleteKeyword 画面自动补全keyword
     * @param index 开始页数
     * @param rows 检索件数
     * @param allUserIds 用户ID
     *
     * @return SolrResultModel.rows 总件数
     * @return SolrResultModel.customVal 文献ID
     */
    public SolrResultModel getUserIdByKeyword(String keyword, String autocompleteKeyword, int index, int rows, List<Long> allUserIds) {
        SolrUserSearchModel searchModel = new SolrUserSearchModel();
        SolrResultModel solrResultModel = null;
        List<Long> retUserIds = new ArrayList<Long>();

        // 显示字段设定
        String[] outputCols = new String[]{"id"};
        searchModel.setOutputCols(outputCols);
        if (allUserIds == null || allUserIds.size() == 0) {
            List<SolrSortSearchItem> sort = new ArrayList<SolrSortSearchItem>();
            sort.add(new SolrSortSearchItem("rank"));
            searchModel.setSortInfoList(sort);
        }

        // keyword
        if (StringUtils.isEmpty(autocompleteKeyword)) {
            searchModel.setKeyword(keyword);
        } else {
            AutocompleteUtil.setKeyword(keyword, autocompleteKeyword, searchModel);
        }

        // 参数判断
        if (!StringUtils.isEmpty(allUserIds)) {
            searchModel.setUserIds(allUserIds);
        }

        // 检索件数
        if (rows > 0) {
            searchModel.setRows(rows);
        }

        if (index <= 0) {
            index = 1;
        }
        searchModel.setStart((index - 1) * searchModel.getRows());

        try {
            solrResultModel = this.search(searchModel, SolrUserSearchModel.class);
            // 用户ID
            if (solrResultModel.getRows() > 0) {
                List<Long> userIds = new ArrayList<Long>();
                List<SolrUserSearchModel> solrUserSearchModelList = solrResultModel.getENTRY();
                for (SolrUserSearchModel solrUserSearchModel : solrUserSearchModelList) {
                    userIds.add(Long.valueOf(solrUserSearchModel.getId()));
                }

                if (allUserIds == null || allUserIds.size() == 0) {
                    solrResultModel.setCustomVal(userIds);
                } else {
                    // 从集合中查询想要的数据
                    for (Long userId : allUserIds) {
                        retUserIds.addAll(userIds.stream().filter(id -> userId.equals(id)).collect(Collectors.toList()));
                    }

                    solrResultModel.setCustomVal(retUserIds);
                }
            }
        } catch (Exception e) {

        }
        return solrResultModel;
    }
}
