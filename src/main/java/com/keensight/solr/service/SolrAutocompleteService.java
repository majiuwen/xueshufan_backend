package com.keensight.solr.service;

import cn.newtouch.dl.solr.model.SolrResultModel;
import cn.newtouch.dl.solr.service.SolrSearchService;
import com.keensight.solr.config.SolrConfig;
import com.keensight.solr.utils.AutocompleteUtil;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class SolrAutocompleteService extends SolrSearchService {


    public SolrAutocompleteService(SolrConfig solrConfig) {
        super();

        SolrClient client = new HttpSolrClient.Builder(solrConfig.getAutocompleteUrl())
                .withConnectionTimeout(solrConfig.getConnectionTimeout())
                .withSocketTimeout(solrConfig.getSocketTimeout())
                .build();
        super.setSolrClient(client);
    }

    /**
     * 根据keyword，自动补全
     *
     * @param keyword 检索keyword
     * @return SolrResultModel
     */
    public List<String> autocompleteByKeyword(String keyword) throws Exception {
        // 参数判断
        if (StringUtils.isEmpty(keyword)) {
            return null;
        }

        // 自动补全
        SolrQuery sq = new SolrQuery();
        SolrResultModel<String> solrResultModel = this.suggest(sq, "mySuggester", keyword);
        List<String> suggestedWordList = solrResultModel.getENTRY();

        return AutocompleteUtil.sortSuggestedWord(keyword, suggestedWordList);
    }
}
