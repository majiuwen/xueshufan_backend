package com.keensight.solr.service;

import cn.newtouch.dl.solr.model.SolrResultModel;
import cn.newtouch.dl.solr.model.SolrSortSearchItem;
import cn.newtouch.dl.solr.service.SolrSearchService;
import com.alibaba.fastjson.JSONObject;
import com.keensight.solr.config.SolrConfig;
import com.keensight.solr.model.SolrPublicationSearchModel;
import com.keensight.solr.utils.AutocompleteUtil;
import com.keensight.web.constants.enums.TopItemType;
import com.keensight.web.db.entity.User;
import com.keensight.web.db.entity.UserExample;
import com.keensight.web.db.mapper.UserMapper;
import com.keensight.web.model.TopItemListModel;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.json.BucketBasedJsonFacet;
import org.apache.solr.client.solrj.response.json.BucketJsonFacet;
import org.apache.solr.client.solrj.response.json.NestableJsonFacet;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SolrPublicationSearchService extends SolrSearchService {
    @Resource
    private UserMapper userMapper;

    public SolrPublicationSearchService(SolrConfig solrConfig) {
        super();

        SolrClient client = new HttpSolrClient.Builder(solrConfig.getPublicationSearchUrl())
                .withConnectionTimeout(solrConfig.getConnectionTimeout())
                .withSocketTimeout(solrConfig.getSocketTimeout())
                .build();
        super.setSolrClient(client);
    }

    /**
     * 文献摘要取得(共通取得用)
     *
     * @param publicationIds 文献ID
     * @return
     */
    public SolrResultModel getPublicationDetailList(List<Long> publicationIds) {
        SolrPublicationSearchModel searchModel = new SolrPublicationSearchModel();
        SolrResultModel solrResultModel = null;

        // 显示字段设定
        String[] outputCols = new String[]{"id", "abstract"};
        searchModel.setOutputCols(outputCols);

        // 参数判断
        if (StringUtils.isEmpty(publicationIds)) {
            return solrResultModel;
        } else {
            searchModel.setPublicationIds(publicationIds);
        }

        try {
            solrResultModel = this.search(searchModel, SolrPublicationSearchModel.class);
        } catch (Exception e) {

        }
        return solrResultModel;
    }

    /**
     * keyword检索，获取文献ID
     *
     * @param keyword             检索keyword
     * @param autocompleteKeyword 画面自动补全keyword
     * @param index               开始页数
     * @param rows                检索件数
     * @param allPublicationIds   文献ID
     * @return SolrResultModel.customVal 文献ID
     */
    public SolrResultModel getPublicationIdByKeyword(String keyword, String autocompleteKeyword, int index, int rows, List<Long> allPublicationIds) {
        SolrPublicationSearchModel searchModel = new SolrPublicationSearchModel();
        SolrResultModel solrResultModel = null;
        List<Long> retPublicationIds = new ArrayList<Long>();

        // 显示字段设定
        String[] outputCols = new String[]{"id"};
        searchModel.setOutputCols(outputCols);

        // keyword
        if (StringUtils.isEmpty(autocompleteKeyword)) {
            searchModel.setKeyword(keyword);
        } else {
            // TODO 自动补全要进一步处理
            AutocompleteUtil.setKeyword(keyword, autocompleteKeyword, searchModel);
        }

        // 参数判断
        if (!StringUtils.isEmpty(allPublicationIds)) {
            searchModel.setPublicationIds(allPublicationIds);
        }

        // 检索件数
        if (rows > 0) {
            searchModel.setRows(rows);
        }

        if (index <= 0) {
            index = 1;
        }
        searchModel.setStart((index - 1) * searchModel.getRows());

        try {
            solrResultModel = this.search(searchModel, SolrPublicationSearchModel.class);

            // 摘要
            if (solrResultModel.getRows() > 0) {
                List<Long> publicationIds = new ArrayList<Long>();
                List<SolrPublicationSearchModel> solrPublicationSearchModelList = solrResultModel.getENTRY();
                for (SolrPublicationSearchModel solrPublicationSearchModel : solrPublicationSearchModelList) {
                    publicationIds.add(Long.valueOf(solrPublicationSearchModel.getId()));
                }

                // 排序
                for (Long publicationId : allPublicationIds) {
                    retPublicationIds.addAll(publicationIds.stream().filter(id -> publicationId.equals(id)).collect(Collectors.toList()));
                }

                solrResultModel.setCustomVal(retPublicationIds);
            }
        } catch (Exception e) {

        }
        return solrResultModel;
    }

    /**
     * keyword检索，获取文献ID
     *
     * @param keyword             检索keyword
     * @param autocompleteKeyword 画面自动补全keyword
     * @param index               开始页数
     * @param rows                检索件数
     * @param sortColumns         排序列
     * @param yearFrom            开始年
     * @param yearTo              终了年
     * @param userIds             顶尖学者
     * @param institutions        顶尖机构
     * @param journalId           顶尖期刊
     * @param authors             顶尖会议
     * @param fieldId             主要研究方向
     * @param doctype             研究成果类型
     * @return SolrResultModel.customVal 文献ID
     */
    public SolrResultModel getPublicationIdByKeyword(String keyword, String autocompleteKeyword, int index,
                                                     int rows, String sortColumns, String yearFrom,
                                                     String yearTo, String userIds, String institutions,
                                                     String journalId, String authors,
                                                     String fieldId, String doctype) {
        SolrPublicationSearchModel searchModel = new SolrPublicationSearchModel();
        SolrResultModel solrResultModel = null;
        // 显示字段设定
        String[] outputCols = new String[]{"id"};
        searchModel.setOutputCols(outputCols);
        List<SolrSortSearchItem> sort = new ArrayList<SolrSortSearchItem>();

        if (("0").equals(sortColumns)) {
            sort.add(new SolrSortSearchItem("rank"));
        } else if (("1").equals(sortColumns)) {
            sort.add(new SolrSortSearchItem("year", SolrQuery.ORDER.asc));
        } else if (("2").equals(sortColumns)) {
            sort.add(new SolrSortSearchItem("year", SolrQuery.ORDER.desc));
        } else if (("3").equals(sortColumns)) {
            sort.add(new SolrSortSearchItem("citations", SolrQuery.ORDER.desc));
        }

        searchModel.setSortInfoList(sort);
        // keyword
        if (StringUtils.isEmpty(autocompleteKeyword)) {
            searchModel.setKeyword(keyword);
        } else {
            AutocompleteUtil.setKeyword(keyword, autocompleteKeyword, searchModel);
        }

        // 参数判断
        // 检索件数
        if (rows > 0) {
            searchModel.setRows(rows);
        }

        if (!StringUtils.isEmpty(yearFrom)) {
            searchModel.setYearFrom(yearFrom);
        }

        if (!StringUtils.isEmpty(yearTo)) {
            searchModel.setYearTo(yearTo);
        }

        if (!StringUtils.isEmpty(userIds)) {
            List<Long> longUserIds = Arrays.asList(userIds.split(",")).stream().map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
            searchModel.setUserId(longUserIds);
        }

        if (!StringUtils.isEmpty(institutions)) {
            List<Long> longInstitutions = Arrays.asList(institutions.split(",")).stream().map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
            searchModel.setInstitutuionId(longInstitutions);
        }

        if (!StringUtils.isEmpty(journalId)) {
            List<Long> longJournalId = Arrays.asList(journalId.split(",")).stream().map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
            searchModel.setJournalId(longJournalId);
        }

        if (!StringUtils.isEmpty(authors)) {
            List<Long> longAuthors = Arrays.asList(authors.split(",")).stream().map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
            searchModel.setConferenceId(longAuthors);
        }

        if (!StringUtils.isEmpty(fieldId)) {
            List<Long> longFieldId = Arrays.asList(fieldId.split(",")).stream().map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
            searchModel.setFieldId(longFieldId);
        }

        if (!StringUtils.isEmpty(doctype)) {
            List<String> longDoctype = Arrays.asList(doctype.split(","));
            searchModel.setDoctypeList(longDoctype);
        }

        if (index <= 0) {
            index = 1;
        }
        searchModel.setStart((index - 1) * searchModel.getRows());

        try {
            solrResultModel = this.search(searchModel, SolrPublicationSearchModel.class);

            // 摘要
            if (solrResultModel.getRows() > 0) {
                List<Long> publicationIds = new ArrayList<Long>();
                List<SolrPublicationSearchModel> solrPublicationSearchModelList = solrResultModel.getENTRY();
                for (SolrPublicationSearchModel solrPublicationSearchModel : solrPublicationSearchModelList) {
                    publicationIds.add(Long.valueOf(solrPublicationSearchModel.getId()));
                }

                solrResultModel.setCustomVal(publicationIds);
            }
        } catch (Exception e) {

        }
        return solrResultModel;
    }

    /**
     * keyword检索，获取文献信息
     *
     * @param keyword             检索keyword
     * @param autocompleteKeyword 画面自动补全keyword
     * @return SolrResultModel    文献信息
     */
    public SolrResultModel getPublicationInfo4Authentication(String keyword, String autocompleteKeyword) {
        SolrPublicationSearchModel searchModel = new SolrPublicationSearchModel();
        SolrResultModel solrResultModel = null;
        List<Long> retPublicationIds = new ArrayList<Long>();

        // 显示字段设定
        String[] outputCols = new String[]{"id", "doctype", "title", "user_id", "user", "year"};
        searchModel.setOutputCols(outputCols);

        // keyword
        if (StringUtils.isEmpty(autocompleteKeyword)) {
            searchModel.setKeyword(keyword);
        } else {
            AutocompleteUtil.setKeyword(keyword, autocompleteKeyword, searchModel);
        }

        // 检索件数
        searchModel.setRows(1);

        try {
            solrResultModel = this.search(searchModel, SolrPublicationSearchModel.class);
        } catch (Exception e) {

        }
        return solrResultModel;
    }

    /**
     * 用户ID检索，获取文献信息
     *
     * @param userId              用户ID
     * @param isMyPublication     是不是该用户的文献
     * @param rows                件数
     * @return SolrResultModel    文献信息
     */
    public SolrResultModel getPublicationInfo4UserId(Long userId, boolean isMyPublication, int rows) {
        SolrPublicationSearchModel searchModel = new SolrPublicationSearchModel();
        SolrResultModel solrResultModel = null;

        // 显示字段设定
        String[] outputCols = new String[]{"id", "title"};
        searchModel.setOutputCols(outputCols);

        // 检索件数
        searchModel.setRows(rows);

        SolrQuery sq = searchModel.toQuery();
        // 用户ID
        if (isMyPublication) {
            sq.addFilterQuery("user_id:" + userId);
        } else {
            sq.addFilterQuery("!user_id:" + userId);
        }

        try {
            solrResultModel = this.search(sq, SolrPublicationSearchModel.class);
        } catch (Exception e) {

        }
        return solrResultModel;
    }

    /**
     * keyword检索，获取Top10
     *
     * @param keyword             检索keyword
     * @param autocompleteKeyword 画面自动补全keyword
     * @param topItemType         类型
     * @return List<TopItemListModel> Top10
     */
    public List<TopItemListModel> getTop10(String keyword, String autocompleteKeyword, TopItemType topItemType) throws Exception {

        SolrPublicationSearchModel searchModel = new SolrPublicationSearchModel();
        searchModel.setRows(0);

        // keyword
        if (StringUtils.isEmpty(autocompleteKeyword)) {
            searchModel.setKeyword(keyword);
        } else {
            AutocompleteUtil.setKeyword(keyword, autocompleteKeyword, searchModel);
        }

        return getTop10(searchModel, topItemType);
    }

    /**
     * 根据研究方向ID，获取文献类型Top10
     *
     * @param fieldId 研究方向ID
     * @return List<TopItemListModel> Top10
     */
    public List<TopItemListModel> getDocTypeTop10ByFieldId(Long fieldId, TopItemType topItemType) throws Exception {
        SolrPublicationSearchModel searchModel = new SolrPublicationSearchModel();
        searchModel.setRows(0);

        // 检索条件 fieldId
        List<Long> fieldIds = new ArrayList<>();
        fieldIds.add(fieldId);
        searchModel.setFieldId(fieldIds);

        return getTop10(searchModel, topItemType);
    }

    /**
     * Top10检索
     *
     * @param searchModel 检索条件
     * @return List<TopItemListModel> Top10
     */
    private List<TopItemListModel> getTop10(SolrPublicationSearchModel searchModel, TopItemType topItemType) throws Exception {
        List<TopItemListModel> topItemList = new ArrayList<>();

        // 根据类型，设定查找内容
        String name = null;
        String id = null;

        // 研究方向
        if (topItemType.equals(TopItemType.FIELD)) {
            name = "field";
            id = "field_id";
        }

        // 作者 作者名需要再获取
        if (topItemType.equals(TopItemType.AUTHOR)) {
            name = "user_id";
        }

        // 文献类型
        if (topItemType.equals(TopItemType.DOCTYPE)) {
            name = "doctype";
        }

        // 研究机构
        if (topItemType.equals(TopItemType.INSTITUTION)) {
            name = "institution";
            id = "institution_id";
        }

        // 期刊
        if (topItemType.equals(TopItemType.JOURNAL)) {
            name = "journal";
            id = "journal_id";
        }

        // 会议
        if (topItemType.equals(TopItemType.CONFERENCE)) {
            name = "conference_serie";
            id = "conference_serie_id";
        }

        List<String> facetField = new ArrayList<>();
        facetField.add(name);
        if (id != null) {
            facetField.add(id);
        }


        JSONObject sumFacet = new JSONObject();
        sumFacet.put("sum", "sum(citations)");

        List<String> jsonFacetList = new ArrayList<>();
        JSONObject jsonFacet = new JSONObject();
        JSONObject fieldNameObj = new JSONObject();
        fieldNameObj.put("type", "terms");
        fieldNameObj.put("field", name);
        fieldNameObj.put("limit", 20);
        fieldNameObj.put("sort", "sum desc");
        fieldNameObj.put("facet", sumFacet);
        jsonFacet.put("field_name", fieldNameObj);
        jsonFacetList.add(jsonFacet.toJSONString());

        if (id != null) {
            JSONObject fieldIdObj = new JSONObject();
            fieldIdObj.put("type", "terms");
            fieldIdObj.put("field", id);
            fieldIdObj.put("limit", 20);
            fieldIdObj.put("sort", "sum desc");
            fieldIdObj.put("facet", sumFacet);

            jsonFacet.clear();
            jsonFacet.put("field_id", fieldIdObj);
            jsonFacetList.add(jsonFacet.toJSONString());
        }

        // solr 集计
        NestableJsonFacet nestableJsonFacet = this.jsonFacet(searchModel.toQuery(), jsonFacetList);
        BucketBasedJsonFacet facetFieldName = nestableJsonFacet.getBucketBasedFacets("field_name");
        ;
        BucketBasedJsonFacet facetFieldId = null;
        if (id != null) {
            facetFieldId = nestableJsonFacet.getBucketBasedFacets("field_id");
        }

        // 如果没有top10，则返回空list
        if (facetFieldName == null) {
            return topItemList;
        }

        // 编辑ID，Name，score
        List<BucketJsonFacet> nameCounts = facetFieldName.getBuckets();
        for (int i = 0; i < nameCounts.size(); i++) {
            TopItemListModel topItem = new TopItemListModel();
            topItem.setTopItemName(nameCounts.get(i).getVal() + "");
            topItem.setScore(nameCounts.get(i).getStatValue("sum") + "");

            if (facetFieldId != null) {
                topItem.setTopItemId(facetFieldId.getBuckets().get(i).getVal() + "");
            }
            topItemList.add(topItem);
        }

        // 作者名有重名的，所以需要根据ID再取得
        if (topItemType.equals(TopItemType.AUTHOR)) {
            List<Long> userIds = new ArrayList<>();
            for (TopItemListModel topItem : topItemList) {
                userIds.add(Long.valueOf(topItem.getTopItemName()));
            }

            UserExample example = new UserExample();
            UserExample.Criteria criteria = example.createCriteria();
            criteria.andUserIdIn(userIds);
            List<User> users = userMapper.selectByExample(example);

            for (TopItemListModel topItem : topItemList) {
                for (User user : users) {
                    if (topItem.getTopItemName().equals(String.valueOf(user.getUserId()))) {
                        topItem.setTopItemName(user.getDisplayName());
                        topItem.setTopItemId(user.getUserId() + "");
                        break;
                    }
                }
            }
        }

        return topItemList;
    }
}
