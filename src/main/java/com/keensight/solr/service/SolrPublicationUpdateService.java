package com.keensight.solr.service;

import cn.newtouch.dl.solr.model.SolrResultModel;
import cn.newtouch.dl.solr.service.SolrImportService;
import com.keensight.solr.config.SolrConfig;
import com.keensight.solr.model.SolrPublicationSearchModel;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class SolrPublicationUpdateService extends SolrImportService {
    @Resource
    private SolrPublicationSearchService solrPublicationSearchService;

    public SolrPublicationUpdateService(SolrConfig solrConfig) {
        super();

        SolrClient client = new HttpSolrClient.Builder(solrConfig.getPublicationSearchUrl())
                .withConnectionTimeout(solrConfig.getConnectionTimeout())
                .withSocketTimeout(solrConfig.getSocketTimeout())
                .build();
        super.setSolrClient(client);
    }

    /**
     * 文献作者更新（认领文章）
     *
     * @param publicationId 文献ID
     * @param oleUserId 原用户ID
     * @param oleUserName 原用户名
     * @param newUserId 新用户ID
     * @param newUserName 新用户名
     *
     * @return
     */
    public boolean updatePublicationAuthor(Long publicationId, Long oleUserId, String oleUserName, Long newUserId, String newUserName) {
        boolean ret = true;

        // 获取文献信息
        SolrPublicationSearchModel searchModel = new SolrPublicationSearchModel();
        SolrResultModel solrResultModel = null;

        // 检索件数
        searchModel.setRows(1);

        // 文献ID
        List<Long> publicationIds = new ArrayList<>();
        publicationIds.add(publicationId);
        searchModel.setPublicationIds(publicationIds);

        try {
            solrResultModel = solrPublicationSearchService.search(searchModel, SolrPublicationSearchModel.class);
            if (solrResultModel.getRows() > 0) {
                searchModel = (SolrPublicationSearchModel)solrResultModel.getENTRY().get(0);

                // 更新文献信息
                // 用户ID
                List<Long> userIdList = searchModel.getUserId();
                for (int i = 0; i < userIdList.size(); i++) {
                    if (userIdList.get(i).equals(oleUserId)) {
                        userIdList.set(i, newUserId);
                        break;
                    }
                }

                // 用户名
                List<String> userList = searchModel.getUser();
                for (int i = 0; i < userList.size(); i++) {
                    if (userList.get(i).equalsIgnoreCase(oleUserName.replaceAll("\\.", ""))) {
                        userList.set(i, newUserName);
                        break;
                    }
                }

                this.add(searchModel);
            }
        } catch (Exception e) {
            ret = false;
        }
        return ret;
    }

}
