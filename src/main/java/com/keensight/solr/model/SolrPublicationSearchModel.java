package com.keensight.solr.model;

import cn.newtouch.dl.solr.parser.SolrQueryParser;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.util.StringUtils;

import java.util.List;

public class SolrPublicationSearchModel extends SolrAutocompleteModel {
    @Field(value = "id")
    private String id;

    @Field(value = "rank")
    private int rank;

    @Field(value = "doctype")
    private String doctype;

    @Field(value = "abstract")
    private String summary;

    @Field(value = "citations")
    private int citations;

    @Field(value = "year")
    private int year;

    /** 作者ID */
    @Field(value = "user_id")
    private List<Long> userId;

    /** 机构ID */
    @Field(value = "institution_id")
    private List<Long> institutuionId;

    /** 研究领域ID */
    @Field(value = "field_id")
    private List<Long> fieldId;

    /** 期刊ID */
    @Field(value = "journal_id")
    private Long journalid;

    /** 期刊名 */
    @Field(value = "journal")
    private String journal;

    /** 会议系列ID */
    @Field(value = "conference_serie_id")
    private List<Long> conferenceSerieId;

    /** 会议系列名 */
    @Field(value = "conference_serie")
    private List<String> conferenceSerie;

    /** 会议实例ID */
    @Field(value = "conference_instance_id")
    private List<Long> conferenceInstanceId;

    /** 会议实例名 */
    @Field(value = "conference_instance")
    private List<String> conferenceInstance;

    @Field(value = "publication_name")
    private String publicationName;

    /** 检索用期刊ID */
    private List<Long> journalId;

    /** 检索用会议ID */
    private List<Long> conferenceId;

    /** 检索用文献类型 */
    private List<String> doctypeList;

    /** 检索用文献ID */
    private List<Long> publicationIds;

    /** 检索用yearFrom*/
    private String yearFrom;

    /** 检索用yearTo */
    private String yearTo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getDoctype() {
        return doctype;
    }

    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public int getCitations() {
        return citations;
    }

    public void setCitations(int citations) {
        this.citations = citations;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public List<Long> getFieldId() {
        return fieldId;
    }

    public void setFieldId(List<Long> fieldId) {
        this.fieldId = fieldId;
    }

    public String getPublicationName() {
        return publicationName;
    }

    public void setPublicationName(String publicationName) {
        this.publicationName = publicationName;
    }

    public List<Long> getPublicationIds() {
        return publicationIds;
    }

    public void setPublicationIds(List<Long> publicationIds) {
        this.publicationIds = publicationIds;
    }

    public String getYearFrom() {
        return yearFrom;
    }

    public void setYearFrom(String yearFrom) {
        this.yearFrom = yearFrom;
    }

    public String getYearTo() {
        return yearTo;
    }

    public void setYearTo(String yearTo) {
        this.yearTo = yearTo;
    }

    public List<Long> getUserId() {
        return userId;
    }

    public void setUserId(List<Long> userId) {
        this.userId = userId;
    }

    public List<Long> getInstitutuionId() {
        return institutuionId;
    }

    public void setInstitutuionId(List<Long> institutuionId) {
        this.institutuionId = institutuionId;
    }

    public List<Long> getJournalId() {
        return journalId;
    }

    public void setJournalId(List<Long> journalId) {
        this.journalId = journalId;
    }

    public List<Long> getConferenceId() {
        return conferenceId;
    }

    public void setConferenceId(List<Long> conferenceId) {
        this.conferenceId = conferenceId;
    }

    public List<String> getDoctypeList() {
        return doctypeList;
    }

    public void setDoctypeList(List<String> doctypeList) {
        this.doctypeList = doctypeList;
    }

    public Long getJournalid() {
        return journalid;
    }

    public void setJournalid(Long journalid) {
        this.journalid = journalid;
    }

    public String getJournal() {
        return journal;
    }

    public void setJournal(String journal) {
        this.journal = journal;
    }

    public List<Long> getConferenceSerieId() {
        return conferenceSerieId;
    }

    public void setConferenceSerieId(List<Long> conferenceSerieId) {
        this.conferenceSerieId = conferenceSerieId;
    }

    public List<String> getConferenceSerie() {
        return conferenceSerie;
    }

    public void setConferenceSerie(List<String> conferenceSerie) {
        this.conferenceSerie = conferenceSerie;
    }

    public List<Long> getConferenceInstanceId() {
        return conferenceInstanceId;
    }

    public void setConferenceInstanceId(List<Long> conferenceInstanceId) {
        this.conferenceInstanceId = conferenceInstanceId;
    }

    public List<String> getConferenceInstance() {
        return conferenceInstance;
    }

    public void setConferenceInstance(List<String> conferenceInstance) {
        this.conferenceInstance = conferenceInstance;
    }

    @Override
    public SolrQuery toQuery() {
        SolrQuery sq = super.toQuery();

        // keyword
        if (!StringUtils.isEmpty(getKeyword())) {
            sq.setQuery("text:" + SolrQueryParser.processKeyWord(getKeyword()));
        }

        // 用文献ID检索
        SolrQueryParser.addFilterStringArr(sq, "id", publicationIds);

        // 用作者名检索
        SolrQueryParser.addFilterStringArr(sq, "user", getUser());

        // 用机构名检索
        SolrQueryParser.addFilterStringArr(sq, "institution", getInstitution());

        // 用研究领域名检索
        SolrQueryParser.addFilterStringArr(sq, "field", getField());

        // 用文献标题检索
        if (!StringUtils.isEmpty(getTitle())) {
            SolrQueryParser.addFilterString(sq,"title", getTitle());
        }

        // 用文献类型检索
        if (!StringUtils.isEmpty(getDoctypeList())) {
            SolrQueryParser.addFilterStringArr(sq,"doctype", getDoctypeList());
        }

        // 用作者ID检索
        if (!StringUtils.isEmpty(getUserId())) {
            SolrQueryParser.addFilterStringArr(sq, "user_id", getUserId());
        }

        // 用机构ID检索
        if (!StringUtils.isEmpty(getInstitutuionId())) {
            SolrQueryParser.addFilterStringArr(sq, "institution_id ", getInstitutuionId());
        }

        // 用研究领域ID检索
        if (!StringUtils.isEmpty(getFieldId())) {
            SolrQueryParser.addFilterStringArr(sq, "field_id", getFieldId());
        }

        // 用期刊ID检索
        if (!StringUtils.isEmpty(getJournalId())) {
            SolrQueryParser.addFilterStringArr(sq, "journal_id", getJournalId());
        }

        // 用会议ID检索
        if (!StringUtils.isEmpty(getConferenceId())) {
            SolrQueryParser.addFilterStringArr(sq, "conference_serie_id", getConferenceId());
        }

        // 用年份检索
        if (!StringUtils.isEmpty(getYearFrom()) && !StringUtils.isEmpty(getYearTo())) {
            SolrQueryParser.addFilterStringFromTo(sq, "year", yearFrom, yearTo);
        }

        return sq;
    }
}
