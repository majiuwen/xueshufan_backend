package com.keensight.solr.model;

import cn.newtouch.dl.solr.model.SolrBaseModel;
import org.apache.solr.client.solrj.beans.Field;

import java.util.List;

public class SolrAutocompleteModel extends SolrBaseModel {
    @Field(value = "user")
    private List<String> user;

    @Field(value = "institution")
    private List<String> institution;

    @Field(value = "field")
    private List<String> field;

    @Field(value = "title")
    private String title;

    public List<String> getUser() {
        return user;
    }

    public void setUser(List<String> user) {
        this.user = user;
    }

    public List<String> getInstitution() {
        return institution;
    }

    public void setInstitution(List<String> institution) {
        this.institution = institution;
    }

    public List<String> getField() {
        return field;
    }

    public void setField(List<String> field) {
        this.field = field;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
