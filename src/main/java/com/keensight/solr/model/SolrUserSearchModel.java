package com.keensight.solr.model;

import cn.newtouch.dl.solr.model.SolrSortSearchItem;
import cn.newtouch.dl.solr.parser.SolrQueryParser;
import cn.newtouch.dl.solr.util.StringUtil;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.util.StringUtils;

import java.util.Iterator;
import java.util.List;

public class SolrUserSearchModel extends SolrAutocompleteModel {
    @Field(value = "id")
    private String id;

    @Field(value = "rank")
    private int rank;

    @Field(value = "citations")
    private int citations;

    /** 检索用用户ID */
    private List<Long> userIds;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getCitations() {
        return citations;
    }

    public void setCitations(int citations) {
        this.citations = citations;
    }

    public List<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Long> userIds) {
        this.userIds = userIds;
    }

    @Override
    public SolrQuery toQuery() {
        SolrQuery sq = super.toQuery();

        // keyword
        if (!StringUtils.isEmpty(getKeyword())) {
            sq.setQuery("text:" + SolrQueryParser.processKeyWord(getKeyword()));
        }

        if (getSortInfoList() != null && getSortInfoList().size() > 0) {
            Iterator var3 = getSortInfoList().iterator();
            while(var3.hasNext()) {
                SolrSortSearchItem sort = (SolrSortSearchItem)var3.next();
                if (StringUtil.isNotEmpty(sort.getField()) && sort.getSortKbn() != null) {
                    sq.addSort(sort.getField(), sort.getSortKbn());
                }
            }
        }

        // 用用户ID检索
        SolrQueryParser.addFilterStringArr(sq, "id", userIds);

        // 用作者名检索
        SolrQueryParser.addFilterStringArr(sq, "user", getUser());

        // 用机构名检索
        SolrQueryParser.addFilterStringArr(sq, "institution", getInstitution());

        // 用研究领域名检索
        SolrQueryParser.addFilterStringArr(sq, "field", getField());

        return sq;
    }
}
