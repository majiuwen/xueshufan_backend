package com.keensight.solr.model;

import cn.newtouch.dl.solr.parser.SolrQueryParser;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.util.StringUtils;

public class SolrInstitutionSearchModel extends SolrAutocompleteModel {
    @Field(value = "id")
    private String id;

    @Field(value = "rank")
    private int rank;

    @Field(value = "citations")
    private String citations;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getCitations() {
        return citations;
    }

    public void setCitations(String citations) {
        this.citations = citations;
    }

    @Override
    public SolrQuery toQuery() {
        SolrQuery sq = super.toQuery();

        // keyword
        if (!StringUtils.isEmpty(getKeyword())) {
            sq.setQuery("text:" + SolrQueryParser.processKeyWord(getKeyword()));
        }

        // 用研究机构名检索
        SolrQueryParser.addFilterStringArr(sq, "institution", getInstitution());
        return sq;
    }
}
