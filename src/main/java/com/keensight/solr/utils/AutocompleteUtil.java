package com.keensight.solr.utils;

import com.keensight.solr.model.SolrAutocompleteModel;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AutocompleteUtil {
    /**
     * 自动补全排序
     *
     * @param keywords 画面上输入的keywords
     * @param suggestedWordList solr返回的补全word
     *
     * @return List<String> 排序后的补全word
     */
    public static List<String> sortSuggestedWord(String keywords, List<String> suggestedWordList) {
        List<String> sortedSuggestedWordList = new ArrayList<String>();
        if (StringUtils.isEmpty(keywords) || suggestedWordList == null) {
            return sortedSuggestedWordList;
        }

        for (String suggestedWord : suggestedWordList) {
            List<String> sortTag = new ArrayList<>();
            String sortedWord = "";

            // 按照keyword 排序
            for (String keyword : keywords.split(" ")) {
                if (sortedWord.indexOf(keyword) > -1) {
                    continue;
                }

                StringBuffer rgex = new StringBuffer();
                rgex.append("(<ks_a>.*" + keyword + ".*</ks_a>)");
                rgex.append("|(<ks_i>.*" + keyword + ".*</ks_i>)");
                rgex.append("|(<ks_f>.*" + keyword + ".*</ks_f>)");
                rgex.append("|(<ks_p>.*" + keyword + ".*</ks_p>)");

                Pattern pattern = Pattern.compile(rgex.toString());
                Matcher m = pattern.matcher(suggestedWord.replaceAll("</?b>", "").replaceAll("<ks", ",<ks"));
                if (m.find()){
                    String tag = m.group(0).substring(0, 6);
                    String word = suggestedWord.substring(suggestedWord.indexOf(tag), suggestedWord.indexOf(tag.replaceAll("<", "</")) + 7);
                    sortedWord += word;
                    suggestedWord = suggestedWord.replaceAll(word, "");
                }
            }

            sortedSuggestedWordList.add(sortedWord + suggestedWord);
        }

        return sortedSuggestedWordList;
    }

    /**
     * keyword检索条件设定
     *
     * @param keyword 检索keyword
     * @param autocompleteKeyword 画面自动补全keyword
     *
     * @return List<String> 排序后的补全word
     */
    public static void setKeyword(String keyword, String autocompleteKeyword, SolrAutocompleteModel autocompleteModel) {
        if (StringUtils.isEmpty(keyword) || StringUtils.isEmpty(autocompleteKeyword)) {
            return;
        }

        autocompleteKeyword = autocompleteKeyword.replaceAll("</?b>", "");
        if (keyword.replaceAll(" ", "").equals(autocompleteKeyword.replaceAll(" ", "").replaceAll("</?ks_.?>", ""))) {
            // 作者名
            if (autocompleteKeyword.indexOf("<ks_a>") > -1) {
                List userList = new ArrayList<String>();
                userList.add(autocompleteKeyword.substring(autocompleteKeyword.indexOf("<ks_a>") + 6, autocompleteKeyword.indexOf("</ks_a>")));
                autocompleteModel.setUser(userList);
            }

            // 研究领域
            if (autocompleteKeyword.indexOf("<ks_f>") > -1) {
                List fieldList = new ArrayList<String>();
                fieldList.add(autocompleteKeyword.substring(autocompleteKeyword.indexOf("<ks_f>") + 6, autocompleteKeyword.indexOf("</ks_f>")));
                autocompleteModel.setField(fieldList);
            }

            // 机构
            if (autocompleteKeyword.indexOf("<ks_i>") > -1) {
                List institutionList = new ArrayList<String>();
                institutionList.add(autocompleteKeyword.substring(autocompleteKeyword.indexOf("<ks_i>") + 6, autocompleteKeyword.indexOf("</ks_i>")));
                autocompleteModel.setInstitution(institutionList);
            }

            // 标题
            if (autocompleteKeyword.indexOf("<ks_p>") > -1) {
                String titleList = autocompleteKeyword.substring(autocompleteKeyword.indexOf("<ks_p>") + 6, autocompleteKeyword.indexOf("</ks_p>"));
                autocompleteModel.setTitle(titleList);
            }

        } else {
            autocompleteModel.setKeyword(keyword);
        }
    }
}
