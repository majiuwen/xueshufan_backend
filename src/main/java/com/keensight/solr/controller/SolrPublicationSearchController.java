package com.keensight.solr.controller;

import cn.newtouch.dl.solr.model.SolrResultModel;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.keensight.solr.model.SolrPublicationSearchModel;
import com.keensight.solr.service.SolrPublicationSearchService;
import com.keensight.web.constants.KeensightResultCode;
import com.keensight.web.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@RestController
@Api
@RequestMapping("/solrPublicationSearch")
public class SolrPublicationSearchController {
    @Resource
    private SolrPublicationSearchService solrPublicationSearchService;
    @Resource
    private UserService userService;

    @RequestMapping(value = "/getPublicationInfo4Authentication", method = {RequestMethod.POST})
    @ApiOperation(value="获取文献信息，身份验证用")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userName", value = "userName", required = true),
            @ApiImplicitParam(paramType = "query", name = "institutionName", value = "institutionName", required = true),
            @ApiImplicitParam(paramType = "query", name = "keyword", value = "keyword", required = true),
    })
    public ResultModelApi getPublicationInfo4Authentication(String userName, String institutionName, String keyword, String autocompleteKeyword) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        Long userId = null;
        String searchPublicationTitle = null;
        Map<String, Object> data = new HashMap<>();

        // keyword整理
        String searchKeyword = keyword + " " + userName + " " + institutionName;
        if (!StringUtils.isEmpty(autocompleteKeyword)) {
            if (!StringUtils.isEmpty(userName)) {
                autocompleteKeyword += "<ks_a>" + userName + "</ks_a> ";
            }
            if (!StringUtils.isEmpty(institutionName)) {
                autocompleteKeyword += "<ks_i>" + institutionName + "</ks_i> ";
            }
        }

        // 从solr里检索出一件数据
        SolrResultModel solrResultModel = solrPublicationSearchService.getPublicationInfo4Authentication(searchKeyword, autocompleteKeyword);
        if (solrResultModel.getRows() > 0) {
            SolrPublicationSearchModel solrPublicationSearchModel = (SolrPublicationSearchModel)solrResultModel.getENTRY().get(0);
            data.put("publicationInfo", solrPublicationSearchModel);

            // 获取用户ID
            for (int i = 0; i < solrPublicationSearchModel.getUser().size(); i++) {
                if (userName.equalsIgnoreCase(solrPublicationSearchModel.getUser().get(i)) || userName.replaceAll("\\.", "").equalsIgnoreCase(solrPublicationSearchModel.getUser().get(i))) {
                    userId = solrPublicationSearchModel.getUserId().get(i);
                    break;
                }
            }
            data.put("userId", userId);

            String accountStatus = userService.getAccountStatus(userId);
            if ("2".equals(accountStatus)) {
                resultModelApi.setRetCode(KeensightResultCode.System_90001.code());
                return resultModelApi;
            }

            // 当前文献
            searchPublicationTitle = solrPublicationSearchModel.getTitle();

            Map<String, Long> selectPublicationMap = new HashMap<>();
            Random random = new Random();
            // 获取当前用户的5条数据。
            solrResultModel = solrPublicationSearchService.getPublicationInfo4UserId(userId, true, 5);
            if (solrResultModel.getRows() > 0) {
                // 先放一个自己的
                int randomIndex = random.nextInt(solrResultModel.getENTRY().size());
                SolrPublicationSearchModel myPublication = (SolrPublicationSearchModel)solrResultModel.getENTRY().get(randomIndex);

                // 如果随机抽取的正好是当前检索的文献title，那这个就不要，继续随机抽取一个
                if (searchPublicationTitle.equalsIgnoreCase(myPublication.getTitle()) || searchPublicationTitle.replaceAll("\\.", "").equalsIgnoreCase(myPublication.getTitle())) {
                    solrResultModel.getENTRY().remove(randomIndex);

                    randomIndex = random.nextInt(solrResultModel.getENTRY().size());
                    myPublication = (SolrPublicationSearchModel)solrResultModel.getENTRY().get(randomIndex);
                }
                selectPublicationMap.put(myPublication.getTitle(), userId);
                solrResultModel.getENTRY().remove(randomIndex);

                // 如果自己的文献数 > 3，那再放一个
                if (solrResultModel.getENTRY().size() > 2) {
                    randomIndex = random.nextInt(solrResultModel.getENTRY().size());
                    myPublication = (SolrPublicationSearchModel)solrResultModel.getENTRY().get(randomIndex);
                    selectPublicationMap.put(myPublication.getTitle(), userId);
                }
            }

            // 获取非当前用户的20条数据。
            solrResultModel = solrPublicationSearchService.getPublicationInfo4UserId(userId, false, 20);
            while (selectPublicationMap.size() < 5) {
                int randomIndex = random.nextInt(solrResultModel.getENTRY().size());
                SolrPublicationSearchModel myPublication = (SolrPublicationSearchModel)solrResultModel.getENTRY().get(randomIndex);
                selectPublicationMap.put(myPublication.getTitle(), null);
                solrResultModel.getENTRY().remove(randomIndex);

                if (solrResultModel.getENTRY().size() == 0) {
                    break;
                }
            }

            data.put("selectPublicationMap", selectPublicationMap);


        }

        resultModelApi.setRetCode("0");
        resultModelApi.setData(data);

        return resultModelApi;
    }
}
