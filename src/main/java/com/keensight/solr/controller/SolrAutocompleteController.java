package com.keensight.solr.controller;

import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.keensight.solr.service.SolrAutocompleteService;
import com.keensight.solr.service.SolrInstitutionSearchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RestController
@Api
@RequestMapping("/autocomplete")
public class SolrAutocompleteController {
    @Resource
    private SolrAutocompleteService solrAutocompleteService;

    @Resource
    private SolrInstitutionSearchService solrInstitutionSearchService;

    @RequestMapping(value = "/getKeyword4AllSearch", method = {RequestMethod.POST})
    @ApiOperation(value="检索keyword自动补全")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "keyword", value = "keyword", required = true),
    })
    public ResultModelApi getKeyword4AllSearch(String keyword) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();

        List<String> suggestedWordList = solrAutocompleteService.autocompleteByKeyword(keyword);

        resultModelApi.setRetCode("0");
        resultModelApi.setData(suggestedWordList);

        return resultModelApi;
    }

    @RequestMapping(value = "/getTitleByKeyword", method = {RequestMethod.POST})
    @ApiOperation(value="检索keyword自动补全 title")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userName", value = "userName", required = true),
            @ApiImplicitParam(paramType = "query", name = "institutionName", value = "institutionName", required = true),
            @ApiImplicitParam(paramType = "query", name = "keyword", value = "keyword", required = true),
    })
    public ResultModelApi getTitleByKeyword(String userName, String institutionName, String keyword) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        List<String> suggestedTitleList = new ArrayList<>();

        String searchKeyword = userName + " " + institutionName + " " + keyword;
        List<String> suggestedWordList = solrAutocompleteService.autocompleteByKeyword(searchKeyword);

        // 只获取title信息
        if (!StringUtils.isEmpty(suggestedWordList)) {
            for (String word : suggestedWordList) {
                if (word.indexOf("<ks_p>") > -1) {
                    String title = word.substring(word.indexOf("<ks_p>"), word.indexOf("</ks_p>")  + 7);
                    if (!suggestedTitleList.contains(title)) {
                        suggestedTitleList.add(title);
                    }
                }
            }
        }
        resultModelApi.setRetCode("0");
        resultModelApi.setData(suggestedTitleList);

        return resultModelApi;
    }

    @RequestMapping(value = "/getInstitutionName", method = {RequestMethod.POST})
    @ApiOperation(value="机构自动补全")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "keyword", value = "keyword", required = true),
    })
    public ResultModelApi getInstitutionName(String keyword) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        List<String> suggestedInstitutionList = new ArrayList<>();

        List<String> suggestedWordList = solrAutocompleteService.autocompleteByKeyword(keyword);

        // 只获取机构信息
        if (!StringUtils.isEmpty(suggestedWordList)) {
            for (String word : suggestedWordList) {
                if (word.indexOf("<ks_i>") > -1) {
                    String institutionName = word.substring(word.indexOf("<ks_i>"), word.indexOf("</ks_i>")  + 7);
                    if (!suggestedInstitutionList.contains(institutionName)) {
                        suggestedInstitutionList.add(institutionName);
                    }
                }
            }
        }
        resultModelApi.setRetCode("0");
        resultModelApi.setData(suggestedInstitutionList);

        return resultModelApi;
    }
}
