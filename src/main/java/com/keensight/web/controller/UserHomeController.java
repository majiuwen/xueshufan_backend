package com.keensight.web.controller;

import cn.newtouch.dl.solr.model.SolrResultModel;
import cn.newtouch.dl.solr.util.StringUtil;
import cn.newtouch.dl.turbocloud.constants.ResultCode;
import cn.newtouch.dl.turbocloud.controller.BaseController;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageInfo;
import com.keensight.solr.service.SolrUserSearchService;
import com.keensight.web.constants.KeensightResultCode;
import com.keensight.web.constants.enums.UploadFolder;
import com.keensight.web.db.entity.*;
import com.keensight.web.helper.FileServerHelper;
import com.keensight.web.model.PublicationModel;
import com.keensight.web.model.SuggestedUserModel;
import com.keensight.web.model.UserModel;
import com.keensight.web.model.UserNewsModel;
import com.keensight.web.service.*;
import com.keensight.web.utils.AwsS3Utils;
import com.keensight.web.utils.DateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 用户首页控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/03] 新建
 */
@RestController
@Api(value = "用户首页")
@RequestMapping("/userHome")
public class UserHomeController extends BaseController {

    // 建议用户关注的学者
    @Autowired
    private SuggestedService suggestedService;
    // 用户在读的文章
    @Autowired
    private UserPublicationBeingReadService userPublicationBeingReadService;
    // 共同作者
    @Autowired
    private UserCoauthorService userCoauthorService;
    // 用户关注的其他用户
    @Autowired
    private UserAuthorFollowingService userAuthorFollowingService;
    // 用户的文献操作
    @Autowired
    private UserActionPublicationService userActionPublicationService;
    // 建议用户阅读的文章
    @Autowired
    private SuggestedPublicationToReadService suggestedPublicationToReadService;
    // 文献（包括论文、专利、著作等）
    @Autowired
    private PublicationService publicationService;
    // 推荐给用户的内容
    @Autowired
    private SuggestedAnnouncementToViewService suggestedAnnouncementToViewService;
    // 作者周文献统计
    @Autowired
    private StatsUserTotalService statsUserTotalService;
    // Solr
    @Autowired
    private SolrUserSearchService solrUserSearchService;
    // 用户
    @Autowired
    private UserService userService;
    @Autowired
    private FileServerHelper fileServerHelper;
    @Autowired
    private UserNewDevelopmentsService userNewDevelopmentsService;

    /**
     * 继续阅读一览
     *
     * @param userId
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/publicationBeingRead", method = {RequestMethod.POST})
    @ApiOperation(value = "继续阅读一览")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getPublicationBeingReadList(String userId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        Map resultMap = new HashMap<>();
        resultMap.put("list",userPublicationBeingReadService.getPublicationBeingReadList(Long.valueOf(userId)));
        resultModelApi.setData(resultMap);
        return resultModelApi;
    }

    /**
     * 获取推荐论文
     *
     * @param userId
     * @param index
     * @param rows
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getSuggestedPublicationToRead", method = {RequestMethod.POST})
    @ApiOperation(value = "获取推荐论文")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数", required = true),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示条数", required = true)
    })
    public ResultModelApi getSuggestedPublicationToRead(String userId, int index, int rows, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        PageInfo<SuggestedPublicationToRead> suggestedPublicationToRead = suggestedPublicationToReadService.getSuggestedPublicationToRead(Long.valueOf(userId), index, rows);
        if (suggestedPublicationToRead.getList() != null && suggestedPublicationToRead.getList().size() > 0) {
            List<Long> suggestedPublicationToReadList = new ArrayList<>();
            List<String> validateTime = new ArrayList<>();
            for (SuggestedPublicationToRead suggestedPublicationToReads : suggestedPublicationToRead.getList()) {
                suggestedPublicationToReadList.add(suggestedPublicationToReads.getPublicationId());
                // 获取论文的时间差
                Date createdAt = suggestedPublicationToReads.getCreatedAt();
                validateTime.add(DateUtil.validateTime(new Date(), createdAt));
            }

            // 获取论文详细内容
            List<PublicationModel> publicationModels = publicationService.getPublicationDetailList(suggestedPublicationToReadList);

            for (int i = 0; i < publicationModels.size(); i++) {
                publicationModels.get(i).setHeaderTitle("推荐论文");
                publicationModels.get(i).setValidateTime(validateTime.get(i));
                publicationModels.get(i).setUserId(suggestedPublicationToRead.getList().get(i).getUserId());
            }

            PageInfo<PublicationModel> publicationModelPageInfo = new PageInfo<PublicationModel>(publicationModels);
            publicationModelPageInfo.setPageNum(suggestedPublicationToRead.getPageNum());
            publicationModelPageInfo.setPageSize(suggestedPublicationToRead.getPageSize());
            publicationModelPageInfo.setStartRow(suggestedPublicationToRead.getStartRow());
            publicationModelPageInfo.setEndRow(suggestedPublicationToRead.getEndRow());
            publicationModelPageInfo.setPages(suggestedPublicationToRead.getPages());
            publicationModelPageInfo.setPrePage(suggestedPublicationToRead.getPrePage());
            publicationModelPageInfo.setNextPage(suggestedPublicationToRead.getNextPage());
            publicationModelPageInfo.setIsFirstPage(suggestedPublicationToRead.isIsFirstPage());
            publicationModelPageInfo.setIsLastPage(suggestedPublicationToRead.isIsLastPage());
            publicationModelPageInfo.setHasPreviousPage(suggestedPublicationToRead.isHasPreviousPage());
            publicationModelPageInfo.setHasNextPage(suggestedPublicationToRead.isHasNextPage());
            publicationModelPageInfo.setNavigatePages(suggestedPublicationToRead.getNavigatePages());
            publicationModelPageInfo.setNavigatepageNums(suggestedPublicationToRead.getNavigatepageNums());
            publicationModelPageInfo.setNavigateFirstPage(suggestedPublicationToRead.getNavigateFirstPage());
            publicationModelPageInfo.setNavigateLastPage(suggestedPublicationToRead.getNavigateLastPage());
            publicationModelPageInfo.setTotal(suggestedPublicationToRead.getTotal());
            publicationModelPageInfo.setList(publicationModels);
            resultModelApi.setData(publicationModelPageInfo);
        }
        return resultModelApi;
    }

    /**
     * 推荐学者一览
     *
     * @param userId
     * @param index
     * @param rows
     * @param keyword
     * @param autocompleteKeyword
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/suggestedUser", method = {RequestMethod.POST})
    @ApiOperation(value = "推荐学者一览")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数"),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示条数"),
            @ApiImplicitParam(paramType = "query", name = "keyword", value = "关键字"),
            @ApiImplicitParam(paramType = "query", name = "autocompleteKeyword", value = "自动补全关键词")
    })
    public ResultModelApi getSuggestedUserList(String userId, int index, int rows, String keyword, String autocompleteKeyword, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        PageInfo<Long> pageInfo;
        List<Long> userIdList = null;
        List<SuggestedUserModel> suggestedUserModelList = new ArrayList<>();

        if (!StringUtils.isEmpty(keyword) && keyword.trim().length() > 0) {
            /*
             * 有关键词时，先从DB检索符合条件所有ID（无分页），
             * 然后从solr 根据这些ID集合以及keyword 进行检索（有分页）
             */
            pageInfo = suggestedService.getUserIdList(Long.valueOf(userId), 1, 0);
            if (pageInfo != null) {
                userIdList = pageInfo.getList();
            }
            // 根据所有ID以及keyword在solr中检索
            if (userIdList != null) {
                // 根据keyword在solr中检索
                SolrResultModel solrResultModel = solrUserSearchService.getUserIdByKeyword(keyword, autocompleteKeyword, index, rows, userIdList);
                userIdList.clear();
                if (solrResultModel != null) {
                    List<Long> entry = solrResultModel.getCustomVal();
                    if (entry != null && entry.size() > 0) {
                        for (Long user : entry) {
                            if (user != null) {
                                userIdList.add(user);
                            }
                        }
                        pageInfo.setTotal(solrResultModel.getRows());
                        pageInfo.setList(userIdList);
                        pageInfo.setPageNum(index);
                        pageInfo.setPageSize(rows);
                        pageInfo.setHasNextPage(solrResultModel.getRows() > 0 && solrResultModel.getRows() > index * rows);
                    }
                }
            }
        } else {
            /*
             * 没有关键词时，只从DB中检索符合条件的ID（有分页）
             */
            // 根据检索条件查询用户ID集合
            pageInfo = suggestedService.getUserIdList(Long.valueOf(userId), index, rows);
            if (pageInfo != null) {
                userIdList = pageInfo.getList();
            }
        }

        if (userIdList != null && userIdList.size() > 0) {
            // 根据检索用户Id获取用户详细信息
            suggestedUserModelList = suggestedService.getSuggestedUserList(Long.valueOf(userId), userIdList);
            // 头像 文件服务器url获取
            for (SuggestedUserModel suggestedUserModel : suggestedUserModelList) {
                if (StringUtil.isNotEmpty(suggestedUserModel.getProfilePhotoPath())) {
                    String profilePath_s3 = AwsS3Utils.getS3FileUrl(suggestedUserModel.getProfilePhotoPath());
                    suggestedUserModel.setProfilePhotoPath(profilePath_s3);
                }
//                suggestedUserModel.setProfilePhotoPath(fileServerHelper.getFileServerUrl(suggestedUserModel.getProfilePhotoPath(), UploadFolder.PROFILE_PHOTO));
            }
        }

        PageInfo<SuggestedUserModel> resultPageInfo = new PageInfo<>();
        if (pageInfo != null) {
            BeanUtils.copyProperties(pageInfo, resultPageInfo);
            resultPageInfo.setList(suggestedUserModelList);
        }
        resultModelApi.setRetCode(ResultCode.Success.code());
        resultModelApi.setData(resultPageInfo);
        return resultModelApi;
    }

    /**
     * 推荐学者一览点击「关注」/「取消关注」
     *
     * @param userId
     * @param userIdFollowed
     * @param followingBtnFlg
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/addRecommendedClickToFollow", method = {RequestMethod.POST})
    @ApiOperation(value = "推荐学者一览点击注「1:关注」/「2:取消关注」")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "userIdFollowed", value = "关注的作者的id", required = true),
            @ApiImplicitParam(paramType = "query", name = "followingBtnFlg", value = "关注按钮区分「1:关注」/「2:取消关注」", required = true)
    })
    public ResultModelApi addRecommendedClickToFollow(String userId, String userIdFollowed, String followingBtnFlg, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        if (userId.equals(userIdFollowed)) {
            resultModelApi.setRetCode(KeensightResultCode.System_90002.code());
            resultModelApi.setRetMsg("不能关注自己");
        } else {
            userAuthorFollowingService.addRecommendedClickToFollow(Long.valueOf(userId), Long.valueOf(userIdFollowed), followingBtnFlg);
            if ("1".equals(followingBtnFlg)) {
                List<Long> userIdList = new ArrayList<>();
                userIdList.add(Long.valueOf(userIdFollowed));
                List<UserModel> userModelList = userService.getUserInfoList(Long.valueOf(userId), userIdList);
                UserModel userMode = new UserModel();
                if (userModelList != null && userModelList.size() > 0) {
                    userMode = userModelList.get(0);
                    resultModelApi.setData(userMode);
                }
            }

            resultModelApi.setRetCode(ResultCode.Success.code());
        }
        return resultModelApi;
    }

    /**
     * 邀请您的合作作者加入学术社区一览
     *
     * @param userId
     * @param index
     * @param rows
     * @param keyword
     * @param autocompleteKeyword
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/userCoauthor", method = {RequestMethod.POST})
    @ApiOperation(value = "邀请您的合作作者加入学术社区一览")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数"),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示条数"),
            @ApiImplicitParam(paramType = "query", name = "keyword", value = "关键字"),
            @ApiImplicitParam(paramType = "query", name = "autocompleteKeyword", value = "自动补全关键词")
    })
    public ResultModelApi getUserCoauthorList(String userId, int index, int rows, String keyword, String autocompleteKeyword, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        PageInfo<Long> pageInfo;
        List<Long> userIdList = null;
        List<UserModel> userModelList = new ArrayList<>();

        if (!StringUtils.isEmpty(keyword) && keyword.trim().length() > 0) {
            /*
             * 有关键词时，先从DB检索符合条件所有ID（无分页），
             * 然后从solr 根据这些ID集合以及keyword 进行检索（有分页）
             */
            pageInfo = userCoauthorService.getUserIdList(Long.valueOf(userId), 1, 0, "1");
            if (pageInfo != null) {
                userIdList = pageInfo.getList();
            }
            // 根据所有ID以及keyword在solr中检索
            if (userIdList != null) {
                // 根据keyword在solr中检索
                SolrResultModel solrResultModel = solrUserSearchService.getUserIdByKeyword(keyword, autocompleteKeyword, index, rows, userIdList);
                userIdList.clear();
                if (solrResultModel != null) {
                    List<Long> entry = solrResultModel.getCustomVal();
                    if (entry != null && entry.size() > 0) {
                        for (Long user : entry) {
                            if (user != null) {
                                userIdList.add(user);
                            }
                        }
                        pageInfo.setTotal(solrResultModel.getRows());
                        pageInfo.setList(userIdList);
                        pageInfo.setPageNum(index);
                        pageInfo.setPageSize(rows);
                        pageInfo.setHasNextPage(solrResultModel.getRows() > 0 && solrResultModel.getRows() > index * rows);
                    }
                }
            }
        } else {
            /*
             * 没有关键词时，只从DB中检索符合条件的ID（有分页）
             */
            // 根据检索条件查询用户ID集合
            pageInfo = userCoauthorService.getUserIdList(Long.valueOf(userId), index, rows, "1");
            if (pageInfo != null) {
                userIdList = pageInfo.getList();
            }
        }

        if (userIdList != null && userIdList.size() > 0) {
            // 根据检索用户Id获取用户详细信息
            userModelList = userService.getUserDetailInfo(Long.valueOf(userId), userIdList, "1");
        }

        PageInfo<UserModel> resultPageInfo = new PageInfo<>();
        if (pageInfo != null) {
            BeanUtils.copyProperties(pageInfo, resultPageInfo);
            resultPageInfo.setList(userModelList);
        }
        resultModelApi.setRetCode(ResultCode.Success.code());
        resultModelApi.setData(resultPageInfo);
        return resultModelApi;
    }

    /**
     * 获取用户关注其他用户的行为
     *
     * @param userId
     * @param index
     * @param rows
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getUserAuthorFollowingList", method = {RequestMethod.POST})
    @ApiOperation(value = "获取用户关注其他用户的行为_滚动查询")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数", required = true),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示条数", required = true)
    })
    public ResultModelApi getUserAuthorFollowingList(String userId, int index, int rows, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();

        PageInfo<UserAuthorFollowing> userAuthorFollowingList = userAuthorFollowingService.getUserAuthorFollowingList(Long.valueOf(userId), index, rows);
        List<UserAuthorFollowing> userAuthorFollowings = userAuthorFollowingList.getList();

        if (userAuthorFollowings.size() != 0) {
            // 设置文献Id
            List<Long> suggestedPublicationToReadList = new ArrayList<>();
            for (UserAuthorFollowing userAuthorFollowing : userAuthorFollowings) {
                suggestedPublicationToReadList.add(Long.parseLong(userAuthorFollowing.getPublicationId() + userAuthorFollowing.getUserAction()));
            }
            // 获取论文详细内容
            List<PublicationModel> publicationModels = publicationService.getPublicationDetailList(suggestedPublicationToReadList, false, true);
            for (int i = 0; i < publicationModels.size(); i++) {
                // 用户Id
                publicationModels.get(i).setUserId(userAuthorFollowings.get(i).getUserId());
                // 关注作者Id
                publicationModels.get(i).setUserIdFollowed(userAuthorFollowings.get(i).getUserIdFollowed());
                // 作者头像设置 文件服务器url获取
                String profilePath_s3 = null;
                if("7".equals(publicationModels.get(i).getUserAction())) {
//                    publicationModels.get(i).setProfilePhotoPath(fileServerHelper.getFileServerUrl(StringUtil.isNotEmpty(publicationModels.get(i).getUserNewsModel().getProfilePath())?publicationModels.get(i).getUserNewsModel().getProfilePath():userAuthorFollowings.get(i).getProfilePhotoPath(), UploadFolder.PROFILE_PHOTO));
                    if (StringUtil.isNotEmpty(publicationModels.get(i).getUserNewsModel().getProfilePath())) {
                        profilePath_s3 =AwsS3Utils.getS3FileUrl(publicationModels.get(i).getUserNewsModel().getProfilePath());
                    } else {
                        profilePath_s3 =AwsS3Utils.getS3FileUrl(userAuthorFollowings.get(i).getProfilePhotoPath());
                    }
                    publicationModels.get(i).setProfilePhotoPath(profilePath_s3);
                    publicationModels.get(i).setIsUser(StringUtil.isNotEmpty(publicationModels.get(i).getUserNewsModel().getProfilePath()));
                }else {
//                    publicationModels.get(i).setProfilePhotoPath(fileServerHelper.getFileServerUrl(StringUtil.isNotEmpty(userAuthorFollowings.get(i).getProfilePath())?userAuthorFollowings.get(i).getProfilePath():userAuthorFollowings.get(i).getProfilePhotoPath(), UploadFolder.PROFILE_PHOTO));
                    if (StringUtil.isNotEmpty(userAuthorFollowings.get(i).getProfilePath())) {
                        profilePath_s3 =AwsS3Utils.getS3FileUrl(userAuthorFollowings.get(i).getProfilePath());
                    } else {
                        profilePath_s3 =AwsS3Utils.getS3FileUrl(userAuthorFollowings.get(i).getProfilePhotoPath());
                    }
                    publicationModels.get(i).setProfilePhotoPath(profilePath_s3);
                    publicationModels.get(i).setIsUser(StringUtil.isNotEmpty(userAuthorFollowings.get(i).getProfilePath()));
                }
                // 作者显示名称
                // 行为文言
                if("7".equals(publicationModels.get(i).getUserAction())){
                    publicationModels.get(i).setDisplayAuthorName(StringUtil.isNotEmpty(publicationModels.get(i).getUserNewsModel().getNickname())?publicationModels.get(i).getUserNewsModel().getNickname():userAuthorFollowings.get(i).getDisplayName());
                } else {
                    publicationModels.get(i).setDisplayAuthorName(StringUtil.isNotEmpty(userAuthorFollowings.get(i).getNickname())?userAuthorFollowings.get(i).getNickname():userAuthorFollowings.get(i).getDisplayName());
                }
                // 行为文言
                if("7".equals(publicationModels.get(i).getUserAction())){
                    publicationModels.get(i).setHeaderTitle(publicationModels.get(i).getUserNewsModel().getNewsTitle());
                } else {
                    publicationModels.get(i).setHeaderTitle(userAuthorFollowings.get(i).getUserActiontTitle());
                }
                // 时间差
                Date createdAt = userAuthorFollowings.get(i).getCreatedAt();
                publicationModels.get(i).setValidateTime(DateUtil.validateTime(new Date(), createdAt));
            }

            PageInfo<PublicationModel> publicationModelPageInfo = new PageInfo<>(publicationModels);
            publicationModelPageInfo.setPageNum(userAuthorFollowingList.getPageNum());
            publicationModelPageInfo.setPageSize(userAuthorFollowingList.getPageSize());
            publicationModelPageInfo.setStartRow(userAuthorFollowingList.getStartRow());
            publicationModelPageInfo.setEndRow(userAuthorFollowingList.getEndRow());
            publicationModelPageInfo.setPages(userAuthorFollowingList.getPages());
            publicationModelPageInfo.setPrePage(userAuthorFollowingList.getPrePage());
            publicationModelPageInfo.setNextPage(userAuthorFollowingList.getNextPage());
            publicationModelPageInfo.setIsFirstPage(userAuthorFollowingList.isIsFirstPage());
            publicationModelPageInfo.setIsLastPage(userAuthorFollowingList.isIsLastPage());
            publicationModelPageInfo.setHasPreviousPage(userAuthorFollowingList.isHasPreviousPage());
            publicationModelPageInfo.setHasNextPage(userAuthorFollowingList.isHasNextPage());
            publicationModelPageInfo.setNavigatePages(userAuthorFollowingList.getNavigatePages());
            publicationModelPageInfo.setNavigatepageNums(userAuthorFollowingList.getNavigatepageNums());
            publicationModelPageInfo.setNavigateFirstPage(userAuthorFollowingList.getNavigateFirstPage());
            publicationModelPageInfo.setNavigateLastPage(userAuthorFollowingList.getNavigateLastPage());
            publicationModelPageInfo.setTotal(userAuthorFollowingList.getTotal());
            publicationModelPageInfo.setList(publicationModels);
            resultModelApi.setData(publicationModelPageInfo);
        }
        return resultModelApi;
    }

    /**
     * 获得您可能感兴趣的工作机会
     *
     * @param userId
     * @param index
     * @param rows
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getUserAuthorFollowingActionList", method = {RequestMethod.POST})
    @ApiOperation(value = "获得您可能感兴趣的工作机会")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数", required = true),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示条数", required = true)
    })
    public ResultModelApi getUserAuthorFollowingActionList(String userId, int index, int rows, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        PageInfo<SuggestedAnnouncementToView> suggestedAnnouncementToViewPageInfo = suggestedAnnouncementToViewService.getSuggestedAnnouncementToViewList(Long.valueOf(userId), index, rows);
        if (suggestedAnnouncementToViewPageInfo != null && suggestedAnnouncementToViewPageInfo.getList().size() > 0) {
            // 文件服务器url获取
            for (SuggestedAnnouncementToView suggestedAnnouncementToView : suggestedAnnouncementToViewPageInfo.getList()) {
                if(StringUtil.isNotEmpty(suggestedAnnouncementToView.getImagePath())){
                    String profilePath_s3 = AwsS3Utils.getS3FileUrl(suggestedAnnouncementToView.getImagePath());
                    suggestedAnnouncementToView.setImagePath(profilePath_s3);
                }
//                suggestedAnnouncementToView.setImagePath(fileServerHelper.getFileServerUrl(suggestedAnnouncementToView.getImagePath(), UploadFolder.PROFILE_PHOTO));
            }
        }
        resultModelApi.setData(suggestedAnnouncementToViewPageInfo);
        return resultModelApi;
    }

    /**
     * 用户操作
     * 定义Flg 0:引用 1:阅读 2:推荐 3:分享 4:收藏 5:下载 6:关注 7:举报
     *
     * @param userId
     * @param publicationId
     * @param authorIds
     * @param actionFlg
     * @param operationalBehaviorFlg
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/updUserAction", method = {RequestMethod.POST})
    @ApiOperation(value = "针对文献的用户操作 0:引用 1:阅读 2:推荐 3:分享 4:收藏 5:下载 6:关注 7:举报")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献id", required = true),
            @ApiImplicitParam(paramType = "query", name = "authorIds", value = "作者id", required = true),
            @ApiImplicitParam(paramType = "query", name = "actionFlg", value = "区分用户操作Flg", required = true),
            @ApiImplicitParam(paramType = "query", name = "operationalBehaviorFlg", value = "区分用户操作行为Flg", required = true),
            @ApiImplicitParam(paramType = "query", name = "deletePrivateFlg", value = "用户删除私有文献Flg", required = true)
    })
    public ResultModelApi updUserAction(String userId, String publicationId,
                                        String authorIds, String actionFlg,
                                        Boolean operationalBehaviorFlg, Boolean deletePrivateFlg, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        userActionPublicationService.updUserAction(Long.valueOf(userId), Long.valueOf(publicationId), authorIds, actionFlg, operationalBehaviorFlg, deletePrivateFlg);
        return resultModelApi;
    }

    /**
     * 本周您研究影响力统计
     *
     * @param userId
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getThisWeekStatistics", method = {RequestMethod.POST})
    @ApiOperation(value = "本周您研究影响力统计")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getThisWeekStatistics(String userId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        resultModelApi.setData(statsUserTotalService.getThisWeekStatistics(Long.valueOf(userId)));
        return resultModelApi;
    }

    /**
     * 获取各类通知，包括广告
     * @param userId
     * @param type
     * @return
     */
    @RequestMapping(value = "/getAnnouncementList", method = {RequestMethod.POST})
    @ApiOperation(value = "获取各类通知，包括广告")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "type", value = "通知类型", required = true),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数", required = true),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示条数", required = true)
    })
    public ResultModelApi getAnnouncementList(String userId, String type, Integer index, Integer rows){
        ResultModelApi resultModelApi = new ResultModelApi();
        if (StringUtils.isEmpty(userId) || !StringUtils.isNumber(userId)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }
        PageInfo<SuggestedAnnouncementToView> suggestedAnnouncementToViewPageInfo = suggestedAnnouncementToViewService.getAnnouncementList(Long.valueOf(userId), type, index, rows);
        if (suggestedAnnouncementToViewPageInfo != null && suggestedAnnouncementToViewPageInfo.getList().size() > 0) {
            // 文件服务器url获取
            for (SuggestedAnnouncementToView suggestedAnnouncementToView : suggestedAnnouncementToViewPageInfo.getList()) {
                if(StringUtil.isNotEmpty(suggestedAnnouncementToView.getImagePath())){
                    String profilePath_s3 = AwsS3Utils.getS3FileUrl(suggestedAnnouncementToView.getImagePath());
                    suggestedAnnouncementToView.setImagePath(profilePath_s3);
                }
            }
        }
        resultModelApi.setData(suggestedAnnouncementToViewPageInfo);
        return resultModelApi;
    }

   /**
     * 学者页推荐动态获取方法
     *
     * @param loginUserId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getAllNewsList", method = {RequestMethod.POST})
    public ResultModelApi getAllNewsList(Long loginUserId, Integer index, Integer rows, HttpServletRequest request, Long userId, Long newsId) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 获取最新动态
        Map<String, PageInfo> result = userNewDevelopmentsService.getAllNewsList(loginUserId,index,rows,userId,newsId);

        List<PublicationModel> publicationModels = result.get("publicationModelList").getList();

        PageInfo userNewsModelList = result.get("userNewsModelList");
        for (int i = 0; i < publicationModels.size(); i++) {
            // 作者头像设置 文件服务器url获取
            if("7".equals(publicationModels.get(i).getUserAction())) {
                String profilePhotoPath_s3 = null;
                if (StringUtil.isNotEmpty(publicationModels.get(i).getUserNewsModel().getProfilePath())) {
                    profilePhotoPath_s3 = AwsS3Utils.getS3FileUrl(publicationModels.get(i).getUserNewsModel().getProfilePath());
                } else {
                    profilePhotoPath_s3 = AwsS3Utils.getS3FileUrl(publicationModels.get(i).getUserNewsModel().getProfilePhotoPath());
                }

                publicationModels.get(i).setProfilePhotoPath(profilePhotoPath_s3);

                // 动态图片
                if (StringUtil.isNotEmpty(publicationModels.get(i).getUserNewsModel().getProfilePath())) {
                    String profilePath_s3 = AwsS3Utils.getS3FileUrl(publicationModels.get(i).getUserNewsModel().getProfilePath());

                    publicationModels.get(i).getUserNewsModel().setProfilePath(profilePath_s3);
                }

                if (publicationModels.get(i).getUserNewsModel().getUserNewsFilesList() != null && publicationModels.get(i).getUserNewsModel().getUserNewsFilesList().size() > 0) {
                    List<UserNewsFiles> userNewsFilesList = publicationModels.get(i).getUserNewsModel().getUserNewsFilesList();
                    for (int j = 0; j < userNewsFilesList.size(); j++) {
                        if (StringUtil.isNotEmpty(userNewsFilesList.get(j).getNewsFileUrl())) {
                            String newsFileUrl_s3 = AwsS3Utils.getS3FileUrl(userNewsFilesList.get(j).getNewsFileUrl());
                            userNewsFilesList.get(j).setNewsFileUrl(newsFileUrl_s3);
                        }

                    }
                }

                publicationModels.get(i).setIsUser(StringUtil.isNotEmpty(publicationModels.get(i).getUserNewsModel().getProfilePath()));
            }

            // 作者显示名称
            if("7".equals(publicationModels.get(i).getUserAction())){
                publicationModels.get(i).setDisplayAuthorName(StringUtil.isNotEmpty(publicationModels.get(i).getUserNewsModel().getNickname())?publicationModels.get(i).getUserNewsModel().getNickname():publicationModels.get(i).getUserNewsModel().getDisplayName());
            }
            // 作者id
            if("7".equals(publicationModels.get(i).getUserAction())){
                publicationModels.get(i).setUserIdFollowed(publicationModels.get(i).getUserNewsModel().getUserId());
            }
            // 动态标题
            if("7".equals(publicationModels.get(i).getUserAction())){
                publicationModels.get(i).setHeaderTitle(publicationModels.get(i).getUserNewsModel().getNewsTitle());
            }
            // 时间差
            Date createdAt = publicationModels.get(i).getUserNewsModel().getNewsPublishTime();
            publicationModels.get(i).setValidateTime(DateUtil.validateTime(new Date(), createdAt));
        }
        PageInfo<PublicationModel> publicationModelPageInfo = new PageInfo<>(publicationModels);
        /*Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("userNews", userNews.getList());
        resultMap.put("hasNextPage", userNews.isHasNextPage());
        resultModelApi.setData(resultMap);*/
        publicationModelPageInfo.setList(publicationModels);
        publicationModelPageInfo.setHasNextPage(userNewsModelList.isHasNextPage());
        resultModelApi.setData(publicationModelPageInfo);
        return resultModelApi;
    }

    /**
     * 获取用户推荐的论文
     *
     * @param userId 用户Id
     * @param index 页数
     * @param rows 条数
     * @return 用户推荐的论文
     */
    @RequestMapping(value = "/getUserRecommendedPapers", method = RequestMethod.POST)
    public ResultModelApi getUserRecommendedPapers(Long userId, int index, int rows) {
        ResultModelApi resultModelApi = new ResultModelApi();
        PageInfo<UserAuthorFollowing> userAuthorFollowingList = userAuthorFollowingService.getUserRecommendedPapers(userId, index, rows);
        List<UserAuthorFollowing> userAuthorFollowings = userAuthorFollowingList.getList();
        if (userAuthorFollowings.size() != 0) {
            // 设置文献Id
            List<Long> suggestedPublicationToReadList = new ArrayList<>();
            for (UserAuthorFollowing userRecommendedPaper : userAuthorFollowings) {
                suggestedPublicationToReadList.add(Long.parseLong(userRecommendedPaper.getPublicationId() + userRecommendedPaper.getUserAction()));
            }
            // 获取论文详细内容
            List<PublicationModel> publicationModels = publicationService.getPublicationDetailList(suggestedPublicationToReadList, false, true);
            for (int i = 0; i < publicationModels.size(); i++) {
                // 用户Id
                publicationModels.get(i).setSummary(null);

            }
            PageInfo<PublicationModel> publicationModelPageInfo = new PageInfo<>(publicationModels);
            publicationModelPageInfo.setPageNum(userAuthorFollowingList.getPageNum());
            publicationModelPageInfo.setPageSize(userAuthorFollowingList.getPageSize());
            publicationModelPageInfo.setStartRow(userAuthorFollowingList.getStartRow());
            publicationModelPageInfo.setEndRow(userAuthorFollowingList.getEndRow());
            publicationModelPageInfo.setPages(userAuthorFollowingList.getPages());
            publicationModelPageInfo.setPrePage(userAuthorFollowingList.getPrePage());
            publicationModelPageInfo.setNextPage(userAuthorFollowingList.getNextPage());
            publicationModelPageInfo.setIsFirstPage(userAuthorFollowingList.isIsFirstPage());
            publicationModelPageInfo.setIsLastPage(userAuthorFollowingList.isIsLastPage());
            publicationModelPageInfo.setHasPreviousPage(userAuthorFollowingList.isHasPreviousPage());
            publicationModelPageInfo.setHasNextPage(userAuthorFollowingList.isHasNextPage());
            publicationModelPageInfo.setNavigatePages(userAuthorFollowingList.getNavigatePages());
            publicationModelPageInfo.setNavigatepageNums(userAuthorFollowingList.getNavigatepageNums());
            publicationModelPageInfo.setNavigateFirstPage(userAuthorFollowingList.getNavigateFirstPage());
            publicationModelPageInfo.setNavigateLastPage(userAuthorFollowingList.getNavigateLastPage());
            publicationModelPageInfo.setTotal(userAuthorFollowingList.getTotal());
            publicationModelPageInfo.setList(publicationModels);
            resultModelApi.setData(publicationModelPageInfo);
        }
        return resultModelApi;
    }
}
