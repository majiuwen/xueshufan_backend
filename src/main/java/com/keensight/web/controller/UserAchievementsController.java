package com.keensight.web.controller;

import cn.newtouch.dl.solr.model.SolrResultModel;
import cn.newtouch.dl.turbocloud.constants.ResultCode;
import cn.newtouch.dl.turbocloud.controller.BaseController;
import cn.newtouch.dl.turbocloud.helper.FileHelper;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import cn.newtouch.dl.turbocloud.model.UserModel;
import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.keensight.solr.service.SolrPublicationSearchService;
import com.keensight.web.constants.WebConst;
import com.keensight.web.constants.enums.FileType;
import com.keensight.web.db.entity.*;
import com.keensight.web.model.PublicationModel;
import com.keensight.web.model.UserAchievementsStatisticsModel;
import com.keensight.web.service.*;
import com.keensight.web.utils.FileUtil;
import com.keensight.web.utils.PDFUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * 用户学术成果控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/04] 新建
 */
@Api(value = "用户学术成果")
@RestController
@RequestMapping("/userAchievements")
public class UserAchievementsController extends BaseController {
    @Autowired
    private PublicationService publicationService;
    @Autowired
    private DocumentService documentService;
    @Autowired
    private CodeService codeService;
    @Autowired
    private SolrPublicationSearchService solrPublicationSearchService;
    @Autowired
    private ConferenceSerieService conferenceSerieService;
    @Autowired
    private ConferenceInstanceService conferenceInstanceService;
    @Autowired
    private JournalService journalService;
    @Autowired
    private PublicationUserInsetitutionService publicationUserInsetitutionService;
    @Autowired
    private UserActionPublicationService userActionPublicationService;
    @Autowired
    private FileHelper fileHelper;

    /**
     * 用户学术成果统计信息
     *
     * @param userId 用户ID
     * @return resultModelApi
     */
    @RequestMapping(value = "/getUserAchievementsStatistics", method = {RequestMethod.POST})
    @ApiOperation(value = "用户学术成果统计信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getUserAchievementsStatistics(String userId) {
        ResultModelApi resultModelApi = new ResultModelApi();
        if (StringUtils.isEmpty(userId) || !StringUtils.isNumber(userId)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }

        Long searchUserId = Long.valueOf(userId);
        List<UserAchievementsStatisticsModel> resultList = new ArrayList<>();
        UserAchievementsStatisticsModel resultModel;
        // 所有类型统计
        long allTypeStatistics = 0;
        // 论文统计
        long paperStatistics = 0;
        // 文献统计结果取得
        List<Publication> publicationList = publicationService.getPublicationStatisticsByUserId(searchUserId);
        for (Publication publication : publicationList) {
            if (publication != null && !publication.getStatisticsResult4DocType().isBlank()) {
                resultModel = new UserAchievementsStatisticsModel();
                resultModel.setGroupCd(publication.getGroupCd());
                resultModel.setDocType(publication.getDocType());
                resultModel.setCode(publication.getGroupCd() + "," + publication.getDocType());
                resultModel.setDocTypeName(publication.getDocTypeName());
                resultModel.setDocTypeNameEn(publication.getDocTypeNameEn());
                resultModel.setTypeBelong(publication.getTypeBelong());
                resultModel.setStatisticsCount(publication.getStatisticsResult4DocType());
                resultList.add(resultModel);

                long statisticsResult4DocType = Long.parseLong(publication.getStatisticsResult4DocType());
                allTypeStatistics += statisticsResult4DocType;
                // 文献类型是论文时
                if (WebConst.DOC_TYPE_BELONG.equals(publication.getTypeBelong())) {
                    paperStatistics += statisticsResult4DocType;
                }
            }
        }
        // 文档统计结果取得
        List<Document> documentList = documentService.getDocumentStatisticsByUserId(searchUserId);
        for (Document document : documentList) {
            if (document != null && !document.getStatisticsResult4DocType().isBlank()) {
                resultModel = new UserAchievementsStatisticsModel();
                resultModel.setGroupCd(document.getGroupCd());
                resultModel.setDocType(document.getDocType());
                resultModel.setCode(document.getGroupCd() + "," + document.getDocType());
                resultModel.setDocTypeName(document.getDocTypeName());
                resultModel.setDocTypeNameEn(document.getDocTypeNameEn());
                resultModel.setStatisticsCount(document.getStatisticsResult4DocType());
                resultList.add(resultModel);
            }
        }

        // 所有类型
        resultModel = new UserAchievementsStatisticsModel();
        resultModel.setGroupCd(UserAchievementsStatisticsModel.DOC_TYPE_ALL_CODE);
        resultModel.setDocType(UserAchievementsStatisticsModel.DOC_TYPE_ALL_CODE);
        resultModel.setCode(UserAchievementsStatisticsModel.DOC_TYPE_ALL_CODE + "," + UserAchievementsStatisticsModel.DOC_TYPE_ALL_CODE);
        resultModel.setDocTypeName(UserAchievementsStatisticsModel.DOC_TYPE_ALL_NAME);
        resultModel.setDocTypeNameEn(UserAchievementsStatisticsModel.DOC_TYPE_ALL_NAME_EN);
        resultModel.setStatisticsCount("" + allTypeStatistics);
        resultList.add(0, resultModel);
        // 文献类型属于论文
        resultModel = new UserAchievementsStatisticsModel();
        resultModel.setGroupCd(UserAchievementsStatisticsModel.DOC_TYPE_PAPER_CODE);
        resultModel.setDocType(UserAchievementsStatisticsModel.DOC_TYPE_PAPER_CODE);
        resultModel.setCode(UserAchievementsStatisticsModel.DOC_TYPE_PAPER_CODE + "," + UserAchievementsStatisticsModel.DOC_TYPE_PAPER_CODE);
        resultModel.setDocTypeName(UserAchievementsStatisticsModel.DOC_TYPE_PAPER_NAME);
        resultModel.setDocTypeNameEn(UserAchievementsStatisticsModel.DOC_TYPE_PAPER_NAME_EN);
        resultModel.setStatisticsCount("" + paperStatistics);
        resultList.add(1, resultModel);

        resultModelApi.setRetCode(ResultCode.Success.code());
        resultModelApi.setData(resultList);
        return resultModelApi;
    }

    /**
     * 用户学术成果信息集合取得
     *
     * @param userId            用户ID
     * @param groupCdAndDocType 组别,类型
     * @param keyword           关键词/题目
     * @param sortColumns       排序列
     * @param index             开始页数
     * @param rows              显示件数
     * @return resultModelApi
     */
    @RequestMapping(value = "/getUserAchievementsList", method = {RequestMethod.POST})
    @ApiOperation(value = "用户学术成果信息集合取得")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "code", value = "组别,类型", required = true),
            @ApiImplicitParam(paramType = "query", name = "keyword", value = "关键词/题目"),
            @ApiImplicitParam(paramType = "query", name = "autocompleteKeyword", value = "自动补全关键词/题目"),
            @ApiImplicitParam(paramType = "query", name = "sortColumns", value = "排序列"),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数"),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示件数")
    })
    public ResultModelApi getUserAchievementsList(String loginUserId, String userId, @RequestParam("code") String groupCdAndDocType, String keyword, String autocompleteKeyword,
                                                  String sortColumns, Integer index, Integer rows) {
        ResultModelApi resultModelApi = new ResultModelApi();

        // 检索条件处理 start
        if (StringUtils.isEmpty(userId) || !StringUtils.isNumber(userId) || StringUtils.isEmpty(groupCdAndDocType) || groupCdAndDocType.split(",").length < 2) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }
        Long searchUserId = Long.valueOf(userId);
        String[] codes = groupCdAndDocType.split(",");
        String groupCd = codes[0];
        String docType = codes[1];
        List<String> docTypeList = new ArrayList<>();
        // 论文的场合
        if (UserAchievementsStatisticsModel.DOC_TYPE_PAPER_CODE.equals(groupCd)) {
            // 从code表中检索出组别时“论文”的文献类型
            List<Code> codeList = codeService.getCodeListInfoByGroupCd(WebConst.GROUP_CD_DOC_TYPE);
            if (codeList != null && codeList.size() > 0) {
                List<Code> resultList = codeList.stream().filter(codeInfo -> WebConst.DOC_TYPE_BELONG.equals(codeInfo.getReserve1())).collect(Collectors.toList());
                for (Code result : resultList) {
                    if (result != null) {
                        docTypeList.add(result.getCode());
                    }
                }
            }
            // 文献或文档的场合
        } else if (WebConst.GROUP_CD_DOC_TYPE.equals(groupCd) || WebConst.GROUP_CD_DOCUMENT_TYPE.equals(groupCd)) {
            docTypeList.add(docType);
        } else { // 所有的场合
            docTypeList = null;
        }
        // 检索条件处理 end

        // 检索处理 start
        PageInfo<Long> pageInfo;
        List<Long> publicationIdList = null;
        List<PublicationModel> detailList = new ArrayList<>();
        if (!StringUtils.isEmpty(keyword) && keyword.trim().length() > 0) {
            /*
             * 有关键词时，先从DB检索符合条件所有ID（无分页），
             * 然后从solr 根据这些ID集合以及keyword 进行检索（有分页）
             */

            pageInfo = getPublicationIdList(groupCd, searchUserId, docTypeList, sortColumns, 1, 0);
            if (pageInfo != null) {
                publicationIdList = pageInfo.getList();
            }
            // 根据所有ID以及keyword在solr中检索
            if (publicationIdList != null && publicationIdList.size() > 0) {
                SolrResultModel solrResultModel = solrPublicationSearchService.getPublicationIdByKeyword(keyword, autocompleteKeyword, index, rows, publicationIdList);
                publicationIdList.clear();
                if (solrResultModel != null) {
                    List<Long> entry = solrResultModel.getCustomVal();
                    if (entry != null && entry.size() > 0) {
                        for (Long publication : entry) {
                            if (publication != null) {
                                publicationIdList.add(publication);
                            }
                        }
                    }
                    pageInfo.setTotal(solrResultModel.getRows());
                    pageInfo.setList(publicationIdList);
                    pageInfo.setPageNum(index);
                    pageInfo.setPageSize(rows);
                    pageInfo.setHasNextPage(solrResultModel.getRows() > 0 && solrResultModel.getRows() > index * rows);
                }
            }
        } else {
            /*
             * 没有关键词时，只从DB中检索符合条件的ID（有分页）
             */
            // 根据检索条件查询文献ID集合
            pageInfo = getPublicationIdList(groupCd, searchUserId, docTypeList, sortColumns, index, rows);
            if (pageInfo != null) {
                publicationIdList = pageInfo.getList();
            }
        }

        if (publicationIdList != null && publicationIdList.size() > 0) {
            // 调用共通检索获取文献详细信息一览数据
            detailList = publicationService.getPublicationDetailList(publicationIdList);
            if (detailList != null && detailList.size() > 0) {
                // 数据处理
                for (PublicationModel model : detailList) {
                    if (model != null) {
                        // 收藏按钮显示Flg
                        model.setSaveBtnFlg("0");
                        // 推荐按钮显示Flg (0：都不显示，1：显示，2：显示取消)
                        model.setRecommendBtnFlg("0");
                        // 关注按钮显示Flg (0：都不显示，1：显示，2：显示取消)
                        model.setFollowingBtnFlg("0");
                        // 分享按钮显示Flg (0：都不显示，1：显示，2：显示取消)
                        model.setShareBtnFlg("0");
                        // 上传全文按钮显示Flg (0：都不显示，1：显示上传全文，2：有全文)
                        if (StringUtils.isEmpty(loginUserId) || !loginUserId.equals(userId)) {
                            model.setUploadPdfBtnFlg(WebConst.UPLOADPDF_BTN_FLG_HIDE);
                            // 增加补充材料按钮显示Flg (0：不显示，1：显示)
                            model.setUploadOtherFileBtnFlg("0");
                        } else {
                            // 下载按钮显示Flg (0：都不显示，1：显示，2：索要全文)
                            model.setDownloadBtnFlg("0");
                            // 增加补充材料按钮显示Flg (0：不显示，1：显示)
                            model.setUploadOtherFileBtnFlg("1");
                            if (StringUtils.isEmpty(model.getFulltextPath())) {
                                model.setUploadPdfBtnFlg(WebConst.UPLOADPDF_BTN_FLG_UPLOAD_PDF);
                            } else {
                                model.setUploadPdfBtnFlg(WebConst.UPLOADPDF_BTN_FLG_EXIST_PDF);
                            }
                        }
                    }
                }
            }
        }
        // 检索处理 end

        PageInfo<PublicationModel> resultPageInfo = new PageInfo<>();
        if (pageInfo != null) {
            BeanUtils.copyProperties(pageInfo, resultPageInfo);
            resultPageInfo.setList(detailList);
        }
        resultModelApi.setRetCode(ResultCode.Success.code());
        resultModelApi.setData(resultPageInfo);
        return resultModelApi;
    }

    /**
     * 符合条件的文献ID集合取得方法
     *
     * @param groupCd     组别
     * @param userId      用户ID
     * @param docTypeList 文献类型集合
     * @param sortColumns 排序列
     * @param pageNum     开始页数
     * @param pageSize    显示件数
     * @return 文献ID集合
     */
    private PageInfo<Long> getPublicationIdList(String groupCd, Long userId, List<String> docTypeList, String sortColumns, Integer pageNum, Integer pageSize) {
        PageInfo<Long> pageInfo;
        // 文献的场合
        if (WebConst.GROUP_CD_DOC_TYPE.equals(groupCd) || UserAchievementsStatisticsModel.DOC_TYPE_PAPER_CODE.equals(groupCd)) {
            // 根据检索条件查询文献ID集合
            pageInfo = publicationService.getPublicationIdList(userId, docTypeList, sortColumns, pageNum, pageSize);
            // 文档的场合
        } else if (WebConst.GROUP_CD_DOCUMENT_TYPE.equals(groupCd)) {
            pageInfo = documentService.getPublicationIdList(false, userId, docTypeList, sortColumns, pageNum, pageSize);
        } else { // 所有的场合
            pageInfo = documentService.getPublicationIdList(true, userId, docTypeList, sortColumns, pageNum, pageSize);
        }
        return pageInfo;
    }

    /**
     * 文件上传
     *
     * @param publicationId 文献ID
     * @param isPublicDoc   文档是否公开（1:是，0:否）
     * @param file          上传的文件
     * @return resultModelApi
     * @throws Exception
     */
    @RequestMapping(value = "/fileUpload", method = {RequestMethod.POST})
    @ApiOperation(value = "上传全文")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献ID", required = true),
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户ID", required = true),
            @ApiImplicitParam(paramType = "query", name = "isPublicDoc", value = "文档是否公开", required = true),
            @ApiImplicitParam(paramType = "query", name = "file", value = "要上传的文件", required = true),
            @ApiImplicitParam(paramType = "query", name = "authorFlg", value = "login用户是否为作者Flg", required = true)
    })
    public ResultModelApi fileUpload(String publicationId, String userId, String isPublicDoc, @RequestParam("file") MultipartFile file, Boolean authorFlg) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 参数处理
        if (StringUtils.isEmpty(publicationId) || !StringUtils.isNumber(publicationId)
                || StringUtils.isEmpty(userId) || !StringUtils.isNumber(userId)
                || StringUtils.isEmpty(isPublicDoc) || file == null || file.isEmpty()) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("上传失败，参数错误");
            return resultModelApi;
        }
        // 文件类型判断
        if (!FileUtil.isSpecifyTypeFile(file.getInputStream(), FileType.PDF)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("上传失败，错误的文件类型");
            return resultModelApi;
        }

        Long publicationIdL = Long.valueOf(publicationId);
        Long userIdL = Long.valueOf(userId);
        // 表更新操作
        Document uploadResult = publicationService.update4FulltextUpload(publicationIdL, userIdL, WebConst.PUBLIC_DOC.equals(isPublicDoc), file, authorFlg);
        if (uploadResult.getDocId() != null) {
            resultModelApi.setData(uploadResult);
            resultModelApi.setRetCode(ResultCode.Success.code());
        } else {
            resultModelApi.setRetCode(ResultCode.Request_Error_500.code());
            resultModelApi.setRetMsg("上传失败");
        }

        return resultModelApi;
    }

    /**
     * 上传pdf到临时文件夹方法
     * @param file
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/uploadTempFile", method = {RequestMethod.POST})
    public ResultModelApi uploadPdf(@RequestParam("file") MultipartFile file,Long userId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        PublicationModel publicationModel = new PublicationModel();
        publicationModel.setFile(file);
        //创建Properties对象
        Properties prop = new Properties();
        //读取classPath中的properties文件
        prop.load(UserAchievementsController.class.getClassLoader().getResourceAsStream("application.properties"));
        // 文件服务器总路径
        String filePath = prop.getProperty("keensight.file.server.path");
        // String filePath = "C:/666/";
        // 正式文件上传路径
        String fileFormalPath = prop.getProperty("pdf.upload.path");
        // 临时文件上传路径
        String fileTempPath = prop.getProperty("pdf.temp.path");
        // 上传文件名
        String uploadfileName = null;
        // 文件流
        InputStream inputStream = null;
        // 获取文件流
        inputStream = file.getInputStream();
        // 获取上传文件名
        uploadfileName = file.getOriginalFilename();
        // pdf文件上传到临时文件夹
        if (fileHelper.mkDir(filePath + fileFormalPath + fileTempPath + File.separator + userId + File.separator)) {
            boolean uploadFlg = fileHelper.saveFile(inputStream, filePath + fileFormalPath + fileTempPath + File.separator + userId + File.separator, uploadfileName);
            if(uploadFlg){
                PublicationModel pdfInfo = PDFUtil.extractPDFInfo(filePath + fileFormalPath + fileTempPath + File.separator + userId + File.separator + uploadfileName);
                BeanUtils.copyProperties(pdfInfo, publicationModel);
            }
        }
        resultModelApi.setData(publicationModel);
        return resultModelApi;
    }

    /**
     * 获取会议实例
     *
     * @param conferenceInstanceId 会议实例ID
     * @return resultModelApi
     * @throws Exception
     */
    @RequestMapping(value = "/getConferenceInstance", method = {RequestMethod.POST})
    @ApiOperation(value = "获取会议实例")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "conferenceInstanceId", value = "会议实例ID", required = true)
    })
    public ResultModelApi getConferenceInstance(Long conferenceInstanceId) throws Exception {

        ResultModelApi resultModelApi = new ResultModelApi();
        ConferenceInstance conferenceInstance = new ConferenceInstance();
        // 参数处理
        if (conferenceInstanceId != null) {
            conferenceInstance = conferenceInstanceService.getConferenceInstanceById(conferenceInstanceId);
        }
        resultModelApi.setData(conferenceInstance);

        return resultModelApi;
    }

    /**
     * 获取会议系列
     *
     * @param conferenceSerieId 会议系列ID
     * @return resultModelApi
     * @throws Exception
     */
    @RequestMapping(value = "/getConferenceSerie", method = {RequestMethod.POST})
    @ApiOperation(value = "获取会议系列")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "conferenceInstanceId", value = "会议系列ID", required = true)
    })
    public ResultModelApi getConferenceSerie(Long conferenceSerieId) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        ConferenceSerie conferenceSerie = new ConferenceSerie();
        // 参数处理
        if (conferenceSerieId != null) {
            conferenceSerie = conferenceSerieService.getConferenceSerieById(conferenceSerieId);
        }

        resultModelApi.setData(conferenceSerie);

        return resultModelApi;
    }

    /**
     * 获取期刊
     *
     * @param journalId 期刊ID
     * @return resultModelApi
     * @throws Exception
     */
    @RequestMapping(value = "/getJournal", method = {RequestMethod.POST})
    @ApiOperation(value = "获取期刊")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "conferenceInstanceId", value = "期刊ID", required = true)
    })
    public ResultModelApi getJournal(Long journalId) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        Journal journal = new Journal();
        // 参数处理
        if (journalId != null) {
            journal = journalService.getJournalById(journalId);
        }
        resultModelApi.setData(journal);

        return resultModelApi;
    }

    /**
     * 新增文献处理
     *
     * @param publicationModelJson 文献对象
     * @return resultModelApi
     * @throws Exception
     */
    @RequestMapping(value = "/insertPublication", method = {RequestMethod.POST})
    @ApiOperation(value = "新增文献")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "publicationModelJson", value = "文献对象", required = true),
            @ApiImplicitParam(paramType = "query", name = "file", value = "文件", required = true)
    })
    public ResultModelApi insertPublication(String publicationModelJson, MultipartFile file) throws Exception {

        ResultModelApi resultModelApi = new ResultModelApi();
        PublicationModel publicationModel = JSONObject.parseObject(publicationModelJson,PublicationModel.class);
        if (!ObjectUtils.isEmpty(publicationModel)) {
            Long publicationId = publicationModel.getPublicationId();
            // 上传文献
            if (publicationModel.getAnalysisFlg()) {
                // 未匹配文献上传解析文献信息

                // 期刊Id
                Long journalId = null;
                if (!StringUtils.isEmpty(publicationModel.getJournalName())) {
                    journalId = journalService.getJournalId(publicationModel.getJournalName(), publicationModel.getPublisherUrl());
                }

                // 会议系列Id
                Long conferenceSerieId = null;
                if (!StringUtils.isEmpty(publicationModel.getConferenceSerieName())) {
                    conferenceSerieId = conferenceSerieService.getConferenceSerieId(publicationModel.getConferenceSerieName());
                }

                // 会议实例Id
                Long conferenceInstanceId = null;
                if (!StringUtils.isEmpty(publicationModel.getConferenceInstanceName())) {
                    conferenceInstanceId = conferenceInstanceService.getConferenceInstanceId(publicationModel.getConferenceInstanceName(), conferenceSerieId);
                }
                publicationModel.setJournalId(journalId);
                publicationModel.setConferenceSerieId(conferenceSerieId);
                publicationModel.setConferenceInstanceId(conferenceInstanceId);
            }
            // 新增文献
            PublicationModel insPublicationModel = publicationService.insPublication(publicationModel);

            if (!ObjectUtils.isEmpty(insPublicationModel)) {
                Long newPublicationId = insPublicationModel.getPublicationId();
                if (!publicationModel.getAnalysisFlg()) {
                    // 上传文献为匹配文献时为新文献匹配作者关系
                    int resCount = publicationUserInsetitutionService.setPublicationUserInstotution(publicationId, newPublicationId);
                    if (resCount == 0) {
                        resultModelApi.setRetCode("-1");
                        resultModelApi.setRetMsg("新文献作者关系插入失败！");
                        return resultModelApi;
                    }
                }
                // 加入收藏夹
                userActionPublicationService.updUserAction(Long.valueOf(publicationModel.getUserId()), Long.valueOf(newPublicationId), null, "4", true, false);
                // 表更新操作
                Document uploadResult = publicationService.update4FulltextUpload(newPublicationId, publicationModel.getUserId(), false, file, false);
                if (uploadResult.getDocId() == null) {
                    resultModelApi.setRetCode("-1");
                    resultModelApi.setRetMsg("上传失败");
                    return resultModelApi;
                }
                resultModelApi.setRetCode("0");
            } else {
                resultModelApi.setRetCode("-1");
                resultModelApi.setRetMsg("新文献插入失败！");
                return resultModelApi;
            }
        }

        return resultModelApi;
    }

    /**
     * 根据doi或文献标题匹配文献
     *
     * @param doi doi
     * @param publicationTitle publicationTitle
     * @return resultModelApi
     * @throws Exception
     */
    @RequestMapping(value = "/getPublicationListByCondition", method = {RequestMethod.POST})
    @ApiOperation(value = "根据doi或文献标题匹配文献")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "doi", value = "doi", required = true),
            @ApiImplicitParam(paramType = "query", name = "publicationTitle", value = "文献标题", required = true),
    })
    public ResultModelApi getPublicationListByCondition(String doi, String publicationTitle) throws Exception {

        ResultModelApi resultModelApi = new ResultModelApi();
//        if (StringUtils.isEmpty(doi) && StringUtils.isEmpty(publicationTitle)) {
//            resultModelApi.setRetCode("-1");
//            resultModelApi.setRetMsg("上传文件内容不符合上传规则，请重新上传！");
//            return resultModelApi;
//        }

        PublicationModel publicationModel = new PublicationModel();
        List<PublicationModel> publicationModels = new ArrayList<>();
        if (!StringUtils.isEmpty(doi)) {
            publicationModels = publicationService.selPublicationModeListlByDoi(doi);
            publicationModel.setMatchFlg("doi");
        }

        if (publicationModels.size() == 0 && !StringUtils.isEmpty(publicationTitle)){
            publicationModels = publicationService.selPublicationModeListlBypublicationTitle(publicationTitle);
            publicationModel.setMatchFlg("title");
        }
        List<Long> publicationIds = new ArrayList<>();
        for (PublicationModel publicationModelNew: publicationModels) {
            publicationIds.add(publicationModelNew.getPublicationId());
        }

        if (publicationIds.size() > 0) {
            publicationModels = publicationService.getPublicationDetailList(publicationIds);
        }
        publicationModel.setPublicationModelList(publicationModels);

        resultModelApi.setData(publicationModel);
        return resultModelApi;
    }

    /**
     * 获取文献类型
     * @return
     */
    @RequestMapping(value = "/getDocTypeListInfoByGroupCd", method = {RequestMethod.POST})
    @ApiOperation(value = "文献类型")
    public ResultModelApi getDocTypeListInfoByGroupCd() {
        ResultModelApi resultModelApi = new ResultModelApi();
        resultModelApi.setData(codeService.getDocTypeListInfoByGroupCd("doc_type"));
        return resultModelApi;
    }

    /**
     * 获取期刊名称自动补全List
     * @return
     */
    @RequestMapping(value = "/getJournalList", method = {RequestMethod.POST})
    @ApiOperation(value = "获取期刊名称自动补全List")
    public ResultModelApi getJournalList(String journalName) {
        ResultModelApi resultModelApi = new ResultModelApi();
        List<Journal> journalList = null;
        if (StringUtils.isEmpty(journalName)) {
            journalList = new ArrayList<>();
        } else {
            journalList = journalService.getJournalList(journalName);
        }

        resultModelApi.setData(journalList);
        return resultModelApi;
    }

    /**
     * 获取会议系列名称自动补全List
     * @return
     */
    @RequestMapping(value = "/getConferenceSerieList", method = {RequestMethod.POST})
    @ApiOperation(value = "获取期刊名称自动补全List")
    public ResultModelApi getConferenceSerieList(String conferenceSerieName) {
        ResultModelApi resultModelApi = new ResultModelApi();
        List<ConferenceSerie> conferenceSerieList = null;
        if (StringUtils.isEmpty(conferenceSerieName)) {
            conferenceSerieList = new ArrayList<>();
        } else {
            conferenceSerieList = conferenceSerieService.getConferenceSerieList(conferenceSerieName);
        }

        resultModelApi.setData(conferenceSerieList);
        return resultModelApi;
    }

    /**
     * 获取会议实例名称自动补全List
     * @return
     */
    @RequestMapping(value = "/getConferenceInstanceList", method = {RequestMethod.POST})
    @ApiOperation(value = "获取会议实例名称自动补全List")
    public ResultModelApi getConferenceInstanceList(String conferenceInstanceName) {
        ResultModelApi resultModelApi = new ResultModelApi();
        List<ConferenceInstance> conferenceInstanceList = null;
        if (StringUtils.isEmpty(conferenceInstanceName)) {
            conferenceInstanceList = new ArrayList<>();
        } else {
            conferenceInstanceList = conferenceInstanceService.getConferenceInstanceList(conferenceInstanceName);
        }

        resultModelApi.setData(conferenceInstanceList);
        return resultModelApi;
    }


    /**
     * 删除临时文件
     * @return
     */
    @RequestMapping(value = "/delTempFile", method = {RequestMethod.POST})
    @ApiOperation(value = "删除临时文件")
    public ResultModelApi delTempFile() throws IOException {
        ResultModelApi resultModelApi = new ResultModelApi();
        //创建Properties对象
        Properties prop = new Properties();
        //读取classPath中的properties文件
        prop.load(UserAchievementsController.class.getClassLoader().getResourceAsStream("application.properties"));
        // 文件服务器总路径
        String filePath = prop.getProperty("keensight.file.server.path");
        // 正式文件上传路径
        String fileFormalPath = prop.getProperty("pdf.upload.path");
        // 临时文件上传路径
        String fileTempPath = prop.getProperty("pdf.temp.path");
        UserModel loginUserModel = getLoginInfo();
        Long userId = Long.valueOf(loginUserModel.getUserId());
        Boolean result = fileHelper.deleteGeneralFile( filePath + fileFormalPath + fileTempPath + File.separator + userId + File.separator);

        resultModelApi.setData(result);
        return resultModelApi;
    }
}
