package com.keensight.web.controller;

import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.keensight.web.db.entity.UserTranslationTool;
import com.keensight.web.service.UserTranslationToolService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * 翻译工具控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2021/12/09] 新建
 */

@Api(value = "翻译工具控")
@RestController
@RequestMapping("/userTranslationTool")
public class UserTranslationToolController {
    @Autowired
    private UserTranslationToolService userTranslationToolService;

    /**
     * 获取翻译工具信息
     * @return
     */
    @RequestMapping(value = "/getUserTranslationToolList", method = {RequestMethod.POST})
    @ApiOperation(value = "获取翻译工具信息")
    public ResultModelApi getUserTranslationToolList() {

        ResultModelApi resultModelApi = new ResultModelApi();

        // 获取翻译工具信息
        List<UserTranslationTool> userTranslationToolList = new ArrayList<UserTranslationTool>();
        userTranslationToolList = userTranslationToolService.getUserTranslationTool4List();
        resultModelApi.setData(userTranslationToolList);

        return resultModelApi;
    }
}
