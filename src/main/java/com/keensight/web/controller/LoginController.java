package com.keensight.web.controller;


import cn.newtouch.dl.solr.util.StringUtil;
import cn.newtouch.dl.turbocloud.constants.ResultCode;
import cn.newtouch.dl.turbocloud.controller.BaseController;
import cn.newtouch.dl.turbocloud.helper.EncryptHelper;
import cn.newtouch.dl.turbocloud.helper.FileHelper;
import cn.newtouch.dl.turbocloud.jwt.JwtTokenHelper;
import cn.newtouch.dl.turbocloud.jwt.JwtUserAuthService;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import cn.newtouch.dl.turbocloud.model.UserModel;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.keensight.web.db.entity.User;
import com.keensight.web.db.entity.UserAuth;
import com.keensight.web.db.entity.UserInfo;
import com.keensight.web.db.entity.UserVerificationCode;
import com.keensight.web.model.UserAuthModel;
import com.keensight.web.model.UserInfoModel;
import com.keensight.web.service.UserAuthService;
import com.keensight.web.service.UserInfoService;
import com.keensight.web.service.UserService;
import com.keensight.web.service.UserVerificationCodeService;
import com.keensight.web.utils.AwsS3Utils;
import com.keensight.web.utils.DateUtil;
import com.keensight.web.utils.SendSms;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@Api
@RequestMapping("/login")
public class LoginController extends BaseController {

    @Resource
    private UserAuthService userAuthService;

    @Resource
    private JwtUserAuthService jwtUserAuthService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private UserVerificationCodeService userVerificationCodeService;

    @Autowired
    private FileHelper fileHelper;

    @Resource
    private JwtTokenHelper jwtTokenHelper;

    @RequestMapping(value = "/login", method = {RequestMethod.POST})
    @ApiOperation(value = "登录")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "password", value = "用户密码", required = true),
            @ApiImplicitParam(paramType = "query", name = "mobile", value = "手机号", required = true),
            @ApiImplicitParam(paramType = "query", name = "captcha", value = "验证码", required = true),
            @ApiImplicitParam(paramType = "query", name = "loginFlg", value = "登录Flg", required = true),
            @ApiImplicitParam(paramType = "query", name = "requestId", value = "请求Id", required = true),
            @ApiImplicitParam(paramType = "query", name = "openId", value = "授权用户唯一标识", required = false),
            @ApiImplicitParam(paramType = "query", name = "accessToken", value = "微信Token", required = false),
    })
    public ResultModelApi login(String password, String captcha, String mobile, String loginFlg, String requestId, String openId, String accessToken) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        EncryptHelper encryptHelper = new EncryptHelper();
        if ("phone".equals(loginFlg) || "wechat".equals(loginFlg)) {
            UserVerificationCode userVerificationCode = userVerificationCodeService.getUserVerificationCodeInfo(requestId, mobile);
            if (!ObjectUtils.isEmpty(userVerificationCode)) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date now = new Date();
                Date nowTime = sdf.parse(sdf.format(now));
                Date expirationTime = sdf.parse(sdf.format(userVerificationCode.getExpirationTime()));
                long nowTimeLong = nowTime.getTime();
                long expirationLong = expirationTime.getTime();
                if (nowTimeLong < expirationLong) {
                    if(captcha.equals(userVerificationCode.getVerificationCode())) {
                        UserAuth userAuth = userAuthService.login("phone", mobile, null, null);
                        if (userAuth != null) {
                            UserModel userModel = new UserModel();
                            userModel.setUserId(userAuth.getUserId() + "");

                            if ("wechat".equals(loginFlg)) {
                                openId = encryptHelper.decrypt(openId);
                                // 用户验证表用户Id更新
                                userAuthService.updUserIdByOpenId(userAuth.getUserId(), openId);
                            }

                            resultModelApi = jwtUserAuthService.successfulAuthentication(userModel);

                            com.keensight.web.model.UserModel userModel1 = new com.keensight.web.model.UserModel();
                            User user = userService.selectByPrimaryKey(userAuth.getUserId());
                            UserInfo userInfo = userInfoService.getUserInfo(userAuth.getUserId());
                            UserInfoModel userInfoModel = new UserInfoModel();
                            if(userInfo != null){
                                BeanUtils.copyProperties(userInfo, userInfoModel);
                            }
                            BeanUtils.copyProperties(user, userModel1);
                            if (StringUtil.isNotEmpty(user.getProfilePhotoPath())) {
                                String profilePhoto = AwsS3Utils.getS3FileUrl(user.getProfilePhotoPath());
                                userModel1.setProfilePhotoPath(profilePhoto);
                            }
                            if (StringUtil.isNotEmpty(userInfoModel.getProfilePath())) {
                                String profilePhoto = AwsS3Utils.getS3FileUrl(userInfoModel.getProfilePath());
                                userInfoModel.setProfilePath(profilePhoto);
                            }
                            userModel1.setUserInfo(userInfoModel);
                            userModel1.setUserInfo(userInfoModel);
                            resultModelApi.setData(userModel1);

                            // 更新当前登录时间和最后一次登录时间
                            userService.updateLoginAt(userAuth.getUserId());
                            // 登陆成功后删除验证码
                            int insCount = userVerificationCodeService.delVerificationCode(requestId, mobile);
                            if (insCount == 0) {
                                resultModelApi.setRetCode("-1");
                                resultModelApi.setRetMsg("登录失败！");
                            }
                        } else {
                            Long longUserId = userService.getMaxUserId() + 1;
                            String userName = "用户" + longUserId.toString().substring(longUserId.toString().length() - 4);
                            // 将数据插入作者表(t_m_user)中
                            int insCount = userService.insertRegist(longUserId, userName, userName);
                            // 将数据插入用户验证表(t_m_user_auth)中
                            if (insCount > 0) {
                                insCount = userAuthService.insertRegistEmailOrTel(longUserId, "phone", mobile, null);
                            }
                            String avatarStr = "";
                            if ("wechat".equals(loginFlg)) {
                                openId = encryptHelper.decrypt(openId);
                                // 用户验证表用户Id更新
                                insCount = userAuthService.updUserIdByOpenId(longUserId, openId);
                                JSONObject weChatUserInfo = getWeChatUserInfo(openId, accessToken);
                                // 微信头像
                                String headImgUrl = weChatUserInfo.getString("headimgurl");
                                // 微信头像保存到服务器
                                avatarStr = saveWeChatHeadImg(longUserId, headImgUrl);
                                // 微信昵称
                                userName = weChatUserInfo.getString("nickname");
                            } else {
                                //创建Properties对象
                                Properties prop = new Properties();
                                //读取classPath中的properties文件
                                prop.load(UserInfoController.class.getClassLoader().getResourceAsStream("application.properties"));
                                Random random = new Random();
                                String avatarRandomName = MessageFormat.format(prop.getProperty("default.avatar.name"), random.nextInt(4));
                                // 随机头像
                                avatarStr = prop.getProperty("default.avatar.path") + "/" + avatarRandomName;
                            }
                            if (insCount > 0) {
                                int count = userInfoService.count(longUserId);
                                if (count == 0) {
                                    userInfoService.insert(longUserId, avatarStr, userName, "", "0", "", "", "1990-01-01");
                                }
                            }
                            if (insCount > 0) {
                                UserModel userModel = new UserModel();
                                userModel.setUserId(longUserId + "");
                                resultModelApi = jwtUserAuthService.successfulAuthentication(userModel);

                                com.keensight.web.model.UserModel userModel1 = new com.keensight.web.model.UserModel();
                                User user = userService.selectByPrimaryKey(longUserId);
                                UserInfo userInfo = userInfoService.getUserInfo(longUserId);
                                UserInfoModel userInfoModel = new UserInfoModel();
                                if(userInfo != null){
                                    BeanUtils.copyProperties(userInfo, userInfoModel);
                                }
                                BeanUtils.copyProperties(user, userModel1);
                                if (StringUtil.isNotEmpty(user.getProfilePhotoPath())) {
                                    String profilePhoto = AwsS3Utils.getS3FileUrl(user.getProfilePhotoPath());
                                    userModel1.setProfilePhotoPath(profilePhoto);
                                }
                                if (StringUtil.isNotEmpty(userInfoModel.getProfilePath())) {
                                    String profilePhoto = AwsS3Utils.getS3FileUrl(userInfoModel.getProfilePath());
                                    userInfoModel.setProfilePath(profilePhoto);
                                }
                                userModel1.setUserInfo(userInfoModel);
                                userModel1.setUserInfo(userInfoModel);
                                resultModelApi.setData(userModel1);

                                // 登陆成功后删除验证码
                                insCount = userVerificationCodeService.delVerificationCode(requestId, mobile);
                                if (insCount == 0) {
                                    resultModelApi.setRetCode("-1");
                                    resultModelApi.setRetMsg("登录失败！");
                                }
                            } else {
                                resultModelApi.setRetCode("-1");
                                resultModelApi.setRetMsg("登录失败！");
                            }
                        }


                    } else {
                        resultModelApi.setRetCode(ResultCode.Auth_Error_Captcha.code());
                        resultModelApi.setRetMsg("登录失败， 验证码输入错误！");
                    }
                } else {
                    resultModelApi.setRetCode(ResultCode.Auth_Error_Captcha_ExpirationTime.code());
                    resultModelApi.setRetMsg("验证码过期，请重新获取！");
                }
            } else {
                resultModelApi.setRetCode(ResultCode.Auth_Error_Captcha.code());
                resultModelApi.setRetMsg("登录失败，验证码输入错误！");
            }
        } else if ("password".equals(loginFlg)) {
            UserAuth userAuth = userAuthService.login("phone", mobile, null, null);
            if (userAuth != null) {
                if (userAuth.getCredential() != null) {
                    String enPassword = encryptHelper.encrypt(password);
                    if (userAuth.getCredential().equals(enPassword)) {
                        UserModel userModel = new UserModel();
                        userModel.setUserId(userAuth.getUserId() + "");
                        resultModelApi = jwtUserAuthService.successfulAuthentication(userModel);

                        com.keensight.web.model.UserModel userModel1 = new com.keensight.web.model.UserModel();
                        User user = userService.selectByPrimaryKey(userAuth.getUserId());
                        UserInfo userInfo = userInfoService.getUserInfo(userAuth.getUserId());
                        UserInfoModel userInfoModel = new UserInfoModel();
                        if(userInfo != null){
                            BeanUtils.copyProperties(userInfo, userInfoModel);
                        }
                        BeanUtils.copyProperties(user, userModel1);
                        if (StringUtil.isNotEmpty(user.getProfilePhotoPath())) {
                            String profilePhoto = AwsS3Utils.getS3FileUrl(user.getProfilePhotoPath());
                            userModel1.setProfilePhotoPath(profilePhoto);
                        }
                        if (StringUtil.isNotEmpty(userInfoModel.getProfilePath())) {
                            String profilePhoto = AwsS3Utils.getS3FileUrl(userInfoModel.getProfilePath());
                            userInfoModel.setProfilePath(profilePhoto);
                        }
                        userModel1.setUserInfo(userInfoModel);
                        resultModelApi.setData(userModel1);

                        // 更新当前登录时间和最后一次登录时间
                        userService.updateLoginAt(userAuth.getUserId());
                    } else {
                        resultModelApi.setRetCode(ResultCode.Auth_Error_Password_Error.code());
                        resultModelApi.setRetMsg("密码错误！");
                    }
                } else {
                    resultModelApi.setRetCode(ResultCode.Auth_Error__Phone_No_Password.code());
                    resultModelApi.setRetMsg("该号码未曾设置密码，请选择“手机号登录”");
                }
            } else {
                resultModelApi.setRetCode(ResultCode.Auth_Error_Phone_No_Rigist.code());
                resultModelApi.setRetMsg("该号码未注册，请选择“手机号登录”快速注册");
            }
        } else if ("wechatLogin".equals(loginFlg)){
            openId = encryptHelper.decrypt(openId);
            UserAuth userAuth = userAuthService.login("wechat", openId, null, null);
            if (userAuth != null) {
                UserModel userModel = new UserModel();
                userModel.setUserId(userAuth.getUserId() + "");

                resultModelApi = jwtUserAuthService.successfulAuthentication(userModel);

                com.keensight.web.model.UserModel userModel1 = new com.keensight.web.model.UserModel();
                User user = userService.selectByPrimaryKey(userAuth.getUserId());
                UserInfo userInfo = userInfoService.getUserInfo(userAuth.getUserId());
                UserInfoModel userInfoModel = new UserInfoModel();
                if(userInfo != null){
                    BeanUtils.copyProperties(userInfo, userInfoModel);
                }
                BeanUtils.copyProperties(user, userModel1);
                userModel1.setUserInfo(userInfoModel);
                resultModelApi.setData(userModel1);

                // 更新当前登录时间和最后一次登录时间
                userService.updateLoginAt(userAuth.getUserId());
            }
        }
        return resultModelApi;
    }

    @RequestMapping(value = "/refreshToken", method = {RequestMethod.POST})
    @ApiOperation(value = "token刷新")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "token", value = "token", required = true)
    })
    public ResultModelApi refreshToken(String token) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        String refreshToken = jwtTokenHelper.refreshToken(token, true);

        resultModelApi.setAccessToken(token);
        resultModelApi.setRefreshToken(refreshToken);
        return resultModelApi;
    }

    @RequestMapping(value = "/verification", method = {RequestMethod.POST})
    @ApiOperation(value = "验证码发送")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "mobile", value = "mobile", required = true),
            @ApiImplicitParam(paramType = "query", name = "type", value = "type", required = true)
    })
    public ResultModelApi verification(String mobile, String type) throws Exception {

        ResultModelApi resultModelApi = new ResultModelApi();
        // 清除无效验证码
        int insCount = userVerificationCodeService.delVerificationCode(null, mobile);
        Map<String, String> resultMap = SendSms.sendSms(mobile, type);
        // 请求Id
        String requestId = resultMap.get("requestId");
        // 验证码
        String verificationCode = resultMap.get("verificationCode");
        // 过期时间
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        Date expirationTime = new Date(now.getTime() + 300000);
        String result = userVerificationCodeService.insUserVerificationCode(requestId, verificationCode, expirationTime, mobile);
        resultModelApi.setData(result);

        return resultModelApi;
    }

    @RequestMapping(value = "/resetPassword", method = {RequestMethod.POST})
    @ApiOperation(value = "密码重置/设置密码")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "password", value = "用户密码", required = true),
            @ApiImplicitParam(paramType = "query", name = "mobile", value = "手机号", required = true),
            @ApiImplicitParam(paramType = "query", name = "captcha", value = "验证码", required = true),
            @ApiImplicitParam(paramType = "query", name = "requestId", value = "请求Id", required = true)
    })
    public ResultModelApi resetPassword(String mail, String password, String captcha, String mobile, String loginFlg, String requestId) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        EncryptHelper encryptHelper = new EncryptHelper();
        UserVerificationCode userVerificationCode = userVerificationCodeService.getUserVerificationCodeInfo(requestId, mobile);
        if (!ObjectUtils.isEmpty(userVerificationCode)) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date now = new Date();
            Date nowTime = sdf.parse(sdf.format(now));
            Date expirationTime = sdf.parse(sdf.format(userVerificationCode.getExpirationTime()));
            long nowTimeLong = nowTime.getTime();
            long expirationLong = expirationTime.getTime();
            if (nowTimeLong < expirationLong) {
                if(captcha.equals(userVerificationCode.getVerificationCode())) {
                    UserAuth userAuth = userAuthService.login("phone", mobile, null, null);
                    if (userAuth != null) {
                        String enPassword = encryptHelper.encrypt(password);
                        int resCount = userAuthService.updUserPassword(userAuth.getUserId(), enPassword);
                        if (resCount > 0) {
                            resultModelApi.setRetCode("0");
                            if (StringUtils.isEmpty(userAuth.getCredential())) {
                                resultModelApi.setRetMsg("密码设置成功");
                            } else {
                                resultModelApi.setRetMsg("密码重置成功");
                            }
                        } else {
                            resultModelApi.setRetCode("-1");
                            resultModelApi.setRetMsg("密码重置失败");
                        }
                    } else {
                        resultModelApi.setRetCode(ResultCode.Auth_Error_Phone_No_Rigist.code());
                        resultModelApi.setRetMsg("该号码未注册，请选择“手机号登录”快速注册");
                    }
                } else {
                    resultModelApi.setRetCode(ResultCode.Auth_Error_Captcha.code());
                    resultModelApi.setRetMsg("登录失败， 验证码输入错误！");
                }
            } else {
                resultModelApi.setRetCode(ResultCode.Auth_Error_Captcha_ExpirationTime.code());
                resultModelApi.setRetMsg("验证码过期，请重新获取！");
            }
        } else {
            resultModelApi.setRetCode(ResultCode.Auth_Error_Captcha.code());
            resultModelApi.setRetMsg("登录失败，验证码输入错误！");
        }

        return resultModelApi;
    }

    @RequestMapping(value = "/selUserMessage", method = {RequestMethod.POST})
    @ApiOperation(value = "查询用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "mobile", value = "手机号", required = true),
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户ID", required = true),
    })
    public ResultModelApi selUserMessage(String mobile, Long userId) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        UserAuthModel userAuthModel = new UserAuthModel();
        UserAuth userAuth = userAuthService.login("phone", mobile, null, userId);
        if (userAuth != null) {
            userAuthModel.setMobileExistenceFlg("1");
            if (StringUtils.isEmpty(userAuth.getCredential())) {
                userAuthModel.setPasswordExistenceFlg("0");
            } else {
                userAuthModel.setPasswordExistenceFlg("1");
            }
            userAuthModel.setIdentifier(userAuth.getIdentifier());
        } else {
            userAuthModel.setMobileExistenceFlg("0");
        }
        resultModelApi.setData(userAuthModel);
        return resultModelApi;
    }

    @RequestMapping(value = "/checkResetPasswordMobole", method = {RequestMethod.POST})
    @ApiOperation(value = "查询用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "mobile", value = "输入手机号", required = true),
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户ID", required = true),
    })
    public ResultModelApi checkResetPasswordMobole(String mobile, Long userId) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        UserAuth userAuth = userAuthService.login("phone", null, null, userId);

        if (userAuth != null) {
            if (userAuth.getIdentifier().equals(mobile)) {
                resultModelApi.setData(true);
            } else {
                resultModelApi.setData(false);
            }
        }
        return resultModelApi;
    }

    @RequestMapping(value = "/checkWechat", method = {RequestMethod.POST})
    @ApiOperation(value = "查询手机号是否绑定微信")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "mobile", value = "输入的手机号", required = true)
    })
    public ResultModelApi checkWechat(String mobile) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        UserAuth userAuth = userAuthService.login("phone", mobile, null, null);

        if (userAuth != null) {
            int  userAuthWeChatCount = userAuthService.selectCountByUserId(userAuth.getUserId(), "wechat");
            if (userAuthWeChatCount > 0) {
                resultModelApi.setData(true);
            } else {
                resultModelApi.setData(false);
            }
        }
        return resultModelApi;
    }

    @RequestMapping(value = "/wechatLoginCheck", method = {RequestMethod.POST})
    @ApiOperation(value = "微信扫码登陆校验")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "code", value = "授权码", required = true)
    })
    public ResultModelApi wechatLoginCheck(String code) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("firstLoginFlg", false);
        String openId = "";
        //创建Properties对象
        Properties prop = new Properties();
        //读取classPath中的properties文件
        prop.load(LoginController.class.getClassLoader().getResourceAsStream("application.properties"));
        String wechatApiUrl = MessageFormat.format(prop.getProperty("wechat.api.openid"), prop.getProperty("wechat.api.appid"), prop.getProperty("wechat.api.secret"), code);
        // 通过code获取access_token
        HttpGet request = new HttpGet(wechatApiUrl);
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
            CloseableHttpResponse response = httpClient.execute(request)) {
            // 请求成功
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    String resultStr = EntityUtils.toString(entity);
                    JSONObject resultObj = JSON.parseObject(resultStr);
                    if (StringUtils.isEmpty(resultObj.getString("errcode"))) {
                        openId = resultObj.getString("openid");
                        String accessToken = resultObj.getString("access_token");
                        UserAuth userAuthInfo = userAuthService.getUserAuthInfoByOpenId(openId);
                        EncryptHelper encryptHelper = new EncryptHelper();
                        resultMap.put("openId", encryptHelper.encrypt(openId));
                        // 首次登陆
                        if (userAuthInfo == null) {
                            Long longUserId = userService.getMaxUserId() + 1;
                            // 将数据插入用户验证表(t_m_user_auth)中
                            int insCount = userAuthService.insertRegistEmailOrTel(longUserId, "wechat", openId, null);
                            if (insCount > 0) {
                                resultMap.put("firstLoginFlg", true);
                                resultMap.put("accessToken", accessToken);
                            }
                        } else {
                            List<UserAuth> userAuth = userAuthService.getUserAuthInfoByUserId(userAuthInfo.getUserId());
                            if(userAuth == null || userAuth.size() == 0){
                                resultMap.put("firstLoginFlg", true);
                                resultMap.put("accessToken", accessToken);
                            }
                        }
                        resultModelApi.setData(resultMap);
                    } else {
                        resultModelApi.setRetCode("-1");
                        resultModelApi.setRetMsg("二维码已失效，请重新扫码登陆！");
                    }
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return resultModelApi;
    }

    /**
     * 根据openId和accessToken获取微信用户信息
     *
     * @param openId 授权用户唯一标识
     * @param accessToken 微信Token
     * @return 微信用户信息
     */
    private JSONObject getWeChatUserInfo(String openId, String accessToken) throws IOException {
        JSONObject resultObj = new JSONObject();
        resultObj.put("error", false);
        //创建Properties对象
        Properties prop = new Properties();
        //读取classPath中的properties文件
        prop.load(LoginController.class.getClassLoader().getResourceAsStream("application.properties"));
        String wechatUserInfoApiUrl = MessageFormat.format(prop.getProperty("wechat.api.userinfo"), accessToken, openId);
        // 通过access_token和openId获取用户信息
        HttpGet request = new HttpGet(wechatUserInfoApiUrl);
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(request)) {
            // 请求成功
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    String retJsonStr = EntityUtils.toString(entity);
                    JSONObject retJsonObj = JSON.parseObject(retJsonStr);
                    if (StringUtils.isEmpty(resultObj.getString("errcode"))) {
                        // 昵称
                        resultObj.put("nickname", new String(retJsonObj.getString("nickname").getBytes("ISO-8859-1"), "UTF-8"));
                        // 头像Url
                        resultObj.put("headimgurl", URLDecoder.decode(retJsonObj.getString("headimgurl"), "utf-8"));
                    }
                }
            }
        } catch (Exception e) {
            return resultObj;
        }
        return resultObj;
    }

    /**
     * 将微信头像保存到文件服务器
     *
     * @param userId 用户Id
     * @param headImgUrl 微信头像Url
     * @return 文件服务器头像Url
     */
    private String saveWeChatHeadImg(Long userId, String headImgUrl) throws IOException {
        // 获取文件存储路径
        //创建Properties对象
        Properties prop = new Properties();
        //读取classPath中的properties文件
        prop.load(UserInfoController.class.getClassLoader().getResourceAsStream("application.properties"));
        // 文件服务器总路径
        String filePath = prop.getProperty("keensight.file.server.path");
        // 用户头像文件上传路径
        String avatorFilePath = prop.getProperty("userInfo.avatar.path");
        // 获取当前时间时间戳
        String timeStamp = DateUtil.getSysDate("yyyyMMddHHmmssSSS");
        // 文件名
        String fileName = timeStamp + ".jpg";
        // 文件服务器头像Url
        String avatarStr = null;
        // 将头像保存到服务器指定路径
        // 从微信获取用户头像
        HttpGet request = new HttpGet(headImgUrl);
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(request)) {
            // 请求成功
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                HttpEntity entity = response.getEntity();
                // 判断服务器是否有该文件夹
                if(fileHelper.mkDir(filePath + avatorFilePath + File.separator + userId + File.separator)) {
                    fileHelper.saveFile(entity.getContent(), filePath + avatorFilePath + File.separator + userId + File.separator, fileName);
                    avatarStr = avatorFilePath + File.separator + userId + File.separator + fileName;
                }
            }
        } catch (Exception e) {
            return avatarStr;
        }
        return avatarStr;
    }
}

