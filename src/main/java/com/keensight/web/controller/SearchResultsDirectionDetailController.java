package com.keensight.web.controller;

import cn.newtouch.dl.turbocloud.constants.ResultCode;
import cn.newtouch.dl.turbocloud.controller.BaseController;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageInfo;
import com.keensight.solr.service.SolrPublicationSearchService;
import com.keensight.web.db.entity.Field;
import com.keensight.web.db.entity.FieldTopEntity;
import com.keensight.web.db.entity.StatsFieldYearly;
import com.keensight.web.model.PublicationModel;
import com.keensight.web.model.TopItemListModel;
import com.keensight.web.service.ConferenceSerieService;
import com.keensight.web.service.FieldService;
import com.keensight.web.service.FieldTopEntityService;
import com.keensight.web.service.PublicationFieldService;
import com.keensight.web.service.PublicationService;
import com.keensight.web.service.StatsFieldYearlyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * 研究结果详细控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/16] 新建
 */
@Api(value = "研究结果详细")
@RestController
@RequestMapping("/searchResultsDirectionDetail")
public class SearchResultsDirectionDetailController extends BaseController {

    @Autowired
    private FieldService fieldService;
    @Autowired
    private StatsFieldYearlyService statsFieldYearlyService;
    @Autowired
    private FieldTopEntityService fieldTopEntityService;
    @Autowired
    private PublicationFieldService publicationFieldService;
    @Autowired
    private PublicationService publicationService;
    @Autowired
    private SolrPublicationSearchService solrPublicationSearchService;
    @Autowired
    private ConferenceSerieService conferenceSerieService;

    /**
     * 根据研究领域ID获取研究领域和相关研究领域信息
     *
     * @param fieldId 研究领域ID
     * @param userId  用户ID
     * @return
     */
    @RequestMapping(value = "/getDirectionDetailInfo", method = {RequestMethod.POST})
    @ApiOperation(value = "根据研究领域ID获取研究领域和相关研究领域信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "fieldId", value = "研究领域ID", required = true),
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户ID")
    })
    public ResultModelApi getDirectionDetailInfo(String fieldId, String userId) {
        ResultModelApi resultModelApi = new ResultModelApi();
        if (StringUtils.isEmpty(fieldId) || !StringUtils.isNumber(fieldId)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }

        Long searchFieldId = Long.valueOf(fieldId);
        Long searchUserId = null;
        if (!StringUtils.isEmpty(userId) && StringUtils.isNumber(userId)) {
            searchUserId = Long.valueOf(userId);
        }
        Field field = fieldService.getFieldAndRelatedFieldListByFieldId(searchFieldId, searchUserId);

        resultModelApi.setRetCode(ResultCode.Success.code());
        resultModelApi.setData(field);
        return resultModelApi;
    }

    /**
     * 根据研究领域ID获取领域年度文献统计信息
     *
     * @param fieldId 研究领域ID
     * @return
     */
    @RequestMapping(value = "/getStatsFieldYearlyInfo", method = {RequestMethod.POST})
    @ApiOperation(value = "据研究领域ID获取领域年度文献统计信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "fieldId", value = "研究领域ID", required = true)
    })
    public ResultModelApi getStatsFieldYearlyInfo(String fieldId) {
        ResultModelApi resultModelApi = new ResultModelApi();
        if (StringUtils.isEmpty(fieldId) || !StringUtils.isNumber(fieldId)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }

        Long searchFieldId = Long.valueOf(fieldId);
        List<StatsFieldYearly> resultList = statsFieldYearlyService.getStatsFieldYearlyInfo(searchFieldId);
        if (resultList == null) {
            resultList = new ArrayList<>();
        }
        resultModelApi.setRetCode(ResultCode.Success.code());
        resultModelApi.setData(resultList);
        return resultModelApi;
    }

    /**
     * 根据实体类型获取指定排名的领域顶尖实体集合
     *
     * @param fieldId    研究领域ID
     * @param entityType 实体类型
     * @param rows       检索件数
     * @return 排名集合
     */
    @RequestMapping(value = "/getFieldTopEntityList", method = {RequestMethod.POST})
    @ApiOperation(value = "根据实体类型获取指定排名的领域顶尖实体集合")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "fieldId", value = "研究领域ID", required = true),
            @ApiImplicitParam(paramType = "query", name = "entityType", value = "实体类型", required = true),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "检索件数", required = true)
    })
    public ResultModelApi getFieldTopEntityList(String fieldId, String entityType, Integer rows) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();

        if (StringUtils.isEmpty(fieldId) || !StringUtils.isNumber(fieldId) || StringUtils.isEmpty(entityType)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }

        List<TopItemListModel> topItemListModelList = new ArrayList<>();
        Long searchFieldId = Long.valueOf(fieldId);

        List<FieldTopEntity> resultList = fieldTopEntityService.getTopEntityByEntityType(searchFieldId, entityType, rows);
        if (resultList != null && resultList.size() > 0) {
            // 设置Top排行组件Model集合
            for (FieldTopEntity entity : resultList) {
                if (entity != null && entity.getEntityId() != null) {
                    TopItemListModel topItemListModel = new TopItemListModel();
                    topItemListModel.setTopItemId(entity.getEntityId().toString());
                    topItemListModel.setTopItemName(entity.getEntityName());
                    topItemListModel.setScore(entity.getScore() != null ? entity.getScore().toString() : "0");

                    topItemListModelList.add(topItemListModel);
                }
            }
        }

        resultModelApi.setRetCode(ResultCode.Success.code());
        resultModelApi.setData(topItemListModelList);
        return resultModelApi;
    }

    /**
     * 领域内最重要的指定件数的论文取得
     *
     * @param fieldId
     * @param index
     * @param rows
     * @param publicationType
     * @return
     */
    @RequestMapping(value = "/getPublicationFieldList", method = {RequestMethod.POST})
    @ApiOperation(value = "领域内最重要的指定件数的论文取得")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "fieldId", value = "研究领域ID", required = true),
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户ID"),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数", required = true),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示件数", required = true),
            @ApiImplicitParam(paramType = "query", name = "publicationType", value = "论文件数类别", required = true)
    })
    public ResultModelApi getPublicationFieldList(String fieldId, String userId, Integer index, Integer rows, String publicationType) {
        ResultModelApi resultModelApi = new ResultModelApi();
        List<PublicationModel> detailList = new ArrayList<>();

        if (StringUtils.isEmpty(fieldId) || !StringUtils.isNumber(fieldId)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }

        Long searchFieldId = Long.valueOf(fieldId);
        Long searchUserId = null;
        if (!StringUtils.isEmpty(userId) && StringUtils.isNumber(userId)) {
            searchUserId = Long.valueOf(userId);
        }

        PageInfo<Long> pageInfo = fieldTopEntityService.getPublicationIdListByFieldId(searchFieldId, index, rows, publicationType);
        if (pageInfo != null && pageInfo.getList() != null && pageInfo.getList().size() > 0) {
            pageInfo.setPageNum(index);
            pageInfo.setPageSize(rows);
            // 调用共通检索获取文献详细信息一览数据
            detailList = publicationService.getPublicationDetailList(pageInfo.getList());
        }

        PageInfo<PublicationModel> resultPageInfo = new PageInfo<>();
        if (pageInfo != null) {
            BeanUtils.copyProperties(pageInfo, resultPageInfo);
            resultPageInfo.setList(detailList);
        }
        resultModelApi.setRetCode(ResultCode.Success.code());
        resultModelApi.setData(resultPageInfo);
        return resultModelApi;
    }
}
