package com.keensight.web.controller;

import cn.newtouch.dl.solr.model.SolrResultModel;
import cn.newtouch.dl.turbocloud.constants.ResultCode;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.keensight.solr.service.SolrPublicationSearchService;
import com.keensight.web.constants.enums.UploadFolder;
import com.keensight.web.db.entity.Publication;
import com.keensight.web.helper.FileServerHelper;
import com.keensight.web.model.PublicationModel;
import com.keensight.web.service.PublicationService;
import com.keensight.web.utils.AwsS3Utils;
import com.keensight.web.utils.DateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 管理全文访问权限控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/13] 新建
 */
@RestController
@Api(value = "管理全文访问权限")
@RequestMapping("/userManageAccess")
public class UserManageAccessController {

    @Autowired
    private PublicationService publicationService;
    @Autowired
    private SolrPublicationSearchService solrPublicationSearchService;
    @Autowired
    private FileServerHelper fileServerHelper;

    /**
     * 获取用户管理全文列表
     *
     * @param userId
     * @param index
     * @param rows
     * @param keyword
     * @param autocompleteKeyword
     * @param sortColumns
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getUserManageAccessList", method = {RequestMethod.POST})
    @ApiOperation(value = "获取用户管理全文列表")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数"),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示条数"),
            @ApiImplicitParam(paramType = "query", name = "sortColumns", value = "排序"),
            @ApiImplicitParam(paramType = "query", name = "keyword", value = "关键字"),
            @ApiImplicitParam(paramType = "query", name = "autocompleteKeyword", value = "自动补全关键词")
    })
    public ResultModelApi getUserManageAccessList(String userId, int index, int rows,
                                                  String keyword, String autocompleteKeyword,
                                                  String sortColumns, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();

        PageInfo<Publication> publicationPageInfo;
        List<Long> publicationIdList = new ArrayList<>();
        PageInfo<Long> pageInfo = null;
        List<PublicationModel> detailList = new ArrayList<>();
        List<Publication> publicationList;
        if (!StringUtils.isEmpty(keyword) && keyword.trim().length() > 0) {
            /*
             * 有关键词时，先从DB检索符合条件所有ID（无分页），
             * 然后从solr 根据这些ID集合以及keyword 进行检索（有分页）
             */
            publicationPageInfo = publicationService.getUserManageAccessList(Long.valueOf(userId), sortColumns, 1, 0);
            publicationList = publicationPageInfo.getList();
            if (publicationList != null) {
                for (Publication publication : publicationList) {
                    publicationIdList.add(publication.getPublicationId());
                }
            }
            // 根据所有ID以及keyword在solr中检索
            if (publicationIdList != null) {
                SolrResultModel solrResultModel = solrPublicationSearchService.getPublicationIdByKeyword(keyword, autocompleteKeyword, index, rows, publicationIdList);
                publicationIdList.clear();
                if (solrResultModel != null) {
                    List<Long> entry = solrResultModel.getCustomVal();
                    if (entry != null && entry.size() > 0) {
                        for (Long publication : entry) {
                            if (publication != null) {
                                publicationIdList.add(publication);
                            }
                        }
                    }
                    publicationPageInfo.setTotal(solrResultModel.getRows());
                    publicationPageInfo.setList(publicationPageInfo.getList());
                    publicationPageInfo.setPageNum(index);
                    publicationPageInfo.setPageSize(rows);
                    publicationPageInfo.setHasNextPage(solrResultModel.getRows() > 0 && solrResultModel.getRows() > index * rows);
                }
            }
        } else {
            // 没有关键词时，只从DB中检索符合条件的结果集（有分页）
            publicationPageInfo = publicationService.getUserManageAccessList(Long.valueOf(userId), sortColumns, index, rows);
            publicationList = publicationPageInfo.getList();
            if (publicationList != null) {
                for (Publication publication : publicationList) {
                    publicationIdList.add(publication.getPublicationId());
                }
            }
        }

        if (publicationIdList != null && publicationIdList.size() > 0) {
            Map<Long, Publication> map = new HashMap<Long, Publication>();
            for (Publication publication : publicationPageInfo.getList()) {
                Long publicationId = publication.getPublicationId();
                if (!map.containsKey(publicationId)) {
                    Publication p = new Publication();
                    p.setUploadUserName(publication.getUploadUserName());
                    p.setUploadCreatedAt(publication.getUploadCreatedAt());
                    p.setDocumentTypeName(publication.getDocumentTypeName());
                    p.setDocumentTypeNameEn(publication.getDocumentTypeNameEn());
                    p.setOriginalFileName(publication.getOriginalFileName());
                    p.setSize(publication.getSize());
                    p.setThumbnailPath(publication.getThumbnailPath());
                    map.put(publicationId, p);
                }
            }

            // 调用共通检索获取文献详细信息一览数据
            detailList = publicationService.getPublicationDetailList(publicationIdList);
            if (detailList != null && detailList.size() > 0) {
                // 数据处理
                for (PublicationModel model : detailList) {
                    if (model != null) {
                        // 收藏按钮显示Flg
                        model.setSaveBtnFlg("0");
                        // 推荐按钮显示Flg (0：都不显示，1：显示，2：显示取消)
                        model.setRecommendBtnFlg("0");
                        // 关注按钮显示Flg (0：都不显示，1：显示，2：显示取消)
                        model.setFollowingBtnFlg("0");
                        // 分享按钮显示Flg (0：都不显示，1：显示，2：显示取消)
                        model.setShareBtnFlg("0");
                        // 下载按钮显示Flg (0：都不显示，1：显示，2：索要全文)
                        model.setDownloadBtnFlg("1");
                        // 增加补充材料按钮显示Flg (0：不显示，1：显示)
                        model.setUploadOtherFileBtnFlg("0");
                        // 上传全文按钮显示Flg (0：都不显示，1：显示上传全文，2：有全文)
                        model.setUploadPdfBtnFlg("0");
                        // 上传文档名称
                        String[] fullTextPath = model.getFulltextPath().split("/");
                        model.setUploadFileName(fullTextPath[fullTextPath.length - 1]);
                        Long key = model.getPublicationId().longValue();
                        if (map.containsKey(key)) {
                            // 上传文档用户
                            model.setUploadUserName(map.get(key).getUploadUserName());
                            // 上传文档时间
                            model.setUploadTime(DateUtil.dateFormat(map.get(key).getUploadCreatedAt()));
                            // 上传文档类型
                            model.setDocumentTypeName(map.get(key).getDocumentTypeName());
                            // 上传文档类型 英文
                            model.setDocumentTypeNameEn(map.get(key).getDocumentTypeNameEn());
                            // 上传文件原始名
                            model.setOriginalFileName(map.get(key).getOriginalFileName());
                            // 上传文档大小
                            model.setSize(map.get(key).getSize());
                            // 缩略图路径 文件服务器url获取
                            if(cn.newtouch.dl.solr.util.StringUtil.isNotEmpty(map.get(key).getThumbnailPath())){
                                String profilePath_s3 = AwsS3Utils.getS3FileUrl(map.get(key).getThumbnailPath());
                                model.setThumbnailPath(profilePath_s3);
                            }
//                            model.setThumbnailPath(fileServerHelper.getFileServerUrl(map.get(key).getThumbnailPath(), UploadFolder.PDF));
                        }
                    }
                }
            }
        }

        PageInfo<PublicationModel> resultPageInfo = new PageInfo<>();
        if (publicationPageInfo.getList() != null) {
            BeanUtils.copyProperties(publicationPageInfo, resultPageInfo);
            resultPageInfo.setList(detailList);
        }

        resultModelApi.setRetCode(ResultCode.Success.code());
        resultModelApi.setData(resultPageInfo);
        return resultModelApi;
    }

    /**
     * 更新文献表的文档公开字段
     *
     * @param publicationId
     * @param publicDoc
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/updatePublicDoc", method = {RequestMethod.POST})
    @ApiOperation(value = "更新文献表的文档公开字段")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献ID", required = false),
            @ApiImplicitParam(paramType = "query", name = "publicDoc", value = "文档公开区分", required = true)
    })
    public ResultModelApi updatePublicDoc(String publicationId, Boolean publicDoc, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        if (StringUtil.isNotEmpty(publicationId)) {
            publicationService.updatePublicDoc(publicationId, publicDoc);
        }
        return resultModelApi;
    }
}
