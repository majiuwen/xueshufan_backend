package com.keensight.web.controller;

import cn.newtouch.dl.solr.model.SolrResultModel;
import cn.newtouch.dl.turbocloud.constants.ResultCode;
import cn.newtouch.dl.turbocloud.controller.BaseController;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageInfo;
import com.keensight.solr.service.SolrUserSearchService;
import com.keensight.web.constants.KeensightResultCode;
import com.keensight.web.db.entity.RelatedPublication;
import com.keensight.web.model.PublicationModel;
import com.keensight.web.model.UserModel;
import com.keensight.web.service.PublicationService;
import com.keensight.web.service.RelatedPublicationService;
import com.keensight.web.service.UserActionPublicationService;
import com.keensight.web.service.UserAuthorFollowingService;
import com.keensight.web.service.UserService;
import com.keensight.web.utils.PDFUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 文献全文控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/17] 新建
 */
@Api(value = "文献全文")
@RestController
@RequestMapping("/articlesFullText")
public class ArticlesFullTextController extends BaseController {

    @Autowired
    private PublicationService publicationService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserAuthorFollowingService userAuthorFollowingService;
    @Autowired
    private RelatedPublicationService relatedPublicationService;
    @Autowired
    private UserActionPublicationService userActionPublicationService;
    @Autowired
    private SolrUserSearchService solrUserSearchService;

    @Value("${keensight.file.server.path}")
    private String keensightFileServerPath;

    /**
     * 根据文献Id获取文献内容
     *
     * @param userId
     * @param publicationId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getPublicationInfoByPublicationId", method = {RequestMethod.POST})
    @ApiOperation(value = "根据文献Id获取文献内容")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = false),
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献id", required = true)
    })
    public ResultModelApi getSuggestedPublicationToRead(String userId, String publicationId) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();

        // 调用共同方法获取论文详细内容
        List<Long> publicationIdList = new ArrayList<>();
        publicationIdList.add(Long.valueOf(publicationId));
        List<PublicationModel> publicationModels = publicationService.getPublicationDetailList(publicationIdList);
        Map<String, List<PublicationModel>> listMap = new HashMap<>();
        listMap.put("list", publicationModels);
        resultModelApi.setData(listMap);

        return resultModelApi;
    }

    /**
     * 更新阅读次数
     *
     * @param userId
     * @param publicationId
     * @param authorIds
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/updUserAction", method = {RequestMethod.POST})
    @ApiOperation(value = "针对文献的用户操作 1:阅读")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = false),
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献id", required = true),
            @ApiImplicitParam(paramType = "query", name = "authorIds", value = "作者id", required = true)
    })
    public ResultModelApi updUserAction(String userId, String publicationId, String authorIds, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        Long uId = null;
        if (!StringUtils.isEmpty(userId) && StringUtils.isNumber(userId)) {
            uId = Long.valueOf(userId);
        }
        userActionPublicationService.updUserAction(uId, Long.valueOf(publicationId), authorIds, "1", true, false);
        return resultModelApi;
    }

    /**
     * 读取PDF文件
     *
     * @param fullTextPath
     * @param response
     */
    @RequestMapping(value = "/getSuggestedPDF", method = {RequestMethod.GET})
    @ApiOperation(value = "读取PDF文件")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "fullTextPath", value = "全文路径", required = true),
    })
    public void getSuggestedPDF(String fullTextPath, HttpServletResponse response) {
        PDFUtil.readPDFFile(new File(keensightFileServerPath.concat(fullTextPath.replaceAll("\\./", "../pdfs/"))), response);
    }

    /**
     * 根据文献ID获取共同作者
     *
     * @param userId
     * @param publicationId
     * @param index
     * @param rows
     * @param keyword
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getAuthorsByPublicationId", method = {RequestMethod.POST})
    @ApiOperation(value = "根据文献ID获取共同作者")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = false),
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献id", required = true),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数"),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示条数"),
            @ApiImplicitParam(paramType = "query", name = "keyword", value = "关键字"),
            @ApiImplicitParam(paramType = "query", name = "autocompleteKeyword", value = "自动补全关键词")
    })
    public ResultModelApi getAuthorsByPublicationId(String userId, String publicationId, int index, int rows, String keyword,
                                                    String autocompleteKeyword, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        PageInfo<Long> pageInfo;
        List<Long> userIdList = null;
        List<UserModel> userModelList = new ArrayList<>();
        List<UserModel> userModels = new ArrayList<>();
        Long uId = null;
        if (!StringUtils.isEmpty(userId)) {
            uId = Long.valueOf(userId);
        }

        if (!com.alibaba.druid.util.StringUtils.isEmpty(keyword) && keyword.trim().length() > 0) {
            /*
             * 有关键词时，先从DB检索符合条件所有ID（无分页），
             * 然后从solr 根据这些ID集合以及keyword 进行检索（有分页）
             */
            pageInfo = userService.getAuthorsByPublicationId(Long.valueOf(publicationId), 1, 0);
            if (pageInfo != null) {
                userIdList = pageInfo.getList();
            }
            // 根据所有ID以及keyword在solr中检索
            if (userIdList != null) {
                // 根据keyword在solr中检索
                SolrResultModel solrResultModel = solrUserSearchService.getUserIdByKeyword(keyword, autocompleteKeyword, index, rows, userIdList);
                userIdList.clear();
                if (solrResultModel != null) {
                    List<Long> entry = solrResultModel.getCustomVal();
                    if (entry != null && entry.size() > 0) {
                        for (Long user : entry) {
                            if (user != null) {
                                userIdList.add(user);
                            }
                        }
                        pageInfo.setTotal(solrResultModel.getRows());
                        pageInfo.setList(userIdList);
                        pageInfo.setPageNum(index);
                        pageInfo.setPageSize(rows);
                        pageInfo.setHasNextPage(solrResultModel.getRows() > 0 && solrResultModel.getRows() > index * rows);
                    }
                }
            }
        } else {
            /*
             * 没有关键词时，只从DB中检索符合条件的ID（有分页）
             */
            // 根据检索条件查询用户ID集合
            pageInfo = userService.getAuthorsByPublicationId(Long.valueOf(publicationId), index, rows);
            if (pageInfo != null) {
                userIdList = pageInfo.getList();
            }
        }

        if (userIdList != null && userIdList.size() > 0) {
            // 根据检索用户Id获取该用户关注的其他用户的详细信息
            userModelList = userService.getUserInfoList(uId, userIdList);
            // 检索结果重排序
            for (Long user : userIdList) {
                userModels.addAll(userModelList.stream().filter(userModel -> user.equals(userModel.getUserId())).collect(Collectors.toList()));
            }
            for (UserModel userModel : userModels) {
                userModel.setInviteBtnFlg("0");
            }
        }

        PageInfo<UserModel> resultPageInfo = new PageInfo<>();
        if (pageInfo != null) {
            BeanUtils.copyProperties(pageInfo, resultPageInfo);
            resultPageInfo.setList(userModels);
        }
        resultModelApi.setRetCode(ResultCode.Success.code());
        resultModelApi.setData(resultPageInfo);
        return resultModelApi;
    }

    /**
     * 点击「关注」/「取消关注」按钮
     *
     * @param userId
     * @param userIdFollowed
     * @param followingBtnFlg
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/updateDeletedByPrimaryKey", method = {RequestMethod.POST})
    @ApiOperation(value = "点击「关注」/「取消关注」按钮")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "userIdFollowed", value = "关注的作者的id", required = true),
            @ApiImplicitParam(paramType = "query", name = "followingBtnFlg", value = "关注按钮区分「1:关注」/「2:取消关注」", required = true)
    })
    public ResultModelApi updateDeletedByPrimaryKey(String userId, String userIdFollowed, String followingBtnFlg, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        if (userId.equals(userIdFollowed)) {
            resultModelApi.setRetCode(KeensightResultCode.System_90002.code());
            resultModelApi.setRetMsg("不能关注自己");
        } else {
            userAuthorFollowingService.updateDeletedByPrimaryKey(Long.valueOf(userId), Long.valueOf(userIdFollowed), followingBtnFlg);
        }
        return resultModelApi;
    }

    /**
     * 获取相关研究
     *
     * @param userId
     * @param publicationId
     * @param index
     * @param rows
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getRelatedPublication", method = {RequestMethod.POST})
    @ApiOperation(value = "获取相关研究")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献id", required = true),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数", required = true),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示条数", required = true)
    })
    public ResultModelApi getRelatedPublication(String userId, String publicationId, int index, int rows, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        Long uId = null;
        if (!StringUtils.isEmpty(userId)) {
            uId = Long.valueOf(userId);
        }
        // 获取相关研究Id
        PageInfo<RelatedPublication> relatedPublicationPageInfo = relatedPublicationService.getRelatedPublicationIds(Long.valueOf(publicationId), index, rows);
        List<RelatedPublication> relatedPublications = relatedPublicationPageInfo.getList();
        if (relatedPublications.size() != 0) {
            // 设置文献Id
            List<Long> publicationIds = new ArrayList<>();
            for (RelatedPublication relatedPublication : relatedPublications) {
                publicationIds.add(relatedPublication.getRelatedPublicationId());
            }
            // 获取论文详细内容
            List<PublicationModel> publicationModels = publicationService.getPublicationDetailList(publicationIds);
            PageInfo<PublicationModel> publicationModelPageInfo = new PageInfo<PublicationModel>(publicationModels);
            publicationModelPageInfo.setPageNum(relatedPublicationPageInfo.getPageNum());
            publicationModelPageInfo.setPageSize(relatedPublicationPageInfo.getPageSize());
            publicationModelPageInfo.setStartRow(relatedPublicationPageInfo.getStartRow());
            publicationModelPageInfo.setEndRow(relatedPublicationPageInfo.getEndRow());
            publicationModelPageInfo.setPages(relatedPublicationPageInfo.getPages());
            publicationModelPageInfo.setPrePage(relatedPublicationPageInfo.getPrePage());
            publicationModelPageInfo.setNextPage(relatedPublicationPageInfo.getNextPage());
            publicationModelPageInfo.setIsFirstPage(relatedPublicationPageInfo.isIsFirstPage());
            publicationModelPageInfo.setIsLastPage(relatedPublicationPageInfo.isIsLastPage());
            publicationModelPageInfo.setHasPreviousPage(relatedPublicationPageInfo.isHasPreviousPage());
            publicationModelPageInfo.setHasNextPage(relatedPublicationPageInfo.isHasNextPage());
            publicationModelPageInfo.setNavigatePages(relatedPublicationPageInfo.getNavigatePages());
            publicationModelPageInfo.setNavigatepageNums(relatedPublicationPageInfo.getNavigatepageNums());
            publicationModelPageInfo.setNavigateFirstPage(relatedPublicationPageInfo.getNavigateFirstPage());
            publicationModelPageInfo.setNavigateLastPage(relatedPublicationPageInfo.getNavigateLastPage());
            publicationModelPageInfo.setTotal(relatedPublicationPageInfo.getTotal());
            publicationModelPageInfo.setList(publicationModels);
            resultModelApi.setData(publicationModelPageInfo);
        }
        return resultModelApi;
    }

    /**
     * 根据文件链接地址下载文件
     *
     * @param userId
     * @param publicationId
     * @param authorIds
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/fileDownload", method = {RequestMethod.POST})
    @ApiOperation(value = "根据文件链接地址下载文件")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献id", required = true),
            @ApiImplicitParam(paramType = "query", name = "authorIds", value = "作者id", required = true),
    })
    public ResultModelApi fileDownload(String userId, String publicationId, String authorIds) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 数据库相关表更新
        try {
            userActionPublicationService.updUserAction(Long.valueOf(userId), Long.valueOf(publicationId), authorIds, "5", true, false);
            resultModelApi.setRetCode(ResultCode.Success.code());
        } catch (Exception e) {
            resultModelApi.setRetCode(KeensightResultCode.System_90003.code());
            throw e;
        }
        return resultModelApi;
    }

    /**
     * 根据文献Id获取文献内容(文献详细用)
     *
     * @param userId
     * @param publicationId
     * @return
     */
    @RequestMapping(value = "/getPublicationDataByPublicationId", method = {RequestMethod.POST})
    @ApiOperation(value = "根据文献Id获取文献内容")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id"),
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献id", required = true)
    })
    public ResultModelApi getPublicationDataByPublicationId(String userId, String publicationId) {
        ResultModelApi resultModelApi = new ResultModelApi();
        if (StringUtils.isEmpty(publicationId) || !StringUtils.isNumber(publicationId)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }

        Long searchUserId = null;
        if (!StringUtils.isEmpty(userId) && StringUtils.isNumber(userId)) {
            searchUserId = Long.valueOf(userId);
        }
        Long searchPublicationId = Long.valueOf(publicationId);
        // 调用共同方法获取论文详细内容
        List<Long> publicationIdList = new ArrayList<>();
        publicationIdList.add(searchPublicationId);
        List<PublicationModel> publicationModels = publicationService.getPublicationDetailList(publicationIdList);
        Map<String, List<PublicationModel>> listMap = new HashMap<>();
        listMap.put("list", publicationModels);
        resultModelApi.setRetCode(ResultCode.Success.code());
        resultModelApi.setData(listMap);
        return resultModelApi;
    }
}
