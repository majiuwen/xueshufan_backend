package com.keensight.web.controller;

import cn.newtouch.dl.solr.util.StringUtil;
import cn.newtouch.dl.turbocloud.helper.FileHelper;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.alibaba.fastjson.JSONObject;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.keensight.web.model.UserExcerptModel;
import com.keensight.web.service.UserExcerptService;
import com.keensight.web.utils.AwsS3Utils;
import com.keensight.web.utils.DateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 论文摘录控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2021/11/16] 新建
 */
@Api(value = "论文摘录")
@RestController
@RequestMapping("/userExcerpt")
public class UserExcerptController {

    @Autowired
    private UserExcerptService userExcerptService;
    @Autowired
    private FileHelper fileHelper;

    /**
     * 获取用户摘录信息
     * @param userId
     * @param excerptId
     * @param publicationId
     * @param seachContent
     * @param docId
     * @param seachAllExcerptFlg
     * @return
     */
    @RequestMapping(value = "/getUserExcerpt", method = {RequestMethod.POST})
    @ApiOperation(value = "获取用户摘录信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "excerptId", value = "摘录id", required = true),
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献id", required = true),
            @ApiImplicitParam(paramType = "query", name = "seachContent", value = "检索内容", required = true),
            @ApiImplicitParam(paramType = "query", name = "docId", value = "文件id", required = true),
            @ApiImplicitParam(paramType = "query", name = "seachAllExcerptFlg", value = "检索全部摘录Flg", required = true)
    })
    public ResultModelApi getUserExcerpt(Long userId, Long excerptId, Long publicationId, String seachContent, Long docId, Boolean seachAllExcerptFlg) {

        ResultModelApi resultModelApi = new ResultModelApi();

        // 用户摘录信息List
        List<UserExcerptModel> userExcerptModelList = new ArrayList<UserExcerptModel>();
        // 当前用户文件ID
        Long userDocId = docId;
        // 是否检索全部摘录
        if (seachAllExcerptFlg) {
            userDocId = null;
        }
        // 获取用户摘录信息
        userExcerptModelList = userExcerptService.getUserExcerpt4List(userId, excerptId, publicationId, seachContent, userDocId);
        if (seachAllExcerptFlg) {
            // 当前文献摘录信息
            List<UserExcerptModel> userExcerptModels = new ArrayList<UserExcerptModel>();
            // 当前文献摘录信息
            List<UserExcerptModel> otherUserExcerptModels = new ArrayList<UserExcerptModel>();
            Map<String, List<UserExcerptModel>> userExcerptMap = new HashMap<String, List<UserExcerptModel>>();
            if (!StringUtils.isEmpty(docId)) {
                userExcerptModels = userExcerptModelList.stream().filter(userExcerptModel -> docId.equals(userExcerptModel.getDocId())).collect(Collectors.toList());
                otherUserExcerptModels = userExcerptModelList.stream().filter(userExcerptModel -> !docId.equals(userExcerptModel.getDocId())).collect(Collectors.toList());
                userExcerptModels.addAll(otherUserExcerptModels);
            } else {
                resultModelApi.setRetCode("-1");
                resultModelApi.setRetMsg("获取用户摘录信息失败");
                return resultModelApi;
            }
            resultModelApi.setData(userExcerptModels);
        } else {
            resultModelApi.setData(userExcerptModelList);
        }

        return resultModelApi;
    }

    /**
     * 新增摘录信息
     * @param file
     * @param excerpt
     * @return
     */
    @RequestMapping(value = "/insertUserExcerpt", method = {RequestMethod.POST})
            @ApiOperation(value = "新增摘录信息")
            @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "file", value = "blob快照对象", required = true),
            @ApiImplicitParam(paramType = "query", name = "excerpt", value = "摘录对象", required = true),
    })
    public ResultModelApi insertUserExcerpt(MultipartFile file, String excerpt) throws IOException {

        ResultModelApi resultModelApi = new ResultModelApi();
        UserExcerptModel userExcerptModel = JSONObject.parseObject(excerpt,UserExcerptModel.class);
        // 文件上传到服务器返回值
        int uploadResult = 0;
        //  获取当前系统时间
        Date sysdate = new Date();
        userExcerptModel.setExcerptDate(sysdate);
        // 用户Id
        String userId = null;
        // 数据库存储路径
        String dbFilePath = null;
        // 服务器存储路径
        String serverFilePath = null;
        // 上传文件名
        String uploadfileName = null;
        // 判断是否上传文件
        if(file != null){
            //创建Properties对象
            Properties prop = new Properties();
            //读取classPath中的properties文件
            prop.load(UserExcerptController.class.getClassLoader().getResourceAsStream("application.properties"));
            // 文件服务器总路径
            String serverPath = prop.getProperty("keensight.file.server.path");
            // String serverPath = "C:/QWER/";
            // 数据库存储路径
            dbFilePath = userExcerptModel.getPdfPath().substring(userExcerptModel.getPdfPath().indexOf("pdf_fulltext/"),userExcerptModel.getPdfPath().lastIndexOf("?"));
            // 服务器存储路径
            serverFilePath = userExcerptModel.getPdfPath().substring(userExcerptModel.getPdfPath().indexOf("pdf_fulltext/") ,userExcerptModel.getPdfPath().lastIndexOf("/"));
            // 获取当前时间时间戳
            String timeStamp = DateUtil.getSysDate("yyyyMMddHHmmssSSS");
            // 获取userId
            userId = userExcerptModel.getUserId().toString();
            // 上传文件名
            uploadfileName = userExcerptModel.getPublicationId().toString()+ "_" + timeStamp +".jpg";
            // 文件流
            InputStream inputStream = file.getInputStream();
            System.out.println(inputStream);
            // 将截图文件上传到服务器
//            if(fileHelper.mkDir(serverPath + serverFilePath + File.separator + userId + File.separator)) {
//                if(fileHelper.saveFile(inputStream, serverPath + serverFilePath + File.separator + userId + File.separator, uploadfileName)){
//                    uploadResult = 1;
//                }
//            }
            // 上传到s3储存桶中
            PutObjectResult putObjectResult = AwsS3Utils.upload(serverFilePath + "/" + userId + "/" + uploadfileName, file.getInputStream());

            if(!ObjectUtils.isEmpty(putObjectResult)){
                userExcerptModel.setSnapshotUrl(serverFilePath + "/" + userId + "/" + uploadfileName);
            }else{
                resultModelApi.setRetCode("-1");
                resultModelApi.setRetMsg("摘录失败");
                return resultModelApi;
            }
        }
        // 新增用户摘录信息
        UserExcerptModel resUserExcerptModel = userExcerptService.insertUserExcerpt(userExcerptModel);
        if (!ObjectUtils.isEmpty(resUserExcerptModel)) {
            if (StringUtil.isNotEmpty(userExcerptModel.getPdfPath())) {
                String profilePath_s3 = AwsS3Utils.signUrl(userExcerptModel.getPdfPath());
                resUserExcerptModel.setFulltextPath(profilePath_s3);
            }
            if (StringUtil.isNotEmpty(userExcerptModel.getSnapshotUrl())) {
                String profilePath_s3 = AwsS3Utils.getS3FileUrl(userExcerptModel.getSnapshotUrl());
                resUserExcerptModel.setSnapshotUrl(profilePath_s3);
            }

            resultModelApi.setData(resUserExcerptModel);
        } else {

            resultModelApi.setRetCode("-1");
            resultModelApi.setRetMsg("摘录失败");
            return resultModelApi;
        }

        return resultModelApi;
    }

    /**
     * 删除摘录信息
     * @param excerptId
     * @return
     */
    @RequestMapping(value = "/deleteUserExcerpt", method = {RequestMethod.POST})
    @ApiOperation(value = "删除摘录信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "excerptId", value = "摘录id", required = true),
    })
    public ResultModelApi deleteUserExcerpt(Long excerptId) {

        ResultModelApi resultModelApi = new ResultModelApi();
        // 删除用户摘录信息
        int resultCount = userExcerptService.deleteUserExcerptByKey(excerptId);
        if (resultCount > 0) {
            resultModelApi.setRetCode("0");
            resultModelApi.setRetMsg("删除摘录成功");
        } else {
            resultModelApi.setRetCode("-1");
            resultModelApi.setRetMsg("删除摘录失败");
        }

        return resultModelApi;
    }

    /**
     * 更新摘录信息
     * @param excerptId
     * @param originalText
     * @param translationText
     * @return
     */
    @RequestMapping(value = "/updateUserExcerpt", method = {RequestMethod.POST})
    @ApiOperation(value = "更新摘录信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "excerptId", value = "摘录id", required = true),
            @ApiImplicitParam(paramType = "query", name = "originalText", value = "原文", required = true),
            @ApiImplicitParam(paramType = "query", name = "translationText", value = "译文", required = true),
    })
    public ResultModelApi updateUserExcerpt(Long excerptId, String originalText, String translationText) {

        ResultModelApi resultModelApi = new ResultModelApi();
        // 更新用户摘录信息
        int resCount = userExcerptService.updateUserExcerptByKey(excerptId, originalText, translationText);
        if (resCount > 0) {
            resultModelApi.setData(resCount);
        } else {
            resultModelApi.setRetCode("-1");
            resultModelApi.setRetMsg("更新摘录信息失败");
            return resultModelApi;
        }

        return resultModelApi;
    }

}
