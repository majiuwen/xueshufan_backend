package com.keensight.web.controller;

import cn.newtouch.dl.solr.model.SolrResultModel;
import cn.newtouch.dl.solr.util.StringUtil;
import cn.newtouch.dl.turbocloud.constants.ResultCode;
import cn.newtouch.dl.turbocloud.controller.BaseController;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageInfo;
import com.keensight.solr.service.SolrUserSearchService;
import com.keensight.web.constants.KeensightResultCode;
import com.keensight.web.constants.enums.UploadFolder;
import com.keensight.web.db.entity.UserFeaturedResearch;
import com.keensight.web.db.entity.UserNews;
import com.keensight.web.helper.FileServerHelper;
import com.keensight.web.model.PublicationModel;
import com.keensight.web.model.UserModel;
import com.keensight.web.model.UserNewsModel;
import com.keensight.web.service.*;
import com.keensight.web.utils.AwsS3Utils;
import com.keensight.web.utils.DateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 用户概要控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/09] 新建
 */
@RestController
@Api(value = "用户概要")
@RequestMapping("/userSummary")
public class UserSummaryController extends BaseController {

    @Autowired
    private UserAuthorFollowingService userAuthorFollowingService;
    @Autowired
    private UserCoauthorService userCoauthorService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserFieldService userFieldService;
    @Autowired
    private StatsUserTotalService statsUserTotalService;
    @Autowired
    private UserFeaturedResearchService userFeaturedResearchService;
    @Autowired
    private PublicationService publicationService;
    @Autowired
    private PublicationUserInsetitutionService publicationUserInsetitutionService;
    @Autowired
    private SolrUserSearchService solrUserSearchService;
    @Autowired
    private UserNewDevelopmentsService userNewDevelopmentsService;
    @Autowired
    private FileServerHelper fileServerHelper;

    /**
     * 用户个人信息共通模块用户个人信息共通模块
     *
     * @param userId
     * @param loginUserId
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getUserInfo", method = {RequestMethod.POST})
    @ApiOperation(value = "获取用户个人信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "loginUserId", value = "登录的用户id"),
            @ApiImplicitParam(paramType = "query", name = "userId", value = "当前用户id", required = true)
    })
    public ResultModelApi getUserInfo(String userId, String loginUserId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 为显示添加新的研究按钮，隐藏修改按钮
        String userInfoFlg = "0";
        if (!StringUtils.isEmpty(loginUserId) && StringUtils.isNumber(loginUserId)) {
            resultModelApi.setData(userService.getUserInfo(Long.valueOf(userId), Long.valueOf(loginUserId),userInfoFlg));
        } else {
            resultModelApi.setData(userService.getUserInfo(Long.valueOf(userId), null,userInfoFlg));
        }
        return resultModelApi;
    }

    /**
     * 获取用户所属机构
     *
     * @param userId
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getUserInstitution", method = {RequestMethod.POST})
    @ApiOperation(value = "获取用户所属机构")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getUserInstitution(String userId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 为显示修改按钮，隐藏添加新的研究按钮
        String userInfoFlg = "1";
        resultModelApi.setData(userService.getUserInstitution(Long.valueOf(userId), userInfoFlg));
        return resultModelApi;
    }

    /**
     * 获取用户的研究领域及学科
     *
     * @param userId
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getUserField", method = {RequestMethod.POST})
    @ApiOperation(value = "获取用户的研究领域及学科")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getUserField(String userId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        resultModelApi.setData(userFieldService.getUserField(Long.valueOf(userId)));
        return resultModelApi;
    }

    /**
     * 获取您在关注的用户信息
     *
     * @param userId
     * @param index
     * @param rows
     * @param keyword
     * @param autocompleteKeyword
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getUserAuthorFollowing", method = {RequestMethod.POST})
    @ApiOperation(value = "获取您在关注的用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数"),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示条数"),
            @ApiImplicitParam(paramType = "query", name = "keyword", value = "关键字"),
            @ApiImplicitParam(paramType = "query", name = "autocompleteKeyword", value = "自动补全关键词")
    })
    public ResultModelApi getSuggestedUserList(String userId, int index, int rows, String keyword, String autocompleteKeyword, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        PageInfo<Long> pageInfo;
        List<Long> userIdList = null;
        List<UserModel> userModelList;
        List<UserModel> userModels = new ArrayList<>();
        Long uId = Long.valueOf(userId);

        if (!StringUtils.isEmpty(keyword) && keyword.trim().length() > 0) {
            /*
             * 有关键词时，先从DB检索符合条件所有ID（无分页），
             * 然后从solr 根据这些ID集合以及keyword 进行检索（有分页）
             */
            pageInfo = userAuthorFollowingService.getUserIdList(uId, 1, 0);
            if (pageInfo != null) {
                userIdList = pageInfo.getList();
            }
            // 根据所有ID以及keyword在solr中检索
            if (userIdList != null) {
                // 根据keyword在solr中检索
                SolrResultModel solrResultModel = solrUserSearchService.getUserIdByKeyword(keyword, autocompleteKeyword, index, rows, userIdList);
                userIdList.clear();
                if (solrResultModel != null) {
                    List<Long> entry = solrResultModel.getCustomVal();
                    if (entry != null && entry.size() > 0) {
                        for (Long user : entry) {
                            if (user != null) {
                                userIdList.add(user);
                            }
                        }
                        pageInfo.setTotal(solrResultModel.getRows());
                        pageInfo.setList(userIdList);
                        pageInfo.setPageNum(index);
                        pageInfo.setPageSize(rows);
                        pageInfo.setHasNextPage(solrResultModel.getRows() > 0 && solrResultModel.getRows() > index * rows);
                    }
                }
            }
        } else {
            /*
             * 没有关键词时，只从DB中检索符合条件的ID（有分页）
             */
            // 根据检索条件查询用户ID集合
            pageInfo = userAuthorFollowingService.getUserIdList(uId, index, rows);
            if (pageInfo != null) {
                userIdList = pageInfo.getList();
            }
        }

        if (userIdList != null && userIdList.size() > 0) {
            // 根据检索用户Id获取该用户关注的其他用户的详细信息
            userModelList = userService.getUserInfoList(uId, userIdList);
            // 检索结果重排序
            for (Long user : userIdList) {
                userModels.addAll(userModelList.stream().filter(userModel -> user.equals(userModel.getUserId())).collect(Collectors.toList()));
            }
        }

        PageInfo<UserModel> resultPageInfo = new PageInfo<>();
        if (pageInfo != null) {
            BeanUtils.copyProperties(pageInfo, resultPageInfo);
            resultPageInfo.setList(userModels);
        }
        resultModelApi.setRetCode(ResultCode.Success.code());
        resultModelApi.setData(resultPageInfo);
        return resultModelApi;
    }

    /**
     * 你在关注的用户/你的粉丝/合作作者点击「取消关注」/「关注」按钮
     *
     * @param userId
     * @param userIdFollowed
     * @param followingBtnFlg
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/updateDeletedByPrimaryKey", method = {RequestMethod.POST})
    @ApiOperation(value = "你在关注的用户/你的粉丝/合作作者点击「取消关注」/「关注」按钮")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "userIdFollowed", value = "关注的作者的id", required = true),
            @ApiImplicitParam(paramType = "query", name = "followingBtnFlg", value = "关注按钮区分「1:关注」/「2:取消关注」", required = true)
    })
    public ResultModelApi updateDeletedByPrimaryKey(String userId, String userIdFollowed, String followingBtnFlg, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        if (userId.equals(userIdFollowed)) {
            resultModelApi.setRetCode(KeensightResultCode.System_90002.code());
            resultModelApi.setRetMsg("不能关注自己");
        } else {
            userAuthorFollowingService.updateDeletedByPrimaryKey(Long.valueOf(userId), Long.valueOf(userIdFollowed), followingBtnFlg);
            if ("1".equals(followingBtnFlg)) {
                List<Long> userIdList = new ArrayList<>();
                userIdList.add(Long.valueOf(userIdFollowed));
                List<UserModel> userModelList = userService.getUserInfoList(Long.valueOf(userId), userIdList);
                UserModel userModel = new UserModel();
                if (userModelList != null && userModelList.size() > 0) {
                    userModel = userModelList.get(0);
                    resultModelApi.setData(userModel);
                }
            }

            resultModelApi.setRetCode(ResultCode.Success.code());
        }
        return resultModelApi;
    }

    /**
     * 获取您的粉丝信息
     *
     * @param userId
     * @param index
     * @param rows
     * @param keyword
     * @param autocompleteKeyword
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getFanInformation", method = {RequestMethod.POST})
    @ApiOperation(value = "获取您的粉丝信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数"),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示条数"),
            @ApiImplicitParam(paramType = "query", name = "keyword", value = "关键字"),
            @ApiImplicitParam(paramType = "query", name = "autocompleteKeyword", value = "自动补全关键词")
    })
    public ResultModelApi getFanInformation(String userId, int index, int rows, String keyword, String autocompleteKeyword, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        PageInfo<Long> pageInfo;
        List<Long> userIdList = null;
        List<UserModel> userModelList;
        List<UserModel> userModels = new ArrayList<>();
        Long uId = Long.valueOf(userId);

        if (!StringUtils.isEmpty(keyword) && keyword.trim().length() > 0) {
            /*
             * 有关键词时，先从DB检索符合条件所有ID（无分页），
             * 然后从solr 根据这些ID集合以及keyword 进行检索（有分页）
             */
            pageInfo = userAuthorFollowingService.getFanUserIdList(uId, 1, 0);
            if (pageInfo != null) {
                userIdList = pageInfo.getList();
            }
            // 根据所有ID以及keyword在solr中检索
            if (userIdList != null) {
                // 根据keyword在solr中检索
                SolrResultModel solrResultModel = solrUserSearchService.getUserIdByKeyword(keyword, autocompleteKeyword, index, rows, userIdList);
                userIdList.clear();
                if (solrResultModel != null) {
                    List<Long> entry = solrResultModel.getCustomVal();
                    if (entry != null && entry.size() > 0) {
                        for (Long user : entry) {
                            if (user != null) {
                                userIdList.add(user);
                            }
                        }
                        pageInfo.setTotal(solrResultModel.getRows());
                        pageInfo.setList(userIdList);
                        pageInfo.setPageNum(index);
                        pageInfo.setPageSize(rows);
                        pageInfo.setHasNextPage(solrResultModel.getRows() > 0 && solrResultModel.getRows() > index * rows);
                    }
                }
            }
        } else {
            /*
             * 没有关键词时，只从DB中检索符合条件的ID（有分页）
             */
            // 根据检索条件查询用户ID集合
            pageInfo = userAuthorFollowingService.getFanUserIdList(uId, index, rows);
            if (pageInfo != null) {
                userIdList = pageInfo.getList();
            }
        }

        if (userIdList != null && userIdList.size() > 0) {
            // 根据检索用户Id获取该用户关注的其他用户的详细信息
            userModelList = userService.getUserInfoList(uId, userIdList);
            // 检索结果重排序
            for (Long user : userIdList) {
                userModels.addAll(userModelList.stream().filter(userModel -> user.equals(userModel.getUserId())).collect(Collectors.toList()));
            }
        }

        PageInfo<UserModel> resultPageInfo = new PageInfo<>();
        if (pageInfo != null) {
            BeanUtils.copyProperties(pageInfo, resultPageInfo);
            resultPageInfo.setList(userModels);
        }
        resultModelApi.setRetCode(ResultCode.Success.code());
        resultModelApi.setData(resultPageInfo);
        return resultModelApi;
    }

    /**
     * 获取合作作者
     *
     * @param userId
     * @param index
     * @param rows
     * @param keyword
     * @param autocompleteKeyword
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getUserCoauthor", method = {RequestMethod.POST})
    @ApiOperation(value = "获取合作作者")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数"),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示条数"),
            @ApiImplicitParam(paramType = "query", name = "keyword", value = "关键字"),
            @ApiImplicitParam(paramType = "query", name = "autocompleteKeyword", value = "自动补全关键词")
    })
    public ResultModelApi getUserCoauthor(String userId, int index, int rows, String keyword, String autocompleteKeyword, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        PageInfo<Long> pageInfo;
        List<Long> userIdList = null;
        List<UserModel> userModelList;
        Long uId = Long.valueOf(userId);
        List<UserModel> userModels = new ArrayList<>();

        if (!StringUtils.isEmpty(keyword) && keyword.trim().length() > 0) {
            /*
             * 有关键词时，先从DB检索符合条件所有ID（无分页），
             * 然后从solr 根据这些ID集合以及keyword 进行检索（有分页）
             */
            pageInfo = userCoauthorService.getUserIdList(uId, 1, 0, "2");
            if (pageInfo != null) {
                userIdList = pageInfo.getList();
            }
            // 根据所有ID以及keyword在solr中检索
            if (userIdList != null) {
                // 根据keyword在solr中检索
                SolrResultModel solrResultModel = solrUserSearchService.getUserIdByKeyword(keyword, autocompleteKeyword, index, rows, userIdList);
                userIdList.clear();
                if (solrResultModel != null) {
                    List<Long> entry = solrResultModel.getCustomVal();
                    if (entry != null && entry.size() > 0) {
                        for (Long user : entry) {
                            if (user != null) {
                                userIdList.add(user);
                            }
                        }
                        pageInfo.setTotal(solrResultModel.getRows());
                        pageInfo.setList(userIdList);
                        pageInfo.setPageNum(index);
                        pageInfo.setPageSize(rows);
                        pageInfo.setHasNextPage(solrResultModel.getRows() > 0 && solrResultModel.getRows() > index * rows);
                    }
                }
            }
        } else {
            /*
             * 没有关键词时，只从DB中检索符合条件的ID（有分页）
             */
            // 根据检索条件查询用户ID集合
            pageInfo = userCoauthorService.getUserIdList(uId, index, rows, "2");
            if (pageInfo != null) {
                userIdList = pageInfo.getList();
            }
        }

        if (userIdList != null && userIdList.size() > 0) {
            // 根据检索用户Id获取该用户关注的其他用户的详细信息
            userModelList = userService.getUserInfoList(uId, userIdList);
            // 检索结果重排序
            for (Long user : userIdList) {
                userModels.addAll(userModelList.stream().filter(userModel -> user.equals(userModel.getUserId())).collect(Collectors.toList()));
            }
            for (UserModel userModel : userModels) {
                userModel.setInviteBtnFlg("0");
            }
        }

        PageInfo<UserModel> resultPageInfo = new PageInfo<>();
        if (pageInfo != null) {
            BeanUtils.copyProperties(pageInfo, resultPageInfo);
            resultPageInfo.setList(userModels);
        }
        resultModelApi.setRetCode(ResultCode.Success.code());
        resultModelApi.setData(resultPageInfo);
        return resultModelApi;
    }

    /**
     * 获取作者文献统计总数
     *
     * @param userId
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getStatsUserTotal", method = {RequestMethod.POST})
    @ApiOperation(value = "获取作者文献统计总数")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getStatsUserTotal(String userId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        resultModelApi.setData(statsUserTotalService.getUserTotal(Long.valueOf(userId)));
        return resultModelApi;
    }

    /**
     * 获取作者年度文献统计
     *
     * @param userId
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getStatsUserYearlyList", method = {RequestMethod.POST})
    @ApiOperation(value = "获取作者年度文献统计")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getStatsUserYearlyList(String userId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        resultModelApi.setData(statsUserTotalService.getStatsUserYearlyList(Long.valueOf(userId)));
        return resultModelApi;
    }

    /**
     * 获取用户的代表性研究
     *
     * @param userId
     * @param index
     * @param rows
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getUserFeatureResearchList", method = {RequestMethod.POST})
    @ApiOperation(value = "获取用户的代表性研究")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "loginUserId", value = "登录用户id", required = false),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数", required = true),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示条数", required = true)
    })
    public ResultModelApi getUserFeatureResearchList(String userId, String loginUserId, int index, int rows, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        Long uId =  null;
        if (!StringUtils.isEmpty(userId)) {
            uId = Long.valueOf(userId);
        }
        Long lId = null;
        if (!StringUtils.isEmpty(loginUserId)) {
            lId = Long.valueOf(loginUserId);
        }
        PageInfo<UserFeaturedResearch> userFeaturedResearchPageInfo = userFeaturedResearchService.getUserFeatureResearchList(uId, index, rows);
        List<UserFeaturedResearch> userFeaturedResearches = userFeaturedResearchPageInfo.getList();

        if (userFeaturedResearches.size() != 0) {
            // 设置文献Id
            List<Long> publicationIdList = new ArrayList<>();
            for (UserFeaturedResearch userFeaturedResearch : userFeaturedResearches) {
                publicationIdList.add(userFeaturedResearch.getPublicationId());
            }

            // 获取论文详细内容
            List<PublicationModel> publicationModels = publicationService.getPublicationDetailList(publicationIdList);
            for (int i = 0; i < publicationModels.size(); i++) {
                PublicationModel publicationModel = publicationModels.get(i);
                UserFeaturedResearch userFeaturedResearch = userFeaturedResearches.get(i);
                // 用户Id
                publicationModel.setUserId(userFeaturedResearch.getUserId());
                // 块标题
                publicationModel.setHeaderTitle("代表性研究");
                // 修改按钮显示
                publicationModel.setUpdateBtnFlg("1");
                if (!StringUtils.isEmpty(loginUserId) && loginUserId.equals(userId)) {
                    // 分享按钮显示Flg (0：都不显示，1：显示，2：显示取消)
                    publicationModel.setShareBtnFlg("1");
                    // 收藏按钮显示Flg
                    publicationModel.setSaveBtnFlg("0");
                    // 推荐按钮显示Flg (0：都不显示，1：显示，2：显示取消)
                    publicationModel.setRecommendBtnFlg("0");
                    // 关注按钮显示Flg (0：都不显示，1：显示，2：显示取消)
                    publicationModel.setFollowingBtnFlg("0");
                    // 增加补充材料按钮显示Flg (0：不显示，1：显示)
                    publicationModel.setUploadOtherFileBtnFlg("0");
                    // 上传全文按钮显示Flg (0：都不显示，1：显示上传全文，2：有全文)
                    if (StringUtils.isEmpty(publicationModel.getFulltextPath())) {
                        publicationModel.setUploadPdfBtnFlg("1");
                        // 下载按钮显示Flg (0：都不显示，1：显示，2：索要全文)
                        publicationModel.setDownloadBtnFlg("0");
                    } else {
                        publicationModel.setUploadPdfBtnFlg("2");
                        // 下载按钮显示Flg (0：都不显示，1：显示，2：索要全文)
                        publicationModel.setDownloadBtnFlg("1");
                    }
                }
            }
            PageInfo<PublicationModel> publicationModelPageInfo = new PageInfo<PublicationModel>(publicationModels);
            publicationModelPageInfo.setPageNum(userFeaturedResearchPageInfo.getPageNum());
            publicationModelPageInfo.setPageSize(userFeaturedResearchPageInfo.getPageSize());
            publicationModelPageInfo.setStartRow(userFeaturedResearchPageInfo.getStartRow());
            publicationModelPageInfo.setEndRow(userFeaturedResearchPageInfo.getEndRow());
            publicationModelPageInfo.setPages(userFeaturedResearchPageInfo.getPages());
            publicationModelPageInfo.setPrePage(userFeaturedResearchPageInfo.getPrePage());
            publicationModelPageInfo.setNextPage(userFeaturedResearchPageInfo.getNextPage());
            publicationModelPageInfo.setIsFirstPage(userFeaturedResearchPageInfo.isIsFirstPage());
            publicationModelPageInfo.setIsLastPage(userFeaturedResearchPageInfo.isIsLastPage());
            publicationModelPageInfo.setHasPreviousPage(userFeaturedResearchPageInfo.isHasPreviousPage());
            publicationModelPageInfo.setHasNextPage(userFeaturedResearchPageInfo.isHasNextPage());
            publicationModelPageInfo.setNavigatePages(userFeaturedResearchPageInfo.getNavigatePages());
            publicationModelPageInfo.setNavigatepageNums(userFeaturedResearchPageInfo.getNavigatepageNums());
            publicationModelPageInfo.setNavigateFirstPage(userFeaturedResearchPageInfo.getNavigateFirstPage());
            publicationModelPageInfo.setNavigateLastPage(userFeaturedResearchPageInfo.getNavigateLastPage());
            publicationModelPageInfo.setTotal(userFeaturedResearchPageInfo.getTotal());
            publicationModelPageInfo.setList(publicationModels);
            resultModelApi.setData(publicationModelPageInfo);
        }
        return resultModelApi;
    }

    /**
     * 获取本月阅读次数最多的文章
     *
     * @param userId
     * @param index
     * @param rows
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getMaxReadByUserId", method = {RequestMethod.POST})
    @ApiOperation(value = "获取本月阅读次数最多的文章")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "loginUserId", value = "登录用户id", required = false),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数", required = true),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示条数", required = true)
    })
    public ResultModelApi getMaxReadByUserId(String userId, String loginUserId, int index, int rows, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        Long uId =  null;
        if (!StringUtils.isEmpty(userId)) {
            uId = Long.valueOf(userId);
        }
        Long lId = null;
        if (!StringUtils.isEmpty(loginUserId)) {
            lId = Long.valueOf(loginUserId);
        }
        // 获取本月
        Date month = DateUtil.getLastDayOfMonth(new Date());
        // 获取本月阅读最多文章
        PageInfo<Long> publicationIds = publicationUserInsetitutionService.getMaxReadByUserId(uId, month, index, rows);
        List<Long> pIds = publicationIds.getList();
        if (pIds != null && pIds.size() > 0) {
            // 设置文献Id
            List<Long> publicationIdList = new ArrayList<>();
            for (Long pId : pIds) {
                publicationIdList.add(pId);
            }
            // 获取论文详细内容
            List<PublicationModel> publicationModels = publicationService.getPublicationDetailList(publicationIdList);
            for (PublicationModel publicationModel : publicationModels) {
                publicationModel.setUserId(Long.valueOf(userId));
                // 获取本月阅读次数最多的文章
                publicationModel.setHeaderTitle("本月阅读次数最多");
                if (!StringUtils.isEmpty(loginUserId) && loginUserId.equals(userId)) {
                    // 分享按钮显示Flg (0：都不显示，1：显示，2：显示取消)
                    publicationModel.setShareBtnFlg("1");
                    // 收藏按钮显示Flg
                    publicationModel.setSaveBtnFlg("0");
                    // 推荐按钮显示Flg (0：都不显示，1：显示，2：显示取消)
                    publicationModel.setRecommendBtnFlg("0");
                    // 关注按钮显示Flg (0：都不显示，1：显示，2：显示取消)
                    publicationModel.setFollowingBtnFlg("0");
                    // 增加补充材料按钮显示Flg (0：不显示，1：显示)
                    publicationModel.setUploadOtherFileBtnFlg("0");
                    // 上传全文按钮显示Flg (0：都不显示，1：显示上传全文，2：有全文)
                    if (StringUtils.isEmpty(publicationModel.getFulltextPath())) {
                        publicationModel.setUploadPdfBtnFlg("1");
                        // 下载按钮显示Flg (0：都不显示，1：显示，2：索要全文)
                        publicationModel.setDownloadBtnFlg("0");
                    } else {
                        publicationModel.setUploadPdfBtnFlg("2");
                        // 下载按钮显示Flg (0：都不显示，1：显示，2：索要全文)
                        publicationModel.setDownloadBtnFlg("1");
                    }
                }
            }
            PageInfo<PublicationModel> publicationModelPageInfo = new PageInfo<PublicationModel>(publicationModels);
            publicationModelPageInfo.setPageNum(publicationIds.getPageNum());
            publicationModelPageInfo.setPageSize(publicationIds.getPageSize());
            publicationModelPageInfo.setStartRow(publicationIds.getStartRow());
            publicationModelPageInfo.setEndRow(publicationIds.getEndRow());
            publicationModelPageInfo.setPages(publicationIds.getPages());
            publicationModelPageInfo.setPrePage(publicationIds.getPrePage());
            publicationModelPageInfo.setNextPage(publicationIds.getNextPage());
            publicationModelPageInfo.setIsFirstPage(publicationIds.isIsFirstPage());
            publicationModelPageInfo.setIsLastPage(publicationIds.isIsLastPage());
            publicationModelPageInfo.setHasPreviousPage(publicationIds.isHasPreviousPage());
            publicationModelPageInfo.setHasNextPage(publicationIds.isHasNextPage());
            publicationModelPageInfo.setNavigatePages(publicationIds.getNavigatePages());
            publicationModelPageInfo.setNavigatepageNums(publicationIds.getNavigatepageNums());
            publicationModelPageInfo.setNavigateFirstPage(publicationIds.getNavigateFirstPage());
            publicationModelPageInfo.setNavigateLastPage(publicationIds.getNavigateLastPage());
            publicationModelPageInfo.setTotal(publicationIds.getTotal());
            publicationModelPageInfo.setList(publicationModels);
            resultModelApi.setData(publicationModelPageInfo);
        }
        return resultModelApi;
    }

    /**
     * 获取用户自己的文章
     *
     * @param userId
     * @param loginUserId
     * @param index
     * @param rows
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getPublicationByUserId", method = {RequestMethod.POST})
    @ApiOperation(value = "获取用户自己的文章")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "loginUserId", value = "登录用户id", required = false),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数", required = true),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示条数", required = true)
    })
    public ResultModelApi getPublicationByUserId(String userId, String loginUserId, int index, int rows, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        Long uId =  null;
        if (!StringUtils.isEmpty(userId)) {
            uId = Long.valueOf(userId);
        }
        Long lId = null;
        if (!StringUtils.isEmpty(loginUserId)) {
            lId = Long.valueOf(loginUserId);
        }
        // 获取自己的文章
        PageInfo<Long> publicationIds = publicationUserInsetitutionService.getMyPublicationList(uId, index, rows);
        List<Long> pIds = publicationIds.getList();
        if (pIds != null && pIds.size() > 0) {
            // 设置文献Id
            List<Long> publicationIdList = new ArrayList<>();
            for (Long pId : pIds) {
                publicationIdList.add(pId);
            }
            // 获取论文详细内容
            List<PublicationModel> publicationModels = publicationService.getPublicationDetailList(publicationIdList);
            for (PublicationModel publicationModel : publicationModels) {
                publicationModel.setUserId(Long.valueOf(userId));
                if (!StringUtils.isEmpty(loginUserId) && loginUserId.equals(userId)) {
                    // 分享按钮显示Flg (0：都不显示，1：显示，2：显示取消)
                    publicationModel.setShareBtnFlg("1");
                    // 收藏按钮显示Flg
                    publicationModel.setSaveBtnFlg("0");
                    // 推荐按钮显示Flg (0：都不显示，1：显示，2：显示取消)
                    publicationModel.setRecommendBtnFlg("0");
                    // 关注按钮显示Flg (0：都不显示，1：显示，2：显示取消)
                    publicationModel.setFollowingBtnFlg("0");
                    // 增加补充材料按钮显示Flg (0：不显示，1：显示)
                    publicationModel.setUploadOtherFileBtnFlg("0");
                    // 上传全文按钮显示Flg (0：都不显示，1：显示上传全文，2：有全文)
                    if (StringUtils.isEmpty(publicationModel.getFulltextPath())) {
                        publicationModel.setUploadPdfBtnFlg("1");
                        // 下载按钮显示Flg (0：都不显示，1：显示，2：索要全文)
                        publicationModel.setDownloadBtnFlg("0");
                    } else {
                        publicationModel.setUploadPdfBtnFlg("2");
                        // 下载按钮显示Flg (0：都不显示，1：显示，2：索要全文)
                        publicationModel.setDownloadBtnFlg("1");
                    }
                }
            }

            PageInfo<PublicationModel> publicationModelPageInfo = new PageInfo<PublicationModel>(publicationModels);
            publicationModelPageInfo.setPageNum(publicationIds.getPageNum());
            publicationModelPageInfo.setPageSize(publicationIds.getPageSize());
            publicationModelPageInfo.setStartRow(publicationIds.getStartRow());
            publicationModelPageInfo.setEndRow(publicationIds.getEndRow());
            publicationModelPageInfo.setPages(publicationIds.getPages());
            publicationModelPageInfo.setPrePage(publicationIds.getPrePage());
            publicationModelPageInfo.setNextPage(publicationIds.getNextPage());
            publicationModelPageInfo.setIsFirstPage(publicationIds.isIsFirstPage());
            publicationModelPageInfo.setIsLastPage(publicationIds.isIsLastPage());
            publicationModelPageInfo.setHasPreviousPage(publicationIds.isHasPreviousPage());
            publicationModelPageInfo.setHasNextPage(publicationIds.isHasNextPage());
            publicationModelPageInfo.setNavigatePages(publicationIds.getNavigatePages());
            publicationModelPageInfo.setNavigatepageNums(publicationIds.getNavigatepageNums());
            publicationModelPageInfo.setNavigateFirstPage(publicationIds.getNavigateFirstPage());
            publicationModelPageInfo.setNavigateLastPage(publicationIds.getNavigateLastPage());
            publicationModelPageInfo.setTotal(publicationIds.getTotal());
            publicationModelPageInfo.setList(publicationModels);
            resultModelApi.setData(publicationModelPageInfo);
        }
        return resultModelApi;
    }

    /**
     * 学者页最新动态获取方法
     *
     * @param userId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getNewDevelopments", method = {RequestMethod.POST})
    public ResultModelApi getNewDevelopments(Long loginUserId, Long userId, Integer index, Integer rows, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
         // 获取最新动态
        PageInfo<UserNewsModel> userNews = userNewDevelopmentsService.getNewDevelopments(loginUserId,userId,index,rows);

        for(UserNewsModel userNewsModel : userNews.getList()){
//            userNewsModel.setProfilePath(fileServerHelper.getFileServerUrl(userNewsModel.getProfilePath(), UploadFolder.PROFILE_PHOTO));
            if (!StringUtils.isEmpty(userNewsModel.getProfilePath())) {
                String url_s3 = AwsS3Utils.getS3FileUrl(userNewsModel.getProfilePath());
                userNewsModel.setProfilePath(url_s3);
            }

            if (userNewsModel.getUserNewsFilesList() != null && userNewsModel.getUserNewsFilesList().size() > 0) {
                for (int i = 0;i < userNewsModel.getUserNewsFilesList().size(); i++) {
                    if (StringUtil.isNotEmpty(userNewsModel.getUserNewsFilesList().get(i).getNewsFileUrl())) {
                        String newsFileUrl_s3 = AwsS3Utils.getS3FileUrl(userNewsModel.getUserNewsFilesList().get(i).getNewsFileUrl());
                        userNewsModel.getUserNewsFilesList().get(i).setNewsFileUrl(newsFileUrl_s3);
                    }
                }
            }

        }

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("userNews", userNews.getList());
        resultMap.put("hasNextPage", userNews.isHasNextPage());
        resultModelApi.setData(resultMap);
        return resultModelApi;
    }

    /**
     * 最新动态点赞方法
     * @param newsId
     * @param userId
     * @param updFlg
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/updLikeNums", method = {RequestMethod.POST})
    public ResultModelApi updLikeNums(Long newsId, Long userId, String updFlg, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        int updLikeNum = 0;
        // 更新点赞数
        updLikeNum = userNewDevelopmentsService.updLikeNums(newsId,userId,updFlg);
        resultModelApi.setData(updLikeNum);
        return resultModelApi;
    }
}
