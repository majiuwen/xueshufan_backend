package com.keensight.web.controller;

import cn.newtouch.dl.turbocloud.constants.ResultCode;
import cn.newtouch.dl.turbocloud.controller.BaseController;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageInfo;
import com.keensight.web.constants.WebConst;
import com.keensight.web.model.PublicationModel;
import com.keensight.web.model.PublicationReferenceModel;
import com.keensight.web.service.PublicationReferenceService;
import com.keensight.web.service.PublicationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * 文献参考文献控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/12] 新建
 */
@Api(value = "文献参考文献")
@RestController
@RequestMapping("/articlesReference")
public class ArticlesReferenceController extends BaseController {
    @Autowired
    private PublicationService publicationService;
    @Autowired
    private PublicationReferenceService publicationReferenceService;

    /**
     * 根据父文献ID检索参考文献ID集合
     *
     * @param publicationId 父文献ID
     * @param sortColumns   排序列
     * @param index         开始页数
     * @param rows          显示件数
     * @return resultModelApi
     */
    @RequestMapping(value = "/getArticlesReferenceList", method = {RequestMethod.POST})
    @ApiOperation(value = "参考文献ID集合取得")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "父文献id", required = true),
            @ApiImplicitParam(paramType = "query", name = "userId", value = "当前用户id"),
            @ApiImplicitParam(paramType = "query", name = "sortColumns", value = "排序列"),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数"),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示件数")
    })
    public ResultModelApi getArticlesReferenceList(Long publicationId, String userId, String sortColumns, Integer index, Integer rows) {
        ResultModelApi resultModelApi = new ResultModelApi();

        // 检索条件处理 start
        if (StringUtils.isEmpty(publicationId.toString())) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }

        Long searchUserId = null;
        if (!StringUtils.isEmpty(userId) && StringUtils.isNumber(userId)) {
            searchUserId = Long.valueOf(userId);
        }

        // 检索处理 start
        PageInfo<Long> pageInfo;
        List<Long> publicationIdList = null;
        List<PublicationModel> detailList = new ArrayList<>();
        // 根据父文献ID查询参考文献ID集合
        pageInfo = getArticlesReferenceIdList(publicationId, sortColumns, index, rows);

        if (pageInfo != null) {
            publicationIdList = pageInfo.getList();
        }
        if (publicationIdList != null && publicationIdList.size() > 0) {
            // 调用共通检索获取文献详细信息一览数据
            detailList = publicationService.getPublicationDetailList(publicationIdList);
            if (detailList != null && detailList.size() > 0) {
                // 数据处理
                for (PublicationModel model : detailList) {
                    if (model != null) {
                        // 上传全文按钮显示Flg (0：都不显示，1：显示上传全文，2：有全文)
                        if (StringUtils.isEmpty(model.getFulltextPath())) {
                            model.setUploadPdfBtnFlg(WebConst.UPLOADPDF_BTN_FLG_UPLOAD_PDF);
                        } else {
                            model.setUploadPdfBtnFlg(WebConst.UPLOADPDF_BTN_FLG_EXIST_PDF);
                        }
                    }
                }
            }
        }
        // 检索处理 end

        PageInfo<PublicationModel> resultPageInfo = new PageInfo<>();
        if (pageInfo != null) {
            BeanUtils.copyProperties(pageInfo, resultPageInfo);
            resultPageInfo.setList(detailList);
        }
        resultModelApi.setRetCode(ResultCode.Success.code());
        resultModelApi.setData(resultPageInfo);
        return resultModelApi;
    }

    /**
     * 根据父文献ID检索参考文献ID集合 文献的参考文献使用
     *
     * @param publicationId 父文献ID
     * @param sortColumns   排序列
     * @param pageNum       开始页数
     * @param pageSize      显示件数
     * @return 文献ID集合
     */
    private PageInfo<Long> getArticlesReferenceIdList(Long publicationId, String sortColumns, Integer pageNum, Integer pageSize) {
        PageInfo<Long> pageInfo;
        pageInfo = publicationService.getArticlesReferenceIdList(publicationId, sortColumns, pageNum, pageSize);
        return pageInfo;
    }

    /**
     * 根据父文献ID检索参考文献数和引证文献数
     *
     * @param publicationId 父文献ID
     * @return
     */
    @RequestMapping(value = "/getPublicationReferenceCount", method = {RequestMethod.POST})
    @ApiOperation(value = "根据父文献ID检索参考文献数和引证文献数")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "父文献id", required = true)
    })
    private ResultModelApi getPublicationReferenceCount(Long publicationId) {

        ResultModelApi resultModelApi = new ResultModelApi();
        PublicationReferenceModel publicationReferenceModel = new PublicationReferenceModel();
        if (publicationId != null) {
            int publicationReferenceCount = publicationReferenceService.getPublicationReferenceCount(publicationId);
            publicationReferenceModel.setPublicationReferenceCount(publicationReferenceCount);
            int publicationCitationCount = publicationReferenceService.getPublicationCitationCount(publicationId);
            publicationReferenceModel.setPublicationCitationCount(publicationCitationCount);
            resultModelApi.setData(publicationReferenceModel);
        } else {
            resultModelApi.setRetCode("-1");
            resultModelApi.setRetMsg("参数错误");
        }
        return resultModelApi;
    }
}
