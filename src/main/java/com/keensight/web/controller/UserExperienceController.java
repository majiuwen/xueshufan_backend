package com.keensight.web.controller;

import cn.newtouch.dl.turbocloud.constants.ResultCode;
import cn.newtouch.dl.turbocloud.controller.BaseController;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.alibaba.druid.util.StringUtils;
import com.keensight.web.db.entity.*;
import com.keensight.web.service.UserExperienceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 学者经历控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2021/11/03] 新建
 */
@Api(value = "作者经历")
@RestController
@RequestMapping("/userExperience")
public class UserExperienceController extends BaseController {

    @Autowired
    private UserExperienceService userExperienceService;

    /**
     * 获取作者工作经历信息
     * @param userId
     * @return
     */
    @RequestMapping(value = "/getUserEmploymentInfo", method = {RequestMethod.POST})
    @ApiOperation(value = "作者工作经历信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getUserEmploymentInfo(String userId) {
        ResultModelApi resultModelApi = new ResultModelApi();

        // 用户ID不存在的情况下
        if (StringUtils.isEmpty(userId) || !StringUtils.isNumber(userId)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }

        // 获取作者工作经历信息
        List<UserEmployment> userEmploymentList = userExperienceService.getUserEmploymentList(Integer.valueOf(userId));
        resultModelApi.setData(userEmploymentList);
        return resultModelApi;
    }

    /**
     * 获取作者教育背景信息
     * @param userId
     * @return
     */
    @RequestMapping(value = "/getUserEducationInfo", method = {RequestMethod.POST})
    @ApiOperation(value = "作者教育背景信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getUserEducationInfo(String userId) {
        ResultModelApi resultModelApi = new ResultModelApi();

        // 用户ID不存在的情况下
        if (StringUtils.isEmpty(userId) || !StringUtils.isNumber(userId)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }

        // 获取作者教育背景信息
        List<UserEducation> userEducationList = userExperienceService.getUserEducationList(Integer.valueOf(userId));
        resultModelApi.setData(userEducationList);
        return resultModelApi;
    }

    /**
     * 获取作者项目信息
     * @param userId
     * @return
     */
    @RequestMapping(value = "/getUserFundingsInfo", method = {RequestMethod.POST})
    @ApiOperation(value = "作者项目信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getUserFundingsInfo(String userId) {
        ResultModelApi resultModelApi = new ResultModelApi();

        // 用户ID不存在的情况下
        if (StringUtils.isEmpty(userId) || !StringUtils.isNumber(userId)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }

        // 获取作者项目信息
        List<UserFundings> userFundingsList = userExperienceService.getUserFundingsList(Integer.valueOf(userId));
        resultModelApi.setData(userFundingsList);
        return resultModelApi;
    }

    /**
     * 获取作者获奖信息
     * @param userId
     * @return
     */
    @RequestMapping(value = "/getUserDistinctionsInfo", method = {RequestMethod.POST})
    @ApiOperation(value = "作者获奖信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getUserDistinctionsInfo(String userId) {
        ResultModelApi resultModelApi = new ResultModelApi();

        // 用户ID不存在的情况下
        if (StringUtils.isEmpty(userId) || !StringUtils.isNumber(userId)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }

        // 获取作者获奖信息
        List<UserDistinctions> userDistinctionsList = userExperienceService.getUserDistinctionsList(Integer.valueOf(userId));
        resultModelApi.setData(userDistinctionsList);
        return resultModelApi;
    }

    /**
     * 获取作者学术组织会员信息
     * @param userId
     * @return
     */
    @RequestMapping(value = "/getUserMembershipInfo", method = {RequestMethod.POST})
    @ApiOperation(value = "作者学术组织会员信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getUserMembershipInfo(String userId) {
        ResultModelApi resultModelApi = new ResultModelApi();

        // 用户ID不存在的情况下
        if (StringUtils.isEmpty(userId) || !StringUtils.isNumber(userId)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }

        // 获取作者学术组织会员信息
        List<UserMembership> userMembershipList = userExperienceService.getUserMembershipList(Integer.valueOf(userId));
        resultModelApi.setData(userMembershipList);
        return resultModelApi;
    }

    /**
     * 获取审稿人信息
     * @param userId
     * @return
     */
    @RequestMapping(value = "/getUserPeerReviewsInfo", method = {RequestMethod.POST})
    @ApiOperation(value = "审稿人信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getUserPeerReviewsInfo(String userId) {
        ResultModelApi resultModelApi = new ResultModelApi();

        // 用户ID不存在的情况下
        if (StringUtils.isEmpty(userId) || !StringUtils.isNumber(userId)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }

        // 获取审稿人信息
        List<UserPeerReviews> userPeerReviewsList = userExperienceService.getUserPeerReviewsList(Integer.valueOf(userId));
        resultModelApi.setData(userPeerReviewsList);
        return resultModelApi;
    }

    /**
     * 获取作者联系方式信息
     * @param userId
     * @return
     */
    @RequestMapping(value = "/getUserContactInfo", method = {RequestMethod.POST})
    @ApiOperation(value = "作者联系方式信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getUserContactInformationInfo(String userId) {
        ResultModelApi resultModelApi = new ResultModelApi();

        // 用户ID不存在的情况下
        if (StringUtils.isEmpty(userId) || !StringUtils.isNumber(userId)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }

        // 获取作者联系方式信息
        List<UserContactInformation> userContactInformationList = userExperienceService.getUserContactInformationList(Integer.valueOf(userId));
        resultModelApi.setData(userContactInformationList);
        return resultModelApi;
    }

    /**
     * 获取作者学术导师信息
     * @param userId
     * @return
     */
    @RequestMapping(value = "/getUserAcademicInfo", method = {RequestMethod.POST})
    @ApiOperation(value = "作者学术导师信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getUserAcademicAdviserInfo(String userId) {
        ResultModelApi resultModelApi = new ResultModelApi();

        // 用户ID不存在的情况下
        if (StringUtils.isEmpty(userId) || !StringUtils.isNumber(userId)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }

        // 获取作者学术导师信息
        List<User> userAcademicAdviserList = userExperienceService.getUserAcademicAdviserList(Integer.valueOf(userId));
        resultModelApi.setData(userAcademicAdviserList);
        return resultModelApi;
    }
}
