package com.keensight.web.controller;


import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.github.pagehelper.PageInfo;
import com.keensight.web.db.entity.UserInfo;
import com.keensight.web.db.entity.UserNewsComment;
import com.keensight.web.model.UserModel;
import com.keensight.web.model.UserNewsCommentModel;
import com.keensight.web.service.UserInfoService;
import com.keensight.web.service.UserNewDevelopmentsService;
import com.keensight.web.service.UserNewsCommentService;
import com.keensight.web.service.UserService;
import com.keensight.web.utils.ContentSafetyResult;
import com.keensight.web.utils.ExamineUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 最新动态评论控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/12/01] 新建
 */
@RestController
@Api(value = "最新动态评论")
@RequestMapping("/userNewsComment")
public class UserNewsCommentController {

    @Autowired
    private UserNewsCommentService userNewsCommentService;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserNewDevelopmentsService userNewDevelopmentsService;

    /**
     * 最新动态一级评论获取方法
     * @param loginUserId
     * @param newsId
     * @param index
     * @param rows
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getNewsComments", method = {RequestMethod.POST})
    public ResultModelApi getNewsComments(Long loginUserId, Long newsId, Integer index, Integer rows, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 获取最新动态评论
        PageInfo<UserNewsCommentModel> userNewsComments = userNewsCommentService.getNewComments(loginUserId,newsId,index,rows);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("userNewsComments", userNewsComments.getList());
        resultMap.put("hasNextPage", userNewsComments.isHasNextPage());
        resultMap.put("commentNum", userNewsComments.getTotal());
        resultModelApi.setData(resultMap);
        return resultModelApi;
    }

    /**
     * 最新动态二级评论获取方法
     * @param loginUserId
     * @param newsCommentId
     * @param index
     * @param rows
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getSecondComments", method = {RequestMethod.POST})
    public ResultModelApi getSecondComments(Long loginUserId, Long newsCommentId, Integer listCount, Integer index, Integer rows, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 获取最新动态评论
        PageInfo<UserNewsCommentModel> secondComments = userNewsCommentService.getSecondComments(loginUserId,newsCommentId,listCount,index,rows);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("secondComments", secondComments.getList());
        resultMap.put("hasNextPage", secondComments.isHasNextPage());
        resultModelApi.setData(resultMap);
        return resultModelApi;
    }

    /**
     * 评论点赞/取消点赞方法
     * @param newsCommentId
     * @param userId
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/updCommentLike", method = {RequestMethod.POST})
    public ResultModelApi updCommentLike(Long newsCommentId, Long userId, String updFlg, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        int updCommentLike = 0;
        // 更新点赞数
        updCommentLike = userNewsCommentService.updCommentLike(newsCommentId,userId,updFlg);
        resultModelApi.setData(updCommentLike);
        return resultModelApi;
    }

    /**
     * 评论删除方法
     * @param newsCommentId
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/delComments", method = {RequestMethod.POST})
    public ResultModelApi delComments(Long newsCommentId, Long newsId, String isFirstLevel, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        int delResult = 0;
        // 删除评论
        delResult = userNewsCommentService.delComments(newsCommentId,newsId,isFirstLevel);
        resultModelApi.setData(delResult);
        return resultModelApi;
    }

    /**
     * 评论保存方法
     * @param newsId
     * @param replyCommentId
     * @param newsComment
     * @param commentUserId
     * @param replyUserId
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/saveNewsComments", method = {RequestMethod.POST})
    public ResultModelApi saveNewsComments(Long newsId, Long replyCommentId, String newsComment, Long commentUserId, Long replyUserId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 链接识别正则
        String findLink = "(https?:\\/\\/|www\\.)[a-zA-Z_0-9\\-@]+(\\.\\w[a-zA-Z_0-9\\-:]+)+(\\/[\\(\\)~#&\\-=?\\+\\%/\\.\\w]+)?";
        // 去除动态内容链接Str
        String contentReplaceLink = ((newsComment.replaceAll(findLink,"")).replace("\\s","")).replace("\n","");
        // 审核评论内容
        if(!"".equals(contentReplaceLink)){
            List<ContentSafetyResult> contentSafetyResultList = ExamineUtil.textSafety(contentReplaceLink, ExamineUtil.SCENE_ANTISPAM);
            if (contentSafetyResultList.size() > 0) {
                if (!"pass".equals(contentSafetyResultList.get(0).getSuggestion())) {
                    resultModelApi.setData(-1);
                    return resultModelApi;
                }
            }
        }

        // 保存评论
        UserNewsComment saveResult = userNewsCommentService.saveNewsComments(newsId,replyCommentId,newsComment,commentUserId,replyUserId);
        resultModelApi.setData(saveResult);
        return resultModelApi;
    }

    /**
     * 获取动态作者信息
     *
     * @param userId 动态作者Id
     * @param loginUserId 登录者Id
     * @return 动态作者信息
     */
    @RequestMapping("/getNewsAutherInfo")
    public ResultModelApi getNewsAutherInfo(Long userId, Long loginUserId) throws ParseException {
        ResultModelApi resultModelApi = new ResultModelApi();
        Map<String, Object> resultData = new HashMap<>();
        // 作者与登录者关注状态
        List<Long> userIdList = new ArrayList<>(1);
        userIdList.add(userId);
        List<UserModel> userModelList = userService.getUserInfoList(loginUserId, userIdList);
        resultData.put("newsAutherInfo", userModelList.get(0));
        // 作者动态数
        Map<String, PageInfo> result = userNewDevelopmentsService.getAllNewsList(loginUserId, 1, 5, userId, null);
        PageInfo userNewsModelList = result.get("userNewsModelList");
        resultData.put("newsCount", userNewsModelList.getTotal());
        // 作者获赞数
        int userLikeNumCnt = userNewsCommentService.getUserLikeNumCnt(userId);
        resultData.put("userLikeNumCnt", userLikeNumCnt);
        // 作者简介
        String introduction = "";
        UserInfo newsAutherInfo = userInfoService.getUserInfo(userId);
        if (newsAutherInfo != null) {
            introduction = newsAutherInfo.getIntroduction();
        }
        resultData.put("introduction", introduction);
        resultModelApi.setData(resultData);
        return resultModelApi;
    }
}
