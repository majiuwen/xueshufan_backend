package com.keensight.web.controller;

import cn.newtouch.dl.turbocloud.constants.ResultCode;
import cn.newtouch.dl.turbocloud.controller.BaseController;
import cn.newtouch.dl.turbocloud.helper.EncryptHelper;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import cn.newtouch.dl.turbocloud.model.UserModel;
import com.alibaba.druid.util.StringUtils;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.keensight.web.constants.WebConst;
import com.keensight.web.constants.enums.FileType;
import com.keensight.web.constants.enums.UploadFolder;
import com.keensight.web.db.entity.User;
import com.keensight.web.db.entity.UserAuth;
import com.keensight.web.db.entity.UserVerificationCode;
import com.keensight.web.service.*;
import com.keensight.web.utils.AwsS3Utils;
import com.keensight.web.utils.DateUtil;
import com.keensight.web.utils.FileUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

/**
 * 注册研究领域控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/10] 新建
 */
@RestController
@Api
@RequestMapping("/register")
public class RigistController extends BaseController {

    @Resource
    private DisciplineService disciplineService;
    @Resource
    private UserService userService;
    @Resource
    private UserAuthService userAuthService;
    @Resource
    private CodeService codeService;
    @Value("${keensight.file.server.path}")
    private String keensightFileServerPath;
    @Autowired
    private UserVerificationCodeService userVerificationCodeService;
    @Resource
    private UserDisciplineService userDisciplineService;

    /**
     * 注册研究领域一级学科
     *
     * @param request
     * @return ResultModelApi
     */
    @RequestMapping(value = "/getRegistFieldDisplayNameList", method = {RequestMethod.POST})
    @ApiOperation(value = "学科")
    public ResultModelApi getRegistFieldDisplayNameList(HttpServletRequest request) {
        ResultModelApi resultModelApi = new ResultModelApi();
        resultModelApi.setData(disciplineService.getRegistFieldDisplayNameList());
        return resultModelApi;
    }

    /**
     * 注册研究领域二级学科
     *
     * @param fieldId
     * @param request
     * @return
     */
    @RequestMapping(value = "/getRegistSubFieldDisplayNameList", method = {RequestMethod.POST})
    @ApiOperation(value = "子学科")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "fieldId", value = "学科ID", required = true)})
    public ResultModelApi getRegistSubFieldDisplayNameList(String fieldId, HttpServletRequest request) {
        ResultModelApi resultModelApi = new ResultModelApi();
        Long longFieldId = Long.valueOf(fieldId);
        resultModelApi.setData(disciplineService.getRegistSubFieldDisplayNameList(longFieldId));
        return resultModelApi;
    }

    /**
     * 邮箱查重
     *
     * @param email
     * @param request
     * @return
     */
    @RequestMapping(value="/checkEmail", method = {RequestMethod.POST})
    @ApiOperation(value = "邮箱查重")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "email", value = "邮箱", required = true)
    })
    public ResultModelApi checkEmail(String email, HttpServletRequest request) {
        ResultModelApi resultModelApi = new ResultModelApi();
        resultModelApi.setData(userAuthService.checkIdentifier(email));
        return resultModelApi;
    }

    /**
     * 邮箱查重
     *
     * @param mobile
     * @param request
     * @return
     */
    @RequestMapping(value="/checkPhone", method = {RequestMethod.POST})
    @ApiOperation(value = "手机号查重")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "mobile", value = "手机号", required = true)
    })
    public ResultModelApi checkPhone(String mobile, HttpServletRequest request) {
        ResultModelApi resultModelApi = new ResultModelApi();
        resultModelApi.setData(userAuthService.checkIdentifier(mobile));
        return resultModelApi;
    }

    /**
     * 上传头像
     *
     * @param file
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/uploadPhoto", method = {RequestMethod.POST})
    @ApiOperation(value = "上传头像")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "file", value = "上传图片", required = true)})
    public ResultModelApi uploadPhoto(@RequestParam(value = "file") MultipartFile file, HttpServletResponse response) throws IOException {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 文件类型判断
        if (file == null || file.isEmpty() || !FileUtil.isSpecifyTypesFile(file.getInputStream(), new FileType[]{FileType.JPEG, FileType.PNG})) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("上传失败，错误的文件类型");
            return resultModelApi;
        }
        String newFileName;
        if (StringUtils.isEmpty(file.getOriginalFilename())) {
            newFileName = UUID.randomUUID().toString() + System.currentTimeMillis() + WebConst.FILE_SUFFIX_JPEG;
        } else {
            newFileName = UUID.randomUUID().toString() + System.currentTimeMillis() + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        }
        // 头像文件路径
        String profilePhotoPath = UploadFolder.UPLOAD.getFolderPath().concat("/").concat(UploadFolder.PROFILE_PHOTO.getFolderPath().concat("/").concat(DateUtil.getSysDate(WebConst.DATE_FORMAT_YEAR_MONTH)).concat("/"));
//        boolean uploadResult = FileUtil.fileUpload(file.getInputStream(), newFileName, keensightFileServerPath.concat(profilePhotoPath));
        // 上传到s3储存桶中
        PutObjectResult putObjectResult = AwsS3Utils.upload(profilePhotoPath + newFileName, file.getInputStream());
        if (!ObjectUtils.isEmpty(putObjectResult)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_500.code());
            resultModelApi.setRetMsg("上传失败，错误的文件类型");
            return resultModelApi;
        }
        resultModelApi.setRetCode(ResultCode.Success.code());
        HashMap<String, String> resultData = new HashMap<>();
        resultData.put("imageUrl", profilePhotoPath + newFileName);
        resultModelApi.setData(resultData);
        return resultModelApi;
    }

    /**
     * 获取职位类型
     * @return
     */
    @RequestMapping(value = "/getCodeListInfoByGroupCd", method = {RequestMethod.POST})
    @ApiOperation(value = "职位")
    public ResultModelApi getCodeListInfoByGroupCd() {
        ResultModelApi resultModelApi = new ResultModelApi();
        resultModelApi.setData(codeService.getCodeListInfoByGroupCd("user_category"));
        return resultModelApi;
    }

    /**
     * 获取在读学位类型
     * @return
     */
    @RequestMapping(value = "/getDegreeListInfoByGroupCd", method = {RequestMethod.POST})
    @ApiOperation(value = "在读学位")
    public ResultModelApi getDegreeListInfoByGroupCd() {
        ResultModelApi resultModelApi = new ResultModelApi();
        resultModelApi.setData(codeService.getCodeListInfoByGroupCd("program_degree"));
        return resultModelApi;
    }

    /**
     * 注册将数据更新到作者表及用户验证表中
     * @param email
     * @param password
     * @param userName
     * @param userId
     * @param phone
     * @param postKey
     * @param postName
     * @param mechanism
     * @param department
     * @param degree
     * @param yearStarted
     * @param photoUrl
     * @param fieldTag
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/updUserInformation", method = {RequestMethod.POST})
    @ApiOperation(value = "用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "email", value = "邮箱", required = true),
            @ApiImplicitParam(paramType = "query", name = "password", value = "密码", required = true),
            @ApiImplicitParam(paramType = "query", name = "userName", value = "用户姓名", required = true),
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户Id", required = true),
            @ApiImplicitParam(paramType = "query", name = "phone", value = "电话", required = true),
            @ApiImplicitParam(paramType = "query", name = "postKey", value = "用户类型", required = true),
            @ApiImplicitParam(paramType = "query", name = "postName", value = "用户类型名称", required = true),
            @ApiImplicitParam(paramType = "query", name = "currentPosition", value = "现在职位", required = true),
            @ApiImplicitParam(paramType = "query", name = "mechanism", value = "学术机构", required = true),
            @ApiImplicitParam(paramType = "query", name = "department", value = "院系", required = false),
            @ApiImplicitParam(paramType = "query", name = "degree", value = "在读学位", required = false),
            @ApiImplicitParam(paramType = "query", name = "yearStarted", value = "开始时间", required = false),
            @ApiImplicitParam(paramType = "query", name = "photoUrl", value = "用户头像", required = false),
            @ApiImplicitParam(paramType = "query", name = "fieldTag", value = "研究领域", required = false)
    })
    public ResultModelApi updUserInformation(String email, String password, String userName, String userId, String phone, String postKey,
                                             String postName, String currentPosition, String mechanism, String department, String degree, String yearStarted,
                                             String photoUrl, String fieldTag, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 密码加密
        String enPassword = "";
        EncryptHelper encryptHelper = new EncryptHelper();
        enPassword = encryptHelper.encrypt(password);
        // 开始时间
        Integer intYearStarted = 0;
        if (!StringUtils.isEmpty(yearStarted)) {
            intYearStarted = Integer.valueOf(yearStarted);
        }
        // 用户类型为【1：学生】时
        if ("1".equals(postKey)) {
            // 现在职位固定设置为“Student”
            currentPosition = "Student";
        }
        if(StringUtils.isEmpty(userId)) {
            // 取得作者表(t_m_user)中最大userId并加一
            Long longUserId = userService.getMaxUserId() + 1;
            // 将数据插入作者表(t_m_user)中
            int insCount = userService.insertRegist(longUserId, userName, userName, photoUrl, postKey, currentPosition, mechanism, department, degree, intYearStarted);
            // 将数据插入用户验证表(t_m_user_auth)中
            if (insCount > 0) {
                userAuthService.insertRegistEmailOrTel(longUserId, "mail", email, enPassword);
                userAuthService.insertRegistEmailOrTel(longUserId, "phone", phone, enPassword);
            }
            // 研究领域
            if (!StringUtils.isEmpty(fieldTag)) {
                // 将数据插入用户研究领域表(t_user_discipline)中
                userDisciplineService.insertUserDiscipline(longUserId, fieldTag);
            }
        } else {
            Long longUserId = Long.valueOf(userId);
            // 更新作者表(t_m_user)中数据
            int upCount = userService.updateRegist(longUserId, userName, userName, photoUrl, postKey, currentPosition, mechanism, department, degree, intYearStarted);
            // 更新用户验证表(t_m_user_auth)中数据
            if(upCount > 0) {
                int mailCount = userAuthService.updateRegistEmailOrTel(longUserId, "mail", email, enPassword);
                if (mailCount == 0) {
                    userAuthService.insertRegistEmailOrTel(longUserId, "mail", email, enPassword);
                }
                int phonCount = userAuthService.updateRegistEmailOrTel(longUserId, "phone", phone, enPassword);
                if (phonCount == 0) {
                    userAuthService.insertRegistEmailOrTel(longUserId, "phone", phone, enPassword);
                }
            }
            // 研究领域
            if (!StringUtils.isEmpty(fieldTag)) {
                // 将数据插入用户研究领域表(t_user_discipline)中
                userDisciplineService.insertUserDiscipline(longUserId, fieldTag);
            }
        }
        return resultModelApi;
    }

    /**
     * 新增用户
     * @param email
     * @param password
     * @param mobile
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/insUserInformation", method = {RequestMethod.POST})
    @ApiOperation(value = "用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "email", value = "邮箱", required = true),
            @ApiImplicitParam(paramType = "query", name = "password", value = "密码", required = true),
            @ApiImplicitParam(paramType = "query", name = "mobile", value = "电话", required = true),
            @ApiImplicitParam(paramType = "query", name = "captcha", value = "验证码", required = true),
            @ApiImplicitParam(paramType = "query", name = "requestId", value = "请求Id", required = true)
    })
    public ResultModelApi insUserInformation(String email, String password, String mobile, String requestId, String captcha, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        UserVerificationCode userVerificationCode = userVerificationCodeService.getUserVerificationCodeInfo(requestId, mobile);
        if (!ObjectUtils.isEmpty(userVerificationCode)) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date now = new Date();
            Date nowTime = sdf.parse(sdf.format(now));
            Date expirationTime = sdf.parse(sdf.format(userVerificationCode.getExpirationTime()));
            long nowTimeLong = nowTime.getTime();
            long expirationLong = expirationTime.getTime();
            if (nowTimeLong < expirationLong) {
                if(captcha.equals(userVerificationCode.getVerificationCode())) {
                    // 密码加密
                    String enPassword = "";
                    EncryptHelper encryptHelper = new EncryptHelper();
                    enPassword = encryptHelper.encrypt(password);
                    // 取得作者表(t_m_user)中最大userId并加一
                    Long longUserId = userService.getMaxUserId() + 1;
                    String userName = "用户" + longUserId.toString().substring(longUserId.toString().length() - 4);
                    // 将数据插入作者表(t_m_user)中
                    int insCount = userService.insertRegist(longUserId, userName, userName);
                    // 将数据插入用户验证表(t_m_user_auth)中
                    if (insCount > 0) {
                        insCount = userAuthService.insertRegistEmailOrTel(longUserId, "mail", email, enPassword);
                        insCount = userAuthService.insertRegistEmailOrTel(longUserId, "phone", mobile, enPassword);
                    }
                    if (insCount > 0) {
                        resultModelApi.setRetCode("0");
                        resultModelApi.setRetMsg("注册成功！");
                        // 注册成功后删除验证码
                        insCount = userVerificationCodeService.delVerificationCode(requestId, mobile);
                        if (insCount == 0) {
                            resultModelApi.setRetCode("-1");
                            resultModelApi.setRetMsg("注册失败！");
                        }
                    } else {
                        resultModelApi.setRetCode("-1");
                        resultModelApi.setRetMsg("注册失败！");
                    }
                } else {
                    resultModelApi.setRetCode("-1");
                    resultModelApi.setRetMsg("验证码错误！");
                }
            } else {
                resultModelApi.setRetCode("-1");
                resultModelApi.setRetMsg("验证码过期，请重新获取！");
            }
        } else {
            resultModelApi.setRetCode("-1");
            resultModelApi.setRetMsg("验证码错误！");
        }

        return resultModelApi;
    }
}
