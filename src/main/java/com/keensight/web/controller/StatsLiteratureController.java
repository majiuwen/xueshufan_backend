package com.keensight.web.controller;

import cn.newtouch.dl.turbocloud.controller.BaseController;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.keensight.web.service.StatsPublicationMonthlyService;
import com.keensight.web.service.StatsPublicationTotalService;
import com.keensight.web.service.StatsPublicationYearlyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 文献统计控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2021/10/26] 新建
 */
@Api(value = "文献统计")
@RestController
@RequestMapping("/statsLiterature")
public class StatsLiteratureController extends BaseController {
    @Autowired
    private StatsPublicationYearlyService statsPublicationYearlyService;
    @Autowired
    private StatsPublicationMonthlyService statsPublicationMonthlyService;
    @Autowired
    private StatsPublicationTotalService statsPublicationTotalService;

    @RequestMapping(value = "/getStatsLiterature", method = {RequestMethod.POST})
    @ApiOperation(value = "文献统计信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献id", required = true),
            @ApiImplicitParam(paramType = "query", name = "timeType", value = "时间类型", required = true)
    })
    public ResultModelApi getStatsPublicationLiterature(String publicationId, String timeType, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        if("0".equals(timeType)) {
            // 按年统计文献
            resultModelApi.setData(statsPublicationYearlyService.getStatsPublicationYearlyList(Integer.valueOf(publicationId)));
        } else {
            // 按月统计文献
            resultModelApi.setData(statsPublicationMonthlyService.getStatsPublicationMonthlyList(Integer.valueOf(publicationId)));
        }
        return resultModelApi;
    }

    @RequestMapping(value = "/getStatsPublicationTotal", method = {RequestMethod.POST})
    @ApiOperation(value = "文献统计总数")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献id", required = true)
    })
    public ResultModelApi getStatsPublicationTotal(String publicationId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 文献统计总数
        resultModelApi.setData(statsPublicationTotalService.getStatsPublicationTotal(Integer.valueOf(publicationId)));
        return resultModelApi;
    }
}
