package com.keensight.web.controller;

import cn.newtouch.dl.solr.model.SolrResultModel;
import cn.newtouch.dl.turbocloud.constants.ResultCode;
import cn.newtouch.dl.turbocloud.controller.BaseController;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageInfo;
import com.keensight.solr.service.SolrUserSearchService;
import com.keensight.web.model.UserModel;
import com.keensight.web.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * 搜索结果学者控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/23] 新建
 */
@RestController
@Api(value = "搜索结果学者")
@RequestMapping("/searchResultsScholar")
public class SearchResultsScholarController extends BaseController {

    // 建议用户关注的学者
    @Autowired
    private UserService userService;
    // Solr
    @Autowired
    private SolrUserSearchService solrUserSearchService;

    /**
     * 搜索结果学者一览
     *
     * @param userId
     * @param index
     * @param rows
     * @param keyword
     * @param autocompleteKeyword
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getSearchResultsScholarList", method = {RequestMethod.POST})
    @ApiOperation(value = "搜索结果学者一览")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数"),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示条数"),
            @ApiImplicitParam(paramType = "query", name = "keyword", value = "关键字"),
            @ApiImplicitParam(paramType = "query", name = "autocompleteKeyword", value = "自动补全关键词")
    })
    public ResultModelApi getSuggestedUserList(String userId, int index, int rows, String keyword, String autocompleteKeyword, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        PageInfo<Long> pageInfo = new PageInfo<>();
        List<Long> userIdList = new ArrayList<>();
        List<UserModel> userModelList = new ArrayList<>();

        if (!StringUtils.isEmpty(keyword) && keyword.trim().length() > 0) {
            // 根据keyword在solr中检索
            SolrResultModel solrResultModel = solrUserSearchService.getUserIdByKeyword(keyword, autocompleteKeyword, index, rows, null);
            if (solrResultModel != null) {
                List<Long> entry = solrResultModel.getCustomVal();
                if (entry != null && entry.size() > 0) {
                    for (Long user : entry) {
                        if (user != null) {
                            userIdList.add(user);
                        }
                    }
                    pageInfo.setTotal(solrResultModel.getRows());
                    pageInfo.setList(userIdList);
                    pageInfo.setPageNum(index);
                    pageInfo.setPageSize(rows);
                    pageInfo.setHasNextPage(solrResultModel.getRows() > 0 && solrResultModel.getRows() > index * rows);
                }
            }
        }
        if (userIdList != null && userIdList.size() > 0) {
            // 根据检索用户Id获取该用户关注的其他用户的详细信息
            if (!StringUtils.isEmpty(userId)) {
                userModelList = userService.getUserInfoList(Long.valueOf(userId), userIdList);
            } else {
                userModelList = userService.getUserInfoList(null, userIdList);
            }
        }

        PageInfo<UserModel> resultPageInfo = new PageInfo<>();
        if (pageInfo != null) {
            BeanUtils.copyProperties(pageInfo, resultPageInfo);
            resultPageInfo.setList(userModelList == null ? null : userModelList);
        }
        resultModelApi.setRetCode(ResultCode.Success.code());
        resultModelApi.setData(resultPageInfo);
        return resultModelApi;
    }
}
