package com.keensight.web.controller;


import cn.newtouch.dl.solr.model.SolrResultModel;
import cn.newtouch.dl.turbocloud.constants.ResultCode;
import cn.newtouch.dl.turbocloud.controller.BaseController;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageInfo;
import com.keensight.solr.service.SolrPublicationSearchService;
import com.keensight.web.constants.enums.TopItemType;
import com.keensight.web.db.entity.ConferenceSerie;
import com.keensight.web.model.PublicationModel;
import com.keensight.web.model.TopItemListModel;
import com.keensight.web.service.ConferenceSerieService;
import com.keensight.web.service.PublicationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 检索结果文献控制器
 *
 * @author : newtouch
 * @date : 2020/11/16
 */

@RestController
@Api(value = "文献")
@RequestMapping("/searchResultsArticles")
public class SearchResultsArticlesController extends BaseController {
    @Autowired
    private SolrPublicationSearchService solrPublicationSearchService;
    @Autowired
    private PublicationService publicationService;
    @Autowired
    private ConferenceSerieService conferenceSerieService;

    /**
     * 检索结果文献一览
     *
     * @param keyword
     * @param sortColumns
     * @param autocompleteKeyword
     * @param index
     * @param rows
     * @param request
     * @param yearFrom
     * @param yearTo
     * @param userIds
     * @param institutions
     * @param journalId
     * @param authors
     * @param fieldId
     * @param doctype
     * @return
     * @throws Exception
     */

    @RequestMapping(value = "/searchResultsArticlesPublication", method = {RequestMethod.POST})
    @ApiOperation(value = "文献详细信息一览取得")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "keyword", value = "关键词/题目"),
            @ApiImplicitParam(paramType = "query", name = "autocompleteKeyword", value = "自动补全关键词/题目"),
            @ApiImplicitParam(paramType = "query", name = "sortColumns", value = "排序列"),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数"),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示件数"),
            @ApiImplicitParam(paramType = "query", name = "yearFrom", value = "开始年"),
            @ApiImplicitParam(paramType = "query", name = "yearTo", value = "终了年"),
            @ApiImplicitParam(paramType = "query", name = "userIds", value = "顶尖学者"),
            @ApiImplicitParam(paramType = "query", name = "institutions", value = "顶尖机构"),
            @ApiImplicitParam(paramType = "query", name = "journalId", value = "顶尖期刊"),
            @ApiImplicitParam(paramType = "query", name = "authors", value = "顶尖会议"),
            @ApiImplicitParam(paramType = "query", name = "fieldId", value = "主要研究方向"),
            @ApiImplicitParam(paramType = "query", name = "doctype", value = "研究成果类型")
    })
    public ResultModelApi getSearchResults( int index, String keyword,
                                           String autocompleteKeyword, String sortColumns,
                                           int rows, HttpServletRequest request, String yearFrom,
                                           String yearTo, String userIds, String institutions, String journalId,
                                           String authors, String fieldId, String doctype) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 检索条件处理 start
        PageInfo<Long> pageInfo;
        List<Long> publicationIdList = new ArrayList<>();
        List<PublicationModel> detailList = new ArrayList<>();

        /*
         * 有关键词时，先从DB检索符合条件所有ID（无分页），
         * 然后从solr 根据这些ID集合以及keyword 进行检索（有分页）
         */
        pageInfo = new PageInfo<>();
        // 根据所有ID以及keyword在solr中检索
        SolrResultModel solrResultModel = solrPublicationSearchService.getPublicationIdByKeyword(keyword, autocompleteKeyword, index,
                rows, sortColumns, yearFrom, yearTo,
                userIds, institutions, journalId,
                authors, fieldId, doctype);
        if (solrResultModel != null) {
            List<Long> entry = solrResultModel.getCustomVal();
            if (entry != null && entry.size() > 0) {
                for (Long publications : entry) {
                    if (publications != null) {
                        publicationIdList.add(publications);
                    }
                }
            }
            pageInfo.setTotal(solrResultModel.getRows());
            pageInfo.setList(publicationIdList);
            pageInfo.setPageNum(index);
            pageInfo.setPageSize(rows);
            pageInfo.setHasNextPage(solrResultModel.getRows() > 0 && solrResultModel.getRows() > index * rows);
        }
        if (publicationIdList != null && publicationIdList.size() > 0) {
            // 调用共通检索获取文献详细信息一览数据
            detailList = publicationService.getPublicationDetailList(publicationIdList);
        }
        // 检索处理 end

        PageInfo<PublicationModel> resultPageInfo = new PageInfo<>();
        if (pageInfo != null) {
            BeanUtils.copyProperties(pageInfo, resultPageInfo);
            resultPageInfo.setList(detailList);
        }
        resultModelApi.setRetCode(ResultCode.Success.code());
        resultModelApi.setData(resultPageInfo);
        return resultModelApi;
    }

    /**
     * 根据实体类型获取指定排名的领域顶尖实体集合
     *
     * @param keyword
     * @param entityType
     * @param autocompleteKeyword
     * @return 排名集合
     */
    @RequestMapping(value = "/getTop10", method = {RequestMethod.POST})
    @ApiOperation(value = "根据实体类型获取指定排名的领域顶尖实体集合")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "keyword", value = "关键词/题目"),
            @ApiImplicitParam(paramType = "query", name = "entityType", value = "实体类型"),
            @ApiImplicitParam(paramType = "query", name = "autocompleteKeyword", value = "自动补全关键词/题目")
    })
    public ResultModelApi getTop10(String keyword, String entityType, String autocompleteKeyword) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 检索处理 start
        TopItemType topItemType = null;

        if (("1").equals(entityType)) {
            topItemType = TopItemType.AUTHOR;
        }
        if (("2").equals(entityType)) {
            topItemType = TopItemType.INSTITUTION;
        }
        if (("3").equals(entityType)) {
            topItemType = TopItemType.JOURNAL;
        }
        if (("4").equals(entityType)) {
            topItemType = TopItemType.CONFERENCE;
        }
        if (("5").equals(entityType)) {
            topItemType = TopItemType.FIELD;
        }
        if (("6").equals(entityType)) {
            topItemType = TopItemType.DOCTYPE;
        }

        List<TopItemListModel> topItemListModelList = solrPublicationSearchService.getTop10(keyword, autocompleteKeyword, topItemType);

        // 顶尖会议的场合
        if (("4").equals(entityType)) {
            if (topItemListModelList != null && topItemListModelList.size() > 0) {
                // 获取会议系列ID集合
                List<Long> conferenceSerieIdList = topItemListModelList.stream().map(topItemListModel -> Long.valueOf(topItemListModel.getTopItemId())).collect(Collectors.toList());
                // 根据ID集合检索会议系列集合信息
                List<ConferenceSerie> conferenceSerieList = conferenceSerieService.getConferenceSerieListByIdList(conferenceSerieIdList);
                // 变量TopItem集合
                for (TopItemListModel topItemListModel : topItemListModelList) {
                    if (topItemListModel != null) {
                        String topItemName = topItemListModel.getTopItemName();
                        if (StringUtils.isEmpty(topItemName)) {
                            topItemName = "";
                        }
                        // 根据TopItemId，从会议系列集合中取得对应信息
                        Optional<ConferenceSerie> optionalConferenceSerie = conferenceSerieList.stream().filter(conferenceSerie -> {
                            if (conferenceSerie.getConferenceSerieId() != null) {
                                return conferenceSerie.getConferenceSerieId().toString().equals(topItemListModel.getTopItemId());
                            }
                            return false;
                        }).findFirst();
                        // 设置显示的名称
                        if (optionalConferenceSerie.isPresent()) {
                            ConferenceSerie conferenceSerie = optionalConferenceSerie.get();
                            if (!StringUtils.isEmpty(conferenceSerie.getDisplayName())) {
                                topItemListModel.setTopItemName(topItemName.concat(": ").concat(conferenceSerie.getDisplayName()));
                            }
                        }
                    }
                }
            }
        }
        // 检索处理 end
        resultModelApi.setRetCode(ResultCode.Success.code());
        resultModelApi.setData(topItemListModelList);
        return resultModelApi;
    }
}
