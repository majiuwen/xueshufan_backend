package com.keensight.web.controller;

import cn.newtouch.dl.turbocloud.constants.ResultCode;
import cn.newtouch.dl.turbocloud.controller.BaseController;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.github.pagehelper.PageInfo;
import com.keensight.solr.service.SolrPublicationUpdateService;
import com.keensight.web.db.entity.*;
import com.keensight.web.model.PublicationModel;
import com.keensight.web.service.PublicationService;
import com.keensight.web.service.PublicationUserInsetitutionService;
import com.keensight.web.service.SuggestedPublicationToClaimService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@Api(value = "认领文章")
@RequestMapping("/UserClaimArticles")
public class UserClaimArticlesController extends BaseController {

    @Autowired
    private SuggestedPublicationToClaimService suggestedPublicationToClaimService;

    @Autowired
    private PublicationService publicationService;

    @Autowired
    private PublicationUserInsetitutionService publicationUserInsetitutionService;

    @Autowired
    private SolrPublicationUpdateService solrPublicationUpdateService;

    /**
     * 认领文章
     *
     * @param userId
     * @param index
     * @param rows
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/GetBookList", method = {RequestMethod.POST})
    @ApiOperation(value = "认领文章")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数", required = true),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示条数", required = true)
    })
    public ResultModelApi getBookList(String userId, int index, int rows, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();

        PageInfo<SuggestedPublicationToClaim> suggestedPublicationToClaim = suggestedPublicationToClaimService.getSuggestedPublicationToClaimByPublicationIds(Long.valueOf(userId), index, rows);

        if (suggestedPublicationToClaim.getList() != null && suggestedPublicationToClaim.getList().size() > 0) {

            List<Long> SuggestedPublicationToClaimList = new ArrayList<>();

            for (SuggestedPublicationToClaim suggestedPublicationToClaims : suggestedPublicationToClaim.getList()) {
                SuggestedPublicationToClaimList.add(suggestedPublicationToClaims.getPublicationId());
            }

            // 获取论文详细内容
            List<PublicationModel> publicationModels = publicationService.getPublicationDetailList(SuggestedPublicationToClaimList);

            PageInfo<PublicationModel> publicationModelPageInfo = new PageInfo<PublicationModel>(publicationModels);
            publicationModelPageInfo.setPageNum(suggestedPublicationToClaim.getPageNum());
            publicationModelPageInfo.setPageSize(suggestedPublicationToClaim.getPageSize());
            publicationModelPageInfo.setStartRow(suggestedPublicationToClaim.getStartRow());
            publicationModelPageInfo.setEndRow(suggestedPublicationToClaim.getEndRow());
            publicationModelPageInfo.setPages(suggestedPublicationToClaim.getPages());
            publicationModelPageInfo.setPrePage(suggestedPublicationToClaim.getPrePage());
            publicationModelPageInfo.setNextPage(suggestedPublicationToClaim.getNextPage());
            publicationModelPageInfo.setIsFirstPage(suggestedPublicationToClaim.isIsFirstPage());
            publicationModelPageInfo.setIsLastPage(suggestedPublicationToClaim.isIsLastPage());
            publicationModelPageInfo.setHasPreviousPage(suggestedPublicationToClaim.isHasPreviousPage());
            publicationModelPageInfo.setHasNextPage(suggestedPublicationToClaim.isHasNextPage());
            publicationModelPageInfo.setNavigatePages(suggestedPublicationToClaim.getNavigatePages());
            publicationModelPageInfo.setNavigatepageNums(suggestedPublicationToClaim.getNavigatepageNums());
            publicationModelPageInfo.setNavigateFirstPage(suggestedPublicationToClaim.getNavigateFirstPage());
            publicationModelPageInfo.setNavigateLastPage(suggestedPublicationToClaim.getNavigateLastPage());
            publicationModelPageInfo.setTotal(suggestedPublicationToClaim.getTotal());
            publicationModelPageInfo.setList(publicationModels);
            resultModelApi.setData(publicationModelPageInfo);
        }
        return resultModelApi;
    }

    /**
     * 更新文献、作者、机构隶属关系
     *
     * @param userId
     * @param publicationId
     * @param clickFlg
     * @param searchUserId
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/UpdatePublicationUser", method = {RequestMethod.POST})
    @ApiOperation(value = "更新文献、作者、机构隶属关系")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "作者ID", required = true),
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献ID", required = true),
            @ApiImplicitParam(paramType = "query", name = "clickFlg", value = "", required = true),
            @ApiImplicitParam(paramType = "query", name = "searchUserId", value = "", required = false)
    })
    public ResultModelApi updatePublicationUser(String userId, String publicationId, String clickFlg, String searchUserId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();

        //当前系统时间取得 设置日期格式
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dataNow = df.format(new Date());
        Timestamp time = Timestamp.valueOf(dataNow);

        // 原始作者名取得
        List<String> originalUsers = new ArrayList<>();
        List<PublicationUserInstitution> originalUsersToList = publicationUserInsetitutionService.getPublicationUserInsetitutionByUserId(Long.valueOf(userId));
        if (originalUsersToList != null && originalUsersToList.size() > 0) {
            for (PublicationUserInstitution publicationUserInstitution : originalUsersToList) {
                originalUsers.add(publicationUserInstitution.getOriginalUser());
            }
        }
        Long oldUserId = null;
        if ("200".equals(clickFlg)) {
            // 现在文献用户作者名取得
            List<String> displayNames = new ArrayList<>();
            List<Long> userIds = new ArrayList<>();
            List<User> displayNamesToList = publicationUserInsetitutionService.getDisplayNames(Long.valueOf(publicationId));
            if (displayNamesToList != null && displayNamesToList.size() > 0) {
                for (User userModel : displayNamesToList) {
                    displayNames.add(userModel.getDisplayName());
                    userIds.add(userModel.getUserId());
                }
            }

            // 用上面查找到的作者名，和现在这篇文献的作者名做比较
            String sameName = "";
            int count = -1;
            for (String displayName : displayNames) {
                count++;
                if (originalUsers.contains(displayName)) {
                    sameName = displayName;
                    break;
                }
            }
            if ("".equals(sameName)) {
                // 弹出一个画面，让客户自己去选要替换那个作者。
                resultModelApi.setRetCode("90000");
                return resultModelApi;
            } else {
                oldUserId = userIds.get(count);
            }
        } else if ("100".equals(clickFlg)) {
            oldUserId = Long.valueOf(searchUserId);
        } else {
            //无处理
        }

        // TODO
        // 在用户替换表里，增加一条数据，说明是哪个用户，替换成哪个ID了（目前没有这个表，客户要新加一个表）

        // 建议用户认领的文章，这个表里 status 字段，更新成 已认领
        String status = "2";
        suggestedPublicationToClaimService.updateSuggestedPublicationToClaim(Long.valueOf(userId), Long.valueOf(publicationId), status);

        // 统计数字变更。涉及两个用户，一个是当前login用户，所有数字增加，还有一个是原作者，他的所有数字减少。
        // 文献统计数字取得
        StatsPublicationTotal statsPublicationTotal = publicationUserInsetitutionService.getAddCountList(Long.valueOf(publicationId));
        if (statsPublicationTotal != null) {
            // 被引用次数
            int citations = statsPublicationTotal.getCitations();
            // 文献被阅读次数
            int reads = statsPublicationTotal.getReads();
            // 文献被推荐次数
            int recommendations = statsPublicationTotal.getRecommendations();
            // 文献被分享次数
            int sharings = statsPublicationTotal.getSharings();
            // 文献被保存次数
            int savings = statsPublicationTotal.getSavings();
            // 文献被下载次数
            int downloads = statsPublicationTotal.getDownloads();
            // 文献被关注次数
            int followings = statsPublicationTotal.getFollowings();
            // 被举报次数
            int reported = statsPublicationTotal.getReported();

            // 旧作者文献统计总数减少更新
            publicationUserInsetitutionService.updateOldStatsUserTotal(oldUserId, citations, reads, recommendations, sharings, savings, downloads, followings, reported, time);

            // 新作者文献统计总数增加更新
            int updateCount = publicationUserInsetitutionService.updateNewStatsUserTotal(Long.valueOf(userId), citations, reads, recommendations, sharings, savings, downloads, followings, reported, time);
            if (updateCount == 0) {
                // 新作者文献统计总数增加新规追加
                publicationUserInsetitutionService.insertNewStatsUserTotal(Long.valueOf(userId), citations, reads, recommendations, sharings, savings, downloads, followings, reported, time);
            }
        }

        // 文献年度统计取得
        List<StatsPublicationYearly> statsPublicationYearly = publicationUserInsetitutionService.getPublicationYear(Long.valueOf(publicationId));
        if (statsPublicationYearly != null) {
            for (StatsPublicationYearly Yearly : statsPublicationYearly) {
                //年
                Date year = Yearly.getYear();
                // 被引用次数
                int yearCitations = Yearly.getCitations();
                // 文献被阅读次数
                int yearReads = Yearly.getReads();
                // 文献被推荐次数
                int yearRecommendations = Yearly.getRecommendations();
                // 文献被分享次数
                int yearSharings = Yearly.getSharings();
                // 文献被保存次数
                int yearSavings = Yearly.getSavings();
                // 文献被下载次数
                int yearDownloads = Yearly.getDownloads();
                // 文献被关注次数
                int yearFollowings = Yearly.getFollowings();
                // 被举报次数
                int yearReported = Yearly.getReported();

                // 旧作者文献年度统计总数减少更新
                publicationUserInsetitutionService.updateOldStatsUsersYearly(oldUserId, year, yearCitations, yearReads, yearRecommendations, yearSharings, yearSavings, yearDownloads, yearFollowings, yearReported, time);
                // 新作者文献年度统计总数增加更新
                int updateCount = publicationUserInsetitutionService.updateNewStatsUserYearly(Long.valueOf(userId), year, yearCitations, yearReads, yearRecommendations, yearSharings, yearSavings, yearDownloads, yearFollowings, yearReported, time);
                if (updateCount == 0) {
                    // 新作者文献年度统计总数增加新规追加
                    publicationUserInsetitutionService.insertNewStatsPublicationYearly(Long.valueOf(userId), year, yearCitations, yearReads, yearRecommendations, yearSharings, yearSavings, yearDownloads, yearFollowings, yearReported, time);
                }
            }
        }

        // 文献月统计取得
        List<StatsPublicationMonthly> statsPublicationMonthly = publicationUserInsetitutionService.getPublicationMonthly(Long.valueOf(publicationId));
        if (statsPublicationMonthly != null) {
            for (StatsPublicationMonthly Monthly : statsPublicationMonthly) {
                // 月
                Date month = Monthly.getMonth();
                // 被引用次数
                int monthCitations = Monthly.getCitations();
                // 文献被阅读次数
                int monthReads = Monthly.getReads();
                // 文献被推荐次数
                int monthRecommendations = Monthly.getRecommendations();
                // 文献被分享次数
                int monthSharings = Monthly.getSharings();
                // 文献被保存次数
                int monthSavings = Monthly.getSavings();
                // 文献被下载次数
                int monthDownloads = Monthly.getDownloads();
                // 文献被关注次数
                int monthFollowings = Monthly.getFollowings();
                // 被举报次数
                int monthReported = Monthly.getReported();

                // 旧作者文献月度统计总数减少更新
                publicationUserInsetitutionService.updateOldStatsUserMonth(oldUserId, month, monthCitations, monthReads, monthRecommendations, monthSharings, monthSavings, monthDownloads, monthFollowings, monthReported, time);
                // 新作者文献月度统计总数增加更新
                int updateCount = publicationUserInsetitutionService.updateNewStatsUserMonth(Long.valueOf(userId), month, monthCitations, monthReads, monthRecommendations, monthSharings, monthSavings, monthDownloads, monthFollowings, monthReported, time);
                if (updateCount == 0) {
                    // 新作者文献月度统计总数增加新规追加
                    publicationUserInsetitutionService.insertNewStatsUserMonth(Long.valueOf(userId), month, monthCitations, monthReads, monthRecommendations, monthSharings, monthSavings, monthDownloads, monthFollowings, monthReported, time);
                }
            }
        }

        // 文献周统计取得
        List<StatsPublicationWeekly> statsPublicationWeekly = publicationUserInsetitutionService.getPublicationWeekly(Long.valueOf(publicationId));
        if (statsPublicationWeekly != null) {
            for (StatsPublicationWeekly Weekly : statsPublicationWeekly) {
                // 周
                Date week = Weekly.getWeek();
                // 被引用次数
                int weekCitations = Weekly.getCitations();
                // 文献被阅读次数
                int weekReads = Weekly.getReads();
                // 文献被推荐次数
                int weekRecommendations = Weekly.getRecommendations();
                // 文献被分享次数
                int weekSharings = Weekly.getSharings();
                // 文献被保存次数
                int weekSavings = Weekly.getSavings();
                // 文献被下载次数
                int weekDownloads = Weekly.getDownloads();
                // 文献被关注次数
                int weekFollowings = Weekly.getFollowings();
                // 被举报次数
                int weekReported = Weekly.getReported();

                // 旧作者周文献统计总数减少更新
                publicationUserInsetitutionService.updateOldStatsUserWeek(oldUserId, week, weekCitations, weekReads, weekRecommendations, weekSharings, weekSavings, weekDownloads, weekFollowings, weekReported, time);
                // 新作者周文献统计总数增加更新
                int updateCount = publicationUserInsetitutionService.updateNewStatsUserWeek(Long.valueOf(userId), week, weekCitations, weekReads, weekRecommendations, weekSharings, weekSavings, weekDownloads, weekFollowings, weekReported, time);
                if (updateCount == 0) {
                    // 新作者周文献统计总数增加新规追加
                    publicationUserInsetitutionService.insertNewStatsUserWeek(Long.valueOf(userId), week, weekCitations, weekReads, weekRecommendations, weekSharings, weekSavings, weekDownloads, weekFollowings, weekReported, time);
                }
            }
        }

        // 文献(文献、专利、著作等)取得
        Publication publication = publicationUserInsetitutionService.getPublication(Long.valueOf(publicationId));
        if (publication != null) {
            // 出版或发表日期
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String searchTime = dateFormat.format(publication.getDate());
            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
            Date date = ft.parse(searchTime);
            Calendar calendar = Calendar.getInstance(Locale.CHINA);

            // 年
            int year = Integer.parseInt(searchTime.substring(0, 4));
            Calendar yearCalendar = Calendar.getInstance();
            yearCalendar.clear();
            yearCalendar.set(Calendar.YEAR, year);
            yearCalendar.roll(Calendar.DAY_OF_YEAR, -1);
            Date currYearLast = yearCalendar.getTime();
            Date yearPublications = currYearLast;

            // 月
            calendar.setTime(date);
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
            Date monthPublications = calendar.getTime();

            // 周
            calendar.setFirstDayOfWeek(Calendar.MONDAY);
            calendar.setTime(date);
            calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
            Date weekPublications = calendar.getTime();

            // 旧作者文献年度统计总数的作者发文数减少更新
            publicationUserInsetitutionService.updateOldStatsUsersYearlyPublications(oldUserId, yearPublications, time);
            // 新作者文献年度统计总数的作者发文数增加更新
            publicationUserInsetitutionService.updateNewStatsUserYearlyPublications(Long.valueOf(userId), yearPublications, time);
            // 旧作者文献月度统计总数的作者发文数减少更新
            publicationUserInsetitutionService.updateOldStatsUserMonthPublications(oldUserId, monthPublications, time);
            // 新作者文献月度统计总数的作者发文数增加更新
            publicationUserInsetitutionService.updateNewStatsUserMonthPublications(Long.valueOf(userId), monthPublications, time);
            // 旧作者文献周统计总数的作者发文数减少更新
            publicationUserInsetitutionService.updateOldStatsUserWeekPublications(oldUserId, weekPublications, time);
            // 新作者文献周统计总数的作者发文数增加更新
            publicationUserInsetitutionService.updateNewStatsUserWeekPublications(Long.valueOf(userId), weekPublications, time);
        }

        // 共同作者表
        // 通过，文献，作者，机构名关系表  当前文献的共同作者取得
        List<PublicationUserInstitution> publicationUserInstitution = publicationUserInsetitutionService.getSameWriterId(Long.valueOf(publicationId), oldUserId, Long.valueOf(userId));
        if (publicationUserInstitution != null) {
            for (PublicationUserInstitution PublicationUserInstitution : publicationUserInstitution) {

                Long secondUserId = PublicationUserInstitution.getUserId();
                // 原作者共同发表的文献数减1更新
                publicationUserInsetitutionService.updateOldSameUser(oldUserId, secondUserId);
                // 新作者共同发表的文献数加1更新
                int updateCount = publicationUserInsetitutionService.updateNewSameUser(Long.valueOf(userId), secondUserId);
                if (updateCount == 0) {
                    // 当前login用户共同作者表新规追加
                    publicationUserInsetitutionService.insertNewSameUserId1(Long.valueOf(userId), secondUserId);
                    publicationUserInsetitutionService.insertNewSameUserId2(Long.valueOf(userId), secondUserId);
                } else if (updateCount == 1) {
                    // 单条共同作者的存在查询
                    UserCoauthor userCoauthor = publicationUserInsetitutionService.getUserCoauthor(Long.valueOf(userId), secondUserId);
                    if (userCoauthor != null) {
                        publicationUserInsetitutionService.insertNewSameUserId2(Long.valueOf(userId), secondUserId);
                    } else {
                        publicationUserInsetitutionService.insertNewSameUserId1(Long.valueOf(userId), secondUserId);
                    }
                }
            }
        }
        // 作者发表论文起始年份
        // TODO 这个表先不更新

        // 用户研究领域
        // 文献研究领域表查询
        List<PublicationField> publicationField = publicationUserInsetitutionService.getPublicationField(Long.valueOf(publicationId));
        if (publicationField != null) {
            for (PublicationField PublicationField : publicationField) {

                // 原用户用户研究领域表的文献数减1更新
                publicationUserInsetitutionService.updateOldUserField(oldUserId, PublicationField.getFieldId(), time);
                // 新用户用户研究领域表的文献数加1更新
                int updateCount = publicationUserInsetitutionService.updateNewUserField(Long.valueOf(userId), PublicationField.getFieldId(), time);
                if (updateCount == 0) {
                    // 新用户用户研究领域表新规追加
                    publicationUserInsetitutionService.insertNewUserField(Long.valueOf(userId), PublicationField.getFieldId(), time);
                }
                // 用户学科与专业
                // 检索学科和子学科表
                List<Discipline> discipline = publicationUserInsetitutionService.getDiscipline(PublicationField.getFieldId());
                if (discipline != null) {
                    // 原用户用户学科与专业表的文献数-1（更新条件用户ID是原用户ID，研究领域是查出来的研究领域）
                    publicationUserInsetitutionService.updateOldUserDiscipline(oldUserId, PublicationField.getFieldId(), time);
                    // 当前login用户，用户学科与专业表的文献数+1
                    int updateDisciplineCount = publicationUserInsetitutionService.updateNewUserDiscipline(Long.valueOf(userId), PublicationField.getFieldId(), time);
                    if (updateDisciplineCount == 0) {
                        // 当前login用户用户学科与专业表新规追加
                        publicationUserInsetitutionService.insertNewUserDiscipline(Long.valueOf(userId), PublicationField.getFieldId(), time);
                    }
                }
            }
        }
        // 更新文献，作者，机构名关系表的作者ID，把原作者的user_id，更新成login的user_id。
        publicationUserInsetitutionService.updateNewPublicationUserInsetitution(oldUserId, Long.valueOf(userId), Long.valueOf(publicationId));

        // login用户名取得
        User newUserDisplayName = publicationUserInsetitutionService.getUserDisplayName(Long.valueOf(userId));
        // 文章原作者名取得
        User oldUserDisplayName = publicationUserInsetitutionService.getUserDisplayName(oldUserId);
        // solr更新
        boolean bolRet = solrPublicationUpdateService.updatePublicationAuthor(Long.valueOf(publicationId), oldUserId, oldUserDisplayName.getDisplayName(), Long.valueOf(userId), newUserDisplayName.getDisplayName());

        if (!bolRet) {
            resultModelApi.setRetCode(ResultCode.Request_Error_500.code());
            resultModelApi.setRetMsg("更新失败");
            return resultModelApi;
        }

        return resultModelApi;
    }

    /**
     * 更新需要用户认领的文章
     *
     * @param userId
     * @param publicationId
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/UpdateSuggestedPublicationToClaim", method = {RequestMethod.POST})
    @ApiOperation(value = "更新需要用户认领的文章")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "作者ID", required = true),
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献ID", required = true)
    })
    public ResultModelApi updateSuggestedPublicationToClaim(String userId, String publicationId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        String status = "3";
        suggestedPublicationToClaimService.updateSuggestedPublicationToClaim(Long.valueOf(userId), Long.valueOf(publicationId), status);
        return resultModelApi;
    }

    /**
     * 获取原始用户信息
     *
     * @param publicationId
     * @param request
     * @param index
     * @param rows
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/GetOldInformation", method = {RequestMethod.POST})
    @ApiOperation(value = "获取原始用户名")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献ID", required = true),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数", required = true),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示条数", required = true),
            @ApiImplicitParam(paramType = "query", name = "keyword", value = "关键字", required = false)

    })
    public ResultModelApi getOldInformation(String publicationId, int index, int rows, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        resultModelApi.setData(publicationUserInsetitutionService.getOldInformation(Long.valueOf(publicationId), index, rows));
        return resultModelApi;
    }
}

