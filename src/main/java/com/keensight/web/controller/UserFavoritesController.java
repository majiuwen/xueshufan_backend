package com.keensight.web.controller;

import cn.newtouch.dl.solr.model.SolrResultModel;
import cn.newtouch.dl.turbocloud.constants.ResultCode;
import cn.newtouch.dl.turbocloud.controller.BaseController;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageInfo;
import com.keensight.solr.service.SolrPublicationSearchService;
import com.keensight.web.db.entity.UserLibraryLabel;
import com.keensight.web.model.PublicationModel;
import com.keensight.web.service.PublicationService;
import com.keensight.web.service.UserActionPublicationService;
import com.keensight.web.service.UserFavoritesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 用户收藏控制器
 *
 * @author : newtouch
 * @date : 2020/11/05
 */
@RestController
@Api(value = "用户收藏")
@RequestMapping("/userFavorites")
public class UserFavoritesController extends BaseController {

    // 用户收藏
    @Autowired
    private UserFavoritesService userFavoritesService;
    // 文献详细信息一览取得
    @Autowired
    private PublicationService publicationService;
    @Autowired
    private SolrPublicationSearchService solrPublicationSearchService;
    @Autowired
    private UserActionPublicationService userActionPublicationService;

    /**
     * 用户收藏一览
     * @param userId
     * @param index
     * @param keyword
     * @param autocompleteKeyword
     * @param sortColumns
     * @param rows
     * @param publicationGrades
     * @param labelIds
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/userFavoritesPublication", method = {RequestMethod.POST})
    @ApiOperation(value = "用户收藏一览信息取得")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "keyword", value = "关键词/题目"),
            @ApiImplicitParam(paramType = "query", name = "autocompleteKeyword", value = "自动补全关键词/题目"),
            @ApiImplicitParam(paramType = "query", name = "sortColumns", value = "排序列"),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数"),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示件数"),
            @ApiImplicitParam(paramType = "query", name = "publicationGrades", value = "星级"),
            @ApiImplicitParam(paramType = "query", name = "labelIds", value = "标签id"),
            @ApiImplicitParam(paramType = "query", name = "folderId", value = "文件夹id")
    })
    public ResultModelApi getUserFavorites(String userId, int index, String keyword, String autocompleteKeyword, String sortColumns, int rows, String publicationGrades, String labelIds, Long folderId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 检索条件处理 start
        if (StringUtils.isEmpty(userId) || !StringUtils.isNumber(userId)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }
        // 检索条件处理 end

        // 检索处理 start
        Long searchUserId = Long.valueOf(userId);
        PageInfo<Long> pageInfo;
        List<Long> publicationIdList = null;
        List<PublicationModel> detailList = new ArrayList<>();
        if (!StringUtils.isEmpty(keyword) && keyword.trim().length() > 0) {
            /*
             * 有关键词时，先从DB检索符合条件所有ID（无分页），
             * 然后从solr 根据这些ID集合以及keyword 进行检索（有分页）
             */
            pageInfo = getPublicationIdList(searchUserId, sortColumns, 1, 0, publicationGrades, labelIds, folderId);
            if (pageInfo != null) {
                publicationIdList = pageInfo.getList();
            }
            // 根据所有ID以及keyword在solr中检索
            if (publicationIdList != null && publicationIdList.size() > 0) {
                SolrResultModel solrResultModel = solrPublicationSearchService.getPublicationIdByKeyword(keyword, autocompleteKeyword, index, rows, publicationIdList);
                publicationIdList.clear();
                if (solrResultModel != null) {
                    List<Long> entry = solrResultModel.getCustomVal();
                    if (entry != null && entry.size() > 0) {
                        for (Long publication : entry) {
                            if (publication != null) {
                                publicationIdList.add(publication);
                            }
                        }
                    }
                    pageInfo.setTotal(solrResultModel.getRows());
                    pageInfo.setList(publicationIdList);
                    pageInfo.setPageNum(index);
                    pageInfo.setPageSize(rows);
                    pageInfo.setHasNextPage(solrResultModel.getRows() > 0 && solrResultModel.getRows() > index * rows);
                }
            }
        } else {
            /*
             * 没有关键词时，只从DB中检索符合条件的ID（有分页）
             */
            // 根据检索条件查询文献ID集合
            pageInfo = getPublicationIdList(searchUserId, sortColumns, index, rows, publicationGrades, labelIds, folderId);
            if (pageInfo != null) {
                publicationIdList = pageInfo.getList();
            }
        }
        if (publicationIdList != null && publicationIdList.size() > 0) {
            // 去重
            List<Long> publicationIds = publicationIdList.stream().distinct().collect(Collectors.toList());
            // 调用共通检索获取文献详细信息一览数据
            detailList = publicationService.getPublicationDetailList(publicationIds, true);

            for(PublicationModel detail : detailList){
                detail.setClickFolderId(folderId);
            }
        }
        // 检索处理 end

        PageInfo<PublicationModel> resultPageInfo = new PageInfo<>();
        if (pageInfo != null) {
            BeanUtils.copyProperties(pageInfo, resultPageInfo);
            resultPageInfo.setList(detailList);
        }
        resultModelApi.setRetCode(ResultCode.Success.code());
        resultModelApi.setData(resultPageInfo);
        return resultModelApi;
    }

    /**
     * 符合条件的文献ID集合取得方法
     * @param userId
     * @param sortColumns
     * @param pageNum
     * @param pageSize
     * @param publicationGrades
     * @param labelIds
     * @return
     */
    private PageInfo<Long> getPublicationIdList(Long userId, String sortColumns, Integer pageNum, Integer pageSize, String publicationGrades, String labelIds, Long folderId) {
        if(folderId != null && folderId == -3){
            folderId = null;
        }
        PageInfo<Long> pageInfo;
        pageInfo = userFavoritesService.getUserFavoritesIdList(userId, sortColumns, pageNum, pageSize, publicationGrades, labelIds, folderId);
        return pageInfo;
    }

    /**
     * 个人收藏文件夹信息取得
     * @param userId
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getUserLibraryFolderInfo", method = {RequestMethod.POST})
    @ApiOperation(value = "个人收藏文件夹信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getUserLibraryFolderInfo(String userId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 用户id
        if (StringUtils.isEmpty(userId) || !StringUtils.isNumber(userId)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }
        // 个人收藏文件夹信息取得
        resultModelApi.setData(userFavoritesService.getUserLibraryFolderList(Integer.valueOf(userId)).get(Long.valueOf(0)).getChildFolder());
        return resultModelApi;
    }

    /**
     * 增加个人收藏文件夹
     * @param userId
     * @param folderId
     * @param folderName
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/insertUserLibraryFolder", method = {RequestMethod.POST})
    @ApiOperation(value = "增加个人收藏文件夹信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "folderId", value = "父文件夹id", required = true),
            @ApiImplicitParam(paramType = "query", name = "folderName", value = "文件夹名称", required = true)
    })
    public ResultModelApi insertUserLibraryFolder(String userId, String folderId, String folderName, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 增加个人收藏文件夹
        int insertFlg = userFavoritesService.insertUserLibraryFolder(Long.valueOf(userId), Long.valueOf(folderId), folderName);
        if(insertFlg > 0) {
            // 新增文件夹成功
            resultModelApi.setData(insertFlg);
            return resultModelApi;
        } else {
            // 新增文件夹失败
            resultModelApi.setRetCode("-1");
            resultModelApi.setRetMsg("文件夹名称重复，新增失败");
            return resultModelApi;
        }
    }

    /**
     * 修改个人收藏文件夹
     * @param folderId
     * @param folderName
     * @param userId
     * @param parentFolderId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/updateUserLibraryFolder", method = {RequestMethod.POST})
    @ApiOperation(value = "修改个人收藏文件夹信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "folderId", value = "文件夹id", required = true),
            @ApiImplicitParam(paramType = "query", name = "folderName", value = "文件夹名称", required = true),
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "parentFolderId", value = "父文件夹id", required = true)
    })
    public ResultModelApi updateUserLibraryFolder(String folderId, String folderName, String userId, String parentFolderId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 修改个人收藏文件夹
        int updateFlg = userFavoritesService.updateUserLibraryFolder(Long.valueOf(folderId), folderName, Long.valueOf(userId), Long.valueOf(parentFolderId));
        if(updateFlg > 0) {
            // 修改文件夹成功
            resultModelApi.setData(updateFlg);
            return resultModelApi;
        } else {
            // 修改文件夹失败
            resultModelApi.setRetCode("-1");
            resultModelApi.setRetMsg("文件夹名称重复，修改失败");
            return resultModelApi;
        }
    }

    /**
     * 删除个人收藏文件夹
     * @param folderId
     * @param userId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/deleteUserLibraryFolder", method = {RequestMethod.POST})
    @ApiOperation(value = "删除个人收藏文件夹信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "folderId", value = "文件夹id", required = true),
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi deleteUserLibraryFolder(String folderId, String userId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();

        // 删除个人收藏文件夹
        int deleteFlg = userFavoritesService.deleteUserLibraryFolder(Long.valueOf(folderId), Long.valueOf(userId));
        if(deleteFlg > 0) {
            // 删除文件夹成功
            resultModelApi.setData(deleteFlg);
            return resultModelApi;
        } else {
            // 删除文件夹失败
            resultModelApi.setRetCode("-1");
            resultModelApi.setRetMsg("文件夹删除失败");
            return resultModelApi;
        }
    }

    /**
     * 文章收藏/删除到个人收藏文件夹
     * @param userId
     * @param publicationId
     * @param authorIds
     * @param actionFlg
     * @param operationalBehaviorFlg
     * @param folderIds
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/updPublicationToFolder", method = {RequestMethod.POST})
    @ApiOperation(value = "文章收藏/删除到个人收藏文件夹")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献id", required = true),
            @ApiImplicitParam(paramType = "query", name = "authorIds", value = "作者id", required = true),
            @ApiImplicitParam(paramType = "query", name = "actionFlg", value = "区分用户操作Flg", required = true),
            @ApiImplicitParam(paramType = "query", name = "operationalBehaviorFlg", value = "区分用户操作行为Flg", required = true),
            @ApiImplicitParam(paramType = "query", name = "folderIds", value = "文件夹id", required = true),
            @ApiImplicitParam(paramType = "query", name = "hasFolder", value = "是否未分类", required = true),
            @ApiImplicitParam(paramType = "query", name = "deletePrivateFlg", value = "用户删除私有文献Flg", required = true),
            @ApiImplicitParam(paramType = "query", name = "docId", value = "文件Id", required = true)
    })
    public ResultModelApi updPublicationToFolder(String userId, String publicationId,
                                                 String authorIds, String actionFlg,
                                                 Boolean operationalBehaviorFlg,
                                                 String folderIds, boolean hasFolder,
                                                 Boolean deletePrivateFlg,
                                                 Long docId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        userActionPublicationService.updUserAction(Long.valueOf(userId), Long.valueOf(publicationId), authorIds, actionFlg, operationalBehaviorFlg, folderIds, hasFolder, deletePrivateFlg, docId);
        return resultModelApi;
    }

    /**
     * 修改星级
     * @param userId
     * @param publicationId
     * @param publicationGrade
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/updPublicationGrade", method = {RequestMethod.POST})
    @ApiOperation(value = "修改星级")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献id", required = true),
            @ApiImplicitParam(paramType = "query", name = "publicationGrade", value = "星级", required = true)
    })
    public ResultModelApi updPublicationGrade(String userId, String publicationId, String publicationGrade, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 修改星级
        int updateFlg = userFavoritesService.updateUserLibraryGrade(Long.valueOf(userId), Long.valueOf(publicationId), publicationGrade);
        if(updateFlg > 0) {
            // 修改星级成功
            resultModelApi.setData(updateFlg);
            return resultModelApi;
        } else {
            // 修改星级失败
            resultModelApi.setRetCode("-1");
            resultModelApi.setRetMsg("修改星级失败");
            return resultModelApi;
        }
    }

    /**
     * 增加标签
     * @param userId
     * @param publicationId
     * @param labelName
     * @return
     */
    @RequestMapping(value = "/addLabel", method = {RequestMethod.POST})
    @ApiOperation(value = "增加标签")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献id", required = true),
            @ApiImplicitParam(paramType = "query", name = "labelName", value = "标签名称", required = true)
    })
    public ResultModelApi addUserLibraryLabelPublication(Long userId, Long publicationId, String labelName) {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 增加标签
        UserLibraryLabel result = userFavoritesService.addLabel(userId, publicationId, labelName);
        if(result != null) {
            // 增加标签成功
            resultModelApi.setData(result);
            return resultModelApi;
        } else {
            // 增加标签失败
            resultModelApi.setRetCode("-1");
            resultModelApi.setRetMsg("标签名称重复，增加标签失败");
            return resultModelApi;
        }
    }

    /**
     * 删除标签
     * @param labelId
     * @param publicationId
     * @return
     */
    @RequestMapping(value = "/delLabel", method = {RequestMethod.POST})
    @ApiOperation(value = "删除标签")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "labelId", value = "标签id", required = true),
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献id", required = true)
    })
    public ResultModelApi delUserLibraryLabelPublication(Long labelId, Long publicationId){
        ResultModelApi resultModelApi=new ResultModelApi();
        // 删除标签
        int result = userFavoritesService.delLabel(labelId, publicationId);
        if(result > 0) {
            // 删除成功
            resultModelApi.setData(result);
            return resultModelApi;
        } else {
            resultModelApi.setRetCode("-1");
            resultModelApi.setRetMsg("删除失败");
            return resultModelApi;
        }
    }

    /**
     * 标签自动补全
     * @param userId
     * @param labelName
     * @return
     */
    @RequestMapping(value = "/autocompeleteLabelName", method = {RequestMethod.POST})
    @ApiOperation(value = "标签自动补全")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "labelName", value = "标签名称", required = true)
    })
    public ResultModelApi autocompeleteLabelName(Long userId, String labelName){
        ResultModelApi resultModelApi=new ResultModelApi();
        // 标签自动补全
        List<UserLibraryLabel> labelList = userFavoritesService.autocompeleteLabelName(userId, labelName);
        resultModelApi.setData(labelList);
        return resultModelApi;
    }
}


