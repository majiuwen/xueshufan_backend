package com.keensight.web.controller;

import cn.newtouch.dl.solr.util.StringUtil;
import cn.newtouch.dl.turbocloud.constants.ResultCode;
import cn.newtouch.dl.turbocloud.controller.BaseController;
import cn.newtouch.dl.turbocloud.helper.FileHelper;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.keensight.web.constants.enums.UploadFolder;
import com.keensight.web.db.entity.UserInfo;
import com.keensight.web.helper.FileServerHelper;
import com.keensight.web.model.UserInfoModel;
import com.keensight.web.service.UserInfoService;
import com.keensight.web.utils.AwsS3Utils;
import com.keensight.web.utils.ContentSafetyResult;
import com.keensight.web.utils.DateUtil;
import com.keensight.web.utils.ExamineUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Properties;

/**
 * 用户个人资料控制器
 *
 * @author : newtouch
 * @date : 2022/1/12
 */
@Api(value = "用户个人资料")
@RestController
@RequestMapping("/userInfo")
public class UserInfoController extends BaseController {

    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private FileHelper fileHelper;
    @Autowired
    private FileServerHelper fileServerHelper;

    /**
     * 用户个人资料获取
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/getUserInfo", method = {RequestMethod.POST})
    @ApiOperation(value = "用户个人资料信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getUserInfo(Long userId) {
        ResultModelApi resultModelApi = new ResultModelApi();
        UserInfoModel userInfoModel = new UserInfoModel();
        UserInfo userInfo = userInfoService.getUserInfo(userId);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if(userInfo != null){
            BeanUtils.copyProperties(userInfo, userInfoModel);
            if (StringUtil.isNotEmpty(userInfoModel.getProfilePath())) {
                String profilePath_s3 = AwsS3Utils.getS3FileUrl(userInfoModel.getProfilePath());
                userInfoModel.setProfilePath(profilePath_s3);
            }
            if(userInfo.getBirthday() != null){
                userInfoModel.setBirthdayStr(sdf.format(userInfo.getBirthday()));
            }
        }
        resultModelApi.setData(userInfoModel);
        return resultModelApi;
    }

    /**
     * 修改用户个人资料
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/updUserInfo", method = {RequestMethod.POST})
    @ApiOperation(value = "修改用户个人资料信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi updUserInfo(Long userId, String avatar, String avatarSource, String nickname, String introduction, String sex, String province, String city, String birthday, Integer avatarFlg, Integer nameFlg, Integer introductionFlg) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        Boolean avatarExamineFlg = true;
        // 头像审核
        if (avatarFlg == 1) {
            int avatarExamine = toExamine ("image", avatar);
            if (avatarExamine == -1) {
                avatarExamineFlg = false;
//                resultModelApi.setRetCode(ResultCode.Auth_Error__Avatar_Examine.code());
//                resultModelApi.setRetMsg("头像审核未通过！");
//                return resultModelApi;
            }
        }

        // 昵称审核
        if (nameFlg == 1) {
            int nicknameExamine = toExamine ("text", nickname);
            if (nicknameExamine == -1) {
                nickname = null;
//                resultModelApi.setRetCode(ResultCode.Auth_Error__Nickname_Examine.code());
//                resultModelApi.setRetMsg("昵称审核未通过！");
//                return resultModelApi;
            }
        }

        // 介绍审核
        if (introductionFlg == 1) {
            int introductionExamine = toExamine ("text", introduction);
            if (introductionExamine == -1) {
                introduction = null;
//                resultModelApi.setRetCode(ResultCode.Auth_Error__Introduction_Examine.code());
//                resultModelApi.setRetMsg("个人介绍审核未通过！");
//                return resultModelApi;
            }
        }

        // 获取文件存储路径
        //创建Properties对象
        Properties prop = new Properties();
        //读取classPath中的properties文件
        prop.load(UserInfoController.class.getClassLoader().getResourceAsStream("application.properties"));
        // 文件服务器总路径
        String filePath = prop.getProperty("keensight.file.server.path");
        //String filePath = "C:/test666/";
        // 用户头像文件上传路径
        String avatorFilePath = prop.getProperty("userInfo.avatar.path");
        // 获取当前时间时间戳
        String timeStamp = DateUtil.getSysDate("yyyyMMddHHmmssSSS");
        // 文件名
        String fileName = timeStamp + ".jpg";
        // 文件名
        String sourceFileName = timeStamp + "_source.jpg";
        // 上传是否成功flg
        Boolean isSave = false;
        // 文件上传成功处理
        int count = userInfoService.count(userId);
        UserInfo userInfo;
        if (count > 0) {
            String avatarStr = null;
            if (avatarExamineFlg) {
                if(avatarFlg > 0){
                    fileHelper.deleteGeneralFile(filePath + avatorFilePath + File.separator + userId + File.separator);
                    // 将base64文件保存到服务器指定路径
                    // 判断服务器是否有该文件夹
//                    if(fileHelper.mkDir(filePath + avatorFilePath + File.separator + userId + File.separator)) {
//                        isSave = fileHelper.saveImg(avatar, filePath + avatorFilePath + File.separator + userId + File.separator, fileName);
//                        isSave = fileHelper.saveImg(avatarSource, filePath + avatorFilePath + File.separator + userId + File.separator, sourceFileName);
//                    }
                    // 头像切图
                    if (avatar.contains("data:")) {
                        int start = avatar.indexOf(",");
                        avatar = avatar.substring(start + 1);
                    }
                    final Base64.Decoder decoder = Base64.getDecoder();
                    avatar = avatar.replaceAll("\r|\n", "");
                    avatar = avatar.trim();
                    byte[] avatarByteArray = decoder.decode(avatar);
                    InputStream avatarInputStream = new ByteArrayInputStream(avatarByteArray);
                    // 上传到s3储存桶中
                    AwsS3Utils.upload(avatorFilePath + "/" + userId + "/" + fileName, avatarInputStream);
                    // 头像原图
                    if (avatarSource.contains("data:")) {
                        int start = avatarSource.indexOf(",");
                        avatarSource = avatarSource.substring(start + 1);
                    }
                    avatarSource = avatarSource.replaceAll("\r|\n", "");
                    avatarSource = avatarSource.trim();
                    byte[] avatarSourceByteArray = decoder.decode(avatarSource);
                    InputStream inputStream = new ByteArrayInputStream(avatarSourceByteArray);
                    // 上传到s3储存桶中
                    AwsS3Utils.upload(avatorFilePath + "/" + userId + "/" + sourceFileName, inputStream);
                }
                avatarStr = avatorFilePath + "/" + userId + "/" + fileName;
            }
            userInfo = userInfoService.update(userId, avatarStr, nickname, introduction, sex, province, city, birthday, avatarFlg);
        } else {
            // 将base64文件保存到服务器指定路径
            // 判断服务器是否有该文件夹
            String avatarStr = null;
            if (avatarExamineFlg) {
//                if(fileHelper.mkDir(filePath + avatorFilePath + File.separator + userId + File.separator)) {
//                    isSave = fileHelper.saveImg(avatar, filePath + avatorFilePath + File.separator + userId + File.separator, fileName);
//                    isSave = fileHelper.saveImg(avatarSource, filePath + avatorFilePath + File.separator + userId + File.separator, sourceFileName);
//                }
                // 头像切图
                if (avatar.contains("data:")) {
                    int start = avatar.indexOf(",");
                    avatar = avatar.substring(start + 1);
                }
                final Base64.Decoder decoder = Base64.getDecoder();
                avatar = avatar.replaceAll("\r|\n", "");
                avatar = avatar.trim();
                byte[] avatarByteArray = decoder.decode(avatar);
                InputStream avatarInputStream = new ByteArrayInputStream(avatarByteArray);
                // 上传到s3储存桶中
                AwsS3Utils.upload(avatorFilePath + "/" + userId + "/" + fileName, avatarInputStream);
//                    isSave = fileHelper.saveImg(avatar, filePath + avatorFilePath + File.separator + userId + File.separator, fileName);

                // 头像原图
                if (avatarSource.contains("data:")) {
                    int start = avatarSource.indexOf(",");
                    avatarSource = avatarSource.substring(start + 1);
                }
                avatarSource = avatarSource.replaceAll("\r|\n", "");
                avatarSource = avatarSource.trim();
                byte[] avatarSourceByteArray = decoder.decode(avatarSource);
                InputStream inputStream = new ByteArrayInputStream(avatarSourceByteArray);
                // 上传到s3储存桶中
                AwsS3Utils.upload(avatorFilePath + "/" + userId + "/" + sourceFileName, inputStream);
                avatarStr = avatorFilePath + "/" + userId + "/" + fileName;
            }
            userInfo = userInfoService.insert(userId, avatarStr, nickname, introduction, sex, province, city, birthday);
        }
        if(StringUtil.isNotEmpty(userInfo.getProfilePath())){
            String profilePath_s3 = AwsS3Utils.getS3FileUrl(userInfo.getProfilePath());
            userInfo.setProfilePath(profilePath_s3);
        }
        resultModelApi.setRetMsg("数据插入成功");
        resultModelApi.setData(userInfo);
        return resultModelApi;
    }

    /**
     * 阿里审核
     *
     * @param examineFlg
     * @param examineContent
     * @return
     */
    public int toExamine(String examineFlg, String examineContent) {
        int result = 1;
        if ("image".equals(examineFlg)) {
            examineContent = examineContent.replaceAll("data:image/png;base64,", "");
            byte[] photp = Base64Utils.decodeFromString(examineContent);
            // 验证头像是否违规
            List<String> scenes = new ArrayList<>();
            scenes.add(ExamineUtil.SCENE_LIVE);
            scenes.add(ExamineUtil.SCENE_PORN);
            scenes.add(ExamineUtil.SCENE_TERRORISM);
            scenes.add(ExamineUtil.SCENE_AD);
            scenes.add(ExamineUtil.SCENE_LOGO);
            List<byte[]> photos = new ArrayList<>();
            photos.add(photp);

            List<ContentSafetyResult> contentSafetyResultList = ExamineUtil.photoSafety(scenes, null, photos);
            if (contentSafetyResultList.size() > 0) {
                for (ContentSafetyResult contentSafetyResult: contentSafetyResultList) {
                    if (!"pass".equals(contentSafetyResult.getSuggestion())) {
                        result = -1;
                        return result;
                    }
                }
            }
        } else {
            List<ContentSafetyResult> contentSafetyResultList = ExamineUtil.textSafety(examineContent, ExamineUtil.SCENE_ANTISPAM);
            if (contentSafetyResultList.size() > 0) {
                if (!"pass".equals(contentSafetyResultList.get(0).getSuggestion())) {
                    result = -1;
                    return result;
                }
            }
        }
        return result;
    }
}
