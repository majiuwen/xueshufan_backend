package com.keensight.web.controller;

import cn.newtouch.dl.solr.model.SolrResultModel;
import cn.newtouch.dl.turbocloud.constants.ResultCode;
import cn.newtouch.dl.turbocloud.controller.BaseController;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.github.pagehelper.PageInfo;
import com.keensight.solr.model.SolrInstitutionSearchModel;
import com.keensight.solr.service.SolrInstitutionSearchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * 检索结果的研究机构控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/25] 新建
 */
@Api(value = "检索结果的研究机构")
@RestController
@RequestMapping("/searchResultsInstitution")
public class SearchResultsInstitutionController extends BaseController {
    @Autowired
    private SolrInstitutionSearchService solrInstitutionSearchService;

    /**
     * 检索结果的研究机构信息集合取得
     *
     * @param keyword 关键词/题目
     * @param index   开始页数
     * @param rows    显示件数
     * @return resultModelApi
     */
    @RequestMapping(value = "/getSearchResultsInstitutionList", method = {RequestMethod.POST})
    @ApiOperation(value = "检索结果的研究机构信息集合取得")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "keyword", value = "关键词/题目"),
            @ApiImplicitParam(paramType = "query", name = "autocompleteKeyword", value = "画面自动补全keyword"),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数"),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示件数")
    })
    public ResultModelApi getSearchResultsInstitutionList(String keyword, String autocompleteKeyword, Integer index, Integer rows) {
        ResultModelApi resultModelApi = new ResultModelApi();

        // 检索处理 start
        SolrResultModel solrResultModel = null;
        PageInfo<Long> pageInfo = new PageInfo<>();
        List<SolrInstitutionSearchModel> detailList = new ArrayList<>();
        solrResultModel = solrInstitutionSearchService.getResultsInstitutionByKeyword(keyword, autocompleteKeyword, index, rows);
        if (solrResultModel != null) {
            pageInfo.setTotal(solrResultModel.getRows());
            pageInfo.setPageNum(index);
            pageInfo.setPageSize(rows);
            pageInfo.setHasNextPage(solrResultModel.getRows() > 0 && solrResultModel.getRows() > index * rows);

            List<SolrInstitutionSearchModel> entry = solrResultModel.getENTRY();
            if (entry != null && entry.size() > 0) {
                for (SolrInstitutionSearchModel solrInstitutionSearchModel : entry) {
                    if (solrInstitutionSearchModel != null) {
                        detailList.add(solrInstitutionSearchModel);
                    }
                }
            }

            PageInfo<SolrInstitutionSearchModel> resultPageInfo = new PageInfo<>();
            if (pageInfo != null) {
                BeanUtils.copyProperties(pageInfo, resultPageInfo);
                resultPageInfo.setList(detailList);
                resultModelApi.setData(resultPageInfo);
            }
        }
        // 检索处理 end
        resultModelApi.setRetCode(ResultCode.Success.code());
        return resultModelApi;
    }
}
