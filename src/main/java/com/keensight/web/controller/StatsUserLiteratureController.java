package com.keensight.web.controller;

import cn.newtouch.dl.turbocloud.controller.BaseController;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.keensight.web.service.StatsUserMonthlyService;
import com.keensight.web.service.StatsUserTotalService;
import com.keensight.web.service.StatsUserYearlyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 作者文献统计控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2021/10/22] 新建
 */
@Api(value = "作者文献统计")
@RestController
@RequestMapping("/statsUserLiterature")
public class StatsUserLiteratureController extends BaseController {
    @Autowired
    private StatsUserYearlyService statsUserYearlyService;
    @Autowired
    private StatsUserMonthlyService statsUserMonthlyService;
    @Autowired
    private StatsUserTotalService statsUserTotalService;

    @RequestMapping(value = "/getStatsUserLiterature", method = {RequestMethod.POST})
    @ApiOperation(value = "作者文献统计信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true),
            @ApiImplicitParam(paramType = "query", name = "timeType", value = "时间类型", required = true)
    })
    public ResultModelApi getStatsUserLiterature(String userId, String timeType, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        if("0".equals(timeType)) {
            // 按年统计作者文献
            resultModelApi.setData(statsUserYearlyService.getStatsUserYearlyList(Integer.valueOf(userId)));
        } else {
            // 按月统计作者文献
            resultModelApi.setData(statsUserMonthlyService.getStatsUserMonthlyList(Integer.valueOf(userId)));
        }
        return resultModelApi;
    }

    @RequestMapping(value = "/getStatsUserTotal", method = {RequestMethod.POST})
    @ApiOperation(value = "作者文献统计总数")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getStatsUserTotal(String userId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 作者文献统计总数
        resultModelApi.setData(statsUserTotalService.getStatsUserTotal(Integer.valueOf(userId)));
        return resultModelApi;
    }
}
