package com.keensight.web.controller.api;

import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.keensight.web.service.PublicationService;
import com.keensight.web.utils.YouDaoTranslationUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 概要翻译控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2021/11/02] 新建
 */
@Api(value = "摘要翻译")
@RestController
@RequestMapping("/translation")
public class AbstractTranslationApi {
    @Autowired
    private PublicationService publicationService;

    /**
     * 摘要翻译
     *
     * @param publicationId
     * @param description
     * @return 中文翻译
     */
    @RequestMapping(value = "/getAbstractTranslation", method = {RequestMethod.POST})
    @ApiOperation(value = "摘要翻译")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献ID"),
            @ApiImplicitParam(paramType = "query", name = "description", value = "文献摘要"),
    })
    public ResultModelApi getAbstractTranslation(Long publicationId, String description) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        String descriptionString = "";
        String summaryCn = publicationService.getSummaryCnByPublicationId(publicationId);
        if ("".equals(summaryCn) || summaryCn == null) {
            //** 处理结果 *//*
            descriptionString = YouDaoTranslationUtil.getTranslationText(description);
            publicationService.updateSummaryByPublicationId(publicationId, descriptionString);
        } else {
            descriptionString = summaryCn;
        }

        resultModelApi.setData(descriptionString);
        return resultModelApi;
    }

    /**
     * 论文翻译
     *
     * @param description
     * @return 论文翻译
     */
    @RequestMapping(value = "/getThesisTranslation", method = {RequestMethod.POST})
    @ApiOperation(value = "论文翻译")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "description", value = "论文原文"),
    })
    public ResultModelApi getThesisTranslation(String description) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        String descriptionString = "";
        if (!"".equals(description) || description != null) {
            //** 处理结果 *//*
            descriptionString = YouDaoTranslationUtil.getTranslationText(description);
        }
        resultModelApi.setData(descriptionString);
        return resultModelApi;
    }

    /**
     * 文献标题翻译
     *
     * @param publicationId
     * @param displayTitle
     * @return 中文翻译
     */
    @RequestMapping(value = "/getDisplayTitleTranslation", method = {RequestMethod.POST})
    @ApiOperation(value = "文献标题")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "publicationId", value = "文献ID"),
            @ApiImplicitParam(paramType = "query", name = "displayTitle", value = "文献标题"),
    })
    public ResultModelApi getDisplayTitleTranslation(Long publicationId, String displayTitle) throws Exception {

        ResultModelApi resultModelApi = new ResultModelApi();
        String displayTitleString = "";
        String titleCn = publicationService.getTitkeCnByPublicationId(publicationId);
        if ("".equals(titleCn) || titleCn == null) {
            //** 处理结果 *//*
            displayTitleString = YouDaoTranslationUtil.getTranslationText(displayTitle);
            publicationService.updateTitleCnByPublicationId(publicationId, displayTitleString);
        } else {
            displayTitleString = titleCn;
        }

        resultModelApi.setData(displayTitleString);
        return resultModelApi;
    }
}
