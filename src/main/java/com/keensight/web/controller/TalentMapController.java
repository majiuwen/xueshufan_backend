package com.keensight.web.controller;

import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.alibaba.druid.util.StringUtils;
import com.keensight.neo4j.dao.UserDao;
import com.keensight.neo4j.entity.Neo4jBasicNode;
import com.keensight.neo4j.entity.Neo4jBasicRelationship;
import com.keensight.neo4j.entity.Neo4jResult;
import com.keensight.web.db.entity.Code;
import com.keensight.web.db.entity.Field;
import com.keensight.web.db.entity.User;
import com.keensight.web.model.UserModel;
import com.keensight.web.model.UserRelationNodeModel;
import com.keensight.web.model.UserRelationShipModel;
import com.keensight.web.service.TalentMapService;
import com.keensight.web.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.neo4j.driver.internal.InternalPath;
import org.neo4j.driver.types.Node;
import org.neo4j.driver.types.Relationship;
import org.neo4j.ogm.model.Result;
import org.apache.commons.collections4.IteratorUtils;
import org.neo4j.ogm.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 用户最新动态控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/09] 新建
 */
@RestController
@Api(value = "人才地图")
@RequestMapping("/talentMap")
public class TalentMapController {

    @Autowired
    private TalentMapService talentMapService;
    @Autowired
    private UserDao userDao;
    @Autowired
    private Session session;
    @Autowired
    private UserService userService;


    /**
     * 获取人才地图学者信息方法
     * @param nLng
     * @param nLat
     * @param sLng
     * @param sLat
     * @param code
     * @param isethniccn
     * @param fieldId
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getUserMaker", method = {RequestMethod.POST})
    public ResultModelApi getUserMaker(Double nLng, Double nLat, Double sLng, Double sLat, String code, String isethniccn, Long fieldId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 获取人才地图学者信息
        List<UserModel> userMaker = talentMapService.getUserMaker(nLng,nLat,sLng,sLat, StringUtils.isEmpty(code)?null:code,StringUtils.isEmpty(isethniccn)?null:isethniccn,fieldId);
        resultModelApi.setData(userMaker);
        return resultModelApi;
    }

    /**
     * 获取国家名称列表
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getCountryNameList", method = {RequestMethod.POST})
    public ResultModelApi getCountryNameList() throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        List<Code> countryNameList = talentMapService.getCountryNameList();
        resultModelApi.setData(countryNameList);
        return resultModelApi;
    }

    /**
     * 研究领域自动补全
     * @param displayName
     * @return
     */
    @RequestMapping(value = "/autocompeleteFieldName", method = {RequestMethod.POST})
    @ApiOperation(value = "研究领域自动补全")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "displayName", value = "领域名称", required = true)
    })
    public ResultModelApi autocompeleteFieldName(String displayName){
        ResultModelApi resultModelApi=new ResultModelApi();
        List<Field> fieldList = talentMapService.autocompeleteFieldName(displayName);
        resultModelApi.setData(fieldList);
        return resultModelApi;
    }

    /**
     * 查询关系
     *
     * @param userId
     * @return
     */
    public List<Neo4jResult> queryRelation(Long userId) {
        //拼接sql
        String cypherSql = String.format("MATCH q=(n:User{user_id:%s})-[]-() RETURN q UNION MATCH q=(n:User{user_id:%s})-[]-()-[]-() RETURN q;", userId, userId);
        Result query = session.query(cypherSql, new HashMap<>());
        Iterable<Map<String, Object>> maps = query.queryResults();
        ArrayList<Neo4jResult> returnList = new ArrayList<>();
        for (Map<String, Object> map : maps) {
            InternalPath.SelfContainedSegment[] ps = (InternalPath.SelfContainedSegment[]) map.get("q");
            for (InternalPath.SelfContainedSegment p : ps) {
                Neo4jResult neo4jResult = changeToNeo4jBasicRelationReturnVO(p);
                List matchList = returnList.stream().filter(item ->
                        item.getStart().getId().equals(neo4jResult.getStart().getId())
                     && item.getRelationship().getId().equals(neo4jResult.getRelationship().getId())
                     && item.getEnd().getId().equals(neo4jResult.getEnd().getId())
                        ).collect(Collectors.toList());
                if (matchList.size() == 0){
                    returnList.add(neo4jResult);
                }
            }
        }
        session.clear();
        return returnList;
    }

    /**
     * 转化neo4j默认查询的参数为自定返回类型
     *
     * @param selfContainedSegment
     * @return Neo4jBasicRelationReturn
     */
    public Neo4jResult changeToNeo4jBasicRelationReturnVO(InternalPath.SelfContainedSegment selfContainedSegment) {
        Neo4jResult neo4JBasicRelationReturnVO = new Neo4jResult();
        //start
        Node start = selfContainedSegment.start();
        Neo4jBasicNode startNodeVo = new Neo4jBasicNode();
        startNodeVo.setId(start.id());
        startNodeVo.setLabels(IteratorUtils.toList(start.labels().iterator()));
        startNodeVo.setProperty(start.asMap());
        neo4JBasicRelationReturnVO.setStart(startNodeVo);
        //end
        Node end = selfContainedSegment.end();
        Neo4jBasicNode endNodeVo = new Neo4jBasicNode();
        endNodeVo.setId(end.id());
        endNodeVo.setLabels(IteratorUtils.toList(end.labels().iterator()));
        endNodeVo.setProperty(end.asMap());
        neo4JBasicRelationReturnVO.setEnd(endNodeVo);
        //relationship
        Neo4jBasicRelationship neo4JQueryRelation = new Neo4jBasicRelationship();
        Relationship relationship = selfContainedSegment.relationship();
        neo4JQueryRelation.setStart(relationship.startNodeId());
        neo4JQueryRelation.setEnd(relationship.endNodeId());
        neo4JQueryRelation.setId(relationship.id());
        neo4JQueryRelation.setType(relationship.type());
        neo4JQueryRelation.setProperty(relationship.asMap());
        neo4JBasicRelationReturnVO.setRelationship(neo4JQueryRelation);
        return neo4JBasicRelationReturnVO;
    }

    /**
     * 获取人才地图学者关系图节点数据
     * @param userId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getUserRelationNode", method = {RequestMethod.POST})
    public ResultModelApi getUserRelationNode(Long userId) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        List<UserRelationNodeModel> userRelationNodeModelList = new ArrayList<>();
        // 获取人才地图学者关系图
        List<Neo4jResult> neo4jResults = queryRelation(userId);
        // 循环获取添加数据
        for(Neo4jResult neo4jResult : neo4jResults) {
            // START节点
            UserRelationNodeModel userRelationNodeModelFrom = new UserRelationNodeModel();

            User userRelationFrom = userService.selectByPrimaryKey((Long) neo4jResult.getStart().getProperty().get("user_id"));

            userRelationNodeModelFrom.setId(String.valueOf(userRelationFrom.getUserId()));
            String userRelationFromPath = userRelationFrom.getProfilePhotoPath() != null?userRelationFrom.getProfilePhotoPath():"/user.png";
            String userRelationNodeModelFromInnerHTML = "<div class='c-my-node2' style='background-image: url("+userRelationFromPath+");'><div class='c-node-name2'>" + userRelationFrom.getDisplayName() + "</div></div>";
            userRelationNodeModelFrom.setInnerHTML(userRelationNodeModelFromInnerHTML);
            // END节点
            UserRelationNodeModel userRelationNodeModelTo = new UserRelationNodeModel();

            User userRelationTo = userService.selectByPrimaryKey((Long) neo4jResult.getEnd().getProperty().get("user_id"));

            userRelationNodeModelTo.setId(String.valueOf(userRelationTo.getUserId()));
            String userRelationToPath = userRelationTo.getProfilePhotoPath() != null?userRelationTo.getProfilePhotoPath():"/user.png";
            String userRelationNodeModelToInnerHTML = "<div class='c-my-node2' style='background-image: url("+userRelationToPath+");'><div class='c-node-name2'>" + userRelationTo.getDisplayName() + "</div></div>";
            userRelationNodeModelTo.setInnerHTML(userRelationNodeModelToInnerHTML);

            List matchFromList = userRelationNodeModelList.stream().filter(item -> item.getId().equals(userRelationNodeModelFrom.getId())).collect(Collectors.toList());
            if (matchFromList.size() == 0) {
                userRelationNodeModelList.add(userRelationNodeModelFrom);
            }
            List matchToList = userRelationNodeModelList.stream().filter(item -> item.getId().equals(userRelationNodeModelTo.getId())).collect(Collectors.toList());
            if (matchToList.size() == 0) {
                userRelationNodeModelList.add(userRelationNodeModelTo);
            }
        }
        resultModelApi.setData(userRelationNodeModelList);
        return resultModelApi;
    }

    /**
     * 获取人才地图学者关系图
     * @param userId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getUserRelation", method = {RequestMethod.POST})
    public ResultModelApi getUserRelation(Long userId) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        List<UserRelationShipModel> UserRelationShipModelList = new ArrayList<>();
        // 获取人才地图学者关系图
        List<Neo4jResult> neo4jResults = queryRelation(userId);
        // 循环获取添加数据
        for(Neo4jResult neo4jResult : neo4jResults) {
            UserRelationShipModel userRelationShipModel = new UserRelationShipModel();
            Long fromId = neo4jResult.getRelationship().getStart();
            Long toId = neo4jResult.getRelationship().getEnd();

            if(neo4jResult.getStart().getId().equals(fromId)){
                userRelationShipModel.setFrom(String.valueOf(neo4jResult.getStart().getProperty().get("user_id")));
            }else if(neo4jResult.getEnd().getId().equals(fromId)){
                userRelationShipModel.setFrom(String.valueOf(neo4jResult.getEnd().getProperty().get("user_id")));
            }

            if(neo4jResult.getStart().getId().equals(toId)){
                userRelationShipModel.setTo(String.valueOf(neo4jResult.getStart().getProperty().get("user_id")));
            }else if(neo4jResult.getEnd().getId().equals(toId)){
                userRelationShipModel.setTo(String.valueOf(neo4jResult.getEnd().getProperty().get("user_id")));
            }

            userRelationShipModel.setText(String.valueOf(neo4jResult.getRelationship().getType()));
            userRelationShipModel.setIsHideArrow("true");
            userRelationShipModel.setData(String.valueOf(neo4jResult.getRelationship().getType()));
            userRelationShipModel.setColor("#d2c0a5");
            userRelationShipModel.setFontColor("#d2c0a5");

            UserRelationShipModelList.add(userRelationShipModel);
        }
        resultModelApi.setData(UserRelationShipModelList);
        return resultModelApi;
    }

    /**
     * 查询用户之间所有关系类型
     * @return
     */
    public List<String> queryRelationType() {
        //拼接sql
        String cypherSql = String.format("MATCH ()-[r]-() RETURN distinct type(r) as name;");
        Result query = session.query(cypherSql, new HashMap<>());
        ArrayList<String> relationNames = new ArrayList<>();
        for (Map<String, Object> map : query.queryResults()) {
            relationNames.add(map.get("name").toString());
        }
        return relationNames;
    }

    /**
     * 查询用户之间所有关系类型
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getAllRelation", method = {RequestMethod.POST})
    public ResultModelApi getAllRelation() throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        List<String> neo4jResults = queryRelationType();
        resultModelApi.setData(neo4jResults);
        return resultModelApi;
    }

    /**
     * 按条件筛选关系
     * @param userId
     * @param relationValueStr
     * @return
     */
    public List<Neo4jResult> checkRelation(Long userId, String relationValueStr) {
        //拼接sql
        String cypherSql = String.format("MATCH q=(n:User{user_id:%s})-[r:%s]-() RETURN q UNION MATCH q=(n:User{user_id:%s})-[r:%s]-()-[s:%s]-() RETURN q;", userId, relationValueStr, userId, relationValueStr, relationValueStr);
        Result query = session.query(cypherSql, new HashMap<>());
        Iterable<Map<String, Object>> maps = query.queryResults();
        ArrayList<Neo4jResult> returnList = new ArrayList<>();
        for (Map<String, Object> map : maps) {
            InternalPath.SelfContainedSegment[] ps = (InternalPath.SelfContainedSegment[]) map.get("q");
            for (InternalPath.SelfContainedSegment p : ps) {
                Neo4jResult neo4jResult = changeToNeo4jBasicRelationReturnVO(p);
                List matchList = returnList.stream().filter(item ->
                        item.getStart().getId().equals(neo4jResult.getStart().getId())
                                && item.getRelationship().getId().equals(neo4jResult.getRelationship().getId())
                                && item.getEnd().getId().equals(neo4jResult.getEnd().getId())
                ).collect(Collectors.toList());
                if (matchList.size() == 0){
                    returnList.add(neo4jResult);
                }
            }
        }
        session.clear();
        return returnList;
    }

    /**
     * 获取筛选关系后的节点
     * @param userId
     * @param relationValueStr
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getUserCheckRelationNode", method = {RequestMethod.POST})
    public ResultModelApi getUserCheckRelationNode(Long userId,String relationValueStr) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        List<UserRelationNodeModel> userRelationNodeModelList = new ArrayList<>();
        // 获取人才地图学者关系图
        List<Neo4jResult> neo4jResults = checkRelation(userId, relationValueStr);
        // 循环获取添加数据
        for(Neo4jResult neo4jResult : neo4jResults) {
            // START节点
            UserRelationNodeModel userRelationNodeModelFrom = new UserRelationNodeModel();

            User userRelationFrom = userService.selectByPrimaryKey((Long) neo4jResult.getStart().getProperty().get("user_id"));

            userRelationNodeModelFrom.setId(String.valueOf(userRelationFrom.getUserId()));
            String userRelationFromPath = userRelationFrom.getProfilePhotoPath() != null?userRelationFrom.getProfilePhotoPath():"/user.png";
            String userRelationNodeModelFromInnerHTML = "<div class='c-my-node2' style='background-image: url("+userRelationFromPath+");'><div class='c-node-name2'>" + userRelationFrom.getDisplayName() + "</div></div>";
            userRelationNodeModelFrom.setInnerHTML(userRelationNodeModelFromInnerHTML);
            // END节点
            UserRelationNodeModel userRelationNodeModelTo = new UserRelationNodeModel();

            User userRelationTo = userService.selectByPrimaryKey((Long) neo4jResult.getEnd().getProperty().get("user_id"));

            userRelationNodeModelTo.setId(String.valueOf(userRelationTo.getUserId()));
            String userRelationToPath = userRelationTo.getProfilePhotoPath() != null?userRelationTo.getProfilePhotoPath():"/user.png";
            String userRelationNodeModelToInnerHTML = "<div class='c-my-node2' style='background-image: url("+userRelationToPath+");'><div class='c-node-name2'>" + userRelationTo.getDisplayName() + "</div></div>";
            userRelationNodeModelTo.setInnerHTML(userRelationNodeModelToInnerHTML);

            List matchFromList = userRelationNodeModelList.stream().filter(item -> item.getId().equals(userRelationNodeModelFrom.getId())).collect(Collectors.toList());
            if (matchFromList.size() == 0) {
                userRelationNodeModelList.add(userRelationNodeModelFrom);
            }
            List matchToList = userRelationNodeModelList.stream().filter(item -> item.getId().equals(userRelationNodeModelTo.getId())).collect(Collectors.toList());
            if (matchToList.size() == 0) {
                userRelationNodeModelList.add(userRelationNodeModelTo);
            }
        }
        resultModelApi.setData(userRelationNodeModelList);
        return resultModelApi;
    }

    /**
     * 筛选后的关系类型
     * @param userId
     * @param relationValueStr
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getUserCheckRelation", method = {RequestMethod.POST})
    public ResultModelApi getUserCheckRelation(Long userId, String relationValueStr) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        List<UserRelationShipModel> UserRelationShipModelList = new ArrayList<>();
        // 获取人才地图学者关系图
        List<Neo4jResult> neo4jResults = checkRelation(userId, relationValueStr);
        // 循环获取添加数据
        for(Neo4jResult neo4jResult : neo4jResults) {
            UserRelationShipModel userRelationShipModel = new UserRelationShipModel();
            Long fromId = neo4jResult.getRelationship().getStart();
            Long toId = neo4jResult.getRelationship().getEnd();

            if(neo4jResult.getStart().getId().equals(fromId)){
                userRelationShipModel.setFrom(String.valueOf(neo4jResult.getStart().getProperty().get("user_id")));
            }else if(neo4jResult.getEnd().getId().equals(fromId)){
                userRelationShipModel.setFrom(String.valueOf(neo4jResult.getEnd().getProperty().get("user_id")));
            }

            if(neo4jResult.getStart().getId().equals(toId)){
                userRelationShipModel.setTo(String.valueOf(neo4jResult.getStart().getProperty().get("user_id")));
            }else if(neo4jResult.getEnd().getId().equals(toId)){
                userRelationShipModel.setTo(String.valueOf(neo4jResult.getEnd().getProperty().get("user_id")));
            }

            userRelationShipModel.setText(String.valueOf(neo4jResult.getRelationship().getType()));
            userRelationShipModel.setIsHideArrow("true");
            userRelationShipModel.setData(String.valueOf(neo4jResult.getRelationship().getType()));
            userRelationShipModel.setColor("#d2c0a5");
            userRelationShipModel.setFontColor("#d2c0a5");

            UserRelationShipModelList.add(userRelationShipModel);
        }
        resultModelApi.setData(UserRelationShipModelList);
        return resultModelApi;
    }
}
