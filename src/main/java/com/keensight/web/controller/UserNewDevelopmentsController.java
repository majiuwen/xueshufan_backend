package com.keensight.web.controller;

import cn.newtouch.dl.turbocloud.helper.FileHelper;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.keensight.web.model.UserModel;
import com.keensight.web.model.UserNewsModel;
import com.keensight.web.service.UserNewDevelopmentsService;
import com.keensight.web.utils.ContentSafetyResult;
import com.keensight.web.utils.ExamineUtil;
import com.keensight.web.utils.FileUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * 用户最新动态控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/09] 新建
 */
@RestController
@Api(value = "用户最新动态")
@RequestMapping("/userNewDevelopments")
public class UserNewDevelopmentsController {
    @Autowired
    private FileHelper fileHelper;
    @Autowired
    private UserNewDevelopmentsService userNewDevelopmentsService;

    /**
     * 最新动态视频/图片文件保存临时文件夹方法
     * @param file
     * @param userId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/uploadFileForNews", method = {RequestMethod.POST})
    public ResultModelApi uploadPdf(@RequestParam("file") MultipartFile file, long userId, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 审核情况
        int examine = 0;
        //创建Properties对象
        Properties prop = new Properties();
        //读取classPath中的properties文件
        prop.load(UserNewDevelopmentsController.class.getClassLoader().getResourceAsStream("application.properties"));
        // 文件服务器总路径
        String filePath = prop.getProperty("keensight.file.server.path");
        // String filePath = "C:/test666/";
        // 正式文件上传路径
        String fileFormalPath = prop.getProperty("news.upload.path");
        // 临时文件上传路径
        String fileTempPath = prop.getProperty("news.temp.path");
        // 图片上传路径
        String imgPath = prop.getProperty("news.image.path");
        // 视频上传路径
        String videoPath = prop.getProperty("news.video.path");
        // 上传文件名
        String uploadfileName = null;
        // 文件流
        InputStream inputStream = null;
        // 获取文件流
        inputStream = file.getInputStream();
        // 获取上传文件名
        uploadfileName = file.getOriginalFilename();
        // 获取文件格式
        String uploadFileType = uploadfileName.substring(uploadfileName.lastIndexOf('.'));
        // 最新动态文件上传到临时文件夹
        if(".jpg".equals(uploadFileType) || ".png".equals(uploadFileType) || ".jpeg".equals(uploadFileType)){
//            if("1".equals(ExamineUtil.ImageCensor(file))) {
            if (!FileUtil.checkFileSize(file.getSize(),10,"M")) {
                examine = -1;
                resultModelApi.setData(examine);
                return resultModelApi;
            }
            // 验证头像是否违规
            List<String> scenes = new ArrayList<>();
            scenes.add(ExamineUtil.SCENE_LIVE);
            scenes.add(ExamineUtil.SCENE_PORN);
            scenes.add(ExamineUtil.SCENE_TERRORISM);
            scenes.add(ExamineUtil.SCENE_AD);
            scenes.add(ExamineUtil.SCENE_LOGO);
            List<byte[]> photos = new ArrayList<>();
            photos.add(file.getBytes());

            List<ContentSafetyResult> contentSafetyResultList = ExamineUtil.photoSafety(scenes, null, photos);
            if (contentSafetyResultList.size() > 0) {
                for (ContentSafetyResult contentSafetyResult: contentSafetyResultList) {
                    if (!"pass".equals(contentSafetyResult.getSuggestion())) {
                        examine = -1;
                        resultModelApi.setData(examine);
                        return resultModelApi;
                    }
                }
            }

            if (examine != -1) {
                if (fileHelper.mkDir(filePath + fileFormalPath + fileTempPath + imgPath + File.separator + userId + File.separator)) {
                    fileHelper.saveFile(inputStream, filePath + fileFormalPath + fileTempPath + imgPath + File.separator + userId + File.separator, uploadfileName);
                }
            }
        }
//        else if (".mp4".equals(uploadFileType)) {
//            if(fileHelper.mkDir(filePath + fileFormalPath + fileTempPath + videoPath + File.separator + userId + File.separator)) {
//                fileHelper.saveFile(inputStream, filePath + fileFormalPath + fileTempPath + videoPath + File.separator + userId + File.separator, uploadfileName);
//                // String videoParam = "name=" + "b95e08ea8e06ab2b3a6c822324c753e9.mp4" + "&extId=" + "999999999" + "&videoUrl=" +"http://140.179.123.131:8000/file/upload/newsFile/newsVideo/999999999/20211124050241838/b95e08ea8e06ab2b3a6c822324c753e9.mp4";
//                // String videoParam = "name=" + uploadfileName + "&extId=" + userId + "&videoUrl=" + filePath + fileFormalPath + fileTempPath + videoPath + File.separator + userId + File.separator + uploadfileName;
////                if("1".equals(ExamineUtil.VideoCensor(videoParam))) {
////                    examine = 1;
////                } else {
////                    // 视频不合规
////                    examine = -2;
////                    // 不合规删除视频文件视频
////                    fileHelper.deleteGeneralFile(filePath + fileFormalPath + fileTempPath + videoPath + File.separator + userId + File.separator);
////                }
//            }
//        }
        else {
            examine = -1;
            resultModelApi.setData(examine);
            return resultModelApi;
        }
        resultModelApi.setData(examine);
        return resultModelApi;
    }

    /**
     * 最新动态保存方法
     * @param userNewsModel
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/saveNewDevelopments", method = {RequestMethod.POST})
    public ResultModelApi saveNewDevelopments(@RequestBody UserNewsModel userNewsModel, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        // 最新动态内容及视频/图片文件保存方法
        Long result = userNewDevelopmentsService.saveNewDevelopments(userNewsModel);
        resultModelApi.setData(result);
        return resultModelApi;
    }

    /**
     * 最新动态删除方法
     * @param visibility
     * @param newsId
     * @param commentNum
     * @param fileNum
     * @param likeNum
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/delNewsData", method = {RequestMethod.POST})
    public ResultModelApi delNewsData(String visibility, Long newsId, int commentNum, int fileNum, int likeNum, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        int delNewsData = 0;
        // 动态删除方法
        delNewsData = userNewDevelopmentsService.delNewsData(visibility,newsId,commentNum,fileNum,likeNum);
        resultModelApi.setData(delNewsData);
        return resultModelApi;
    }
}
