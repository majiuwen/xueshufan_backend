package com.keensight.web.controller;

import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.keensight.web.model.UserAnnotationModel;
import com.keensight.web.service.UserAnnotationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 论文批注控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2021/12/17] 新建
 */

@Api(value = "论文批注")
@RestController
@RequestMapping("/userAnnotation")
public class UserAnnotationController {
    @Autowired
    private UserAnnotationService userAnnotationService;

    /**
     * 新增用户批注信息
     * @param userAnnotationModel
     * @return
     */
    @RequestMapping(value = "/insertUserAnnotation", method = {RequestMethod.POST})
    @ApiOperation(value = "新增用户批注信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userAnnotationModel", value = "批注model", required = true),
    })
    public ResultModelApi insertUserAnnotation(@RequestBody UserAnnotationModel userAnnotationModel) {

        ResultModelApi resultModelApi = new ResultModelApi();
        // 新增用户摘录信息
        UserAnnotationModel newUserAnnotationModel = userAnnotationService.insertUserAnnotation(userAnnotationModel);
        if (!ObjectUtils.isEmpty(newUserAnnotationModel)) {
            resultModelApi.setData(newUserAnnotationModel);
        } else {
            resultModelApi.setRetCode("-1");
            resultModelApi.setRetMsg("新增用户批注失败");
            return resultModelApi;
        }

        return resultModelApi;
    }

    /**
     * 删除用户批注信息
     * @param annotationId
     * @return
     */
    @RequestMapping(value = "/deleteUserAnnotation", method = {RequestMethod.POST})
    @ApiOperation(value = "删除用户批注信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "annotationId", value = "批注id", required = true),
    })
    public ResultModelApi deleteUserAnnotation(Long annotationId) {

        ResultModelApi resultModelApi = new ResultModelApi();
        // 删除用户批注信息
        int resultCount = userAnnotationService.deleteUserAnnotationByKey(annotationId);
        if (resultCount > 0) {
            resultModelApi.setData(resultCount);
        } else {
            resultModelApi.setRetCode("-1");
            resultModelApi.setRetMsg("删除用户批注失败");
            return resultModelApi;
        }

        return resultModelApi;
    }

    /**
     * 更新用户批注信息
     * @param userAnnotationModel
     * @return
     */
    @RequestMapping(value = "/updateUserAnnotation", method = {RequestMethod.POST})
    @ApiOperation(value = "更新用户批注信息")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "query", name = "userAnnotationModel", value = "批注model", required = true),
    })
    public ResultModelApi updateUserAnnotation(@RequestBody UserAnnotationModel userAnnotationModel) {

        ResultModelApi resultModelApi = new ResultModelApi();
        // 更新用户批注信息
        int resultCount = userAnnotationService.updateUserAnnotationByKey(userAnnotationModel);
        if (resultCount > 0) {
            resultModelApi.setData(resultCount);
        } else {
            resultModelApi.setRetCode("-1");
            resultModelApi.setRetMsg("更新用户批注失败");
            return resultModelApi;
        }

        return resultModelApi;
    }
}
