package com.keensight.web.controller;

import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.keensight.web.service.UserAuthService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户基本信息控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/03] 新建
 */
@RestController
@Api(value = "用户基本信息")
@RequestMapping("/userAuth")
public class UserAuthController {
    @Autowired
    private UserAuthService userAuthService;

    /**
     * 用户个人资料获取
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/getUserAuthCountByUserId", method = {RequestMethod.POST})
    @ApiOperation(value = "用户个人资料信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getUserAuthCountByUserId(Long userId) {
        ResultModelApi resultModelApi = new ResultModelApi();

        int resCount = userAuthService.selectCountByUserId(userId, null);
        resultModelApi.setData(resCount);
        return resultModelApi;
    }
}
