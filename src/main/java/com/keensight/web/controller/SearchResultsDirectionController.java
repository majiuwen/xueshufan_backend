package com.keensight.web.controller;

import cn.newtouch.dl.solr.model.SolrResultModel;
import cn.newtouch.dl.turbocloud.constants.ResultCode;
import cn.newtouch.dl.turbocloud.controller.BaseController;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageInfo;
import com.keensight.solr.model.SolrFieldSearchModel;
import com.keensight.solr.service.SolrFieldSearchService;
import com.keensight.web.db.entity.Field;
import com.keensight.web.service.FieldService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 检索结果的研究方向控制器
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/19] 新建
 */
@Api(value = "检索结果的研究方向")
@RestController
@RequestMapping("/searchResultsDirection")
public class SearchResultsDirectionController extends BaseController {
    @Autowired
    private SolrFieldSearchService solrFieldSearchService;
    @Autowired
    private FieldService fieldService;

    /**
     * 检索结果的研究方向信息集合取得
     *
     * @param keyword 关键词/题目
     * @param index   开始页数
     * @param rows    显示件数
     * @return resultModelApi
     */
    @RequestMapping(value = "/getSearchResultsDirectionList", method = {RequestMethod.POST})
    @ApiOperation(value = "检索结果的研究方向信息集合取得")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "keyword", value = "关键词/题目"),
            @ApiImplicitParam(paramType = "query", name = "autocompleteKeyword", value = "画面自动补全keyword"),
            @ApiImplicitParam(paramType = "query", name = "index", value = "开始页数"),
            @ApiImplicitParam(paramType = "query", name = "rows", value = "显示件数")
    })
    public ResultModelApi getSearchResultsDirectionList(String keyword, String autocompleteKeyword, Integer index, Integer rows) {
        ResultModelApi resultModelApi = new ResultModelApi();

        // 检索处理 start
        SolrResultModel solrResultModel;
        List<Field> fieldList = null;
        PageInfo<SolrFieldSearchModel> pageInfo = new PageInfo<>();
        List<SolrFieldSearchModel> detailList = new ArrayList<>();
        solrResultModel = solrFieldSearchService.getResultsDirectionByKeyword(keyword, autocompleteKeyword, index, rows);
        if (solrResultModel != null) {
            pageInfo.setTotal(solrResultModel.getRows());
            pageInfo.setPageNum(index);
            pageInfo.setPageSize(rows);
            pageInfo.setHasNextPage(solrResultModel.getRows() > 0 && solrResultModel.getRows() > index * rows);

            List<SolrFieldSearchModel> entry = solrResultModel.getENTRY();
            if (entry != null && entry.size() > 0) {
                for (SolrFieldSearchModel solrFieldSearchModel : entry) {
                    if (solrFieldSearchModel != null) {
                        detailList.add(solrFieldSearchModel);
                    }
                }
            }
            pageInfo.setList(detailList);
            // 根据研究领域Id从DB中检索相应信息
            if (detailList.size() > 0) {
                // 从SolrFieldSearchModel集合中获取研究领域ID
                List<Long> fieldIdList = detailList.stream().filter(data -> !StringUtils.isEmpty(data.getId()) && StringUtils.isNumber(data.getId())).map(data -> Long.valueOf(data.getId())).collect(Collectors.toList());
                fieldList = fieldService.getFieldInfoByFieldIdList(fieldIdList);
            }
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("pageInfo", pageInfo);
            resultMap.put("fieldList", fieldList);

            resultModelApi.setRetCode(ResultCode.Success.code());
            resultModelApi.setData(resultMap);
        }
        // 检索处理 end
        resultModelApi.setRetCode(ResultCode.Success.code());
        return resultModelApi;
    }

    /**
     * 根据研究领域ID更新用户关注的研究方向
     *
     * @param userId
     * @param fieldId
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/updUserFieldFollowingByFieldId", method = {RequestMethod.POST})
    @ApiOperation(value = "根据研究领域ID更新用户关注的研究方向")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户ID", required = true),
            @ApiImplicitParam(paramType = "query", name = "fieldId", value = "研究领域ID", required = true),
            @ApiImplicitParam(paramType = "query", name = "followingBtnFlg", value = "关注按钮区分「1:关注」/「2:取消关注」", required = true)
    })
    public ResultModelApi updUserFieldFollowingByFieldId(String userId, String fieldId, String followingBtnFlg, HttpServletRequest request) throws Exception {
        ResultModelApi resultModelApi = new ResultModelApi();
        if (StringUtils.isEmpty(fieldId) || !StringUtils.isNumber(fieldId) || StringUtils.isEmpty(userId) || !StringUtils.isNumber(userId)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }

        Long updFieldId = Long.valueOf(fieldId);
        Long updUserId = Long.valueOf(userId);
        resultModelApi.setData(fieldService.updUserFieldFollowingByFieldId(updUserId, updFieldId, followingBtnFlg));
        resultModelApi.setRetCode(ResultCode.Success.code());
        return resultModelApi;
    }
}
