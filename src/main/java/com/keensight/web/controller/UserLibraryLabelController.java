package com.keensight.web.controller;

import cn.newtouch.dl.turbocloud.constants.ResultCode;
import cn.newtouch.dl.turbocloud.controller.BaseController;
import cn.newtouch.dl.turbocloud.model.ResultModelApi;
import com.alibaba.druid.util.StringUtils;
import com.keensight.web.db.entity.UserLibraryLabel;
import com.keensight.web.service.UserLibraryLabelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api(value = "用户文章标签")
@RequestMapping("/userLibraryLabel")
public class UserLibraryLabelController extends BaseController {
    @Autowired
    private UserLibraryLabelService userLibraryLabelService;

    @RequestMapping(value = "/getUserLibraryLabelInfo", method = {RequestMethod.POST})
    @ApiOperation(value = "用户标签信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "userId", value = "用户id", required = true)
    })
    public ResultModelApi getUserLibraryLabelInfo(String userId) {
        ResultModelApi resultModelApi = new ResultModelApi();

        // 用户ID不存在的情况下
        if (StringUtils.isEmpty(userId) || !StringUtils.isNumber(userId)) {
            resultModelApi.setRetCode(ResultCode.Request_Error_Params.code());
            resultModelApi.setRetMsg("参数错误");
            return resultModelApi;
        }

        List<UserLibraryLabel> UserLibraryLabelList = userLibraryLabelService.getUserLibraryLabelList(Integer.valueOf(userId));
        resultModelApi.setData(UserLibraryLabelList);
        return resultModelApi;
    }

}
