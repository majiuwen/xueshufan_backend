package com.keensight.web.model;

/**
 * 用户学术成果画面用Model
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/05] 新建
 */
public class UserAchievementsStatisticsModel {
    public static final String DOC_TYPE_ALL_CODE = "all";
    public static final String DOC_TYPE_ALL_NAME = "所有";
    public static final String DOC_TYPE_ALL_NAME_EN = "All";
    public static final String DOC_TYPE_PAPER_CODE = "paper";
    public static final String DOC_TYPE_PAPER_NAME = "论文";
    public static final String DOC_TYPE_PAPER_NAME_EN = "Paper";

    /** 组别 */
    private String groupCd;
    /** 类型 */
    private String docType;
    /** 组别,类型 */
    private String code;
    /** 类型名称 */
    private String docTypeNameEn;
    /** 类型英文名称 */
    private String docTypeName;
    /** 文献类型所属(paper:论文) */
    private String typeBelong;
    /** 统计数量 */
    private String statisticsCount;

    /**
     * 获取 组别
     *
     * @return groupCd 组别
     */
    public String getGroupCd() {
        return groupCd;
    }

    /**
     * 设置 组别
     *
     * @param groupCd 组别
     */
    public void setGroupCd(String groupCd) {
        this.groupCd = groupCd;
    }

    /**
     * 获取 类型
     *
     * @return docType 类型
     */
    public String getDocType() {
        return docType;
    }

    /**
     * 设置 类型
     *
     * @param docType 类型
     */
    public void setDocType(String docType) {
        this.docType = docType;
    }

    /**
     * 获取 组别,类型
     *
     * @return code 组别,类型
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置 组别,类型
     *
     * @param code 组别,类型
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取 类型名称
     *
     * @return docTypeNameEn 类型名称
     */
    public String getDocTypeNameEn() {
        return docTypeNameEn;
    }

    /**
     * 设置 类型名称
     *
     * @param docTypeNameEn 类型名称
     */
    public void setDocTypeNameEn(String docTypeNameEn) {
        this.docTypeNameEn = docTypeNameEn;
    }

    /**
     * 获取 类型英文名称
     *
     * @return docTypeName 类型英文名称
     */
    public String getDocTypeName() {
        return docTypeName;
    }

    /**
     * 设置 类型英文名称
     *
     * @param docTypeName 类型英文名称
     */
    public void setDocTypeName(String docTypeName) {
        this.docTypeName = docTypeName;
    }

    /**
     * 获取 文献类型所属（paper:论文）
     *
     * @return typeBelong 文献类型所属（paper:论文）
     */
    public String getTypeBelong() {
        return typeBelong;
    }

    /**
     * 设置 文献类型所属（paper:论文）
     *
     * @param typeBelong 文献类型所属（paper:论文）
     */
    public void setTypeBelong(String typeBelong) {
        this.typeBelong = typeBelong;
    }

    /**
     * 获取 统计数量
     *
     * @return statisticsCount 统计数量
     */
    public String getStatisticsCount() {
        return statisticsCount;
    }

    /**
     * 设置 统计数量
     *
     * @param statisticsCount 统计数量
     */
    public void setStatisticsCount(String statisticsCount) {
        this.statisticsCount = statisticsCount;
    }
}
