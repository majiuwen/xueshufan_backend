package com.keensight.web.model;


import com.keensight.web.db.entity.UserExcerpt;
import java.util.List;

public class UserExcerptModel extends UserExcerpt {
    /** 选择文本内容 */
    private String selectContent;
    /** 摘录日期 */
    private String excerptDateMonAndDayStr;
    /** 摘录时间  */
    private String excerptDateHourAndMinStr;
    /** 页数 */
    private Integer page_num;
    private Integer origin_page;
    /** 开始Dom坐标 */
    private String start;
    /** 结束Dom坐标 */
    private String end;
    /** 快照页上坐标 */
    private String top;
    /** 快照页高度 */
    private String height;
    /** 快照页宽度 */
    private String width;
    /** 快照页左坐标 */
    private String left;
    /** 批注List */
    private List<UserAnnotationModel> annotationList;
    /** pdf路径 */
    private String pdfPath;

    public String getSelectContent() {
        return selectContent;
    }

    public void setSelectContent(String selectContent) {
        this.selectContent = selectContent;
    }

    public String getExcerptDateMonAndDayStr() {
        return excerptDateMonAndDayStr;
    }

    public void setExcerptDateMonAndDayStr(String excerptDateMonAndDayStr) {
        this.excerptDateMonAndDayStr = excerptDateMonAndDayStr;
    }

    public String getExcerptDateHourAndMinStr() {
        return excerptDateHourAndMinStr;
    }

    public void setExcerptDateHourAndMinStr(String excerptDateHourAndMinStr) {
        this.excerptDateHourAndMinStr = excerptDateHourAndMinStr;
    }

    public Integer getPage_num() {
        return page_num;
    }

    public void setPage_num(Integer page_num) {
        this.page_num = page_num;
    }

    public Integer getOrigin_page() {
        return origin_page;
    }

    public void setOrigin_page(Integer origin_page) {
        this.origin_page = origin_page;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getTop() {
        return top;
    }

    public void setTop(String top) {
        this.top = top;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getLeft() {
        return left;
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public List<UserAnnotationModel> getAnnotationList() {
        return annotationList;
    }

    public void setAnnotationList(List<UserAnnotationModel> annotationList) {
        this.annotationList = annotationList;
    }

    public String getPdfPath() {
        return pdfPath;
    }

    public void setPdfPath(String pdfPath) {
        this.pdfPath = pdfPath;
    }
}
