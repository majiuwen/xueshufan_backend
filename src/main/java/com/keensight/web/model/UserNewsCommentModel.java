package com.keensight.web.model;

import com.keensight.web.db.entity.UserNewsComment;

import java.util.List;

public class UserNewsCommentModel extends UserNewsComment {
    /*评论点赞数*/
    private int commentLikeNum;
    /*评论点赞数*/
    private int level2CommentNum;
    /*最新动态二级级评论List*/
    private List<UserNewsCommentModel> replyCommentList;
    /*是否已点赞当前评论,点过赞为1，没点过赞为0*/
    private String isLikeCommentFlg;
    /*评论时间Str*/
    private String commentTimeStr;
    /*删除权限,有权限为1，没有权限为0*/
    private String delCommentFlg;
    /*评论者名*/
    private String commentUserName;
    /*评论者昵称*/
    private String commentNickName;
    /*评论者头像*/
    private String commentUserProfilePhotoPath;
    /*评论者用户头像*/
    private String commentUserProfilePath;
    /*回复者名*/
    private String replyUserName;
    /*回复者昵称*/
    private String replyNickName;
    /*评论者头像*/
    private String replyUserProfilePhotoPath;
    /*评论者用户头像*/
    private String replyUserProfilePath;
    /*二级有没有下一页*/
    private Boolean level2HasNextPage;
    /*下页页码*/
    private int level2NextPage;

    public List<UserNewsCommentModel> getReplyCommentList() {
        return replyCommentList;
    }

    public void setReplyCommentList(List<UserNewsCommentModel> replyCommentList) {
        this.replyCommentList = replyCommentList;
    }

    public String getIsLikeCommentFlg() {
        return isLikeCommentFlg;
    }

    public void setIsLikeCommentFlg(String isLikeCommentFlg) {
        this.isLikeCommentFlg = isLikeCommentFlg;
    }

    public String getCommentTimeStr() {
        return commentTimeStr;
    }

    public void setCommentTimeStr(String commentTimeStr) {
        this.commentTimeStr = commentTimeStr;
    }

    public String getDelCommentFlg() {
        return delCommentFlg;
    }

    public void setDelCommentFlg(String delCommentFlg) {
        this.delCommentFlg = delCommentFlg;
    }

    public String getCommentUserName() {
        return commentUserName;
    }

    public void setCommentUserName(String commentUserName) {
        this.commentUserName = commentUserName;
    }

    public String getCommentUserProfilePhotoPath() {
        return commentUserProfilePhotoPath;
    }

    public void setCommentUserProfilePhotoPath(String commentUserProfilePhotoPath) {
        this.commentUserProfilePhotoPath = commentUserProfilePhotoPath;
    }

    public String getReplyUserName() {
        return replyUserName;
    }

    public void setReplyUserName(String replyUserName) {
        this.replyUserName = replyUserName;
    }

    public int getLevel2CommentNum() {
        return level2CommentNum;
    }

    public void setLevel2CommentNum(int level2CommentNum) {
        this.level2CommentNum = level2CommentNum;
    }

    public int getCommentLikeNum() {
        return commentLikeNum;
    }

    public void setCommentLikeNum(int commentLikeNum) {
        this.commentLikeNum = commentLikeNum;
    }

    public Boolean getLevel2HasNextPage() {
        return level2HasNextPage;
    }

    public void setLevel2HasNextPage(Boolean level2HasNextPage) {
        this.level2HasNextPage = level2HasNextPage;
    }

    public int getLevel2NextPage() {
        return level2NextPage;
    }

    public void setLevel2NextPage(int level2NextPage) {
        this.level2NextPage = level2NextPage;
    }

    /**
     * Gets the value of commentNickName.
     *
     * @return the value of commentNickName
     */
    public String getCommentNickName() {
        return commentNickName;
    }

    /**
     * Sets the commentNickName.
     *
     * <p>You can use getCommentNickName() to get the value of commentNickName</p>
     *
     * @param commentNickName commentNickName
     */
    public void setCommentNickName(String commentNickName) {
        this.commentNickName = commentNickName;
    }

    /**
     * Gets the value of commentUserProfilePath.
     *
     * @return the value of commentUserProfilePath
     */
    public String getCommentUserProfilePath() {
        return commentUserProfilePath;
    }

    /**
     * Sets the commentUserProfilePath.
     *
     * <p>You can use getCommentUserProfilePath() to get the value of commentUserProfilePath</p>
     *
     * @param commentUserProfilePath commentUserProfilePath
     */
    public void setCommentUserProfilePath(String commentUserProfilePath) {
        this.commentUserProfilePath = commentUserProfilePath;
    }

    /**
     * Gets the value of replyNickName.
     *
     * @return the value of replyNickName
     */
    public String getReplyNickName() {
        return replyNickName;
    }

    /**
     * Sets the replyNickName.
     *
     * <p>You can use getReplyNickName() to get the value of replyNickName</p>
     *
     * @param replyNickName replyNickName
     */
    public void setReplyNickName(String replyNickName) {
        this.replyNickName = replyNickName;
    }

    /**
     * Gets the value of replyUserProfilePhotoPath.
     *
     * @return the value of replyUserProfilePhotoPath
     */
    public String getReplyUserProfilePhotoPath() {
        return replyUserProfilePhotoPath;
    }

    /**
     * Sets the replyUserProfilePhotoPath.
     *
     * <p>You can use getReplyUserProfilePhotoPath() to get the value of replyUserProfilePhotoPath</p>
     *
     * @param replyUserProfilePhotoPath replyUserProfilePhotoPath
     */
    public void setReplyUserProfilePhotoPath(String replyUserProfilePhotoPath) {
        this.replyUserProfilePhotoPath = replyUserProfilePhotoPath;
    }

    /**
     * Gets the value of replyUserProfilePath.
     *
     * @return the value of replyUserProfilePath
     */
    public String getReplyUserProfilePath() {
        return replyUserProfilePath;
    }

    /**
     * Sets the replyUserProfilePath.
     *
     * <p>You can use getReplyUserProfilePath() to get the value of replyUserProfilePath</p>
     *
     * @param replyUserProfilePath replyUserProfilePath
     */
    public void setReplyUserProfilePath(String replyUserProfilePath) {
        this.replyUserProfilePath = replyUserProfilePath;
    }
}
