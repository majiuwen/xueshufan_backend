package com.keensight.web.model;

import com.keensight.web.db.entity.UserInfo;

public class UserInfoModel extends UserInfo{
    private String birthdayStr;

    /**
     * Gets the value of birthdayStr.
     *
     * @return the value of birthdayStr
     */
    public String getBirthdayStr() {
        return birthdayStr;
    }

    /**
     * Sets the birthdayStr.
     *
     * <p>You can use getBirthdayStr() to get the value of birthdayStr</p>
     *
     * @param birthdayStr birthdayStr
     */
    public void setBirthdayStr(String birthdayStr) {
        this.birthdayStr = birthdayStr;
    }
}
