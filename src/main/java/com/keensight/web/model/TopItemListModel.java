package com.keensight.web.model;

/**
 * Top排行组件Mode
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/24] 新建
 */
public class TopItemListModel {
    /** ID */
    private String topItemId;
    /** 名称 */
    private String topItemName;
    /** 分数 */
    private String score;

    /**
     * 获取 ID
     *
     * @return topItemId ID
     */
    public String getTopItemId() {
        return topItemId;
    }

    /**
     * 设置 ID
     *
     * @param topItemId ID
     */
    public void setTopItemId(String topItemId) {
        this.topItemId = topItemId;
    }

    /**
     * 获取 名称
     *
     * @return topItemName 名称
     */
    public String getTopItemName() {
        return topItemName;
    }

    /**
     * 设置 名称
     *
     * @param topItemName 名称
     */
    public void setTopItemName(String topItemName) {
        this.topItemName = topItemName;
    }

    /**
     * 获取 分数
     *
     * @return score 分数
     */
    public String getScore() {
        return score;
    }

    /**
     * 设置 分数
     *
     * @param score 分数
     */
    public void setScore(String score) {
        this.score = score;
    }
}
