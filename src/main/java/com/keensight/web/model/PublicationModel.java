package com.keensight.web.model;

import com.keensight.web.db.entity.Publication;
import com.keensight.web.db.entity.UserLibraryLabelPublication;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public class PublicationModel extends Publication {
    /** 作者 list */
    private List<Map<String, String>> authorInfoList;
    /** 机构 list */
    private List<Map<String, String>> institutionList;
    /** 标签 list */
    private List<UserLibraryLabelPublication> labelList;
    /** 文献所在文件夹folderId list */
    private List<Long> folderIdList;
    /** 摘要 */
    private String summary;
    /** 星级 */
    private int rateScore;
    /** 期刊名 */
    private String journalName;
    /** 链接 */
    private String link;
    /** ISSN */
    private String issn;
    /** 出版商网址 */
    private String publisherLink;
    /** 会议系列名称 */
    private String conferenceSerieName;
    /** 会议名称 */
    private String conferenceInstanceName;
    /** 出版日期str */
    private String dateStr;

    /** 收藏按钮显示Flg (0：都不显示，1：显示，2：显示取消)*/
    private String saveBtnFlg;
    /** 推荐按钮显示Flg (0：都不显示，1：显示，2：显示取消)*/
    private String recommendBtnFlg;
    /** 关注按钮显示Flg (0：都不显示，1：显示，2：显示取消)*/
    private String followingBtnFlg;
    /** 分享按钮显示Flg (0：都不显示，1：显示，2：显示取消)*/
    private String shareBtnFlg;
    /** 下载按钮显示Flg (0：都不显示，1：显示，2：索要全文)*/
    private String downloadBtnFlg;
    /** 增加补充材料按钮显示Flg (0：不显示，1：显示)*/
    private String uploadOtherFileBtnFlg;
    /** 上传全文按钮显示Flg (0：都不显示，1：显示上传全文，2：有全文)*/
    private String uploadPdfBtnFlg;

    /** 修改按钮显示Flg (0：不显示，1：显示)*/
    private String updateBtnFlg;
    /** 块标题 */
    private String headerTitle;
    /** 时间差 */
    private String validateTime;
    /** 头像 */
    private String profilePhotoPath;
    /** 行为 */
    private String userAction;
    /** 关注用户名称 */
    private String displayAuthorName;
    /** 关注用户Id */
    private Long userIdFollowed;
    /** 用户Id */
    private Long userId;

    /** 上传文档时间 */
    private String uploadTime;
    /** 上传文档名称 */
    private String uploadFileName;

    /** 最新动态 */
    private UserNewsModel userNewsModel;

    /** 当前登录用户是否为作者Flg (true：是，false：不是)*/
    private Boolean authorFlg;

    /** 当前登录用户是否为作者Flg (true：是，false：不是)*/
    private Boolean isUser;

    /** 根据文献doi获取文献list*/
    private List<PublicationModel> publicationModelList;

    /** 根据文献doi获取文献Flg (1：有，2：没有)*/
    private String publicationByDoiFlg;

    private MultipartFile file;

    /** 匹配Flg (doi：doi，title：标题)*/
    private String matchFlg;

    /** 解析Flg (true：解析文献，false：匹配文献)*/
    private Boolean analysisFlg;

    /** 出版社链接 */
    private String publisherUrl;

    /** 是否收藏 */
    private Long clickFolderId;


    public String getUploadFileName() {
        return uploadFileName;
    }

    public void setUploadFileName(String uploadFileName) {
        this.uploadFileName = uploadFileName;
    }

    public String getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(String uploadTime) {
        this.uploadTime = uploadTime;
    }

    public String getUpdateBtnFlg() {
        return updateBtnFlg;
    }

    public void setUpdateBtnFlg(String updateBtnFlg) {
        this.updateBtnFlg = updateBtnFlg;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserIdFollowed() {
        return userIdFollowed;
    }

    public void setUserIdFollowed(Long userIdFollowed) {
        this.userIdFollowed = userIdFollowed;
    }

    public String getProfilePhotoPath() {
        return profilePhotoPath;
    }

    public void setProfilePhotoPath(String profilePhotoPath) {
        this.profilePhotoPath = profilePhotoPath;
    }

    public String getUserAction() {
        return userAction;
    }

    public void setUserAction(String userAction) {
        this.userAction = userAction;
    }

    public String getDisplayAuthorName() {
        return displayAuthorName;
    }

    public void setDisplayAuthorName(String displayAuthorName) {
        this.displayAuthorName = displayAuthorName;
    }

    public String getValidateTime() {
        return validateTime;
    }

    public void setValidateTime(String validateTime) {
        this.validateTime = validateTime;
    }

    public List<Map<String, String>> getAuthorInfoList() {
        return authorInfoList;
    }

    public void setAuthorInfoList(List<Map<String, String>> authorInfoList) {
        this.authorInfoList = authorInfoList;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getSaveBtnFlg() {
        return saveBtnFlg;
    }

    public void setSaveBtnFlg(String saveBtnFlg) {
        this.saveBtnFlg = saveBtnFlg;
    }

    public String getRecommendBtnFlg() {
        return recommendBtnFlg;
    }

    public void setRecommendBtnFlg(String recommendBtnFlg) {
        this.recommendBtnFlg = recommendBtnFlg;
    }

    public String getFollowingBtnFlg() {
        return followingBtnFlg;
    }

    public void setFollowingBtnFlg(String followingBtnFlg) {
        this.followingBtnFlg = followingBtnFlg;
    }

    public String getShareBtnFlg() {
        return shareBtnFlg;
    }

    public void setShareBtnFlg(String shareBtnFlg) {
        this.shareBtnFlg = shareBtnFlg;
    }

    public String getDownloadBtnFlg() {
        return downloadBtnFlg;
    }

    public void setDownloadBtnFlg(String downloadBtnFlg) {
        this.downloadBtnFlg = downloadBtnFlg;
    }

    public String getUploadOtherFileBtnFlg() {
        return uploadOtherFileBtnFlg;
    }

    public void setUploadOtherFileBtnFlg(String uploadOtherFileBtnFlg) {
        this.uploadOtherFileBtnFlg = uploadOtherFileBtnFlg;
    }

    public String getUploadPdfBtnFlg() {
        return uploadPdfBtnFlg;
    }

    public void setUploadPdfBtnFlg(String uploadPdfBtnFlg) {
        this.uploadPdfBtnFlg = uploadPdfBtnFlg;
    }

    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public List<Map<String, String>> getInstitutionList() {
        return institutionList;
    }

    public void setInstitutionList(List<Map<String, String>> institutionList) {
        this.institutionList = institutionList;
    }

    public List<UserLibraryLabelPublication> getLabelList() {
        return labelList;
    }

    public void setLabelList(List<UserLibraryLabelPublication> labelList) {
        this.labelList = labelList;
    }

    public int getRateScore() {
        return rateScore;
    }

    public void setRateScore(int rateScore) {
        this.rateScore = rateScore;
    }

    public List<Long> getFolderIdList() {
        return folderIdList;
    }

    public void setFolderIdList(List<Long> folderIdList) {
        this.folderIdList = folderIdList;
    }

    public UserNewsModel getUserNewsModel() {
        return userNewsModel;
    }

    public void setUserNewsModel(UserNewsModel userNewsModel) {
        this.userNewsModel = userNewsModel;
    }

    public Boolean getAuthorFlg() {
        return authorFlg;
    }

    public void setAuthorFlg(Boolean authorFlg) {
        this.authorFlg = authorFlg;
    }

    /**
     * Gets the value of journalName.
     *
     * @return the value of journalName
     */
    public String getJournalName() {
        return journalName;
    }

    /**
     * Sets the journalName.
     *
     * <p>You can use getJournalName() to get the value of journalName</p>
     *
     * @param journalName journalName
     */
    public void setJournalName(String journalName) {
        this.journalName = journalName;
    }

    /**
     * Gets the value of link.
     *
     * @return the value of link
     */
    public String getLink() {
        return link;
    }

    /**
     * Sets the link.
     *
     * <p>You can use getLink() to get the value of link</p>
     *
     * @param link link
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * Gets the value of issn.
     *
     * @return the value of issn
     */
    public String getIssn() {
        return issn;
    }

    /**
     * Sets the issn.
     *
     * <p>You can use getIssn() to get the value of issn</p>
     *
     * @param issn issn
     */
    public void setIssn(String issn) {
        this.issn = issn;
    }

    /**
     * Gets the value of publisherLink.
     *
     * @return the value of publisherLink
     */
    public String getPublisherLink() {
        return publisherLink;
    }

    /**
     * Sets the publisherLink.
     *
     * <p>You can use getPublisherLink() to get the value of publisherLink</p>
     *
     * @param publisherLink publisherLink
     */
    public void setPublisherLink(String publisherLink) {
        this.publisherLink = publisherLink;
    }

    /**
     * Gets the value of conferenceSerieName.
     *
     * @return the value of conferenceSerieName
     */
    public String getConferenceSerieName() {
        return conferenceSerieName;
    }

    /**
     * Sets the conferenceSerieName.
     *
     * <p>You can use getConferenceSerieName() to get the value of conferenceSerieName</p>
     *
     * @param conferenceSerieName conferenceSerieName
     */
    public void setConferenceSerieName(String conferenceSerieName) {
        this.conferenceSerieName = conferenceSerieName;
    }

    /**
     * Gets the value of conferenceInstanceName.
     *
     * @return the value of conferenceInstanceName
     */
    public String getConferenceInstanceName() {
        return conferenceInstanceName;
    }

    /**
     * Sets the conferenceInstanceName.
     *
     * <p>You can use getConferenceInstanceName() to get the value of conferenceInstanceName</p>
     *
     * @param conferenceInstanceName conferenceInstanceName
     */
    public void setConferenceInstanceName(String conferenceInstanceName) {
        this.conferenceInstanceName = conferenceInstanceName;
    }

    /**
     * Gets the value of dateStr.
     *
     * @return the value of dateStr
     */
    public String getDateStr() {
        return dateStr;
    }

    /**
     * Sets the dateStr.
     *
     * <p>You can use getDateStr() to get the value of dateStr</p>
     *
     * @param dateStr dateStr
     */
    public void setDateStr(String dateStr) {
        this.dateStr = dateStr;
    }

    public List<PublicationModel> getPublicationModelList() {
        return publicationModelList;
    }

    public void setPublicationModelList(List<PublicationModel> publicationModelList) {
        this.publicationModelList = publicationModelList;
    }

    public String getPublicationByDoiFlg() {
        return publicationByDoiFlg;
    }

    public void setPublicationByDoiFlg(String publicationByDoiFlg) {
        this.publicationByDoiFlg = publicationByDoiFlg;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getMatchFlg() {
        return matchFlg;
    }

    public void setMatchFlg(String matchFlg) {
        this.matchFlg = matchFlg;
    }

    public Boolean getAnalysisFlg() {
        return analysisFlg;
    }

    public void setAnalysisFlg(Boolean analysisFlg) {
        this.analysisFlg = analysisFlg;
    }

    public String getPublisherUrl() {
        return publisherUrl;
    }

    public void setPublisherUrl(String publisherUrl) {
        this.publisherUrl = publisherUrl;
    }

    public Long getClickFolderId() {
        return clickFolderId;
    }

    public void setClickFolderId(Long clickFolderId) {
        this.clickFolderId = clickFolderId;
    }

    /**
     * Gets the value of isUser.
     *
     * @return the value of isUser
     */
    public Boolean getIsUser() {
        return isUser;
    }

    /**
     * Sets the isUser.
     *
     * <p>You can use getUser() to get the value of isUser</p>
     *
     * @param isUser isUser
     */
    public void setIsUser(Boolean isUser) {
        this.isUser = isUser;
    }
}
