package com.keensight.web.model;

import java.util.List;

public class SummaryTranslationModel {
    /** 翻译结果 */
    private List<String> translation;
    /** 错误返回码 */
    private String errorCode;
    /** 源语言 */
    private String query;
    /** 词义 */
    private String basic;
    /** 词义 */
    private List<String> web;
    /** 源语言和目标语言 */
    private String l;
    /** 词典deeplink */
    private String dict;
    /** webdeeplink */
    private String webdict;
    /** 翻译结果发音地址 */
    private String tSpeakUrl;
    /** 源语言发音地址 */
    private String speakUrl;
    /** 单词校验后的结果 */
    private List<String> returnPhrase;

    public List<String> getTranslation() {
        return translation;
    }

    public void setTranslation(List<String> translation) {
        this.translation = translation;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getBasic() {
        return basic;
    }

    public void setBasic(String basic) {
        this.basic = basic;
    }

    public List<String> getWeb() {
        return web;
    }

    public void setWeb(List<String> web) {
        this.web = web;
    }

    public String getL() {
        return l;
    }

    public void setL(String l) {
        this.l = l;
    }

    public String getDict() {
        return dict;
    }

    public void setDict(String dict) {
        this.dict = dict;
    }

    public String getWebdict() {
        return webdict;
    }

    public void setWebdict(String webdict) {
        this.webdict = webdict;
    }

    public String gettSpeakUrl() {
        return tSpeakUrl;
    }

    public void settSpeakUrl(String tSpeakUrl) {
        this.tSpeakUrl = tSpeakUrl;
    }

    public String getSpeakUrl() {
        return speakUrl;
    }

    public void setSpeakUrl(String speakUrl) {
        this.speakUrl = speakUrl;
    }

    public List<String> getReturnPhrase() {
        return returnPhrase;
    }

    public void setReturnPhrase(List<String> returnPhrase) {
        this.returnPhrase = returnPhrase;
    }
}
