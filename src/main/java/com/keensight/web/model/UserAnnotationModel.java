package com.keensight.web.model;

import com.keensight.web.db.entity.UserAnnotation;

import java.util.Date;

public class UserAnnotationModel extends UserAnnotation {

    /** 批注日期 */
    private String annotationDateMonAndDayStr;
    /** 批注时间  */
    private String annotationDateHourAndMinStr;

    public String getAnnotationDateMonAndDayStr() {
        return annotationDateMonAndDayStr;
    }

    public void setAnnotationDateMonAndDayStr(String annotationDateMonAndDayStr) {
        this.annotationDateMonAndDayStr = annotationDateMonAndDayStr;
    }

    public String getAnnotationDateHourAndMinStr() {
        return annotationDateHourAndMinStr;
    }

    public void setAnnotationDateHourAndMinStr(String annotationDateHourAndMinStr) {
        this.annotationDateHourAndMinStr = annotationDateHourAndMinStr;
    }
}
