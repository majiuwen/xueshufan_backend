package com.keensight.web.model;

import com.keensight.web.db.entity.User;

public class UserRelationNodeModel extends User {
    /**
     * 关系用户ID
     */
    private String id;
    /**
     * 关系用户头像
     */
    private String innerHTML;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInnerHTML() {
        return innerHTML;
    }

    public void setInnerHTML(String innerHTML) {
        this.innerHTML = innerHTML;
    }
}
