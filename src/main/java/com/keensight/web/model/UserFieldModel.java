package com.keensight.web.model;

import com.keensight.web.db.entity.UserDiscipline;
import com.keensight.web.db.entity.UserField;

import java.util.List;

public class UserFieldModel extends UserField {
    /** 修改按钮显示Flg (0：不显示，1：显示)*/
    private String updateBtnFlg;
    /** 研究领域 */
    private List<UserField> userFields;
    /** 学科 */
    private List<UserDiscipline> userDisciplines;

    public List<UserField> getUserFields() {
        return userFields;
    }

    public void setUserFields(List<UserField> userFields) {
        this.userFields = userFields;
    }

    public List<UserDiscipline> getUserDisciplines() {
        return userDisciplines;
    }

    public void setUserDisciplines(List<UserDiscipline> userDisciplines) {
        this.userDisciplines = userDisciplines;
    }

    public String getUpdateBtnFlg() {
        return updateBtnFlg;
    }

    public void setUpdateBtnFlg(String updateBtnFlg) {
        this.updateBtnFlg = updateBtnFlg;
    }
}
