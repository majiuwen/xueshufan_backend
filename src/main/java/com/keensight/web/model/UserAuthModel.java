package com.keensight.web.model;

import com.keensight.web.db.entity.UserAuth;

public class UserAuthModel extends UserAuth {
    /** 手机号存在Flg 1：存在 0 不存在 */
    private String mobileExistenceFlg;
    /** 密码存在Flg 1：存在 0 不存在 */
    private String passwordExistenceFlg;

    public String getMobileExistenceFlg() {
        return mobileExistenceFlg;
    }

    public void setMobileExistenceFlg(String mobileExistenceFlg) {
        this.mobileExistenceFlg = mobileExistenceFlg;
    }

    public String getPasswordExistenceFlg() {
        return passwordExistenceFlg;
    }

    public void setPasswordExistenceFlg(String passwordExistenceFlg) {
        this.passwordExistenceFlg = passwordExistenceFlg;
    }
}
