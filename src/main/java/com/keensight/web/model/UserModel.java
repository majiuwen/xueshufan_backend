package com.keensight.web.model;

import com.keensight.web.db.entity.User;

public class UserModel extends User {
    /** 添加新的研究按钮显示Flg (0：不显示，1：显示)*/
    private String filedBtnFlg;
    /** 修改按钮显示Flg (0：不显示，1：显示)*/
    private String updateBtnFlg;
    /** 关注按钮显示Flg (0：都不显示，1：显示关注，2：显示取消关注)*/
    private String followingBtnFlg;
    /** 联系按钮显示Flg (0：不显示，1：显示)*/
    private String contactBtnFlg;
    /** 邀请按钮显示Flg (0：不显示，1：显示)*/
    private String inviteBtnFlg;
    /** 关注用户Id */
    private Long userIdFollowedForReq;
    /** 用户Id */
    private Long userIdForReq;
    /** 图层等级 */
    private Integer zoom;
    /** 国家 */
    private String country;
    /** 是否为华裔 */
    private String isethniccn;
    /** 经度 */
    private Double longitude;
    /** 纬度 */
    private Double latitude;
    /** 領域 */
    private String fieldStr;
    /** 已编辑用户信息 */
    private UserInfoModel userInfo;
    /** 关注用户简介 */
    private String introduction;

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public UserInfoModel getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfoModel userInfo) {
        this.userInfo = userInfo;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Integer getZoom() {
        return zoom;
    }

    public void setZoom(Integer zoom) {
        this.zoom = zoom;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getInviteBtnFlg() {
        return inviteBtnFlg;
    }

    public void setInviteBtnFlg(String inviteBtnFlg) {
        this.inviteBtnFlg = inviteBtnFlg;
    }

    public String getUpdateBtnFlg() {
        return updateBtnFlg;
    }

    public void setUpdateBtnFlg(String updateBtnFlg) {
        this.updateBtnFlg = updateBtnFlg;
    }

    public String getFiledBtnFlg() {
        return filedBtnFlg;
    }

    public void setFiledBtnFlg(String filedBtnFlg) {
        this.filedBtnFlg = filedBtnFlg;
    }

    public String getFollowingBtnFlg() {
        return followingBtnFlg;
    }

    public void setFollowingBtnFlg(String followingBtnFlg) {
        this.followingBtnFlg = followingBtnFlg;
    }

    public String getContactBtnFlg() {
        return contactBtnFlg;
    }

    public void setContactBtnFlg(String contactBtnFlg) {
        this.contactBtnFlg = contactBtnFlg;
    }

    public Long getUserIdFollowedForReq() {
        return userIdFollowedForReq;
    }

    public void setUserIdFollowedForReq(Long userIdFollowedForReq) {
        this.userIdFollowedForReq = userIdFollowedForReq;
    }

    public Long getUserIdForReq() {
        return userIdForReq;
    }

    public void setUserIdForReq(Long userIdForReq) {
        this.userIdForReq = userIdForReq;
    }

    public String getFieldStr() {
        return fieldStr;
    }

    public void setFieldStr(String fieldStr) {
        this.fieldStr = fieldStr;
    }
}
