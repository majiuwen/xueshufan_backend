package com.keensight.web.model;

import com.keensight.web.db.entity.User;

public class UserRelationShipModel extends User {
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getIsHideArrow() {
        return isHideArrow;
    }

    public void setIsHideArrow(String isHideArrow) {
        this.isHideArrow = isHideArrow;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    /**
     * 关系类型
     */
    private String text;

    /**
     * 关系起始节点ID
     */
    private String from;

    /**
     * 关系结束节点ID
     */
    private String to;

    /**
     * 关系指向是否为双重
     */
    private String isHideArrow;

    /**
     * 关系类型
     */
    private String data;

    /**
     * 关系线颜色
     */
    private String color;

    /**
     * 关系字体颜色
     */
    private String fontColor;
}
