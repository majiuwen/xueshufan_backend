package com.keensight.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.keensight.web.db.entity.UserLibraryFolder;

import java.util.List;

public class UserLibraryFolderModel extends UserLibraryFolder{

    // 文件夹的子文件夹
    @JsonProperty(value="children")
    List<UserLibraryFolderModel> childFolder;

    public List<UserLibraryFolderModel> getChildFolder() {
        return childFolder;
    }

    public void setChildFolder(List<UserLibraryFolderModel> childFolder) {
        this.childFolder = childFolder;
    }
}
