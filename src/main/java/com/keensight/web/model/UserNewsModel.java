package com.keensight.web.model;

import com.keensight.web.db.entity.UserNews;
import com.keensight.web.db.entity.UserNewsFiles;

import java.util.List;

public class UserNewsModel extends UserNews {
    /*文件个数*/
    private Integer fileNum;
    /*文件上传类型*/
    private String uploadType;
    /*上传文件List*/
    private List<NewsFileModel> fileNameList;

    /*学者名*/
    private String displayName;

    /*学者昵称*/
    private String nickname;

    /*学者头像*/
    private String profilePhotoPath;

    /*用户头像*/
    private String profilePath;
    /*上传时间str*/
    private String publishTime;
    /*视频截图*/
    private String videoCut;
    /*学者头像*/
    private String headImg;
    /*查询文件List*/
    private List<UserNewsFiles> userNewsFilesList;
    /*显示文件类型*/
    private String showFileType;
    /*是否点赞Flg*/
    private String isLikeBtnFlg;
    /*查看权限Flg 0不可见，1可见*/
    private String visiblityFlg;

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getVideoCut() {
        return videoCut;
    }

    public void setVideoCut(String videoCut) {
        this.videoCut = videoCut;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(String publishTime) {
        this.publishTime = publishTime;
    }

    public String getUploadType() {
        return uploadType;
    }

    public void setUploadType(String uploadType) {
        this.uploadType = uploadType;
    }

    public Integer getFileNum() {
        return fileNum;
    }

    public void setFileNum(Integer fileNum) {
        this.fileNum = fileNum;
    }

    public List<NewsFileModel> getFileNameList() {
        return fileNameList;
    }

    public void setFileNameList(List<NewsFileModel> fileNameList) {
        this.fileNameList = fileNameList;
    }

    public List<UserNewsFiles> getUserNewsFilesList() {
        return userNewsFilesList;
    }

    public void setUserNewsFilesList(List<UserNewsFiles> userNewsFilesList) {
        this.userNewsFilesList = userNewsFilesList;
    }

    public String getShowFileType() {
        return showFileType;
    }

    public void setShowFileType(String showFileType) {
        this.showFileType = showFileType;
    }

    public String getIsLikeBtnFlg() {
        return isLikeBtnFlg;
    }

    public void setIsLikeBtnFlg(String isLikeBtnFlg) {
        this.isLikeBtnFlg = isLikeBtnFlg;
    }

    public String getVisiblityFlg() {
        return visiblityFlg;
    }

    public void setVisiblityFlg(String visiblityFlg) {
        this.visiblityFlg = visiblityFlg;
    }

    /**
     * Gets the value of nickname.
     *
     * @return the value of nickname
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * Sets the nickname.
     *
     * <p>You can use getNickname() to get the value of nickname</p>
     *
     * @param nickname nickname
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * Gets the value of profilePhotoPath.
     *
     * @return the value of profilePhotoPath
     */
    public String getProfilePhotoPath() {
        return profilePhotoPath;
    }

    /**
     * Sets the profilePhotoPath.
     *
     * <p>You can use getProfilePhotoPath() to get the value of profilePhotoPath</p>
     *
     * @param profilePhotoPath profilePhotoPath
     */
    public void setProfilePhotoPath(String profilePhotoPath) {
        this.profilePhotoPath = profilePhotoPath;
    }

    /**
     * Gets the value of profilePath.
     *
     * @return the value of profilePath
     */
    public String getProfilePath() {
        return profilePath;
    }

    /**
     * Sets the profilePath.
     *
     * <p>You can use getProfilePath() to get the value of profilePath</p>
     *
     * @param profilePath profilePath
     */
    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }
}
