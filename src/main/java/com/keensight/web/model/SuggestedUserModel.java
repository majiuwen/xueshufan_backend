package com.keensight.web.model;

import com.keensight.web.db.entity.SuggestedUserToFollow;

public class SuggestedUserModel extends SuggestedUserToFollow {

    /** 关注按钮显示Flg (0：都不显示，1：显示关注，2：显示取消关注)*/
    private String followingBtnFlg;

    /** 联系按钮显示Flg (0：不显示，1：显示)*/
    private String contactBtnFlg;

    public String getFollowingBtnFlg() {
        return followingBtnFlg;
    }

    public void setFollowingBtnFlg(String followingBtnFlg) {
        this.followingBtnFlg = followingBtnFlg;
    }

    public String getContactBtnFlg() {
        return contactBtnFlg;
    }

    public void setContactBtnFlg(String contactBtnFlg) {
        this.contactBtnFlg = contactBtnFlg;
    }
}
