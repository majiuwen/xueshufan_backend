package com.keensight.web.model;

public class PublicationReferenceModel {

    /** 参考文献数 */
    private int publicationReferenceCount;
    /** 引证文献数 */
    private int publicationCitationCount;

    public int getPublicationReferenceCount() {
        return publicationReferenceCount;
    }

    public void setPublicationReferenceCount(int publicationReferenceCount) {
        this.publicationReferenceCount = publicationReferenceCount;
    }

    public int getPublicationCitationCount() {
        return publicationCitationCount;
    }

    public void setPublicationCitationCount(int publicationCitationCount) {
        this.publicationCitationCount = publicationCitationCount;
    }
}
