package com.keensight.web.service;


import cn.newtouch.dl.solr.model.SolrResultModel;
import cn.newtouch.dl.turbocloud.helper.FileHelper;
import cn.newtouch.dl.turbocloud.model.UserModel;
import cn.newtouch.dl.turbocloud.service.BaseService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.keensight.solr.model.SolrPublicationSearchModel;
import com.keensight.solr.service.SolrPublicationSearchService;
import com.keensight.web.constants.WebConst;
import com.keensight.web.constants.enums.FileType;
import com.keensight.web.constants.enums.UploadFolder;
import com.keensight.web.controller.UserAchievementsController;
import com.keensight.web.db.entity.*;
import com.keensight.web.db.mapper.*;
import com.keensight.web.helper.FileServerHelper;
import com.keensight.web.model.PublicationModel;
import com.keensight.web.model.UserNewsCommentModel;
import com.keensight.web.model.UserNewsModel;
import com.keensight.web.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.text.Normalizer;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PublicationService extends BaseService {
    @Value("${keensight.file.server.path}")
    private String keensightFileServerPath;
    @Value("${create.pdf.thumbnail.dpi}")
    private String createPdfThumbnailDpi;
    @Resource
    private PublicationMapper publicationMapper;
    @Resource
    private DocumentMapper documentMapper;

    @Resource
    private SolrPublicationSearchService solrPublicationSearchService;
    @Resource
    private FileServerHelper fileServerHelper;
    @Resource
    private UserLibraryLabelPublicationMapper userLibraryLabelPublicationMapper;
    @Resource
    private UserLibraryMapper userLibraryMapper;
    @Resource
    private UserLibraryFolderPublicationMapper userLibraryFolderPublicationMapper;
    @Resource
    private UserNewsMapper userNewsMapper;
    @Resource
    private UserNewsFilesMapper userNewsFilesMapper;
    @Resource
    private UserNewsLikeMapper userNewsLikeMapper;
    @Resource
    private UserAuthorFollowingMapper userAuthorFollowingMapper;
    @Resource
    private UserNewsCommentMapper userNewsCommentMapper;
    @Autowired
    private FileHelper fileHelper;

    /**
     * 文献详细信息一览取得(共通取得用)
     *
     * @param publicationIds 文献ID

     * @return
     */
    public List<PublicationModel> getPublicationDetailList(List<Long> publicationIds, boolean userFavorite, boolean userHome) {
        return getPublicationDetailList(publicationIds, null, userFavorite, userHome);
    }

    /**
     * 文献详细信息一览取得(共通取得用)
     *
     * @param publicationIds 文献ID

     * @return
     */
    public List<PublicationModel> getPublicationDetailList(List<Long> publicationIds) {
        return getPublicationDetailList(publicationIds, null, false, false);
    }

    /**
     * 文献详细信息一览取得(共通取得用)
     *
     * @param publicationIds 文献ID

     * @return
     */
    public List<PublicationModel> getPublicationDetailList(List<Long> publicationIds, boolean userFavorite) {
        return getPublicationDetailList(publicationIds, null, userFavorite, false);
    }

    /**
     * 文献详细信息一览取得(共通取得用)
     *
     * @param publicationIds 文献ID
     * @param userId         login用户ID
     * @return
     */
    private List<PublicationModel> getPublicationDetailList(List<Long> publicationIds, Long userId , boolean userFavorite, boolean userHome) {
        List<PublicationModel> publicationModelList = new ArrayList<PublicationModel>();

        UserModel loginUserModel = getLoginInfo();
        if (loginUserModel != null && !StringUtils.isEmpty(loginUserModel.getUserId())) {
            userId = Long.valueOf(loginUserModel.getUserId());
        } else {
            userId = null;
        }

        // 参数判断
        if (StringUtils.isEmpty(publicationIds)) {
            return publicationModelList;
        }
        List<Publication> publicationList = new ArrayList<>();
        List<Long> newPublicationIds = new ArrayList<>();
        if(userHome){
            List<Long> newsIds = new ArrayList<>();
            for(Long publicationId : publicationIds){
                String publicationIdStr = String.valueOf(publicationId);
                String userAction = publicationIdStr.substring(publicationIdStr.length() - 1);
                String publicationIdNew = publicationIdStr.substring(0, publicationIdStr.length() - 1);
                if ("7".equals(userAction)){
                    newsIds.add(Long.parseLong(publicationIdNew));
                } else {
                    newPublicationIds.add(Long.parseLong(publicationIdNew));
                }
            }
            if (newsIds.size() > 0) {
                List<UserNewsModel> userNewsModelList = userNewsMapper.getNewDevelopmentsByIds(userId, newsIds);
                for (UserNewsModel userNews : userNewsModelList) {
                    PublicationModel publicationModel = new PublicationModel();
                    List<UserNewsFiles> filesList = userNewsFilesMapper.getUserNewsFiles(userNews.getNewsId());

                    userNews.setUserNewsFilesList(filesList);
                    if (filesList.size() > 0) {
                        if (("1").equals(filesList.get(0).getNewsFileType())) {
                            userNews.setShowFileType("1");
                            userNews.setVideoCut(filesList.get(0).getNewsFileUrl().substring(0, filesList.get(0).getNewsFileUrl().lastIndexOf(".")) + ".jpg");
                        } else {
                            userNews.setShowFileType("0");
                        }
                    }
                    publicationModel.setPublicationId(userNews.getNewsId());
                    publicationModel.setUserAction("7");
                    publicationModel.setUserNewsModel(userNews);
                    publicationModelList.add(publicationModel);
                }
                List<UserNewsCommentModel> firstLevelComments = new ArrayList<>();
                firstLevelComments = userNewsCommentMapper.getFirstLevelComments(userId, newsIds);
                for (UserNewsModel userNews : userNewsModelList) {
                    for (UserNewsCommentModel userNewsCommentModel : firstLevelComments) {
                        if (userNews.getNewsId().equals(userNewsCommentModel.getNewsId())) {
                            userNews.setCommentNum(userNews.getCommentNum() +  userNewsCommentModel.getLevel2CommentNum());
                        }
                    }
                }
            }
            if (newPublicationIds.size() > 0) {
                publicationList = publicationMapper.getPublicationDetailList(newPublicationIds, userId);
            }
        } else {
            publicationList = publicationMapper.getPublicationDetailList(publicationIds, userId);
        }

        SolrResultModel solrResultModel = new SolrResultModel();
        // 摘要solr获取
        if (userHome) {
            solrResultModel = solrPublicationSearchService.getPublicationDetailList(newPublicationIds);
        } else {
            solrResultModel = solrPublicationSearchService.getPublicationDetailList(publicationIds);
        }

        for (Publication publication : publicationList) {
            PublicationModel publicationModel = new PublicationModel();
            BeanUtils.copyProperties(publication, publicationModel);

            // 摘要
            if (solrResultModel.getRows() > 0) {
                List<SolrPublicationSearchModel> solrPublicationSearchModelList = solrResultModel.getENTRY();
                for (SolrPublicationSearchModel solrPublicationSearchModel : solrPublicationSearchModelList) {
                    if (String.valueOf(publicationModel.getPublicationId()).equals(solrPublicationSearchModel.getId())) {
                        publicationModel.setSummary(solrPublicationSearchModel.getSummary());
                        break;
                    }
                }
            }

            // 作者处理，转换成list
            String authorInfos = publicationModel.getAuthorInfos();
            LinkedHashMap<String, Map<String, String>> authorInfosMap = new LinkedHashMap<>();
            List<Map<String, String>> institutionList = new ArrayList<>();
            if (!StringUtils.isEmpty(authorInfos)) {
                for (String authorInfo : authorInfos.split(";")) {
                    Map<String, String> authorMap = new HashMap<>();
                    String[] authorAttr = authorInfo.split("#", 4);

                    String authorId = authorAttr[0];
                    // 去掉重复作者
                    if (authorInfosMap.containsKey(authorId)) {
                        authorMap = authorInfosMap.get(authorId);
                    } else {
                        authorMap = new HashMap<>();
                        authorInfosMap.put(authorId, authorMap);
                    }

                    // 作者ID，名
                    authorMap.put("authorId", authorId);
                    authorMap.put("authorName", authorAttr[1]);
                    if (authorMap.get("institutionsIndex") == null) {
                        authorMap.put("institutionsIndex", "");
                    }

                    // 没有机构的时候，不需要处理
                    if ("-1".equals(authorAttr[2])) {
                        continue;
                    }

                    // 机构index
                    int institutionsIndex = 1;
                    boolean addFlg = false;
                    String institutionsIndexStr = authorMap.get("institutionsIndex");

                    // 先查看机构list中，是否有这个机构
                    for (Map<String, String> institution : institutionList) {
                        if (institution.get("institutionId").equals(authorAttr[2])) {
                            if (!StringUtils.isEmpty(institutionsIndexStr)) {
                                institutionsIndexStr = institutionsIndexStr + "," + institutionsIndex;
                            } else {
                                institutionsIndexStr = institutionsIndex + "";
                            }

                            addFlg = true;
                            authorMap.put("institutionsIndex", institutionsIndexStr);
                            break;
                        }
                        institutionsIndex++;
                    }

                    // 新机构时，增加机构list
                    if (!addFlg) {
                        if (!StringUtils.isEmpty(institutionsIndexStr)) {
                            institutionsIndexStr = institutionsIndexStr + "," + institutionsIndex;
                        } else {
                            institutionsIndexStr = institutionsIndex + "";
                        }
                        authorMap.put("institutionsIndex", institutionsIndexStr);

                        Map<String, String> institution = new HashMap<>();
                        institution.put("institutionId", authorAttr[2]);
                        institution.put("institutionName", authorAttr[3]);
                        institution.put("institutionIndex", institutionsIndex + "");

                        institutionList.add(institution);
                    }
                }

                publicationModel.setAuthorInfoList(new ArrayList<>(authorInfosMap.values()));
                publicationModel.setInstitutionList(institutionList);
            }

            if (userFavorite) {
                // 星级
                Long publicationId = publicationModel.getPublicationId();
                UserLibraryExample userLibraryExample = new UserLibraryExample();
                UserLibraryExample.Criteria criteria =  userLibraryExample.createCriteria();
                criteria.andPublicationIdEqualTo(publicationId);
                criteria.andUserIdEqualTo(userId);
                List<UserLibrary> userLibraries = userLibraryMapper.selectByExample(userLibraryExample);
                publicationModel.setRateScore(Integer.valueOf(userLibraries.get(0).getPublicationGrade()));
                // 标签
                List<UserLibraryLabelPublication> labelList = userLibraryLabelPublicationMapper.getUserLibraryLabelPublicationList(publicationId, userId);
                if(labelList == null) {
                    labelList = new ArrayList<>();
                }
                publicationModel.setLabelList(labelList);
            }

            // 根据文献id反选文件夹
            Long publicationId = publicationModel.getPublicationId();
            List<Long> folderIds = userLibraryFolderPublicationMapper.getUserLibraryFolderIdsList(publicationId);
            if(folderIds == null) {
                folderIds = new ArrayList<>();
            }
            publicationModel.setFolderIdList(folderIds);

            // 按钮显示状态设定
            // 关注
            if ("1".equals(publicationModel.getFollowingFlag())) {
                publicationModel.setFollowingBtnFlg("2");
            } else {
                publicationModel.setFollowingBtnFlg("1");
            }

            // 推荐
            if ("1".equals(publicationModel.getRecommendationFlag())) {
                publicationModel.setRecommendBtnFlg("2");
            } else {
                publicationModel.setRecommendBtnFlg("1");
            }

            // 收藏
            if ("1".equals(publicationModel.getLibraryFlag())) {
                publicationModel.setSaveBtnFlg("2");
            } else {
                publicationModel.setSaveBtnFlg("1");
            }

            // 分享
            publicationModel.setShareBtnFlg("1");

            // 下载/要全文
            if (StringUtils.isEmpty(publicationModel.getFulltextPath()) && StringUtils.isEmpty(publicationModel.getDocPathNotAuthor())) {
                // 无文件时，login这个人是作者
                if (authorInfosMap.containsKey(userId + "") || userFavorite) {
                    publicationModel.setUploadPdfBtnFlg("1");
                    publicationModel.setUploadOtherFileBtnFlg("1");
                } else{
                  //显示要全文
                    publicationModel.setDownloadBtnFlg("2");
                }
            } else {
                // 文件是公开，或者login这个人是作者
                if (publicationModel.getPublicDoc() || authorInfosMap.containsKey(userId + "") || !StringUtils.isEmpty(publicationModel.getDocPathNotAuthor())) {
                    publicationModel.setDownloadBtnFlg("1");
                } else {
                    publicationModel.setDownloadBtnFlg("2");
                }
            }

            // login这个人是否为作者
            if (authorInfosMap.containsKey(userId + "")) {
                publicationModel.setAuthorFlg(true);
            } else  {
                publicationModel.setAuthorFlg(false);
            }

            // pdf 文件服务器url获取
            if (!StringUtils.isEmpty(publicationModel.getFulltextPath())) {
                String url_s3 = AwsS3Utils.signUrl(publicationModel.getFulltextPath());
                publicationModel.setFulltextPath(url_s3);
            }

            if (!StringUtils.isEmpty(publicationModel.getDocPathNotAuthor())) {
                String url_s3 = AwsS3Utils.signUrl(publicationModel.getDocPathNotAuthor());
                publicationModel.setDocPathNotAuthor(url_s3);
            }

            // 上线日期转成字符串格式
            if (publicationModel.getDate() != null && ("3".equals(publicationModel.getStatus()) || "4".equals(publicationModel.getStatus()))) {
                publicationModel.setDateStr(DateUtil.dateFormat(publicationModel.getDate(), "YYYY-MM-DD"));
            }

            publicationModelList.add(publicationModel);
        }

        // 共通检索结果重排序
        List<PublicationModel> resultDetailList = new ArrayList<>();
        if(userHome){
            for (Long publicationId : publicationIds) {
                resultDetailList.addAll(publicationModelList.stream().filter(publication -> String.valueOf(publicationId).substring(0,String.valueOf(publicationId).length()-1).equals(String.valueOf(publication.getPublicationId()))).collect(Collectors.toList()));
            }
        } else {
            for (Long publicationId : publicationIds) {
                resultDetailList.addAll(publicationModelList.stream().filter(publication -> publicationId.equals(publication.getPublicationId())).collect(Collectors.toList()));
            }
        }


        return resultDetailList;
    }

    /**
     * 文献统计结果取得方法
     *
     * @param userId 用户ID
     * @return 文献统计结果集合
     */
    public List<Publication> getPublicationStatisticsByUserId(Long userId) {
        return publicationMapper.getPublicationStatisticsByUserId(userId);
    }

    /**
     * 根据检索条件查询文献ID集合
     *
     * @param userId      用户ID
     * @param docTypeList 文献类型集合
     * @param sortColumns 排序列
     * @param pageNum     开始页数
     * @param pageSize    显示件数
     * @return 文献ID集合
     */
    public PageInfo<Long> getPublicationIdList(Long userId, List<String> docTypeList, String sortColumns, Integer pageNum, Integer pageSize) {
        // pageNum=1，pageSize=0，pageSizeZero=true 时检索全部信息
        PageHelper.startPage(pageNum, pageSize, true, null, true);
        List<Long> publicationIdList = publicationMapper.getPublicationIdList(userId, docTypeList, sortColumns);
        return new PageInfo<Long>(publicationIdList);
    }

    /**
     * 上传全文更新文献信息方法
     *
     * @param publicationId 文献ID
     * @param userId        用户ID
     * @param isPublicDoc   文档公开
     * @param file          上传的文件
     * @return 更新成功的件数
     * @throws Exception
     */
    public Document update4FulltextUpload(Long publicationId, Long userId, Boolean isPublicDoc, MultipartFile file, Boolean authorFlg) throws Exception {
        Document document = new Document();
        // 全文路径
        String fulltextPath = UploadFolder.UPLOAD.getFolderPath().concat("/").concat(UploadFolder.PDF.getFolderPath().concat("/").concat(DateUtil.getSysDate(WebConst.DATE_FORMAT_YEAR_MONTH)).concat("/"));
        // 缩略图路径
        String thumbnailPath = UploadFolder.UPLOAD.getFolderPath().concat("/").concat(UploadFolder.PDF_THUMBNAIL.getFolderPath().concat("/").concat(DateUtil.getSysDate(WebConst.DATE_FORMAT_YEAR_MONTH)).concat("/"));
        // 原文件名
        String oldFileName = file.getOriginalFilename();
        // 新文件名
        String newFileName;
        // 根据文献ID检索文献信息
        PublicationKey publicationKey = new PublicationKey();
        publicationKey.setPublicationId(publicationId);
        Publication publication = publicationMapper.selectByPrimaryKey(publicationKey);
        if (publication != null) {
            int rec = 0;
            // 文档表新增处理
            document.setPublicationId(publicationId);
            document.setUserId(userId);
            // 判断是否为作者
            if (authorFlg) {
                // 作者上传DocType为1
                document.setDocType("1");
            } else {
                // 作者上传DocType为10
                document.setDocType("10");
            }
            document.setPublicDoc(isPublicDoc);
            document.setCreatedAt(DateUtil.getSysDate());
            rec = documentMapper.insert(document);
            if (rec > 0 && document.getDocId() != null) {
                newFileName = document.getDocId() + WebConst.FILE_SUFFIX_PDF;
                // 文献表更新处理
                // 全文路径
                if (authorFlg) {
                    publication.setFulltextPath(fulltextPath + newFileName);
                }
                // 文档公开
                publication.setPublicDoc(isPublicDoc);
                // 更新数据
                rec = publicationMapper.updateByPrimaryKey(publication);
                if (rec > 0) {
//                    boolean uploadResult = FileUtil.fileUpload(file.getInputStream(), newFileName, keensightFileServerPath.concat(fulltextPath));
                    // 上传到s3储存桶中
                    AwsS3Utils.upload(fulltextPath + newFileName, file.getInputStream());
                    // 创建并保存pdf文件第一页缩略图
                    String thumbnailName = document.getDocId() + WebConst.FILE_SUFFIX_JPEG;
                    Integer dpi = null;
                    if (!StringUtils.isEmpty(createPdfThumbnailDpi)) {
                        dpi = Integer.valueOf(createPdfThumbnailDpi);
                    }
                    boolean createResult = PDFUtil.createPDFThumbnail(file.getInputStream(), 0, dpi, keensightFileServerPath.concat(thumbnailPath), thumbnailName, FileType.JPEG.getFileType());
                    if (!createResult) {
                        // 缩略图创建失败的场合，手动跑出异常，事务回滚
                        throw new Exception();
                    }
                    // 文档表更新处理
                    // 原文件名
                    document.setOriginalFileName(oldFileName);
                    // 全文路径
                    document.setDocPath(fulltextPath + newFileName);
                    // PDF文件大小
                    document.setSize((int) file.getSize());
                    // 缩略图路径
                    document.setThumbnailPath(thumbnailPath + thumbnailName);
                    // 更新数据
                    documentMapper.updateByPrimaryKey(document);
                }

                //创建Properties对象
                Properties prop = new Properties();
                //读取classPath中的properties文件
                prop.load(PublicationService.class.getClassLoader().getResourceAsStream("application.properties"));
                // 文件服务器总路径
                String filePath = prop.getProperty("keensight.file.server.path");
                // 正式文件上传路径
                String fileFormalPath = prop.getProperty("pdf.upload.path");
                // 临时文件上传路径
                String fileTempPath = prop.getProperty("pdf.temp.path");
                fileHelper.deleteGeneralFile( filePath + fileFormalPath + fileTempPath + File.separator + userId + File.separator);
            }
        }
        return document;
    }

    /**
     * 根据父文献ID检索参考文献ID集合 文献的参考文献使用
     *
     * @param publicationId 父文献ID
     * @param sortColumns   排序列
     * @param pageNum       开始页数
     * @param pageSize      显示件数
     * @return 参考文献ID集合
     */

    public PageInfo<Long> getArticlesReferenceIdList(Long publicationId, String sortColumns, Integer pageNum, Integer pageSize) {
        // pageNum=1，pageSize=0，pageSizeZero=true 时检索全部信息
        PageHelper.startPage(pageNum, pageSize, true, null, true);
        List<Long> publicationIdList = publicationMapper.getArticlesReferenceIdList(publicationId, sortColumns);
        return new PageInfo<Long>(publicationIdList);
    }

    /**
     * 根据被引用文献ID检索引文ID集合 文献的引文使用
     *
     * @param publicationId 父文献ID
     * @param sortColumns   排序列
     * @param pageNum       开始页数
     * @param pageSize      显示件数
     * @return 参考文献ID集合
     */

    public PageInfo<Long> getArticlesCitationIdList(Long publicationId, String sortColumns, Integer pageNum, Integer pageSize) {
        // pageNum=1，pageSize=0，pageSizeZero=true 时检索全部信息
        PageHelper.startPage(pageNum, pageSize, true, null, true);
        List<Long> publicationIdList = publicationMapper.getArticlesCitationIdList(publicationId, sortColumns);
        return new PageInfo<Long>(publicationIdList);
    }

    /**
     * 获取用户管理全文列表
     *
     * @param userId
     * @param sortColumns
     * @param index
     * @param rows
     * @return
     */
    public PageInfo<Publication> getUserManageAccessList(Long userId, String sortColumns, int index, int rows) {
        int pageNum = index;
        int pageSize = rows;
        PageHelper.startPage(pageNum, pageSize, true, null, true);
        List<Publication> publications = publicationMapper.getUserManageAccessList(userId, sortColumns);
        return new PageInfo<Publication>(publications);
    }

    /**
     * 更新文献表的文档公开字段
     *
     * @param publicationId
     * @param publicDoc
     * @return
     */
    public int updatePublicDoc(String publicationId, Boolean publicDoc) {
        String[] publicationArry = publicationId.split(",");
        List<Long> publicationIdL = new ArrayList<>();
        for (String publicationIdS : publicationArry) {
            publicationIdL.add(Long.valueOf(publicationIdS));
        }
        return publicationMapper.updatePublicDoc(publicationIdL, publicDoc);
    }

    /**
     * 根据文献ID获取中文摘要内容
     *
     * @param publicationId 文献ID
     * @return summaryCn
     */
    public String getSummaryCnByPublicationId (Long publicationId) {
        String summaryCn = "";
        PublicationKey publicationKey = new PublicationKey();
        publicationKey.setPublicationId(publicationId);
        Publication publication = publicationMapper.selectByPrimaryKey(publicationKey);
        if(!ObjectUtils.isEmpty(publication)) {
            summaryCn = publication.getSummaryCn();
        }
        return summaryCn;
    }

    /**
     * 根据文献ID获取中文标题内容
     *
     * @param publicationId 文献ID
     * @return summaryCn
     */
    public String getTitkeCnByPublicationId (Long publicationId) {
        String titleCn = "";
        PublicationKey publicationKey = new PublicationKey();
        publicationKey.setPublicationId(publicationId);
        Publication publication = publicationMapper.selectByPrimaryKey(publicationKey);
        if(!ObjectUtils.isEmpty(publication)) {
            titleCn = publication.getTitleCn();
        }
        return titleCn;
    }


    /**
     * 根据文献ID更新中文摘要内容
     * @param publicationId 文献ID
     * @param summaryCn 摘要内容
     * @return summaryCn
     */
    public int updateSummaryByPublicationId (Long publicationId, String summaryCn) {
        Publication publication = new Publication();
        publication.setPublicationId(publicationId);
        publication.setSummaryCn(summaryCn);
        int count = publicationMapper.updateByPrimaryKeySelective(publication);
        return count;
    }

    /**
     * 根据文献ID更新中文标题内容
     * @param publicationId 文献ID
     * @param titleCn 标题内容
     * @return summaryCn
     */
    public int updateTitleCnByPublicationId (Long publicationId, String titleCn) {
        Publication publication = new Publication();
        publication.setPublicationId(publicationId);
        publication.setTitleCn(titleCn);
        int count = publicationMapper.updateByPrimaryKeySelective(publication);
        return count;
    }

    /**
     * 根据文献doi查询相关文献
     * @param doi 文献DOI,
     * @return List<PublicationModel>
     */
    public List<PublicationModel> selPublicationModeListlByDoi (String doi) {

        List<PublicationModel> publicationModelList = new ArrayList<PublicationModel>();
        if (!StringUtils.isEmpty(doi)) {
            List<Publication> publicationList = publicationMapper.selPublicationByILikeDoi(doi);
            if (publicationList.size() > 0) {
                for (Publication publication: publicationList) {
                    PublicationModel publicationModel = new PublicationModel();
                    BeanUtils.copyProperties(publication, publicationModel);
                    publicationModelList.add(publicationModel);
                }
            }
        }

        return publicationModelList;
    }

    /**
     * 根据文献doi查询相关文献
     * @param publicationTitle 文献标题,
     * @return List<PublicationModel>
     */
    public List<PublicationModel> selPublicationModeListlBypublicationTitle (String publicationTitle) {

        List<PublicationModel> publicationModelList = new ArrayList<PublicationModel>();
        if (!StringUtils.isEmpty(publicationTitle)) {
            PublicationExample publicationExample = new PublicationExample();
            PublicationExample.Criteria criteria =  publicationExample.createCriteria();
            criteria.andStatusEqualTo("1");
            publicationTitle = NormalizedUtil.getNormalizedTitle(publicationTitle);
            criteria.andNormalizedTitleEqualTo(publicationTitle);
            List<Publication> publicationList = publicationMapper.selectByExample(publicationExample);
            if (publicationList.size() > 0) {
                for (Publication publication: publicationList) {
                    PublicationModel publicationModel = new PublicationModel();
                    BeanUtils.copyProperties(publication, publicationModel);
                    publicationModelList.add(publicationModel);
                }
            }
        }

        return publicationModelList;
    }

    /**
     * 新增文献
     * @param publicationModel 文献Model
     */
    public PublicationModel insPublication (PublicationModel publicationModel) {

        Publication publication = new Publication();
        Long publicationId = publicationMapper.getMaxPublicationId() + 1;
        publication.setPublicationId(publicationId);
        publication.setRank(publicationModel.getRank());
        publication.setDoi(publicationModel.getDoi());
        publication.setDocType(publicationModel.getDocType());
        publication.setNormalizedTitle(NormalizedUtil.getNormalizedTitle(publicationModel.getDisplayTitle()));
        publication.setDisplayTitle(publicationModel.getDisplayTitle());
        publication.setBookTitle(publicationModel.getBookTitle());
        publication.setYear(publicationModel.getYear());
        publication.setDate(publicationModel.getDate());
        publication.setOnlineDate(publicationModel.getOnlineDate());
        publication.setPublisher(publicationModel.getPublisher());
        publication.setJournalId(publicationModel.getJournalId());
        publication.setConferenceSerieId(publicationModel.getConferenceSerieId());
        publication.setConferenceInstanceId(publicationModel.getConferenceInstanceId());
        publication.setVolume(publicationModel.getVolume());
        publication.setIssue(publicationModel.getIssue());
        publication.setFirstPage(publicationModel.getFirstPage());
        publication.setLastPage(publicationModel.getLastPage());
        publication.setReferenceCount(publicationModel.getReferenceCount());
        publication.setEstimatedCitations(publicationModel.getEstimatedCitations());
        publication.setOriginalVenue(publicationModel.getOriginalVenue());
        publication.setFamilyId(publicationModel.getFamilyId());
        publication.setFamilyRank(publicationModel.getFamilyRank());
        publication.setPublicDoc(publicationModel.getPublicDoc());
        publication.setFulltextPath(publicationModel.getFulltextPath());
        if(publicationModel.getAnalysisFlg()) {
            publication.setStatus("4");
        } else {
            publication.setStatus("3");
        }
        Date nowDate = new Date();
        publication.setCreatedAt(nowDate);
        publication.setSummaryCn(publicationModel.getSummary());
        publication.setTranslateTime(nowDate);
        publication.setTitleCn(publicationModel.getTitleCn());
        publication.setDisplayAuthors(publicationModel.getAuthorInfos());
        publication.setPublicationLink(publicationModel.getPublicationLink());

        int resultCount = publicationMapper.insertSelective(publication);
        if (resultCount > 0) {
            BeanUtils.copyProperties(publication, publicationModel);
        }
        return publicationModel;
    }

}
