package com.keensight.web.service;

import com.keensight.web.db.entity.StatsUserYearly;
import com.keensight.web.db.mapper.StatsUserYearlyMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 作者年度文献统计Service
 *
 * @author : newtouch
 * @version : v1.0.0 [2021/10/22] 新建
 */
@Service
public class StatsUserYearlyService {
    @Resource
    private StatsUserYearlyMapper statsUserYearlyMapper;

    /**
     * 根据作者ID获取作者年度文献统计信息
     * @param userId
     * @return
     */
    public List<StatsUserYearly> getStatsUserYearlyList(int userId) {
        List<StatsUserYearly> statsUserYearlyInfo = statsUserYearlyMapper.getStatsUserYearlyLists(userId);
        if(statsUserYearlyInfo == null) {
            statsUserYearlyInfo = new ArrayList<>();
        }
        return statsUserYearlyInfo;
    }
}
