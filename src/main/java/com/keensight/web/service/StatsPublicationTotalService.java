package com.keensight.web.service;

import com.keensight.web.db.entity.StatsPublicationTotal;
import com.keensight.web.db.entity.StatsUserTotal;
import com.keensight.web.db.mapper.StatsPublicationTotalMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class StatsPublicationTotalService {
    @Resource
    private StatsPublicationTotalMapper statsPublicationTotalMapper;

    /**
     * 根据文献ID获取文献统计总数
     * @param publicationId
     * @return
     */
    public StatsPublicationTotal getStatsPublicationTotal(int publicationId) {
        return statsPublicationTotalMapper.getStatsPublicationTotals(publicationId);
    }
}
