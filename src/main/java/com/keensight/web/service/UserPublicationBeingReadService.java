package com.keensight.web.service;

import com.keensight.web.db.entity.UserPublicationBeingRead;
import com.keensight.web.db.mapper.UserPublicationBeingReadMapper;
import com.keensight.web.model.PublicationModel;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
public class UserPublicationBeingReadService {

    @Resource
    private UserPublicationBeingReadMapper userPublicationBeingReadMapper;
    @Resource
    private PublicationService publicationService;
    /**
     * 继续阅读一览
     * @param userId
     * @return
     */
    public List<PublicationModel> getPublicationBeingReadList(Long userId) {
        List<UserPublicationBeingRead> userPublicationBeingReadList = userPublicationBeingReadMapper.getPublicationBeingReadTop20(userId);
        List<PublicationModel> resultDetailList = new ArrayList<>();
        List<Long> publicationIds = new ArrayList<>();
        for (UserPublicationBeingRead userPublicationBeingRead: userPublicationBeingReadList) {
            publicationIds.add(userPublicationBeingRead.getPublicationId());
        }

        if (publicationIds.size() > 0) {
            resultDetailList = publicationService.getPublicationDetailList(publicationIds);
        }
        return resultDetailList;
    }
}
