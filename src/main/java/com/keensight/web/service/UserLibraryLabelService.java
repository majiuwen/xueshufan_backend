package com.keensight.web.service;

import com.keensight.web.db.entity.UserLibraryLabel;
import com.keensight.web.db.mapper.UserLibraryLabelMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserLibraryLabelService {

    @Resource
    private UserLibraryLabelMapper userLibraryLabelMapper;

    /**
     * 获取用户标签信息
     *
     * @param userId
     * @return
     */
    public List<UserLibraryLabel> getUserLibraryLabelList(int userId) {

        List<UserLibraryLabel> userLibraryLabelInfo = userLibraryLabelMapper.getUserLibraryLabelList(userId);
        if (userLibraryLabelInfo != null) {

        } else {
            userLibraryLabelInfo = new ArrayList<>();
        }
        return userLibraryLabelInfo;
    }
}
