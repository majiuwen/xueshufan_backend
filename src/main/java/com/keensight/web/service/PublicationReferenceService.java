package com.keensight.web.service;

import com.keensight.web.db.entity.PublicationReferenceExample;
import com.keensight.web.db.mapper.PublicationReferenceMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class PublicationReferenceService {

    @Resource
    private PublicationReferenceMapper publicationReferenceMapper;

    /**
     * 获取参考文献数
     * @param publicationId
     * @return
     */
    public int getPublicationReferenceCount(Long publicationId) {

        PublicationReferenceExample publicationReferenceExample = new PublicationReferenceExample();
        PublicationReferenceExample.Criteria publicationReferenceCriteria = publicationReferenceExample.createCriteria();
        publicationReferenceCriteria.andPublicationIdEqualTo(publicationId);

        return publicationReferenceMapper.countByExample(publicationReferenceExample);
    }

    /**
     * 获取引证文献数
     * @param publicationId
     * @return
     */
    public int getPublicationCitationCount(Long publicationId) {

        PublicationReferenceExample publicationReferenceExample = new PublicationReferenceExample();
        PublicationReferenceExample.Criteria publicationReferenceCriteria = publicationReferenceExample.createCriteria();
        publicationReferenceCriteria.andPublicationReferenceIdEqualTo(publicationId);

        return publicationReferenceMapper.countByExample(publicationReferenceExample);
    }
}
