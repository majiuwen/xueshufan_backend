package com.keensight.web.service;

import com.alibaba.druid.util.StringUtils;
import com.keensight.web.db.entity.UserVerificationCode;
import com.keensight.web.db.entity.UserVerificationCodeExample;
import com.keensight.web.db.mapper.UserVerificationCodeMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author : newtouch
 * @version : v1.0.0 [2021/12/29] 新建
 */
@Service
public class UserVerificationCodeService {
    @Resource
    private UserVerificationCodeMapper userVerificationCodeMapper;

    /**
     * 新增验证码信息
     *
     * @param requestId 请求Id
     * @param verificationCode 验证码
     * @param expirationTime 过期时间
     * @param mobile 手机号
     * @return resRequestId
     */
    public String insUserVerificationCode(String requestId, String verificationCode, Date expirationTime, String mobile) {
        String resRequestId = null;
        UserVerificationCode userVerificationCode = new UserVerificationCode();
        userVerificationCode.setExpirationTime(expirationTime);
        userVerificationCode.setPhoneNumber(mobile);
        userVerificationCode.setRequestId(requestId);
        userVerificationCode.setVerificationCode(verificationCode);
        int resCount = userVerificationCodeMapper.insertSelective(userVerificationCode);
        if (resCount > 0) {
            resRequestId = requestId;
        }
        return resRequestId;
    }

    /**
     * 获取验证码
     * @param requestId 请求Id
     * @param mobile 手机号
     * @return UserVerificationCode
     */
    public UserVerificationCode getUserVerificationCodeInfo(String requestId, String mobile) {
        UserVerificationCode userVerificationCode = null;
        if (!StringUtils.isEmpty(mobile)) {
            userVerificationCode = userVerificationCodeMapper.selUserVerificationCode(requestId, mobile);
        }
        return userVerificationCode;
    }

    /**
     * 注册/登录成功后删除验证码
     * @param requestId 请求Id
     * @param mobile 手机号
     * @return UserVerificationCode
     */
    public int delVerificationCode(String requestId, String mobile) {
        int resCount = 0;
        if (!StringUtils.isEmpty(mobile)) {
            UserVerificationCodeExample userVerificationCodeExample = new UserVerificationCodeExample();
            UserVerificationCodeExample.Criteria codeExampleCriteria = userVerificationCodeExample.createCriteria();
            if (!StringUtils.isEmpty(requestId)) {
                codeExampleCriteria.andRequestIdEqualTo(requestId);
            }
            codeExampleCriteria.andPhoneNumberEqualTo(mobile);
            resCount = userVerificationCodeMapper.deleteByExample(userVerificationCodeExample);
        }
        return resCount;
    }
}
