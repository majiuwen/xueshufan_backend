package com.keensight.web.service;

import com.keensight.web.db.entity.Discipline;
import com.keensight.web.db.mapper.DisciplineMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DisciplineService {
    @Resource
    private DisciplineMapper disciplineMapper;

    /**
     * 注册研究领域学科查询
     * @return 学科集合
     */
    public List<Discipline> getRegistFieldDisplayNameList(){
        List<Discipline> registFieldDisplayNameList = disciplineMapper.getRegistFieldDisplayNameList();
        return registFieldDisplayNameList;
    }

    /**
     * 根据学科进行注册研究领域子学科查询
     * @param fieldId
     * @return 子学科集合
     */
    public List<Discipline> getRegistSubFieldDisplayNameList(Long fieldId){
        List<Discipline> registSubFieldDisplayNameList = disciplineMapper.getRegistSubFieldDisplayNameList(fieldId);
        return registSubFieldDisplayNameList;
    }
}
