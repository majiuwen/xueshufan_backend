package com.keensight.web.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.keensight.web.db.mapper.PublicationFieldMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文献研究领域Service
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/16] 新建
 */
@Service
public class PublicationFieldService {
    @Resource
    private PublicationFieldMapper publicationFieldMapper;

    /**
     * 领域内最重要的指定件数的论文取得
     *
     * @param fieldId
     * @param rows
     * @return
     */
    public PageInfo<Long> getPublicationIdListByFieldId(Long fieldId, Integer index, Integer rows) {
        PageHelper.startPage(index, rows, true, null, true);
        List<Long> publicationIdList = publicationFieldMapper.getPublicationIdListByFieldId(fieldId);
        return new PageInfo<Long>(publicationIdList);
    }
}
