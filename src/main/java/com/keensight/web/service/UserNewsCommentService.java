package com.keensight.web.service;

import cn.newtouch.dl.solr.util.StringUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.keensight.web.constants.enums.UploadFolder;
import com.keensight.web.db.entity.UserNewsComment;
import com.keensight.web.db.entity.UserNewsCommentLike;
import com.keensight.web.db.mapper.UserNewsCommentLikeMapper;
import com.keensight.web.db.mapper.UserNewsCommentMapper;
import com.keensight.web.db.mapper.UserNewsMapper;
import com.keensight.web.helper.FileServerHelper;
import com.keensight.web.model.UserNewsCommentModel;
import com.keensight.web.utils.AwsS3Utils;
import com.keensight.web.utils.DateUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserNewsCommentService {

    @Resource
    private UserNewsCommentMapper userNewsCommentMapper;

    @Resource
    private UserNewsCommentLikeMapper userNewsCommentLikeMapper;

    @Resource
    private UserNewsMapper userNewsMapper;

    @Resource
    private FileServerHelper fileServerHelper;

    /**
     * 最新动态一级评论获取方法
     *
     * @param loginUserId
     * @param newsId
     * @param index
     * @param rows
     * @return
     */
    public PageInfo<UserNewsCommentModel> getNewComments(Long loginUserId, Long newsId, Integer index, Integer rows) throws ParseException {
        List<UserNewsCommentModel> firstLevelComments = new ArrayList<>();
        // 获取最新动态一级评论总数
         int listCount = userNewsCommentMapper.getCommentsCount(newsId);
        PageInfo<UserNewsCommentModel> pagesInfo= new PageInfo<UserNewsCommentModel>();
        if (listCount > 0) {
            pagesInfo.setTotal(listCount);
            if(index != 0 && rows != 0){
                pagesInfo.setPageNum(index);
                pagesInfo.setPageSize(rows);
                pagesInfo.setHasNextPage(listCount > 0 && listCount > index * rows);
                PageHelper.startPage(index, rows, true, null, true);
            }
            List<Long> newsIds = new ArrayList<>();
            newsIds.add(newsId);
            firstLevelComments = userNewsCommentMapper.getFirstLevelComments(loginUserId, newsIds);
            for (UserNewsCommentModel newsComments : firstLevelComments) {
                newsComments.setCommentTimeStr(DateUtil.validateTime(new Date(), newsComments.getCommentTime()));
                if(StringUtil.isNotEmpty(newsComments.getCommentUserProfilePhotoPath())){
                    String profilePath_s3 = AwsS3Utils.getS3FileUrl(newsComments.getCommentUserProfilePhotoPath());
                    newsComments.setCommentUserProfilePhotoPath(profilePath_s3);
                }
                if(StringUtil.isNotEmpty(newsComments.getCommentUserProfilePath())){
                    String profilePath_s3 = AwsS3Utils.getS3FileUrl(newsComments.getCommentUserProfilePath());
                    newsComments.setCommentUserProfilePath(profilePath_s3);
                }
//                newsComments.setCommentUserProfilePhotoPath(fileServerHelper.getFileServerUrl(newsComments.getCommentUserProfilePhotoPath(), UploadFolder.PROFILE_PHOTO));
//                newsComments.setCommentUserProfilePath(fileServerHelper.getFileServerUrl(newsComments.getCommentUserProfilePath(), UploadFolder.PROFILE_PHOTO));
                // 查询二级评论
                if (newsComments.getLevel2CommentNum() > 0){
                    PageInfo<UserNewsCommentModel> secondComments = getSecondComments(loginUserId, newsComments.getNewsCommentId(), newsComments.getLevel2CommentNum(), 1, 5);
                    newsComments.setReplyCommentList(secondComments.getList());
                    newsComments.setLevel2HasNextPage(secondComments.isHasNextPage());
                    newsComments.setLevel2NextPage(secondComments.getNextPage());
                }else {
                    newsComments.setReplyCommentList(new ArrayList<>());
                    newsComments.setLevel2HasNextPage(false);
                }
            }
        }
        pagesInfo.setList(firstLevelComments);
        return pagesInfo;
    }

    /**
     * 评论点赞/取消点赞方法
     * @param newsCommentId
     * @param userId
     * @return
     */
    public int updCommentLike(Long newsCommentId, Long userId, String updFlg) {
        // 返回值
        int result = 0;
        // 获取当前系统时间
        Date sysDate = new Date();
        if("0".equals(updFlg)){
            // 取消评论点赞
            result = userNewsCommentLikeMapper.deleteCommentLike(newsCommentId,userId);
        }else{
            // 评论点赞实例化
            UserNewsCommentLike userNewsCommentLike = new UserNewsCommentLike();
            userNewsCommentLike.setNewsCommentId(newsCommentId);
            userNewsCommentLike.setUserId(userId);
            userNewsCommentLike.setLikeTime(sysDate);
            // 评论点赞
            result = userNewsCommentLikeMapper.insertSelective(userNewsCommentLike);
        }
        return result;
    }

    /**
     * 最新动态二级评论获取方法
     * @param loginUserId
     * @param newsCommentId
     * @param index
     * @param rows
     * @return
     * @throws ParseException
     */
    public PageInfo<UserNewsCommentModel> getSecondComments(Long loginUserId, Long newsCommentId,Integer listCount , Integer index, Integer rows) throws ParseException {
        List<UserNewsCommentModel> secondLevelComments = new ArrayList<>();

        PageInfo<UserNewsCommentModel> pagesInfo= new PageInfo<UserNewsCommentModel>();
        if (listCount > 0) {
            pagesInfo.setTotal(listCount);
            pagesInfo.setPageNum(index);
            pagesInfo.setPageSize(rows);
            pagesInfo.setHasNextPage(listCount > 0 && listCount > index * rows);
            PageHelper.startPage(index, rows, true, null, true);
            secondLevelComments = userNewsCommentMapper.getSecondLevelComments(loginUserId, newsCommentId);
            for (UserNewsCommentModel newsComments : secondLevelComments) {
                newsComments.setCommentTimeStr(DateUtil.validateTime(new Date(), newsComments.getCommentTime()));
            }
            index += 1;
            pagesInfo.setNextPage(index);
        }

        pagesInfo.setList(secondLevelComments);
        return pagesInfo;
    }

    /**
     * 评论删除方法
     * @param newsCommentId
     * @return
     */
    public int delComments(Long newsCommentId, Long newsId, String isFirstLevel) {
        int result = 0;
        // 评论删除方法
        result = userNewsCommentMapper.delComments(newsCommentId,isFirstLevel);
        // 判断删除评论成功，且评论为一级评论
        if(result > 0 && "1".equals(isFirstLevel)){
            int updNewsCommentNum = userNewsMapper.updNewsCommentNum(0,newsId);
        }
        return result;
    }

    /**
     * 评论保存方法
     * @param newsId
     * @param replyCommentId
     * @param newsComment
     * @param commentUserId
     * @param replyUserId
     * @return
     */
    public UserNewsComment saveNewsComments(Long newsId, Long replyCommentId, String newsComment, Long commentUserId, Long replyUserId) {
        int result = 0;
        Date sysDate = new Date();
        UserNewsComment userNewsComment = new UserNewsComment();
        userNewsComment.setNewsId(newsId);
        if(replyCommentId != null){
            userNewsComment.setReplyCommentId(replyCommentId);
        }
        userNewsComment.setNewsComment(newsComment);
        userNewsComment.setCommentUserId(commentUserId);
        if(replyUserId != null){
            userNewsComment.setReplyUserId(replyUserId);
        }
        userNewsComment.setCommentTime(sysDate);
        userNewsComment.setDelFlg("0");
        result = userNewsCommentMapper.insertSelective(userNewsComment);
        if(result > 0 && replyCommentId == null){
            int updNewsCommentNum = userNewsMapper.updNewsCommentNum(1,newsId);
        }

    return  userNewsComment;
    }

    /**
     * 获取动态总数
     *
     * @param userId 用户Id
     * @return 动态总数
     */
    public int getNewsCount(Long userId) {
        return userNewsMapper.getNewsCount(userId);
    }

    /**
     * 获取动态获赞数
     *
     * @param userId 用户Id
     * @return 动态获赞数
     */
    public int getUserLikeNumCnt(Long userId) {
        return userNewsMapper.getUserLikeNumCnt(userId);
    }
}
