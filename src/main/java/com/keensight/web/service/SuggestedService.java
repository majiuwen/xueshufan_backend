package com.keensight.web.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.keensight.web.db.entity.SuggestedUserToFollow;
import com.keensight.web.db.mapper.SuggestedUserToFollowMapper;
import com.keensight.web.model.SuggestedUserModel;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class SuggestedService {

    @Resource
    private SuggestedUserToFollowMapper suggestedUserToFollowMapper;

    /**
     * 推荐学者一览
     *
     * @param userId
     * @param userIds
     * @return
     */
    public List<SuggestedUserModel> getSuggestedUserList(Long userId, List<Long> userIds) {
        List<SuggestedUserModel> suggestedUserModels = new ArrayList<SuggestedUserModel>();
        List<SuggestedUserToFollow> suggestedUserToFollowList = suggestedUserToFollowMapper.getSuggestedUserList(userId, userIds);
        for (SuggestedUserToFollow suggestedUserToFollow : suggestedUserToFollowList) {
            SuggestedUserModel suggestedUserModel = new SuggestedUserModel();
            BeanUtils.copyProperties(suggestedUserToFollow, suggestedUserModel);
            if ("3".equals(suggestedUserToFollow.getFollowingFlg())) {
                suggestedUserModel.setFollowingBtnFlg("3");
            } else if ("2".equals(suggestedUserToFollow.getFollowingFlg())) {
                suggestedUserModel.setFollowingBtnFlg("2");
            } else {
                suggestedUserModel.setFollowingBtnFlg("1");
            }
            suggestedUserModel.setContactBtnFlg("1");
            suggestedUserModels.add(suggestedUserModel);
        }
        return suggestedUserModels;
    }

    /**
     * 根据检索条件查询用户ID集合
     *
     * @param userId
     * @param index
     * @param rows
     * @return
     */
    public PageInfo<Long> getUserIdList(Long userId, int index, int rows) {
        int pageNum = index;
        int pageSize = rows;
        PageHelper.startPage(pageNum, pageSize, true, null, true);
        List<Long> userIdList = suggestedUserToFollowMapper.getUserIdList(userId);
        return new PageInfo<Long>(userIdList);
    }
}
