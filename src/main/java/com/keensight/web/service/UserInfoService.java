package com.keensight.web.service;

import cn.newtouch.dl.solr.util.StringUtil;
import cn.newtouch.dl.turbocloud.service.BaseService;
import com.alibaba.druid.util.StringUtils;
import com.keensight.web.constants.enums.UploadFolder;
import com.keensight.web.db.entity.UserInfo;
import com.keensight.web.db.entity.UserInfoExample;
import com.keensight.web.db.mapper.UserInfoMapper;
import com.keensight.web.helper.FileServerHelper;
import com.keensight.web.utils.AwsS3Utils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@Service
public class UserInfoService extends BaseService {

    @Resource
    private UserInfoMapper userInfoMapper;
    @Resource
    private FileServerHelper fileServerHelper;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * 根据用户ID获取用户个人资料
     * @param userId
     * @return
     */
    public UserInfo getUserInfo(Long userId) {
        UserInfo userInfo = userInfoMapper.getUserInfo(userId);
        if(userInfo != null){
//            userInfo.setProfilePath(fileServerHelper.getFileServerUrl(userInfo.getProfilePath(), UploadFolder.PROFILE_PHOTO));
            if (StringUtils.isEmpty(userInfo.getProfilePath())) {
                String url_s3 = AwsS3Utils.getS3FileUrl(userInfo.getProfilePath());
                userInfo.setProfilePath(url_s3);
            }

        }
        return userInfo;
    }

    /**
     * 根据用户ID获取用户是否存在
     * @param userId
     * @return
     */
    public int count(Long userId) {
        UserInfoExample example = new UserInfoExample();
        example.createCriteria().andUserIdEqualTo(userId);
        return userInfoMapper.countByExample(example);
    }

    /**
     * 未存在用户新增用户个人信息方法
     * @param userId
     * @param avatarStr
     * @param nickname
     * @param introduction
     * @param sex
     * @param province
     * @param city
     * @param birthday
     * @return
     * @throws ParseException
     */
    public UserInfo insert(Long userId, String avatarStr, String nickname, String introduction, String sex, String province, String city, String birthday) throws ParseException {
        int insertNum;
        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(userId);
        if(StringUtil.isNotEmpty(avatarStr) ) {
            userInfo.setProfilePath(avatarStr);
        }
        if (StringUtil.isNotEmpty(nickname)) {
            userInfo.setNickname(nickname);
        }
        if (StringUtil.isNotEmpty(introduction)) {
            userInfo.setIntroduction(introduction);
        }
        userInfo.setProfilePath(avatarStr);
        userInfo.setNickname(nickname);
        userInfo.setIntroduction(introduction);
        userInfo.setSex(sex);
        userInfo.setProvince(province);
        userInfo.setCity(city);
        userInfo.setBirthday(sdf.parse(birthday));
        insertNum = userInfoMapper.insertSelective(userInfo);
        return userInfo;
    }

    /**
     * 已存在用户更新用户个人信息方法
     * @param userId
     * @param avatarStr
     * @param nickname
     * @param introduction
     * @param sex
     * @param province
     * @param city
     * @param birthday
     * @return
     * @throws ParseException
     */
    public UserInfo update(Long userId, String avatarStr, String nickname, String introduction, String sex, String province, String city, String birthday, Integer avatarFlg) throws ParseException {
        int updateNum;
        UserInfo userInfo = new UserInfo();
        UserInfoExample example = new UserInfoExample();
        example.createCriteria().andUserIdEqualTo(userId);
        if(avatarFlg > 0 && StringUtil.isNotEmpty(avatarStr) ) {
            userInfo.setProfilePath(avatarStr);
        }
        if (StringUtil.isNotEmpty(nickname)) {
            userInfo.setNickname(nickname);
        }
        if (StringUtil.isNotEmpty(introduction)) {
            userInfo.setIntroduction(introduction);
        }
        userInfo.setSex(sex);
        userInfo.setProvince(province);
        userInfo.setCity(city);
        userInfo.setBirthday(sdf.parse(birthday));
        updateNum = userInfoMapper.updateByExampleSelective(userInfo,example);
        return userInfo;
    }
}
