package com.keensight.web.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.keensight.web.db.entity.RelatedPublication;
import com.keensight.web.db.mapper.RelatedPublicationMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RelatedPublicationService {

    @Resource
    private RelatedPublicationMapper relatedPublicationMapper;

    /**
     * 获取该文献的相关研究的文献ID
     * @param publicationId
     * @param index
     * @param rows
     * @return
     */
    public PageInfo<RelatedPublication> getRelatedPublicationIds(Long publicationId, int index, int rows) {
        int pageNum = index;
        int pageSize = rows;
        PageHelper.startPage(pageNum, pageSize);
        List<RelatedPublication> relatedPublicationList = relatedPublicationMapper.getRelatedPublicationIds(publicationId);
        return new PageInfo<>(relatedPublicationList);
    }
}
