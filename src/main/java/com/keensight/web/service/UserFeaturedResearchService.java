package com.keensight.web.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.keensight.web.db.entity.UserFeaturedResearch;
import com.keensight.web.db.mapper.UserFeaturedResearchMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserFeaturedResearchService {

    @Resource
    private UserFeaturedResearchMapper userFeaturedResearchMapper;

    /**
     * 获取用户的代表性研究
     * @param userId
     * @param index
     * @param rows
     * @return
     */
    public PageInfo<UserFeaturedResearch> getUserFeatureResearchList(Long userId, int index, int rows) {
        int pageNum = index;
        int pageSize = rows;
        PageHelper.startPage(pageNum, pageSize);
        List<UserFeaturedResearch> userFeaturedResearches = userFeaturedResearchMapper.getUserFeatureResearchList(userId);
        return new PageInfo<UserFeaturedResearch>(userFeaturedResearches);
    }
}
