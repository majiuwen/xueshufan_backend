package com.keensight.web.service;

import com.keensight.web.db.entity.StatsFieldYearly;
import com.keensight.web.db.mapper.StatsFieldYearlyMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 领域年度文献统计Service
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/19] 新建
 */
@Service
public class StatsFieldYearlyService {
    @Resource
    private StatsFieldYearlyMapper statsFieldYearlyMapper;

    /**
     * 根据研究领域ID获取领域年度文献统计信息
     *
     * @param fieldId 研究领域ID
     * @return
     */
    public List<StatsFieldYearly> getStatsFieldYearlyInfo(Long fieldId) {
        return statsFieldYearlyMapper.getStatsFieldYearlyInfo(fieldId);
    }
}
