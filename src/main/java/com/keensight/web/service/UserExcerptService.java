package com.keensight.web.service;

import cn.newtouch.dl.solr.util.StringUtil;
import com.keensight.web.constants.enums.UploadFolder;
import com.keensight.web.db.entity.UserAnnotation;
import com.keensight.web.db.entity.UserExcerpt;
import com.keensight.web.db.entity.UserExcerptKey;
import com.keensight.web.db.mapper.UserAnnotationMapper;
import com.keensight.web.db.mapper.UserExcerptMapper;
import com.keensight.web.helper.FileServerHelper;
import com.keensight.web.model.UserAnnotationModel;
import com.keensight.web.model.UserExcerptModel;
import com.keensight.web.utils.AwsS3Utils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserExcerptService {
    @Resource
    private UserExcerptMapper userExcerptMapper;
    @Resource
    private UserAnnotationMapper userAnnotationMapper;
    @Resource
    private FileServerHelper fileServerHelper;

    /**
     * 获取用户摘录信息
     * @param userId
     * @param excerptId
     * @param publicationId
     * @param seachContent
     * @param docId
     * @return
     */
    public List<UserExcerptModel> getUserExcerpt4List(Long userId, Long excerptId, Long publicationId, String seachContent, Long docId) {

        // 用户摘录信息List
        List<UserExcerptModel> userExcerptModelList = new ArrayList<UserExcerptModel>();
        List<Long> excerptIdList = new ArrayList<Long>();
        // 获取用户摘录信息
        List<UserExcerpt> userExcerptList = userExcerptMapper.selectUserExcerptByCondition(excerptId,  userId, publicationId, seachContent, docId);
        if (userExcerptList.size() > 0) {
            for (UserExcerpt userExcerpt : userExcerptList) {
                UserExcerptModel userExcerptModel = new UserExcerptModel();
                userExcerptModel = copyUserExcerptToUserExcerptModel(userExcerpt);
                userExcerptModelList.add(userExcerptModel);
                excerptIdList.add(userExcerpt.getExcerptId());
            }
            // 批注list
            List<UserAnnotationModel> userAnnotationModelList = new ArrayList<UserAnnotationModel>();
            // 获取用户批注信息
            List<UserAnnotation> userAnnotationList = userAnnotationMapper.selectUserAnnotationByCondition(excerptIdList, seachContent);
            if (userAnnotationList.size() > 0) {
                for (UserAnnotation userAnnotation : userAnnotationList) {
                    UserAnnotationModel userAnnotationModel = new UserAnnotationModel();
                    BeanUtils.copyProperties(userAnnotation, userAnnotationModel);
                    // 批注日期
                    String annotationDateMonAndDay = new SimpleDateFormat("MM-dd").format(userAnnotationModel.getAnnotationDate());
                    // 批注时间
                    String annotationDateHourAndMin = new SimpleDateFormat("HH:mm").format(userAnnotationModel.getAnnotationDate());
                    userAnnotationModel.setAnnotationDateMonAndDayStr(annotationDateMonAndDay);
                    userAnnotationModel.setAnnotationDateHourAndMinStr(annotationDateHourAndMin);
                    userAnnotationModelList.add(userAnnotationModel);
                }
            }
            // 将检索批注放入对应摘录中
            for (UserExcerptModel userExcerpt : userExcerptModelList) {
                List<UserAnnotationModel> userAnnotationModels = new ArrayList<UserAnnotationModel>();
                userAnnotationModels = userAnnotationModelList.stream().filter(userAnnotation -> userExcerpt.getExcerptId().equals(userAnnotation.getExcerptId())).collect(Collectors.toList());
                userExcerpt.setAnnotationList(userAnnotationModels);
            }

        }

        return userExcerptModelList;
    }

    /**
     * 新增摘录信息
     * @param userExcerptModel
     * @return
     */
    public UserExcerptModel insertUserExcerpt(UserExcerptModel userExcerptModel) {

        UserExcerptModel newUserExcerptModel = new UserExcerptModel();
        int resCount = 0;
        if (userExcerptModel != null) {
            UserExcerpt userExcerpt = new UserExcerpt();
            userExcerpt.setPublicationId(userExcerptModel.getPublicationId());
            userExcerpt.setUserId(userExcerptModel.getUserId());
            userExcerpt.setOriginalText(userExcerptModel.getOriginalText());
            userExcerpt.setTranslationText(userExcerptModel.getTranslationText());
            userExcerpt.setTranslationToolId(userExcerptModel.getTranslationToolId());
            userExcerpt.setSelectedContent(userExcerptModel.getSelectContent());
            userExcerpt.setActionType(userExcerptModel.getActionType());
            userExcerpt.setContentType(userExcerptModel.getContentType());
            userExcerpt.setCreateType(userExcerptModel.getCreateType());
            userExcerpt.setIsDataCollect(userExcerptModel.getIsDataCollect());
            userExcerpt.setName(userExcerptModel.getName());
            userExcerpt.setOriginPage(userExcerptModel.getOrigin_page());
            userExcerpt.setPageNum(userExcerptModel.getPage_num());
            userExcerpt.setPreRender(userExcerptModel.getPreRender());
            userExcerpt.setStartDom(userExcerptModel.getStart());
            userExcerpt.setEndDom(userExcerptModel.getEnd());
            userExcerpt.setStartOffset(userExcerptModel.getStartOffset());
            userExcerpt.setEndOffset(userExcerptModel.getEndOffset());
            userExcerpt.setSubActionType(userExcerptModel.getSubActionType());
            userExcerpt.setUid(userExcerptModel.getUid());
            userExcerpt.setType(userExcerptModel.getType());
            userExcerpt.setPageTop(userExcerptModel.getTop());
            userExcerpt.setPageHeight(userExcerptModel.getHeight());
            userExcerpt.setPageLeft(userExcerptModel.getLeft());
            userExcerpt.setPageWidth(userExcerptModel.getWidth());
            userExcerpt.setAllPageExcerptFlg(userExcerptModel.getAllPageExcerptFlg());
            userExcerpt.setColor(userExcerptModel.getColor());
            userExcerpt.setSize(userExcerptModel.getSize());
            userExcerpt.setRectMode(userExcerptModel.getRectMode());
            userExcerpt.setSnapshotUrl(userExcerptModel.getSnapshotUrl());
            userExcerpt.setExcerptDate(userExcerptModel.getExcerptDate());
            userExcerpt.setDocId(userExcerptModel.getDocId());
            // 新增摘录信息
            resCount = userExcerptMapper.insertSelective(userExcerpt);
            if (resCount > 0) {
                newUserExcerptModel = copyUserExcerptToUserExcerptModel(userExcerpt);
            }
        }
        return newUserExcerptModel;
    }

    /**
     * 更新摘录信息
     * @param excerptId
     * @param originalText
     * @param translationText
     * @return
     */
    public int updateUserExcerptByKey(Long excerptId, String originalText, String translationText) {

        UserExcerptModel newUserExcerptModel = new UserExcerptModel();
        UserExcerpt userExcerpt = new UserExcerpt();
        if (excerptId != null) {
            userExcerpt.setExcerptId(excerptId);
        }
        if (originalText != null) {
            userExcerpt.setOriginalText(originalText);
        }
        if (translationText != null) {
            userExcerpt.setTranslationText(translationText);
        }

        // 更新摘录信息
        int resCount = userExcerptMapper.updateByPrimaryKeySelective(userExcerpt);

        return resCount;
    }

    /**
     * 删除摘录信息
     * @param excerptId
     * @return
     */
    public int deleteUserExcerptByKey(Long excerptId) {

        // 设定摘录key
        UserExcerptKey UserExcerptKey = new UserExcerptKey();
        UserExcerpt userExcerpt = new UserExcerpt();
        UserExcerptKey.setExcerptId(excerptId);
        // 删除摘录信息
        int resCount = userExcerptMapper.deleteByPrimaryKey(UserExcerptKey);

        return resCount;
    }

    /**
     * copy摘录对象
     * @param userExcerpt
     * @return
     */
    public UserExcerptModel copyUserExcerptToUserExcerptModel(UserExcerpt userExcerpt) {
        UserExcerptModel userExcerptModel = new UserExcerptModel();
        if (!ObjectUtils.isEmpty(userExcerpt)) {
            userExcerptModel.setUserId(userExcerpt.getUserId());
            userExcerptModel.setOriginalText(userExcerpt.getOriginalText());
            userExcerptModel.setTranslationText(userExcerpt.getTranslationText());
            userExcerptModel.setTranslationToolId(userExcerpt.getTranslationToolId());
            userExcerptModel.setSelectContent(userExcerpt.getSelectedContent());
            userExcerptModel.setExcerptDate(userExcerpt.getExcerptDate());
            userExcerptModel.setPublicationId(userExcerpt.getPublicationId());
            userExcerptModel.setTranslationToolCode(userExcerpt.getTranslationToolCode());
            userExcerptModel.setTranslationToolName(userExcerpt.getTranslationToolName());
            userExcerptModel.setPage_num(userExcerpt.getPageNum());
            userExcerptModel.setActionType(userExcerpt.getActionType());
            userExcerptModel.setContentType(userExcerpt.getContentType());
            userExcerptModel.setCreateType(userExcerpt.getCreateType());
            userExcerptModel.setIsDataCollect(userExcerpt.getIsDataCollect());
            userExcerptModel.setName(userExcerpt.getName());
            userExcerptModel.setOrigin_page(userExcerpt.getOriginPage());
            userExcerptModel.setPreRender(userExcerpt.getPreRender());
            userExcerptModel.setStart(userExcerpt.getStartDom());
            userExcerptModel.setEnd(userExcerpt.getEndDom());
            userExcerptModel.setStartOffset(userExcerpt.getStartOffset());
            userExcerptModel.setEndOffset(userExcerpt.getEndOffset());
            userExcerptModel.setSubActionType(userExcerpt.getSubActionType());
            userExcerptModel.setUid(userExcerpt.getUid());
            userExcerptModel.setType(userExcerpt.getType());
            userExcerptModel.setTop(userExcerpt.getPageTop());
            userExcerptModel.setHeight(userExcerpt.getPageHeight());
            userExcerptModel.setWidth(userExcerpt.getPageWidth());
            userExcerptModel.setLeft(userExcerpt.getPageLeft());
            userExcerptModel.setAllPageExcerptFlg(userExcerpt.getAllPageExcerptFlg());
            userExcerptModel.setColor(userExcerpt.getColor());
            userExcerptModel.setSize(userExcerpt.getSize());
            userExcerptModel.setRectMode(userExcerpt.getRectMode());
            if (StringUtil.isNotEmpty(userExcerpt.getFulltextPath())) {
                String profilePath_s3 = AwsS3Utils.signUrl(userExcerpt.getFulltextPath());
                userExcerptModel.setFulltextPath(profilePath_s3);
            }
            if (StringUtil.isNotEmpty(userExcerpt.getSnapshotUrl())) {
                String profilePath_s3 = AwsS3Utils.getS3FileUrl(userExcerpt.getSnapshotUrl());
                userExcerptModel.setSnapshotUrl(profilePath_s3);
            }
            userExcerptModel.setExcerptId(userExcerpt.getExcerptId());
            userExcerptModel.setDocId(userExcerpt.getDocId());
            userExcerptModel.setOriginalTextHtml(userExcerpt.getOriginalTextHtml());
            userExcerptModel.setTranslationTextHtml(userExcerpt.getTranslationTextHtml());
            userExcerptModel.setDisplayTitle(userExcerpt.getDisplayTitle());
            userExcerptModel.setPublisher(userExcerpt.getPublisher());
            if (StringUtil.isNotEmpty(userExcerpt.getPrivateDocpath())) {
                String profilePath_s3 = AwsS3Utils.signUrl(userExcerpt.getPrivateDocpath());
                userExcerptModel.setPrivateDocpath(profilePath_s3);
            }

            // 摘录日期
            String excerptDateMonAndDay = new SimpleDateFormat("MM-dd").format(userExcerptModel.getExcerptDate());
            // 摘录时间
            String excerptDateHourAndMin = new SimpleDateFormat("HH:mm").format(userExcerptModel.getExcerptDate());
            userExcerptModel.setExcerptDateMonAndDayStr(excerptDateMonAndDay);
            userExcerptModel.setExcerptDateHourAndMinStr(excerptDateHourAndMin);
        }
        return userExcerptModel;
    }
}
