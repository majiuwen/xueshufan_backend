package com.keensight.web.service;

import cn.newtouch.dl.solr.util.StringUtil;
import cn.newtouch.dl.turbocloud.service.BaseService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.keensight.web.constants.enums.UploadFolder;
import com.keensight.web.db.entity.User;
import com.keensight.web.db.entity.UserInfo;
import com.keensight.web.db.entity.UserKey;
import com.keensight.web.db.mapper.UserInfoMapper;
import com.keensight.web.db.mapper.UserMapper;
import com.keensight.web.helper.FileServerHelper;
import com.keensight.web.model.UserInfoModel;
import com.keensight.web.model.UserModel;
import com.keensight.web.utils.AwsS3Utils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService extends BaseService {

    @Resource
    private UserMapper userMapper;
    @Resource
    private FileServerHelper fileServerHelper;
    @Resource
    private UserInfoMapper userInfoMapper;
    @Resource
    private UserService userService;

    /**
     * 获取用户所属机构
     *
     * @param userId
     * @param userInfoFlg
     * @return
     */
    public UserModel getUserInstitution(Long userId, String userInfoFlg) {
        UserKey key = new UserKey();
        key.setUserId(userId);
        User user = userMapper.selectByPrimaryKey(key);
        UserModel userModel = new UserModel();
        BeanUtils.copyProperties(user, userModel);
        if ("0".equals(userInfoFlg)) {
            // 显示添加新的研究按钮
            userModel.setFiledBtnFlg("1");
            // 不显示修改按钮
            userModel.setUpdateBtnFlg("0");
        } else {
            // 不显示添加新的研究按钮
            userModel.setFiledBtnFlg("0");
            // 显示修改按钮
            userModel.setUpdateBtnFlg("1");
        }
        // 显示联系
        userModel.setContactBtnFlg("1");
        return userModel;
    }

    /**
     * 获取用户个人信息
     *
     * @param userId
     * @param loginUserId
     * @param userInfoFlg
     * @return
     */
    public UserModel getUserInfo(Long userId, Long loginUserId, String userInfoFlg) {
        User user = userMapper.selectUserInfoByUserId(userId, loginUserId);
        UserModel userModel = new UserModel();
        BeanUtils.copyProperties(user, userModel);
        if ("0".equals(userInfoFlg)) {
            // 显示添加新的研究按钮
            userModel.setFiledBtnFlg("1");
            // 不显示修改按钮
            userModel.setUpdateBtnFlg("0");
        } else {
            // 不显示添加新的研究按钮
            userModel.setFiledBtnFlg("0");
            // 显示修改按钮
            userModel.setUpdateBtnFlg("1");
        }
        // 用户登录的情况下
        if (loginUserId != null) {
            List<Long> userIdList = new ArrayList<>();
            userIdList.add(Long.valueOf(userId));
            List<UserModel> userModelList = userService.getUserInfoList(loginUserId, userIdList);
            UserModel userModelNew = new UserModel();
            if (userModelList != null && userModelList.size() > 0) {
                userModelNew = userModelList.get(0);
                // 关注按钮判断
                if ("1".equals(userModelNew.getFollowingFlg())) {
                    // 显示关注
                    userModel.setFollowingBtnFlg("1");
                } else if ("2".equals(userModelNew.getFollowingFlg())) {
                    // 显示已关注
                    userModel.setFollowingBtnFlg("2");
                } else if ("3".equals(userModelNew.getFollowingFlg())) {
                    userModel.setFollowingBtnFlg("3");
                }
            }

        } else {
            // 显示关注
            userModel.setFollowingBtnFlg("1");
        }
        // 显示联系
        userModel.setContactBtnFlg("1");

        // 头像 文件服务器url获取
        String profilePath_s3 = null;
        if (StringUtil.isNotEmpty(user.getProfilePhotoPath())) {
            profilePath_s3 = AwsS3Utils.getS3FileUrl(user.getProfilePhotoPath());
            userModel.setProfilePhotoPath(profilePath_s3);
        }
//        userModel.setProfilePhotoPath(fileServerHelper.getFileServerUrl(user.getProfilePhotoPath(), UploadFolder.PROFILE_PHOTO));
        // 获取已编辑用户的信息
        UserInfoModel userInfo = new UserInfoModel();
        UserInfo dbUserInfo = userInfoMapper.getUserInfo(userId);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if(dbUserInfo != null) {
            // 头像 文件服务器url获取
            if (StringUtil.isNotEmpty(dbUserInfo.getProfilePath())) {
                profilePath_s3 = AwsS3Utils.getS3FileUrl(dbUserInfo.getProfilePath());
                dbUserInfo.setProfilePath(profilePath_s3);
            }
//            dbUserInfo.setProfilePath(fileServerHelper.getFileServerUrl(dbUserInfo.getProfilePath(), UploadFolder.PROFILE_PHOTO));
            BeanUtils.copyProperties(dbUserInfo, userInfo);
            if(dbUserInfo.getBirthday() != null){
                userInfo.setBirthdayStr(sdf.format(dbUserInfo.getBirthday()));
            }
        }
        userModel.setUserInfo(userInfo);

        return userModel;
    }

    /**
     * 根据文献ID获取共同作者(文献全文)
     *
     * @param publicationId
     * @param index
     * @param rows
     * @return
     */
    public PageInfo<Long> getAuthorsByPublicationId(Long publicationId, int index, int rows) {
        int pageNum = index;
        int pageSize = rows;
        PageHelper.startPage(pageNum, pageSize, true, null, true);
        List<Long> userIdList = userMapper.getAuthorsByPublicationId(publicationId);
        return new PageInfo<>(userIdList);
    }

    /**
     * 用户注册 插入
     *
     * @param userId
     * @param normalizedName
     * @param displayName
     * @param profilePhotoPath
     * @param userCategory
     * @param currentPosition
     * @param institutionName
     * @param departmentName
     * @param programDegree
     * @param yearStartedProgram
     */
    public int insertRegist(Long userId, String normalizedName, String displayName, String profilePhotoPath, String userCategory,
                            String currentPosition, String institutionName, String departmentName, String programDegree,
                            Integer yearStartedProgram) {
        Date createdAt = new Date();
        Date updatedAt = new Date();
        int insCount = userMapper.insertRegist(userId, normalizedName, displayName, profilePhotoPath, userCategory, currentPosition,
                institutionName, departmentName, programDegree, yearStartedProgram, createdAt, updatedAt);
        return insCount;
    }

    /**
     * 用户注册 插入
     *
     * @param userId
     * @param normalizedName
     * @param displayName
     */
    public int insertRegist(Long userId, String normalizedName, String displayName) {
        Date createdAt = new Date();
        Date updatedAt = new Date();
        User user = new User();
        user.setUserId(userId);
        user.setDisplayName(displayName);
        user.setDisplayNameCn(displayName);
        user.setNormalizedName(normalizedName);
        user.setCreatedAt(createdAt);
        user.setAccountStatus("3");
        user.setUpdatedAt(updatedAt);
        int insCount = userMapper.insertSelective(user);
        return insCount;
    }

    /**
     * 用户注册 更新
     *
     * @param userId
     * @param normalizedName
     * @param displayName
     * @param profilePhotoPath
     * @param userCategory
     * @param currentPosition
     * @param institutionName
     * @param departmentName
     * @param programDegree
     * @param yearStartedProgram
     * @return
     */
    public int updateRegist(Long userId, String normalizedName, String displayName, String profilePhotoPath, String userCategory,
                            String currentPosition, String institutionName, String departmentName, String programDegree,
                            Integer yearStartedProgram) {
        Date updatedAt = new Date();
        int insCount = userMapper.updateRegist(userId, normalizedName, displayName, profilePhotoPath, userCategory, currentPosition,
                institutionName, departmentName, programDegree, yearStartedProgram, updatedAt);
        return insCount;
    }

    /**
     * 用户注册时userId最大值查询
     *
     * @return
     */
    public Long getMaxUserId() {
        Long longUserId;
        if (userMapper.getMaxUserAuthId() != null) {
            longUserId = userMapper.getMaxUserAuthId();
        } else {
            longUserId = userMapper.getMaxUserId();
        }
        return longUserId;
    }


    /**
     * 用户详细信息一览
     *
     * @param userId
     * @param userIds
     * @return
     */
    public List<UserModel> getUserInfoList(Long userId, List<Long> userIds) {

        cn.newtouch.dl.turbocloud.model.UserModel loginUserModel = getLoginInfo();
        if (loginUserModel != null && !StringUtils.isEmpty(loginUserModel.getUserId())) {
            userId = Long.valueOf(loginUserModel.getUserId());
        } else {
            userId = null;
        }

        List<UserModel> userModels = new ArrayList<>();
        List<User> userList = userMapper.getUserInfoList(userId, userIds);
        for (User user : userList) {
            UserModel userModel = new UserModel();
            BeanUtils.copyProperties(user, userModel);
            // 用户登录的情况下
            if (userId != null) {
                // 关注
                if ("1".equals(userModel.getFollowingFlg())) {
                    userModel.setFollowingBtnFlg("1");
                }
                // 取消关注
                else if ("2".equals(userModel.getFollowingFlg())) {
                    userModel.setFollowingBtnFlg("2");
                }
                else if ("3".equals(userModel.getFollowingFlg())) {
                    userModel.setFollowingBtnFlg("3");
                }
            } else {
                // 用户不登录的情况下显示关注
                userModel.setFollowingBtnFlg("1");
            }
            // 联系
            userModel.setContactBtnFlg("1");

            // 头像 文件服务器url获取
            if (StringUtil.isNotEmpty(userModel.getProfilePhotoPath())) {
                String profilePath_s3 = AwsS3Utils.getS3FileUrl(userModel.getProfilePhotoPath());
                userModel.setProfilePhotoPath(profilePath_s3);
            }
//            userModel.setProfilePhotoPath(fileServerHelper.getFileServerUrl(user.getProfilePhotoPath(), UploadFolder.PROFILE_PHOTO));

            if (StringUtil.isNotEmpty(userModel.getProfilePath())) {
                String profilePath_s3 = AwsS3Utils.getS3FileUrl(userModel.getProfilePath());
                userModel.setProfilePath(profilePath_s3);
            }
//            userModel.setProfilePath(fileServerHelper.getFileServerUrl(user.getProfilePath(), UploadFolder.PROFILE_PHOTO));
            // 用户Id
            userModel.setUserIdForReq(userId);
            // 关注人Id
            userModel.setUserIdFollowedForReq(user.getUserId());
            // 关注人简介
            userModel.setIntroduction(user.getIntroduction());

            userModels.add(userModel);
        }
        return userModels;
    }

    /**
     * 获取用户详细信息
     *
     * @param userId
     * @param userIds
     * @return
     */
    public List<UserModel> getUserDetailInfo(Long userId, List<Long> userIds, String actionFlg) {
        List<UserModel> userModels = new ArrayList<UserModel>();
        List<User> userList = userMapper.getUserDetailInfo(userIds);
        List<User> users = new ArrayList<>();
        // 判断用户是否已登录
        if (userId != null) {
            // 检索结果重排序
            for (Long uId : userIds) {
                users.addAll(userList.stream().filter(user -> uId.equals(user.getUserId())).collect(Collectors.toList()));
            }
        } else {
            users = userList;
        }

        // 前端页面按钮flg设置
        for (User user : users) {
            UserModel userModel = new UserModel();
            BeanUtils.copyProperties(user, userModel);
            // 登录用户ID
            userModel.setUserIdForReq(userId);
            // 要关注用户ID
            userModel.setUserIdFollowedForReq(user.getUserId());
            // 头像 文件服务器url获取
            if (StringUtil.isNotEmpty(user.getProfilePhotoPath())) {
                String profilePath_s3 = AwsS3Utils.getS3FileUrl(user.getProfilePhotoPath());
                user.setProfilePhotoPath(profilePath_s3);
            }
//            userModel.setProfilePhotoPath(fileServerHelper.getFileServerUrl(user.getProfilePhotoPath(), UploadFolder.PROFILE_PHOTO));

            // 邀请您的合作作者加入社区按钮设置
            if ("1".equals(actionFlg)) {
                // 显示邀请按钮
                userModel.setInviteBtnFlg("1");
                // 隐藏关注按钮
                userModel.setFollowingBtnFlg("0");
                // 已关注的合作作者按钮设置
            } else if ("2".equals(actionFlg)) {
                // 隐藏邀请按钮
                userModel.setInviteBtnFlg("0");
                // 显示取消关注按钮
                userModel.setFollowingBtnFlg("2");
                // 你的粉丝按钮显示/合作作者
            } else {
                // 隐藏邀请按钮
                userModel.setInviteBtnFlg("0");
                // 显示关注按钮
                userModel.setFollowingBtnFlg("1");
                // 显示联系按钮
                userModel.setContactBtnFlg("1");
            }

            userModels.add(userModel);
        }

        return userModels;
    }

    /**
     * 获取账户状态
     *
     * @param userId
     * @return
     */
    public String getAccountStatus(Long userId) {
        return userMapper.getAccountStatus(userId);
    }

    /**
     * 更新当前登录时间和最后一次登录时间
     *
     * @param userId
     * @return
     */
    public int updateLoginAt(Long userId) {
        int count = 0;

        // 获取当前登录时间
        UserKey userKey = new UserKey();
        userKey.setUserId(userId);
        User user = userMapper.selectByPrimaryKey(userKey);
        Date currentLoginAt = user.getCurrentLoginAt();

        User record = new User();
        // 判断当前登录时间是否为空
        if (currentLoginAt != null) {
            // 当前登录时间不为空时，更新最后一次登录时间
            record.setUserId(userId);
            record.setCurrentLoginAt(new Date());
            record.setLastLoginAt(currentLoginAt);
            count = userMapper.updateByPrimaryKeySelective(record);
        } else {
            // 当前登录时间不为空时，更新最后一次登录时间
            record.setUserId(userId);
            record.setCurrentLoginAt(new Date());
            record.setLastLoginAt(new Date());
            count = userMapper.updateByPrimaryKeySelective(record);
        }
        return count;
    }

    /**
     * 查询用户基本信息
     * @return
     */
    public User selectByPrimaryKey(Long userId){
        UserKey userKey = new UserKey();
        userKey.setUserId(userId);
        User user = userMapper.selectByPrimaryKey(userKey);
        return user;
    }
}
