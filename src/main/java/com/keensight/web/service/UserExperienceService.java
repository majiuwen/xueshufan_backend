package com.keensight.web.service;

import com.keensight.web.db.entity.*;
import com.keensight.web.db.mapper.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserExperienceService {
    @Resource
    private UserEmploymentMapper userEmploymentMapper;
    @Resource
    private UserEducationMapper userEducationMapper;
    @Resource
    private UserFundingsMapper userFundingsMapper;
    @Resource
    private UserDistinctionsMapper userDistinctionsMapper;
    @Resource
    private UserMembershipMapper userMembershipMapper;
    @Resource
    private UserPeerReviewsMapper userPeerReviewsMapper;
    @Resource
    private UserContactInformationMapper userContactInformationMapper;
    @Resource
    private UserAcademicAdviserMapper userAcademicAdviserMapper;

    // 时间Date型转换成String型
    Date date = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月");

    /**
     * 获取作者工作经历信息
     * @param userId
     * @return
     */
    public List<UserEmployment> getUserEmploymentList(int userId) {
        // 获取作者工作经历信息
        List<UserEmployment> userEmploymentInfo = userEmploymentMapper.getUserEmploymentList(userId);
        if(userEmploymentInfo != null) {
            for (UserEmployment userEmployment : userEmploymentInfo) {
                if(userEmployment.getStartDate() != null) {
                    // 在职开始时间
                    userEmployment.setStartDateStr(sdf.format(userEmployment.getStartDate()));
                }
                if(userEmployment.getEndDate() != null) {
                    // 在职结束时间
                    userEmployment.setEndDateStr(sdf.format(userEmployment.getEndDate()));
                }
            }
        } else {
            userEmploymentInfo = new ArrayList<>();
        }
        return userEmploymentInfo;
    }

    /**
     * 获取作者教育背景信息
     * @param userId
     * @return
     */
    public List<UserEducation> getUserEducationList(int userId) {
        // 获取作者教育背景信息
        List<UserEducation> userEducationInfo = userEducationMapper.getUserEducationList(userId);
        if(userEducationInfo != null) {
            for (UserEducation userEducation : userEducationInfo) {
                if(userEducation.getStartDate() != null) {
                    // 教育开始时间
                    userEducation.setStartDateStr(sdf.format(userEducation.getStartDate()));
                }
                if(userEducation.getEndDate() != null) {
                    // 教育结束时间
                    userEducation.setEndDateStr(sdf.format(userEducation.getEndDate()));
                }
            }
        } else {
            userEducationInfo = new ArrayList<>();
        }
        return userEducationInfo;
    }

    /**
     * 获取作者项目信息
     * @param userId
     * @return
     */
    public List<UserFundings> getUserFundingsList(int userId) {
        // 获取作者项目信息
        List<UserFundings> userFundingsInfo = userFundingsMapper.getUserFundingsList(userId);
        if(userFundingsInfo != null) {
            for (UserFundings userFundings : userFundingsInfo) {
                if(userFundings.getStartDate() != null) {
                    // 项目开始时间
                    userFundings.setStartDateStr(sdf.format(userFundings.getStartDate()));
                }
                if(userFundings.getEndDate() != null) {
                    // 项目结束时间
                    userFundings.setEndDateStr(sdf.format(userFundings.getEndDate()));
                }
            }
        } else {
            userFundingsInfo = new ArrayList<>();
        }
        return userFundingsInfo;
    }

    /**
     * 获取作者获奖信息
     * @param userId
     * @return
     */
    public List<UserDistinctions> getUserDistinctionsList(int userId) {
        // 获取作者获奖信息
        List<UserDistinctions> userDistinctionsInfo = userDistinctionsMapper.getUserDistinctionsList(userId);
        if(userDistinctionsInfo != null) {
            for (UserDistinctions userDistinctions : userDistinctionsInfo) {
                if(userDistinctions.getStartDate() != null) {
                    // 获奖时间
                    userDistinctions.setStartDateStr(sdf.format(userDistinctions.getStartDate()));
                }
            }
        } else {
            userDistinctionsInfo = new ArrayList<>();
        }
        return userDistinctionsInfo;
    }

    /**
     * 获取作者学术组织会员信息
     * @param userId
     * @return
     */
    public List<UserMembership> getUserMembershipList(int userId) {
        // 获取作者学术组织会员信息
        List<UserMembership> userMembershipInfo = userMembershipMapper.getUserMembershipInfoList(userId);
        if(userMembershipInfo != null) {
            for (UserMembership userMembership : userMembershipInfo) {
                if(userMembership.getStartDate() != null) {
                    // 作者学术组织会员获奖时间
                    userMembership.setStartDateStr(sdf.format(userMembership.getStartDate()));
                }
            }
        } else {
            userMembershipInfo = new ArrayList<>();
        }
        return userMembershipInfo;
    }

    /**
     * 获取审稿人信息
     * @param userId
     * @return
     */
    public List<UserPeerReviews> getUserPeerReviewsList(int userId) {
        // 获取审稿人信息
        List<UserPeerReviews> userPeerReviewsInfo = userPeerReviewsMapper.getUserPeerReviewsList(userId);
        if(userPeerReviewsInfo != null) {
            for (UserPeerReviews userPeerReviews : userPeerReviewsInfo) {
                if(userPeerReviews.getYear() != null) {
                    // 审稿人年份
                    userPeerReviews.setYearStr(sdf.format(userPeerReviews.getYear()));
                }
            }
        } else {
            userPeerReviewsInfo = new ArrayList<>();
        }
        return userPeerReviewsInfo;
    }

    /**
     * 获取作者联系方式信息
     * @param userId
     * @return
     */
    public List<UserContactInformation> getUserContactInformationList(int userId) {
        List<UserContactInformation> userContactInformationInfo = userContactInformationMapper.getUserContactInformationInfoList(userId);
        if(userContactInformationInfo == null) {
            userContactInformationInfo = new ArrayList<>();
        }
        return userContactInformationInfo;
    }

    /**
     * 获取作者学术导师信息
     * @param userId
     * @return
     */
    public List<User> getUserAcademicAdviserList(int userId) {
        List<User> userAcademicAdviserInfo = userAcademicAdviserMapper.getUserAcademicAdviserInfoList(userId);
        if(userAcademicAdviserInfo == null) {
            userAcademicAdviserInfo = new ArrayList<>();
        }
        return userAcademicAdviserInfo;
    }
}
