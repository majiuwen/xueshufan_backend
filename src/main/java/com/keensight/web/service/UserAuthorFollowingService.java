package com.keensight.web.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.keensight.web.db.entity.SuggestedUserToFollow;
import com.keensight.web.db.entity.UserAuthorFollowing;
import com.keensight.web.db.mapper.SuggestedUserToFollowMapper;
import com.keensight.web.db.mapper.UserAuthorFollowingMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class UserAuthorFollowingService {

    @Resource
    private UserAuthorFollowingMapper userAuthorFollowingMapper;

    @Resource
    private SuggestedUserToFollowMapper suggestedUserToFollowMapper;

    /**
     * 推荐学者一览点击「1:关注」/「2:取消关注」
     *
     * @param userId
     * @param userIdFollowed
     * @param followingBtnFlg
     */
    public void addRecommendedClickToFollow(Long userId, Long userIdFollowed, String followingBtnFlg) {
        if ("1".equals(followingBtnFlg)) {
            // 判断是否已关注该用户
            int count = userAuthorFollowingMapper.getCount(userId, userIdFollowed);
            if (count == 0) {
                // 用户关注的其他用户表中追加当前关注用户
                UserAuthorFollowing userAuthorFollowing = new UserAuthorFollowing();
                userAuthorFollowing.setUserId(userId);
                userAuthorFollowing.setUserIdFollowed(userIdFollowed);
                userAuthorFollowing.setDeleted(false);
                userAuthorFollowing.setCreatedAt(new Date());
                userAuthorFollowing.setUpdatedAt(new Date());
                userAuthorFollowingMapper.insert(userAuthorFollowing);
            } else {
                // 更新记录更新时间
                UserAuthorFollowing userAuthorFollowing = new UserAuthorFollowing();
                userAuthorFollowing.setUserId(userId);
                userAuthorFollowing.setUserIdFollowed(userIdFollowed);
                userAuthorFollowing.setUpdatedAt(new Date());
                userAuthorFollowingMapper.updateDeletedByPrimaryKey(userAuthorFollowing);
            }
            // 修改建议用户关注的学者的状态「2: followed: 已关注」
            SuggestedUserToFollow suggestedUserToFollow = new SuggestedUserToFollow();
            suggestedUserToFollow.setUserId(userId);
            suggestedUserToFollow.setUserIdToFollow(userIdFollowed);
            suggestedUserToFollow.setStatus("2");
            suggestedUserToFollowMapper.updateStatsByPrimaryKey(suggestedUserToFollow);
        } else {
            // 修改用户关注的其他用户表中关注用户状态
            UserAuthorFollowing userAuthorFollowing = new UserAuthorFollowing();
            userAuthorFollowing.setUserId(userId);
            userAuthorFollowing.setUserIdFollowed(userIdFollowed);
            userAuthorFollowing.setDeleted(true);
            userAuthorFollowing.setUpdatedAt(new Date());
            userAuthorFollowingMapper.updateDeletedByPrimaryKey(userAuthorFollowing);
            // 修改建议用户关注的学者的状态「3: ingored: 不关注」
            SuggestedUserToFollow suggestedUserToFollow = new SuggestedUserToFollow();
            suggestedUserToFollow.setUserId(userId);
            suggestedUserToFollow.setUserIdToFollow(userIdFollowed);
            suggestedUserToFollow.setStatus("3");
            suggestedUserToFollowMapper.updateStatsByPrimaryKey(suggestedUserToFollow);
        }
    }

    /**
     * 获取用户关注其他用户的行为
     *
     * @param userId
     * @param index
     * @param rows
     * @return
     */
    public PageInfo<UserAuthorFollowing> getUserAuthorFollowingList(Long userId, int index, int rows) {
        int pageNum = index;
        int pageSize = rows;
        PageHelper.startPage(pageNum, pageSize);
        List<UserAuthorFollowing> userCoauthorList = userAuthorFollowingMapper.getUserAuthorFollowingList(userId);
        return new PageInfo<UserAuthorFollowing>(userCoauthorList);
    }

    /**
     * 你在关注的用户/你的粉丝/已关注的合作作者点击「取消关注」/「关注」按钮
     *
     * @param userId
     * @param userIdFollowed
     * @param followingBtnFlg
     */
    public void updateDeletedByPrimaryKey(Long userId, Long userIdFollowed, String followingBtnFlg) {
        // 点击取消按钮
        if ("2".equals(followingBtnFlg) || "3".equals(followingBtnFlg)) {
            UserAuthorFollowing userAuthorFollowing = new UserAuthorFollowing();
            userAuthorFollowing.setUserId(userId);
            userAuthorFollowing.setUserIdFollowed(userIdFollowed);
            userAuthorFollowing.setDeleted(true);
            userAuthorFollowing.setUpdatedAt(new Date());
            userAuthorFollowingMapper.updateDeletedByPrimaryKey(userAuthorFollowing);
        // 点击关注按钮
        } else {
            // 判断是否已关注该用户
            int count = userAuthorFollowingMapper.getCount(userId, userIdFollowed);
            if (count == 0) {
                // 用户关注的其他用户表中追加当前关注用户
                UserAuthorFollowing userAuthorFollowing = new UserAuthorFollowing();
                userAuthorFollowing.setUserId(userId);
                userAuthorFollowing.setUserIdFollowed(userIdFollowed);
                userAuthorFollowing.setDeleted(false);
                userAuthorFollowing.setCreatedAt(new Date());
                userAuthorFollowing.setUpdatedAt(new Date());
                userAuthorFollowingMapper.insert(userAuthorFollowing);
            } else {
                // 更新修改时间
                UserAuthorFollowing userAuthorFollowing = new UserAuthorFollowing();
                userAuthorFollowing.setUserId(userId);
                userAuthorFollowing.setUserIdFollowed(userIdFollowed);
                userAuthorFollowing.setUpdatedAt(new Date());
                userAuthorFollowingMapper.updateDeletedByPrimaryKey(userAuthorFollowing);
            }
        }
    }

    /**
     * 根据检索条件查询用户ID集合
     *
     * @param userId
     * @param index
     * @param rows
     * @return
     */
    public PageInfo<Long> getUserIdList(Long userId, int index, int rows) {
        int pageNum = index;
        int pageSize = rows;
        PageHelper.startPage(pageNum, pageSize, true, null, true);
        List<Long> userIdList = userAuthorFollowingMapper.getUserIdList(userId);
        return new PageInfo<>(userIdList);
    }

    /**
     * 根据检索条件查询用户ID集合
     *
     * @param userId
     * @param index
     * @param rows
     * @return
     */
    public PageInfo<Long> getFanUserIdList(Long userId, int index, int rows) {
        int pageNum = index;
        int pageSize = rows;
        PageHelper.startPage(pageNum, pageSize, true, null, true);
        List<Long> userIdList = userAuthorFollowingMapper.getFanUserIdList(userId);
        return new PageInfo<Long>(userIdList);
    }

    /**
     * 获取用户推荐的论文
     *
     * @param userId 用户Id
     * @param index 页数
     * @param rows 条数
     * @return 用户推荐的论文
     */
    public PageInfo<UserAuthorFollowing> getUserRecommendedPapers(Long userId, int index, int rows) {
        PageHelper.startPage(index, rows);
        List<UserAuthorFollowing> userCoauthorList = userAuthorFollowingMapper.getUserRecommendedPapers(userId);
        return new PageInfo<UserAuthorFollowing>(userCoauthorList);
    }
}
