package com.keensight.web.service;

import com.keensight.web.db.entity.ConferenceInstance;
import com.keensight.web.db.entity.ConferenceInstanceExample;
import com.keensight.web.db.entity.ConferenceInstanceKey;
import com.keensight.web.db.entity.ConferenceSerie;
import com.keensight.web.db.mapper.ConferenceInstanceMapper;
import com.keensight.web.db.mapper.ConferenceSerieMapper;
import com.keensight.web.utils.NormalizedUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 会议实例Service
 *
 * @author : newtouch
 * @version : v1.0.0 [2022/01/11] 新建
 */
@Service
public class ConferenceInstanceService {
    @Resource
    private ConferenceInstanceMapper conferenceInstanceMapper;

    /**
     * 根据会议实例Id检索会议实例信息
     *
     * @param conferenceInstanceId
     * @return
     */
    public ConferenceInstance getConferenceInstanceById(Long conferenceInstanceId) {
        ConferenceInstanceKey conferenceInstanceKey = new ConferenceInstanceKey();
        conferenceInstanceKey.setConferenceInstanceId(conferenceInstanceId);
        ConferenceInstance conferenceInstance = conferenceInstanceMapper.selectByPrimaryKey(conferenceInstanceKey);
        if (conferenceInstance == null) {
            conferenceInstance = new ConferenceInstance();
        }
        return conferenceInstance;
    }

    /**
     * 根据会议实例Name获取会议实例Id
     *
     * @param conferenceInstanceName
     * @return
     */
    public Long getConferenceInstanceId(String conferenceInstanceName, Long conferenceSerieId) {

        // 获取normalizedName
        String normalizedName = NormalizedUtil.getNormalizedTitle(conferenceInstanceName);

        Long conferenceInstanceId = conferenceInstanceMapper.getConferenceInstanceId(normalizedName);
        if (conferenceInstanceId == null) {
            conferenceInstanceId = conferenceInstanceMapper.getMaxConferenceInstanceId() + 1;
            ConferenceInstance conferenceInstance = new ConferenceInstance();
            conferenceInstance.setConferenceInstanceId(conferenceInstanceId);
            conferenceInstance.setNormalizedName(normalizedName);
            conferenceInstance.setDisplayName(conferenceInstanceName);
            conferenceInstance.setConferenceSerieId(conferenceSerieId);
            // 新增会议实例
            int resCont = conferenceInstanceMapper.insertSelective(conferenceInstance);
            if (resCont == 0) {
                conferenceInstanceId = null;
            }
        }

        return conferenceInstanceId;
    }

    /**
     * 自动补全会议名称
     *
     * @param conferenceInstanceName
     * @return
     */
    public List<ConferenceInstance> getConferenceInstanceList(String conferenceInstanceName) {

        List<ConferenceInstance> conferenceInstanceList = conferenceInstanceMapper.selectConferenceInstanceNameList(conferenceInstanceName);

        return conferenceInstanceList;
    }
}
