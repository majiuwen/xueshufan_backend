package com.keensight.web.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.keensight.web.db.mapper.UserCoauthorMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserCoauthorService {

    @Resource
    private UserCoauthorMapper userCoauthorMapper;

    /**
     * 根据检索条件查询用户ID集合
     *
     * @param userId
     * @param index
     * @param rows
     * @return
     */
    public PageInfo<Long> getUserIdList(Long userId, int index, int rows, String followFlg) {
        int pageNum = index;
        int pageSize = rows;
        PageHelper.startPage(pageNum, pageSize, true, null, true);
        List<Long> userIdList = userCoauthorMapper.getUserIdList(userId, followFlg);
        return new PageInfo<Long>(userIdList);
    }
}
