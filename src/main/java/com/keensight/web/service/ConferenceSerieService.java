package com.keensight.web.service;

import com.keensight.web.db.entity.ConferenceInstance;
import com.keensight.web.db.entity.ConferenceSerie;
import com.keensight.web.db.entity.ConferenceSerieKey;
import com.keensight.web.db.mapper.ConferenceSerieMapper;
import com.keensight.web.utils.NormalizedUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 会议系列Service
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/12/03] 新建
 */
@Service
public class ConferenceSerieService {
    @Resource
    private ConferenceSerieMapper conferenceSerieMapper;

    /**
     * 根据会议系列Id集合检索会议系列信息
     *
     * @param conferenceSerieIdList
     * @return
     */
    public List<ConferenceSerie> getConferenceSerieListByIdList(List<Long> conferenceSerieIdList) {
        List<ConferenceSerie> resultList = conferenceSerieMapper.getConferenceSerieListByIdList(conferenceSerieIdList);
        if (resultList == null) {
            resultList = new ArrayList<>();
        }
        return resultList;
    }

    /**
     * 根据会议系列Id检索会议实例信息
     *
     * @param ConferenceSerieId
     * @return
     */
    public ConferenceSerie getConferenceSerieById(Long ConferenceSerieId) {
        ConferenceSerieKey conferenceSerieKey = new ConferenceSerieKey();
        conferenceSerieKey.setConferenceSerieId(ConferenceSerieId);
        ConferenceSerie conferenceSerie = conferenceSerieMapper.selectByPrimaryKey(conferenceSerieKey);
        if (conferenceSerie == null) {
            conferenceSerie = new ConferenceSerie();
        }
        return conferenceSerie;
    }

    /**
     * 根据会议系列Name获取会议实例Id
     *
     * @param conferenceSerieName
     * @return
     */
    public Long getConferenceSerieId(String conferenceSerieName) {

        // 获取normalizedName
        String normalizedName = NormalizedUtil.getNormalizedTitle(conferenceSerieName);

        Long conferenceSerieId = conferenceSerieMapper.getConferenceSerieId(normalizedName);
        if (conferenceSerieId == null) {
            conferenceSerieId = conferenceSerieMapper.getMaxConferenceSerieId() + 1;
            ConferenceSerie conferenceSerie = new ConferenceSerie();
            conferenceSerie.setConferenceSerieId(conferenceSerieId);
            conferenceSerie.setNormalizedName(normalizedName);
            conferenceSerie.setDisplayName(conferenceSerieName);
            // 新增会议实例
            int resCont = conferenceSerieMapper.insertSelective(conferenceSerie);
            if (resCont == 0) {
                conferenceSerieId = null;
            }
        }

        return conferenceSerieId;
    }

    /**
     * 自动补全会议系列名称
     *
     * @param conferenceSerieName
     * @return
     */
    public List<ConferenceSerie> getConferenceSerieList(String conferenceSerieName) {

        List<ConferenceSerie> conferenceInstanceList = conferenceSerieMapper.selectConferenceSerieNameList(conferenceSerieName);

        return conferenceInstanceList;
    }
}
