package com.keensight.web.service;

import com.keensight.web.db.entity.UserAnnotation;
import com.keensight.web.db.entity.UserAnnotationKey;
import com.keensight.web.db.mapper.UserAnnotationMapper;
import com.keensight.web.model.UserAnnotationModel;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserAnnotationService {
    @Resource
    private UserAnnotationMapper userAnnotationMapper;

    /**
     * 获取用户批注信息
     * @param excerptIdList
     * @param seachContent
     * @return
     */
    public List<UserAnnotationModel> getUserAnnotation4List(ArrayList excerptIdList, String seachContent) {

        // 用户批注信息List
        List<UserAnnotationModel> userAnnotationModelList = new ArrayList<UserAnnotationModel>();
        // 获取用户批注信息
        List<UserAnnotation> userAnnotationList = userAnnotationMapper.selectUserAnnotationByCondition(excerptIdList, seachContent);
        if (userAnnotationList.size() > 0) {
            for (UserAnnotation userAnnotation : userAnnotationList) {
                UserAnnotationModel userAnnotationModel = new UserAnnotationModel();
                BeanUtils.copyProperties(userAnnotation, userAnnotationModel);
                // 批注日期
                String annotationDateMonAndDay = new SimpleDateFormat("MM-dd").format(userAnnotationModel.getAnnotationDate());
                // 批注时间
                String annotationDateHourAndMin = new SimpleDateFormat("HH:mm").format(userAnnotationModel.getAnnotationDate());
                userAnnotationModel.setAnnotationDateMonAndDayStr(annotationDateMonAndDay);
                userAnnotationModel.setAnnotationDateHourAndMinStr(annotationDateHourAndMin);
                userAnnotationModelList.add(userAnnotationModel);
            }
        }

        return userAnnotationModelList;
    }

    /**
     * 新批注录信息
     * @param userAnnotationModel
     * @return
     */
    public UserAnnotationModel insertUserAnnotation(UserAnnotationModel userAnnotationModel) {

        int resCount = 0;
        if (!ObjectUtils.isEmpty(userAnnotationModel)) {
            UserAnnotation userAnnotation = new UserAnnotation();
            userAnnotation.setExcerptId(userAnnotationModel.getExcerptId());
            userAnnotation.setAnnotationDate(new Date());
            userAnnotation.setAnnotationContent(userAnnotationModel.getAnnotationContent());
            resCount = userAnnotationMapper.insertSelective(userAnnotation);
            if (resCount > 0) {
                BeanUtils.copyProperties(userAnnotation, userAnnotationModel);
                // 批注日期
                String annotationDateMonAndDay = new SimpleDateFormat("MM-dd").format(userAnnotationModel.getAnnotationDate());
                // 批注时间
                String annotationDateHourAndMin = new SimpleDateFormat("HH:mm").format(userAnnotationModel.getAnnotationDate());
                userAnnotationModel.setAnnotationDateMonAndDayStr(annotationDateMonAndDay);
                userAnnotationModel.setAnnotationDateHourAndMinStr(annotationDateHourAndMin);
            }
        }

        return userAnnotationModel;
    }

    /**
     * 删除用户批注信息
     * @param annotationId
     * @return
     */
    public int deleteUserAnnotationByKey(Long annotationId) {

        // 设定批注key
        UserAnnotationKey userAnnotationKey = new UserAnnotationKey();
        userAnnotationKey.setAnnotationId(annotationId);
        // 删除批注信息
        int resCount = userAnnotationMapper.deleteByPrimaryKey(userAnnotationKey);

        return resCount;
    }

    /**
     * 更新用户批注信息
     * @param userAnnotationModel
     * @return
     */
    public int updateUserAnnotationByKey(UserAnnotationModel userAnnotationModel) {

        // 设定更新批注信息
        UserAnnotation userAnnotation = new UserAnnotation();
        userAnnotation.setAnnotationId(userAnnotationModel.getAnnotationId());
        userAnnotation.setAnnotationContent(userAnnotationModel.getAnnotationContent());
        // 删除批注信息
        int resCount = userAnnotationMapper.updateByPrimaryKeySelective(userAnnotation);

        return resCount;
    }
}
