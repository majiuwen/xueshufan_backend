package com.keensight.web.service;

import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.keensight.web.db.entity.FieldTopEntity;
import com.keensight.web.db.mapper.FieldTopEntityMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 领域顶尖实体Service
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/16] 新建
 */
@Service
public class FieldTopEntityService {
    @Resource
    private FieldTopEntityMapper fieldTopEntityMapper;

    /**
     * 根据实体类型获取指定排名的领域顶尖实体集合
     * （目前Top数据从Solr中获取，后期会使用该方法）
     *
     * @param fieldId    研究领域ID
     * @param entityType 实体类型
     * @param rows       检索件数
     * @return 排名集合
     */
    public List<FieldTopEntity> getTopEntityByEntityType(Long fieldId, String entityType, Integer rows) {
        List<FieldTopEntity> resultList = new ArrayList<>();
        if (fieldId != null && !StringUtils.isEmpty(entityType) && rows != null) {
            resultList = fieldTopEntityMapper.getTopEntityByEntityType(fieldId, entityType, rows);
        }
        return resultList;
    }

    /**
     * 领域内最重要的指定件数的论文取得
     *
     * @param fieldId
     * @param rows
     * @return
     */
    public PageInfo<Long> getPublicationIdListByFieldId(Long fieldId, Integer index, Integer rows, String publicationType) {
        // 指定件数的领域内最重要的论文总件数取得
        int publicationCount = fieldTopEntityMapper.getPublicationIdListsByFieldIdCount(fieldId, publicationType);
        PageInfo<Long> pagesInfo= new PageInfo<Long>();
        pagesInfo.setTotal(publicationCount);
        pagesInfo.setHasNextPage(publicationCount > 0 && publicationCount > index * rows);
        PageHelper.startPage(index, rows, true, null, true);
        // 指定件数的领域内最重要的论文取得
        List<Long> publicationIdList = fieldTopEntityMapper.getPublicationIdListsByFieldId(fieldId, publicationType);
        pagesInfo.setList(publicationIdList);
        return pagesInfo;
    }
}
