package com.keensight.web.service;

import com.keensight.web.db.entity.Code;
import com.keensight.web.db.entity.CodeExample;
import com.keensight.web.db.mapper.CodeMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author : newtouch
 * @version : v1.0.0 [2020/11/09] 新建
 */
@Service
public class CodeService {
    @Resource
    private CodeMapper codeMapper;

    /**
     * 根据代码区分取得code集合
     *
     * @param groupCd 代码区分
     * @return code集合
     */
    public List<Code> getCodeListInfoByGroupCd(String groupCd) {
        CodeExample codeExample = new CodeExample();
        CodeExample.Criteria codeExampleCriteria = codeExample.createCriteria();
        codeExampleCriteria.andGroupCdEqualTo(groupCd);
        return codeMapper.selectByExample(codeExample);
    }

    /**
     * 根据代码区分取得code集合
     *
     * @param groupCd 代码区分
     * @return code集合
     */
    public List<Code> getDocTypeListInfoByGroupCd(String groupCd) {
        CodeExample codeExample = new CodeExample();
        CodeExample.Criteria codeExampleCriteria = codeExample.createCriteria();
        List<String> codeList = new ArrayList<String>();
        codeList.add("Journal");
        codeList.add("Conference");
        codeList.add("Repository");
        codeList.add("Book");
        codeList.add("BookChapter");
        return codeMapper.getDocTypeNameList(groupCd, codeList);
    }
}
