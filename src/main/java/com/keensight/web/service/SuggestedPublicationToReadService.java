package com.keensight.web.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.keensight.web.db.entity.SuggestedPublicationToRead;
import com.keensight.web.db.mapper.SuggestedPublicationToReadMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SuggestedPublicationToReadService {

    @Resource
    private SuggestedPublicationToReadMapper suggestedPublicationToReadMapper;

    /**
     * 获取推荐论文
     *
     * @param userId
     * @param index
     * @param rows
     * @return
     */
    public PageInfo<SuggestedPublicationToRead> getSuggestedPublicationToRead(Long userId, int index, int rows) {
        int pageNum = index;
        int pageSize = rows;
        PageHelper.startPage(pageNum, pageSize);
        List<SuggestedPublicationToRead> suggestedPublicationToReads = suggestedPublicationToReadMapper.getSuggestedPublicationToRead(userId);
        return new PageInfo<SuggestedPublicationToRead>(suggestedPublicationToReads);
    }
}
