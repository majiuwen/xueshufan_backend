package com.keensight.web.service;

import com.keensight.web.db.entity.StatsPublicationMonthly;
import com.keensight.web.db.mapper.StatsPublicationMonthlyMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 文献月统计Service
 *
 * @author : newtouch
 * @version : v1.0.0 [2021/10/26] 新建
 */
@Service
public class StatsPublicationMonthlyService {
    @Resource
    private StatsPublicationMonthlyMapper statsPublicationMonthlyMapper;

    /**
     * 根据文献ID获取文献月统计信息
     * @param publicationId
     * @return
     */
    public List<StatsPublicationMonthly> getStatsPublicationMonthlyList(int publicationId) {
        List<StatsPublicationMonthly> statsPublicationMonthlyInfo = statsPublicationMonthlyMapper.getStatsPublicationMonthlyList(publicationId);
        if(statsPublicationMonthlyInfo == null) {
            statsPublicationMonthlyInfo = new ArrayList<>();
        }
        return statsPublicationMonthlyInfo;
    }
}
