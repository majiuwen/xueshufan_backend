package com.keensight.web.service;

import com.keensight.web.db.entity.StatsUserTotal;
import com.keensight.web.db.entity.StatsUserWeekly;
import com.keensight.web.db.entity.StatsUserYearly;
import com.keensight.web.db.mapper.StatsUserTotalMapper;
import com.keensight.web.db.mapper.StatsUserWeeklyMapper;
import com.keensight.web.db.mapper.StatsUserYearlyMapper;
import com.keensight.web.utils.DateUtil;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class StatsUserTotalService {

    @Resource
    private StatsUserTotalMapper statsUserTotalMapper;
    @Resource
    private StatsUserWeeklyMapper statsUserWeeklyMapper;
    @Resource
    private StatsUserYearlyMapper statsUserYearlyMapper;

    /**
     * 获取作者文献统计总数
     * @param userId
     * @return
     */
    public StatsUserTotal getUserTotal(Long userId) {
        return statsUserTotalMapper.getUserTotal(userId);
    }

    /**
     * 本周您研究影响力统计
     * @param userId
     * @return
     */
    public List<StatsUserWeekly> getThisWeekStatistics(Long userId){
        // 获取当前时间
        Date date = new Date();
        // 获取上周周日
        Date lastWeek = DateUtil.getLastMonday(date);
        // 获取本周周日
        Date thisWeek = DateUtil.getLastDayOfWeek(date);
        // 获取上周数据
        List<StatsUserWeekly> statsUserWeeklyList = new ArrayList<>();
        StatsUserWeekly lastWeekly = statsUserWeeklyMapper.getThisWeekStatistics(userId, lastWeek);
        if (StringUtils.isEmpty(lastWeekly)) {
            StatsUserWeekly statsUserWeekly = new StatsUserWeekly();
            statsUserWeekly.setWeekFlg("lastWeek");
            statsUserWeekly.setPublications(0);
            statsUserWeekly.setCitations(0);
            statsUserWeekly.setReads(0);
            statsUserWeekly.setRecommendations(0);
            statsUserWeekly.setSharings(0);
            statsUserWeekly.setSavings(0);
            statsUserWeekly.setDownloads(0);
            statsUserWeekly.setFollowings(0);
            statsUserWeekly.setReported(0);
            statsUserWeeklyList.add(statsUserWeekly);
        } else {
            lastWeekly.setWeekFlg("lastWeek");
            statsUserWeeklyList.add(lastWeekly);
        }
        // 获取本周数据
        StatsUserWeekly thisWeekly = statsUserWeeklyMapper.getThisWeekStatistics(userId, thisWeek);
        if (StringUtils.isEmpty(thisWeekly)) {
            StatsUserWeekly statsUserWeekly = new StatsUserWeekly();
            statsUserWeekly.setWeekFlg("thisWeek");
            statsUserWeekly.setPublications(0);
            statsUserWeekly.setCitations(0);
            statsUserWeekly.setReads(0);
            statsUserWeekly.setRecommendations(0);
            statsUserWeekly.setSharings(0);
            statsUserWeekly.setSavings(0);
            statsUserWeekly.setDownloads(0);
            statsUserWeekly.setFollowings(0);
            statsUserWeekly.setReported(0);
            statsUserWeeklyList.add(statsUserWeekly);
        } else {
            thisWeekly.setWeekFlg("thisWeek");
            statsUserWeeklyList.add(thisWeekly);
        }
        return statsUserWeeklyList;
    }

    /**
     * 获取作者年度文献统计
     * @param userId
     * @return
     */
    public List<StatsUserYearly> getStatsUserYearlyList(Long userId) {
        return statsUserYearlyMapper.getStatsUserYearlyList(userId);
    }

    /**
     * 根据作者ID获取作者文献统计总数
     * @param userId
     * @return
     */
    public StatsUserTotal getStatsUserTotal(int userId) {
        return statsUserTotalMapper.getStatsUserTotals(userId);
    }
}
