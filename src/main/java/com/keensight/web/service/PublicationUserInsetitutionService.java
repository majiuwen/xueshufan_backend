package com.keensight.web.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.keensight.web.db.entity.*;
import com.keensight.web.db.mapper.*;
import com.keensight.web.model.SuggestedUserModel;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PublicationUserInsetitutionService {

    @Resource
    private PublicationUserInstitutionMapper publicationUserInstitutionMapper;

    @Resource
    private StatsPublicationTotalMapper statsPublicationTotalMapper;

    @Resource
    private StatsUserTotalMapper statsUserTotalMapper;

    @Resource
    private StatsPublicationYearlyMapper statsPublicationYearlyMapper;

    @Resource
    private StatsUserYearlyMapper statsUserYearlyMapper;

    @Resource
    private StatsPublicationMonthlyMapper statsPublicationMonthlyMapper;

    @Resource
    private StatsUserMonthlyMapper statsUserMonthlyMapper;

    @Resource
    private StatsPublicationWeeklyMapper statsPublicationWeeklyMapper;

    @Resource
    private StatsUserWeeklyMapper statsUserWeeklyMapper;

    @Resource
    private UserCoauthorMapper userCoauthorMapper;

    @Resource
    private PublicationFieldMapper publicationFieldMapper;

    @Resource
    private UserFieldMapper userFieldMapper;

    @Resource
    private DisciplineMapper disciplineMapper;

    @Resource
    private UserDisciplineMapper userDisciplineMapper;

    @Resource
    private UserMapper userMapper;

    @Resource
    private PublicationMapper publicationMapper;

    /**
     * 原始作者名取得
     *
     * @param userId
     * @return
     */

    public List<PublicationUserInstitution> getPublicationUserInsetitutionByUserId(Long userId) {
        List<PublicationUserInstitution> PublicationUserInstitutionToList = new ArrayList<>();
        List<PublicationUserInstitution> publicationUserInstitutionList = publicationUserInstitutionMapper.getPublicationUserInstitutionByUserId(userId);
        for (PublicationUserInstitution publicationUserInstitution : publicationUserInstitutionList) {
            PublicationUserInstitution PublicationUserInstitution = new PublicationUserInstitution();

            BeanUtils.copyProperties(publicationUserInstitution, PublicationUserInstitution);
            PublicationUserInstitutionToList.add(PublicationUserInstitution);
        }
        return PublicationUserInstitutionToList;
    }

    /**
     * 现在文献用户作者名取得
     *
     * @param publicationId
     * @return
     */

    public List<User> getDisplayNames(Long publicationId) {
        List<User> userToList = new ArrayList<>();
        List<User> userList = userMapper.getDisplayNames(publicationId);
        for (User user : userList) {
            User User = new User();
            BeanUtils.copyProperties(user, User);
            userToList.add(User);
        }
        return userToList;
    }

    /**
     * 文献统计数字取得
     *
     * @param publicationId
     * @return
     */

    public StatsPublicationTotal getAddCountList(Long publicationId) {
        StatsPublicationTotal statsPublicationTotal = new StatsPublicationTotal();
        statsPublicationTotal.setPublicationId(publicationId);
        return statsPublicationTotalMapper.selectByPrimaryKey(statsPublicationTotal);
    }

    /**
     * 旧作者文献统计总数减少更新
     *
     * @param citations
     * @param reads
     * @param recommendations
     * @param sharings
     * @param savings
     * @param downloads
     * @param followings
     * @param reported
     * @param userId
     * @param time
     */
    public void updateOldStatsUserTotal(Long userId, Integer citations, Integer reads, Integer recommendations, Integer sharings, Integer savings, Integer downloads, Integer reported, Integer followings, Date time) {
        StatsUserTotal oldStatsUserTotal = new StatsUserTotal();
        oldStatsUserTotal.setUserId(userId);
        oldStatsUserTotal.setCitations(citations);
        oldStatsUserTotal.setReads(reads);
        oldStatsUserTotal.setRecommendations(recommendations);
        oldStatsUserTotal.setSharings(sharings);
        oldStatsUserTotal.setSavings(savings);
        oldStatsUserTotal.setDownloads(downloads);
        oldStatsUserTotal.setReported(reported);
        oldStatsUserTotal.setFollowings(followings);
        oldStatsUserTotal.setUpdatedAt(time);
        statsUserTotalMapper.updateOldStatsUserTotal(oldStatsUserTotal);
    }

    /**
     * 新作者文献统计总数增加更新
     *
     * @param citations
     * @param reads
     * @param recommendations
     * @param sharings
     * @param savings
     * @param downloads
     * @param followings
     * @param reported
     * @param userId
     * @param time
     */
    public int updateNewStatsUserTotal(Long userId, Integer citations, Integer reads, Integer recommendations, Integer sharings, Integer savings, Integer downloads, Integer reported, Integer followings, Date time) {
        StatsUserTotal newStatsUserTotal = new StatsUserTotal();
        newStatsUserTotal.setUserId(userId);
        newStatsUserTotal.setCitations(citations);
        newStatsUserTotal.setReads(reads);
        newStatsUserTotal.setRecommendations(recommendations);
        newStatsUserTotal.setSharings(sharings);
        newStatsUserTotal.setSavings(savings);
        newStatsUserTotal.setDownloads(downloads);
        newStatsUserTotal.setReported(reported);
        newStatsUserTotal.setFollowings(followings);
        newStatsUserTotal.setUpdatedAt(time);
        int updateCount = statsUserTotalMapper.updateNewStatsUserTotal(newStatsUserTotal);
        return updateCount;
    }

    /**
     * 新作者文献统计总数增加更新
     *
     * @param citations
     * @param reads
     * @param recommendations
     * @param sharings
     * @param savings
     * @param downloads
     * @param followings
     * @param reported
     * @param userId
     * @param time
     */
    public void insertNewStatsUserTotal(Long userId, Integer citations, Integer reads, Integer recommendations, Integer sharings, Integer savings, Integer downloads, Integer reported, Integer followings, Date time) {
        StatsUserTotal insertNewStatsUserTotal = new StatsUserTotal();
        insertNewStatsUserTotal.setUserId(userId);
        insertNewStatsUserTotal.setPublications(1);
        insertNewStatsUserTotal.setCitations(citations);
        insertNewStatsUserTotal.setReads(reads);
        insertNewStatsUserTotal.setRecommendations(recommendations);
        insertNewStatsUserTotal.setSharings(sharings);
        insertNewStatsUserTotal.setSavings(savings);
        insertNewStatsUserTotal.setDownloads(downloads);
        insertNewStatsUserTotal.setReported(reported);
        insertNewStatsUserTotal.setFollowings(followings);
        insertNewStatsUserTotal.setUpdatedAt(time);
        statsUserTotalMapper.insert(insertNewStatsUserTotal);
    }

    /**
     * 文献年度统计取得
     *
     * @param publicationId
     * @return
     */
    public List<StatsPublicationYearly> getPublicationYear(Long publicationId) {
        List<StatsPublicationYearly> StatsPublicationYearlyToList = new ArrayList<>();
        List<StatsPublicationYearly> StatsPublicationYearlyList = statsPublicationYearlyMapper.getPublicationYear(publicationId);
        for (StatsPublicationYearly statsPublicationYearlyIndex : StatsPublicationYearlyList) {
            StatsPublicationYearly statsPublicationYearly = new StatsPublicationYearly();
            BeanUtils.copyProperties(statsPublicationYearlyIndex, statsPublicationYearly);
            StatsPublicationYearlyToList.add(statsPublicationYearly);
        }
        return StatsPublicationYearlyToList;
    }

    /**
     * 旧作者文献年度统计总数减少更新
     *
     * @param year
     * @param yearCitations
     * @param yearReads
     * @param yearRecommendations
     * @param yearSharings
     * @param yearSavings
     * @param yearDownloads
     * @param yearFollowings
     * @param yearReported
     * @param userId
     * @param time
     */
    public void updateOldStatsUsersYearly(Long userId, Date year, Integer yearCitations, Integer yearReads, Integer yearRecommendations, Integer yearSharings, Integer yearSavings, Integer yearDownloads, Integer yearFollowings, Integer yearReported, Date time) {
        StatsUserYearly oldStatsUserYearly = new StatsUserYearly();
        oldStatsUserYearly.setUserId(userId);
        oldStatsUserYearly.setYear(year);
        oldStatsUserYearly.setCitations(yearCitations);
        oldStatsUserYearly.setReads(yearReads);
        oldStatsUserYearly.setRecommendations(yearRecommendations);
        oldStatsUserYearly.setSharings(yearSharings);
        oldStatsUserYearly.setSavings(yearSavings);
        oldStatsUserYearly.setDownloads(yearDownloads);
        oldStatsUserYearly.setReported(yearReported);
        oldStatsUserYearly.setFollowings(yearFollowings);
        oldStatsUserYearly.setUpdatedAt(time);
        statsUserYearlyMapper.updateOldStatsUsersYearly(oldStatsUserYearly);
    }

    /**
     * 新作者文献年度统计总数增加更新
     *
     * @param year
     * @param yearCitations
     * @param yearReads
     * @param yearRecommendations
     * @param yearSharings
     * @param yearSavings
     * @param yearDownloads
     * @param yearFollowings
     * @param yearReported
     * @param userId
     * @param time
     */
    public int updateNewStatsUserYearly(Long userId, Date year, Integer yearCitations, Integer yearReads, Integer yearRecommendations, Integer yearSharings, Integer yearSavings, Integer yearDownloads, Integer yearFollowings, Integer yearReported, Date time) {
        StatsUserYearly newStatsUserYearly = new StatsUserYearly();
        newStatsUserYearly.setUserId(userId);
        newStatsUserYearly.setYear(year);
        newStatsUserYearly.setCitations(yearCitations);
        newStatsUserYearly.setReads(yearReads);
        newStatsUserYearly.setRecommendations(yearRecommendations);
        newStatsUserYearly.setSharings(yearSharings);
        newStatsUserYearly.setSavings(yearSavings);
        newStatsUserYearly.setDownloads(yearDownloads);
        newStatsUserYearly.setReported(yearReported);
        newStatsUserYearly.setFollowings(yearFollowings);
        newStatsUserYearly.setUpdatedAt(time);
        int updateCount = statsUserYearlyMapper.updateNewStatsUserYearly(newStatsUserYearly);
        return updateCount;
    }

    /**
     * 新作者文献年度统计总数增加新规追加
     *
     * @param year
     * @param yearCitations
     * @param yearReads
     * @param yearRecommendations
     * @param yearSharings
     * @param yearSavings
     * @param yearDownloads
     * @param yearFollowings
     * @param yearReported
     * @param userId
     * @param time
     */
    public void insertNewStatsPublicationYearly(Long userId, Date year, Integer yearCitations, Integer yearReads, Integer yearRecommendations, Integer yearSharings, Integer yearSavings, Integer yearDownloads, Integer yearFollowings, Integer yearReported, Date time) {
        StatsUserYearly insertNewStatsUserYearly = new StatsUserYearly();
        insertNewStatsUserYearly.setUserId(userId);
        insertNewStatsUserYearly.setYear(year);
        insertNewStatsUserYearly.setPublications(0);
        insertNewStatsUserYearly.setCitations(yearCitations);
        insertNewStatsUserYearly.setReads(yearReads);
        insertNewStatsUserYearly.setRecommendations(yearRecommendations);
        insertNewStatsUserYearly.setSharings(yearSharings);
        insertNewStatsUserYearly.setSavings(yearSavings);
        insertNewStatsUserYearly.setDownloads(yearDownloads);
        insertNewStatsUserYearly.setReported(yearReported);
        insertNewStatsUserYearly.setFollowings(yearFollowings);
        insertNewStatsUserYearly.setUpdatedAt(time);
        statsUserYearlyMapper.insert(insertNewStatsUserYearly);
    }

    /**
     * 文献月统计取得
     *
     * @param publicationId
     * @return
     */
    public List<StatsPublicationMonthly> getPublicationMonthly(Long publicationId) {
        List<StatsPublicationMonthly> StatsPublicationMonthlyToList = new ArrayList<>();
        List<StatsPublicationMonthly> StatsPublicationMonthlyList = statsPublicationMonthlyMapper.getPublicationMonthly(publicationId);

        for (StatsPublicationMonthly statsPublicationMonthlyIndex : StatsPublicationMonthlyList) {
            StatsPublicationMonthly statsPublicationMonthly = new StatsPublicationMonthly();
            BeanUtils.copyProperties(statsPublicationMonthlyIndex, statsPublicationMonthly);
            StatsPublicationMonthlyToList.add(statsPublicationMonthly);
        }
        return StatsPublicationMonthlyToList;
    }

    /**
     * 旧作者文献月度统计总数减少更新
     *
     * @param month
     * @param monthCitations
     * @param monthReads
     * @param monthRecommendations
     * @param monthSharings
     * @param monthSavings
     * @param monthDownloads
     * @param monthFollowings
     * @param monthReported
     * @param userId
     * @param time
     */
    public void updateOldStatsUserMonth(Long userId, Date month, Integer monthCitations, Integer monthReads, Integer monthRecommendations, Integer monthSharings, Integer monthSavings, Integer monthDownloads, Integer monthFollowings, Integer monthReported, Date time) {
        StatsUserMonthly oldStatsUserMonthly = new StatsUserMonthly();
        oldStatsUserMonthly.setUserId(userId);
        oldStatsUserMonthly.setMonth(month);
        oldStatsUserMonthly.setCitations(monthCitations);
        oldStatsUserMonthly.setReads(monthReads);
        oldStatsUserMonthly.setRecommendations(monthRecommendations);
        oldStatsUserMonthly.setSharings(monthSharings);
        oldStatsUserMonthly.setSavings(monthSavings);
        oldStatsUserMonthly.setDownloads(monthDownloads);
        oldStatsUserMonthly.setReported(monthFollowings);
        oldStatsUserMonthly.setFollowings(monthReported);
        oldStatsUserMonthly.setUpdatedAt(time);
        statsUserMonthlyMapper.updateOldStatsUserMonth(oldStatsUserMonthly);
    }

    /**
     * 新作者文献月度统计总数增加更新
     *
     * @param month
     * @param monthCitations
     * @param monthReads
     * @param monthRecommendations
     * @param monthSharings
     * @param monthSavings
     * @param monthDownloads
     * @param monthFollowings
     * @param monthReported
     * @param userId
     * @param time
     */
    public int updateNewStatsUserMonth(Long userId, Date month, Integer monthCitations, Integer monthReads, Integer monthRecommendations, Integer monthSharings, Integer monthSavings, Integer monthDownloads, Integer monthFollowings, Integer monthReported, Date time) {
        StatsUserMonthly newStatsUserMonthly = new StatsUserMonthly();
        newStatsUserMonthly.setUserId(userId);
        newStatsUserMonthly.setMonth(month);
        newStatsUserMonthly.setCitations(monthCitations);
        newStatsUserMonthly.setReads(monthReads);
        newStatsUserMonthly.setRecommendations(monthRecommendations);
        newStatsUserMonthly.setSharings(monthSharings);
        newStatsUserMonthly.setSavings(monthSavings);
        newStatsUserMonthly.setDownloads(monthDownloads);
        newStatsUserMonthly.setReported(monthFollowings);
        newStatsUserMonthly.setFollowings(monthReported);
        newStatsUserMonthly.setUpdatedAt(time);
        int updateCount = statsUserMonthlyMapper.updateNewStatsUserMonth(newStatsUserMonthly);
        return updateCount;
    }

    /**
     * 新作者文献月度统计总数增加新规追加
     *
     * @param month
     * @param monthCitations
     * @param monthReads
     * @param monthRecommendations
     * @param monthSharings
     * @param monthSavings
     * @param monthDownloads
     * @param monthFollowings
     * @param monthReported
     * @param userId
     * @param time
     */
    public void insertNewStatsUserMonth(Long userId, Date month, Integer monthCitations, Integer monthReads, Integer monthRecommendations, Integer monthSharings, Integer monthSavings, Integer monthDownloads, Integer monthFollowings, Integer monthReported, Date time) {
        StatsUserMonthly insertNewStatsUserMonthly = new StatsUserMonthly();
        insertNewStatsUserMonthly.setUserId(userId);
        insertNewStatsUserMonthly.setMonth(month);
        insertNewStatsUserMonthly.setPublications(0);
        insertNewStatsUserMonthly.setCitations(monthCitations);
        insertNewStatsUserMonthly.setReads(monthReads);
        insertNewStatsUserMonthly.setRecommendations(monthRecommendations);
        insertNewStatsUserMonthly.setSharings(monthSharings);
        insertNewStatsUserMonthly.setSavings(monthSavings);
        insertNewStatsUserMonthly.setDownloads(monthDownloads);
        insertNewStatsUserMonthly.setReported(monthReported);
        insertNewStatsUserMonthly.setFollowings(monthFollowings);
        insertNewStatsUserMonthly.setUpdatedAt(time);
        statsUserMonthlyMapper.insert(insertNewStatsUserMonthly);
    }

    /**
     * 文献周统计取得
     *
     * @param publicationId
     * @return
     */
    public List<StatsPublicationWeekly> getPublicationWeekly(Long publicationId) {
        List<StatsPublicationWeekly> StatsPublicationWeeklyToList = new ArrayList<>();
        List<StatsPublicationWeekly> StatsPublicationWeeklyList = statsPublicationWeeklyMapper.getPublicationWeekly(publicationId);

        for (StatsPublicationWeekly statsPublicationWeeklyIndex : StatsPublicationWeeklyList) {
            StatsPublicationWeekly statsPublicationWeekly = new StatsPublicationWeekly();
            BeanUtils.copyProperties(statsPublicationWeeklyIndex, statsPublicationWeekly);
            StatsPublicationWeeklyToList.add(statsPublicationWeekly);
        }
        return StatsPublicationWeeklyToList;
    }

    /**
     * 旧作者文献周统计总数减少更新
     *
     * @param week
     * @param weekCitations
     * @param weekReads
     * @param weekRecommendations
     * @param weekSharings
     * @param weekSavings
     * @param weekDownloads
     * @param weekFollowings
     * @param weekReported
     * @param userId
     * @param time
     */
    public void updateOldStatsUserWeek(Long userId, Date week, Integer weekCitations, Integer weekReads, Integer weekRecommendations, Integer weekSharings, Integer weekSavings, Integer weekDownloads, Integer weekFollowings, Integer weekReported, Date time) {
        StatsUserWeekly oldStatsUserWeekly = new StatsUserWeekly();
        oldStatsUserWeekly.setUserId(userId);
        oldStatsUserWeekly.setWeek(week);
        oldStatsUserWeekly.setCitations(weekCitations);
        oldStatsUserWeekly.setReads(weekReads);
        oldStatsUserWeekly.setRecommendations(weekRecommendations);
        oldStatsUserWeekly.setSharings(weekSharings);
        oldStatsUserWeekly.setSavings(weekSavings);
        oldStatsUserWeekly.setDownloads(weekDownloads);
        oldStatsUserWeekly.setReported(weekReported);
        oldStatsUserWeekly.setFollowings(weekFollowings);
        oldStatsUserWeekly.setUpdatedAt(time);
        statsUserWeeklyMapper.updateOldStatsUserWeek(oldStatsUserWeekly);
    }

    /**
     * 新作者周文献统计总数增加更新
     *
     * @param week
     * @param weekCitations
     * @param weekReads
     * @param weekRecommendations
     * @param weekSharings
     * @param weekSavings
     * @param weekDownloads
     * @param weekFollowings
     * @param weekReported
     * @param userId
     * @param time
     */
    public int updateNewStatsUserWeek(Long userId, Date week, Integer weekCitations, Integer weekReads, Integer weekRecommendations, Integer weekSharings, Integer weekSavings, Integer weekDownloads, Integer weekFollowings, Integer weekReported, Date time) {
        StatsUserWeekly newStatsUserWeekly = new StatsUserWeekly();
        newStatsUserWeekly.setUserId(userId);
        newStatsUserWeekly.setWeek(week);
        newStatsUserWeekly.setCitations(weekCitations);
        newStatsUserWeekly.setReads(weekReads);
        newStatsUserWeekly.setRecommendations(weekRecommendations);
        newStatsUserWeekly.setSharings(weekSharings);
        newStatsUserWeekly.setSavings(weekSavings);
        newStatsUserWeekly.setDownloads(weekDownloads);
        newStatsUserWeekly.setReported(weekReported);
        newStatsUserWeekly.setFollowings(weekFollowings);
        newStatsUserWeekly.setUpdatedAt(time);
        int updateCount = statsUserWeeklyMapper.updateNewStatsUserWeek(newStatsUserWeekly);
        return updateCount;
    }

    /**
     * 新作者周文献统计总数增加新规追加
     *
     * @param week
     * @param weekCitations
     * @param weekReads
     * @param weekRecommendations
     * @param weekSharings
     * @param weekSavings
     * @param weekDownloads
     * @param weekFollowings
     * @param weekReported
     * @param userId
     * @param time
     */
    public void insertNewStatsUserWeek(Long userId, Date week, Integer weekCitations, Integer weekReads, Integer weekRecommendations, Integer weekSharings, Integer weekSavings, Integer weekDownloads, Integer weekFollowings, Integer weekReported, Date time) {
        StatsUserWeekly insertNewStatsUserWeekly = new StatsUserWeekly();
        insertNewStatsUserWeekly.setUserId(userId);
        insertNewStatsUserWeekly.setWeek(week);
        insertNewStatsUserWeekly.setPublications(0);
        insertNewStatsUserWeekly.setCitations(weekCitations);
        insertNewStatsUserWeekly.setReads(weekReads);
        insertNewStatsUserWeekly.setRecommendations(weekRecommendations);
        insertNewStatsUserWeekly.setSharings(weekSharings);
        insertNewStatsUserWeekly.setSavings(weekSavings);
        insertNewStatsUserWeekly.setDownloads(weekDownloads);
        insertNewStatsUserWeekly.setReported(weekReported);
        insertNewStatsUserWeekly.setFollowings(weekFollowings);
        insertNewStatsUserWeekly.setUpdatedAt(time);
        statsUserWeeklyMapper.insert(insertNewStatsUserWeekly);
    }

    /**
     * 文献(文献、专利、著作等)取得
     *
     * @param publicationId
     * @return
     */

    public Publication getPublication(Long publicationId) {
        Publication publication = new Publication();
        publication.setPublicationId(publicationId);
        return publicationMapper.selectByPrimaryKey(publication);
    }

    /**
     * 旧作者文献年度统计总数的作者发文数减少更新
     *
     * @param oldUserId
     * @param yearPublications
     * @param time
     */
    public void updateOldStatsUsersYearlyPublications(Long oldUserId, Date yearPublications, Date time) {
        StatsUserYearly oldStatsUserYearlyPublications = new StatsUserYearly();
        oldStatsUserYearlyPublications.setUserId(oldUserId);
        oldStatsUserYearlyPublications.setYear(yearPublications);
        oldStatsUserYearlyPublications.setUpdatedAt(time);
        statsUserYearlyMapper.updateOldStatsUsersYearlyPublications(oldStatsUserYearlyPublications);
    }

    /**
     * 新作者文献年度统计总数的作者发文数增加更新
     *
     * @param userId
     * @param yearPublications
     * @param time
     */
    public void updateNewStatsUserYearlyPublications(Long userId, Date yearPublications, Date time) {
        StatsUserYearly newStatsUserYearlyPublications = new StatsUserYearly();
        newStatsUserYearlyPublications.setUserId(userId);
        newStatsUserYearlyPublications.setYear(yearPublications);
        newStatsUserYearlyPublications.setUpdatedAt(time);
        statsUserYearlyMapper.updateNewStatsUserYearlyPublications(newStatsUserYearlyPublications);
    }

    /**
     * 旧作者文献月度统计总数的作者发文数减少更新
     *
     * @param oldUserId
     * @param monthPublications
     * @param time
     */
    public void updateOldStatsUserMonthPublications(Long oldUserId, Date monthPublications, Date time) {
        StatsUserMonthly oldStatsUserMonthlyPublications = new StatsUserMonthly();
        oldStatsUserMonthlyPublications.setUserId(oldUserId);
        oldStatsUserMonthlyPublications.setMonth(monthPublications);
        oldStatsUserMonthlyPublications.setUpdatedAt(time);
        statsUserMonthlyMapper.updateOldStatsUserMonthPublications(oldStatsUserMonthlyPublications);
    }

    /**
     * 新作者文献月度统计总数的作者发文数增加更新
     *
     * @param userId
     * @param monthPublications
     * @param time
     */
    public void updateNewStatsUserMonthPublications(Long userId, Date monthPublications, Date time) {
        StatsUserMonthly newStatsUserMonthlyPublications = new StatsUserMonthly();
        newStatsUserMonthlyPublications.setUserId(userId);
        newStatsUserMonthlyPublications.setMonth(monthPublications);
        newStatsUserMonthlyPublications.setUpdatedAt(time);
        statsUserMonthlyMapper.updateNewStatsUserMonthPublications(newStatsUserMonthlyPublications);
    }

    /**
     * 旧作者文献周统计总数的作者发文数减少更新
     *
     * @param oldUserId
     * @param weekPublications
     * @param time
     */
    public void updateOldStatsUserWeekPublications(Long oldUserId, Date weekPublications, Date time) {
        StatsUserWeekly oldStatsUserWeeklyPublications = new StatsUserWeekly();
        oldStatsUserWeeklyPublications.setUserId(oldUserId);
        oldStatsUserWeeklyPublications.setWeek(weekPublications);
        oldStatsUserWeeklyPublications.setUpdatedAt(time);
        statsUserWeeklyMapper.updateOldStatsUserWeekPublications(oldStatsUserWeeklyPublications);
    }

    /**
     * 新作者文献周统计总数的作者发文数增加更新
     *
     * @param userId
     * @param weekPublications
     * @param time
     */
    public void updateNewStatsUserWeekPublications(Long userId, Date weekPublications, Date time) {
        StatsUserWeekly newStatsUserWeeklyPublications = new StatsUserWeekly();
        newStatsUserWeeklyPublications.setUserId(userId);
        newStatsUserWeeklyPublications.setWeek(weekPublications);
        newStatsUserWeeklyPublications.setUpdatedAt(time);
        statsUserWeeklyMapper.updateNewStatsUserWeekPublications(newStatsUserWeeklyPublications);
    }

    /**
     * 当前文献的共同作者取得
     *
     * @param publicationId
     * @param oldUserId
     * @param userId
     * @return
     */
    public List<PublicationUserInstitution> getSameWriterId(Long publicationId, Long oldUserId, Long userId) {
        List<PublicationUserInstitution> PublicationUserInstitutionToList = new ArrayList<>();
        List<PublicationUserInstitution> PublicationUserInstitutionList = publicationUserInstitutionMapper.getSameWriterId(publicationId, oldUserId, userId);

        for (PublicationUserInstitution publicationUserInstitutionIndex : PublicationUserInstitutionList) {
            PublicationUserInstitution publicationUserInstitution = new PublicationUserInstitution();
            BeanUtils.copyProperties(publicationUserInstitutionIndex, publicationUserInstitution);
            PublicationUserInstitutionToList.add(publicationUserInstitution);
        }
        return PublicationUserInstitutionToList;
    }

    /**
     * 原作者共同发表的文献数减1更新
     *
     * @param oldUserId
     * @param secondUserId
     */
    public void updateOldSameUser(Long oldUserId, Long secondUserId) {
        UserCoauthor oldUserCoauthor = new UserCoauthor();
        oldUserCoauthor.setUserId1(oldUserId);
        oldUserCoauthor.setUserId2(secondUserId);

        userCoauthorMapper.updateOldSameUser(oldUserCoauthor);
    }

    /**
     * 新作者共同发表的文献数加1更新
     *
     * @param userId
     * @param secondUserId
     */
    public int updateNewSameUser(Long userId, Long secondUserId) {
        UserCoauthor newUserCoauthor = new UserCoauthor();
        newUserCoauthor.setUserId1(userId);
        newUserCoauthor.setUserId2(secondUserId);

        int updateCount = userCoauthorMapper.updateNewSameUser(newUserCoauthor);
        return updateCount;
    }

    /**
     * 获取本月阅读次数最多文章
     *
     * @param userId
     * @param month
     * @return
     */
    public PageInfo<Long> getMaxReadByUserId(Long userId, Date month, int index, int rows) {
        int pageNum = index;
        int pageSize = rows;
        PageHelper.startPage(pageNum, pageSize);
        List<Long> pIds = publicationUserInstitutionMapper.getMaxReadByUserId(userId, month);
        return new PageInfo<>(pIds);
    }

    /**
     * 获取用户自己的文章
     *
     * @param userId
     * @param index
     * @param rows
     * @return
     */
    public PageInfo<Long> getMyPublicationList(Long userId, int index, int rows) {
        int pageNum = index;
        int pageSize = rows;
        PageHelper.startPage(pageNum, pageSize);
        List<Long> pIds = publicationUserInstitutionMapper.getMyPublicationList(userId);
        return new PageInfo<>(pIds);
    }

    /**
     * 当前login用户共同作者表新规追加
     *
     * @param userId
     * @param secondUserId
     */
    public void insertNewSameUserId1(Long userId, Long secondUserId) {
        UserCoauthor insertNewUserCoauthor = new UserCoauthor();
        insertNewUserCoauthor.setUserId1(userId);
        insertNewUserCoauthor.setUserId2(secondUserId);
        insertNewUserCoauthor.setCoauthoredPublications(1);
        userCoauthorMapper.insert(insertNewUserCoauthor);
    }

    /**
     * 当前login用户共同作者表新规追加
     *
     * @param userId
     * @param secondUserId
     */
    public void insertNewSameUserId2(Long userId, Long secondUserId) {
        UserCoauthor insertNewUserCoauthor = new UserCoauthor();
        insertNewUserCoauthor.setUserId1(secondUserId);
        insertNewUserCoauthor.setUserId2(userId);
        insertNewUserCoauthor.setCoauthoredPublications(1);
        userCoauthorMapper.insert(insertNewUserCoauthor);
    }

    /**
     * 单条共同作者的存在查询
     *
     * @param userId
     * @param secondUserId
     * @return
     */
    public UserCoauthor getUserCoauthor(Long userId, long secondUserId) {
        UserCoauthor userCoauthor = new UserCoauthor();
        userCoauthor.setUserId1(userId);
        userCoauthor.setUserId2(secondUserId);
        return userCoauthorMapper.selectByPrimaryKey(userCoauthor);
    }

    /**
     * 文献研究领域表查询
     *
     * @param publicationId
     * @return
     */
    public List<PublicationField> getPublicationField(Long publicationId) {
        List<PublicationField> PublicationFieldToList = new ArrayList<>();
        List<PublicationField> PublicationFieldList = publicationFieldMapper.getPublicationField(publicationId);

        for (PublicationField publicationFieldIndex : PublicationFieldList) {
            PublicationField publicationField = new PublicationField();
            BeanUtils.copyProperties(publicationFieldIndex, publicationField);
            PublicationFieldToList.add(publicationField);
        }
        return PublicationFieldToList;
    }

    /**
     * 原用户用户研究领域表的文献数减1更新
     *
     * @param userId
     * @param fieldId
     * @param time
     */
    public void updateOldUserField(Long userId, Long fieldId, Date time) {
        UserField oldUserField = new UserField();
        oldUserField.setUserId(userId);
        oldUserField.setFieldId(fieldId);
        oldUserField.setUpdatedAt(time);
        userFieldMapper.updateOldUserField(oldUserField);
    }

    /**
     * 新用户用户研究领域表的文献数加1更新
     *
     * @param userId
     * @param fieldId
     * @param time
     */
    public int updateNewUserField(Long userId, Long fieldId, Date time) {
        UserField newUserField = new UserField();
        newUserField.setUserId(userId);
        newUserField.setFieldId(fieldId);
        newUserField.setUpdatedAt(time);
        int updateCount = userFieldMapper.updateNewUserField(newUserField);
        return updateCount;
    }

    /**
     * 新用户用户研究领域表新规追加
     *
     * @param userId
     * @param fieldId
     * @param time
     */
    public void insertNewUserField(Long userId, Long fieldId, Date time) {
        UserField insertNewUserField = new UserField();
        insertNewUserField.setUserId(userId);
        insertNewUserField.setFieldId(fieldId);
        insertNewUserField.setPublications(1);
        insertNewUserField.setCreatedAt(time);
        insertNewUserField.setUpdatedAt(time);
        userFieldMapper.insert(insertNewUserField);
    }

    /**
     * 检索学科和子学科表
     *
     * @param fieldId
     * @return
     */
    public List<Discipline> getDiscipline(Long fieldId) {
        List<Discipline> DisciplineToList = new ArrayList<>();
        List<Discipline> DisciplineList = disciplineMapper.getDiscipline(fieldId);

        for (Discipline disciplineIndex : DisciplineList) {
            Discipline discipline = new Discipline();
            BeanUtils.copyProperties(disciplineIndex, discipline);
            DisciplineToList.add(discipline);
        }
        return DisciplineToList;
    }

    /**
     * 原用户用户学科与专业表的文献数-1（更新条件用户ID是原用户ID，研究领域是查出来的研究领域）
     *
     * @param userId
     * @param fieldId
     * @param time
     */
    public void updateOldUserDiscipline(Long userId, Long fieldId, Date time) {
        UserDiscipline oldUserDiscipline = new UserDiscipline();
        oldUserDiscipline.setUserId(userId);
        oldUserDiscipline.setFieldId(fieldId);
        oldUserDiscipline.setUpdatedAt(time);
        userDisciplineMapper.updateOldUserDiscipline(oldUserDiscipline);
    }

    /**
     * 当前login用户，用户学科与专业表的文献数+1
     *
     * @param userId
     * @param fieldId
     * @param time
     */
    public int updateNewUserDiscipline(Long userId, Long fieldId, Date time) {
        UserDiscipline newUserDiscipline = new UserDiscipline();
        newUserDiscipline.setUserId(userId);
        newUserDiscipline.setFieldId(fieldId);
        newUserDiscipline.setUpdatedAt(time);
        int updateDisciplineCount = userDisciplineMapper.updateNewUserDiscipline(newUserDiscipline);
        return updateDisciplineCount;
    }

    /**
     * 当前login用户用户学科与专业表新规追加
     *
     * @param userId
     * @param fieldId
     * @param time
     */
    public void insertNewUserDiscipline(Long userId, Long fieldId, Date time) {
        UserDiscipline insertNewUserDiscipline = new UserDiscipline();
        insertNewUserDiscipline.setUserId(userId);
        insertNewUserDiscipline.setFieldId(fieldId);
        insertNewUserDiscipline.setPublications(1);
        insertNewUserDiscipline.setCreatedAt(time);
        insertNewUserDiscipline.setUpdatedAt(time);
        userDisciplineMapper.insert(insertNewUserDiscipline);
    }

    /**
     * 更新文献，作者，机构名关系表的作者ID，把原作者的user_id，更新成login的user_id
     *
     * @param oldUserId
     * @param userId
     * @param publicationId
     */
    public void updateNewPublicationUserInsetitution(Long oldUserId, Long userId, Long publicationId) {
        PublicationUserInstitution publicationUserInstitution = new PublicationUserInstitution();
        publicationUserInstitution.setPublicationId(publicationId);
        publicationUserInstitution.setUserId(oldUserId);
        publicationUserInstitution.setInstitutionId(userId);
        publicationUserInstitutionMapper.updateNewPublicationUserInsetitution(publicationUserInstitution);
    }

    /**
     * 获取原始用户信息
     *
     * @param publicationId
     * @param index
     * @param rows
     * @return
     */
    public PageInfo<SuggestedUserModel> getOldInformation(Long publicationId, int index, int rows) {
        int pageNum = index;
        int pageSize = rows;
        PageHelper.startPage(pageNum, pageSize);
        List<SuggestedUserModel> suggestedUserModels = new ArrayList<SuggestedUserModel>();
        List<User> userList = userMapper.getDisplayNames(publicationId);
        PageInfo<User> UserPageInfo = new PageInfo<User>(userList);
        for (User user : UserPageInfo.getList()) {
            SuggestedUserModel suggestedUserModel = new SuggestedUserModel();
            BeanUtils.copyProperties(user, suggestedUserModel);
            suggestedUserModels.add(suggestedUserModel);
        }

        PageInfo<SuggestedUserModel> suggestedUserModelPageInfo = new PageInfo<SuggestedUserModel>(suggestedUserModels);
        suggestedUserModelPageInfo.setPageNum(UserPageInfo.getPageNum());
        suggestedUserModelPageInfo.setPageSize(UserPageInfo.getPageSize());
        suggestedUserModelPageInfo.setStartRow(UserPageInfo.getStartRow());
        suggestedUserModelPageInfo.setEndRow(UserPageInfo.getEndRow());
        suggestedUserModelPageInfo.setPages(UserPageInfo.getPages());
        suggestedUserModelPageInfo.setPrePage(UserPageInfo.getPrePage());
        suggestedUserModelPageInfo.setNextPage(UserPageInfo.getNextPage());
        suggestedUserModelPageInfo.setIsFirstPage(UserPageInfo.isIsFirstPage());
        suggestedUserModelPageInfo.setIsLastPage(UserPageInfo.isIsLastPage());
        suggestedUserModelPageInfo.setHasPreviousPage(UserPageInfo.isHasPreviousPage());
        suggestedUserModelPageInfo.setHasNextPage(UserPageInfo.isHasNextPage());
        suggestedUserModelPageInfo.setNavigatePages(UserPageInfo.getNavigatePages());
        suggestedUserModelPageInfo.setNavigatepageNums(UserPageInfo.getNavigatepageNums());
        suggestedUserModelPageInfo.setNavigateFirstPage(UserPageInfo.getNavigateFirstPage());
        suggestedUserModelPageInfo.setNavigateLastPage(UserPageInfo.getNavigateLastPage());
        suggestedUserModelPageInfo.setTotal(UserPageInfo.getTotal());

        return suggestedUserModelPageInfo;
    }

    /**
     * 用户名取得
     *
     * @param userId
     * @return
     */

    public User getUserDisplayName(Long userId) {
        User user = new User();
        user.setUserId(userId);
        return userMapper.selectByPrimaryKey(user);
    }

    /**
     * 上传文献为匹配文献时为新文献匹配作者关系
     * @param publicationId
     * @param newPublicationId
     * @return
     */

    public int setPublicationUserInstotution(Long publicationId, Long newPublicationId) {

        // 获取匹配文献相关作者信息
        PublicationUserInstitutionExample publicationUserInstitutionExample = new PublicationUserInstitutionExample();
        PublicationUserInstitutionExample.Criteria criteria =  publicationUserInstitutionExample.createCriteria();
        criteria.andPublicationIdEqualTo(publicationId);
        List<PublicationUserInstitution> publicationUserInstitutionList = publicationUserInstitutionMapper.selectByExample(publicationUserInstitutionExample);
        // 新文献相关作者list
        List<PublicationUserInstitution> newPublicationUserInstitutionList = new ArrayList<>();
        for (PublicationUserInstitution publicationUserInstitution : publicationUserInstitutionList) {
            PublicationUserInstitution newPublicationUserInstitution = new PublicationUserInstitution();
            BeanUtils.copyProperties(publicationUserInstitution, newPublicationUserInstitution);
            newPublicationUserInstitution.setPublicationId(newPublicationId);
            newPublicationUserInstitutionList.add(newPublicationUserInstitution);
        }
        int resCount = 0;
        if (newPublicationUserInstitutionList.size() > 0) {
            resCount = publicationUserInstitutionMapper.insertPublicationUserInsetitution(newPublicationUserInstitutionList);
        }


        return resCount;
    }
}
