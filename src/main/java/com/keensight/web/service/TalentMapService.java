package com.keensight.web.service;

import com.keensight.web.db.entity.Code;
import com.keensight.web.db.entity.Field;
import com.keensight.web.db.entity.FieldExample;
import com.keensight.web.db.mapper.CodeMapper;
import com.keensight.web.db.mapper.FieldMapper;
import com.keensight.web.db.mapper.UserMapper;
import com.keensight.web.model.UserModel;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class TalentMapService {
    @Resource
    private UserMapper userMapper;
    @Resource
    private FieldMapper fieldMapper;
    @Resource
    private CodeMapper codeMapper;
    // 国家code
    private static String groupCd = "country_iso_code";

    /**
     * 人才地图maker信息获取方法
     * @param nLng
     * @param nLat
     * @param sLng
     * @param sLat
     * @param code
     * @param isethniccn
     * @param fieldId
     * @return
     */
    public List<UserModel> getUserMaker(Double nLng, Double nLat, Double sLng, Double sLat, String code, String isethniccn, Long fieldId) {
        List<UserModel> UserMakerList = userMapper.getUserMaker(nLng,nLat,sLng,sLat,code,isethniccn,fieldId);
        return UserMakerList;
    }

    /**
     * 获取国家名称列表
     * @return
     */
    public List<Code> getCountryNameList() {
        List<Code> countryNameList = codeMapper.getCountryNameList(groupCd);
        if (countryNameList == null) {
            countryNameList = new ArrayList<>();
        }
        return countryNameList;
    }

    /**
     * 研究领域自动补全
     * @param displayName
     * @return
     */
    public List<Field> autocompeleteFieldName(String displayName) {
        FieldExample fieldExample = new FieldExample();
        FieldExample.Criteria criteria = fieldExample.createCriteria();
        criteria.andDisplayNameLike(displayName+'%');
        fieldExample.setOrderByClause("display_name asc limit 10");
        List<Field> fieldList = fieldMapper.selectByExample(fieldExample);
        return fieldList;
    }
}