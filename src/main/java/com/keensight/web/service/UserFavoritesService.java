package com.keensight.web.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.keensight.web.db.entity.*;
import com.keensight.web.db.mapper.*;
import com.keensight.web.model.UserLibraryFolderModel;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.*;

@Service
public class UserFavoritesService {

    @Resource
    private UserLibraryMapper userLibraryMapper;
    @Resource
    private UserLibraryFolderMapper userLibraryFolderMapper;
    @Resource
    private UserLibraryFolderSearchMapper userLibraryFolderSearchMapper;
    @Resource
    private UserLibraryLabelMapper userLibraryLabelMapper;
    @Resource
    private UserLibraryLabelPublicationMapper userLibraryLabelPublicationMapper;
    @Resource
    private UserLibraryFolderPublicationMapper userLibraryFolderPublicationMapper;

    // 当前系统时间
    Date systemTime = new Date();

    /**
     * 用户收藏文献Id集合取得
     * @param userId
     * @param sortColumns
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageInfo<Long> getUserFavoritesIdList(Long userId,String sortColumns, Integer pageNum, Integer pageSize, String publicationGrades, String labelIds, Long folderId) {
        PageHelper.startPage(pageNum, pageSize, true, null, true);
        // 星级List
        List<String> publicationGradeList = new ArrayList<>();
        if (!StringUtils.isEmpty(publicationGrades)) {
            String[] publicationGradeArry = publicationGrades.split(",");
            for (String publicationGradeIdsS : publicationGradeArry) {
                publicationGradeList.add(publicationGradeIdsS);
            }
        }
        // 标签List
        List<Long> labelIdList = new ArrayList<>();
        if (!StringUtils.isEmpty(labelIds)) {
            String[] labelIdArry = labelIds.split(",");
            for (String labelIdIdsS : labelIdArry) {
                labelIdList.add(Long.valueOf(labelIdIdsS));
            }
        }
        // 检索用户收藏一览
        List<Long> userLibraryIdList = userLibraryMapper.getUserFavoritesList(userId, sortColumns, publicationGradeList, labelIdList, folderId);
        return new PageInfo<Long>(userLibraryIdList);
    }

    /**
     * 个人收藏文件夹取得
     * @param userId
     * @return
     */
    public Map<Long, UserLibraryFolderModel> getUserLibraryFolderList(Integer userId) {
        // 个人收藏文件夹取得
        List<UserLibraryFolder> userLibraryFolder = userLibraryFolderMapper.getUserLibraryFolderList(userId);
        if(userLibraryFolder == null || userLibraryFolder.size() == 0) {
            // 默认文件夹信息取得
            List<UserLibraryFolder> folderDefaultList = userLibraryFolderMapper.getUserLibraryFolderList(-1);
            for(UserLibraryFolder folderDefaultData : folderDefaultList) {
                // 用户Id赋值
                folderDefaultData.setUserId(Long.valueOf(userId));
                // 默认文件夹信息插入
                userLibraryFolderMapper.insertSelective(folderDefaultData);
            }

            userLibraryFolder = userLibraryFolderMapper.getUserLibraryFolderList(userId);
        }

        // 创建存放父节点的MAP
        Map<Long, UserLibraryFolderModel> userLibraryFolderMap = new HashMap<Long, UserLibraryFolderModel>();
        userLibraryFolderMap.put(Long.valueOf(0), new UserLibraryFolderModel());
        // 每个层级文件夹循环
        for (UserLibraryFolder level : userLibraryFolder) {
            UserLibraryFolderModel levelModel = userLibraryFolderMap.get(level.getFolderId());
            if (levelModel == null) {
                levelModel = new UserLibraryFolderModel();
            }

            // 把查出的菜单 copy到MODEL中
            BeanUtils.copyProperties(level, levelModel);
            userLibraryFolderMap.put(level.getFolderId(), levelModel);

            // 查找每个层级的父节点
            UserLibraryFolderModel parent = userLibraryFolderMap.get(level.getParentFolderId());
            if (parent == null) {
                parent = new UserLibraryFolderModel();
                userLibraryFolderMap.put(level.getParentFolderId(), parent);
            }
            List<UserLibraryFolderModel> childFolderList = parent.getChildFolder();
            if (childFolderList == null) {
                childFolderList = new ArrayList<UserLibraryFolderModel>();
                parent.setChildFolder(childFolderList);
            }
            // 把子节点的model放到父节点的childFolderModel中
            childFolderList.add(levelModel);

        }
        return userLibraryFolderMap;
    }

    /**
     * 新增个人收藏文件夹
     * @param userId
     * @param folderId
     * @param folderName
     * @return
     */
    public int insertUserLibraryFolder(Long userId, Long folderId, String folderName) {
        UserLibraryFolder folderRecord = new UserLibraryFolder();
        // 用户Id
        folderRecord.setUserId(userId);
        // 父节点key
        folderRecord.setParentFolderId(folderId);
        // 文件夹名称
        folderRecord.setFolderName(folderName);
        // 是否为默认文件夹
        folderRecord.setIsSysFolder("1");
        // 查找文件名是否已存在
        UserLibraryFolder folderRecordRepeat = userLibraryFolderMapper.selectFolderByUserIdAndName(userId, folderId, folderName);
        // 新增文件夹成功flg
        int insertFlg = 0;
        if(folderRecordRepeat == null) {
            // 新增文件夹
            insertFlg = userLibraryFolderMapper.insertUserLibraryFolder(folderRecord);

            // 更新search表
            updateUserLibrarySearch(userId);
        }
        return insertFlg;
    }

    /**
     * 修改个人收藏文件夹
     * @param folderId
     * @param folderName
     * @return
     */
    public int updateUserLibraryFolder(Long folderId, String folderName, Long userId, Long parentFolderId) {
        UserLibraryFolder folderRecord = new UserLibraryFolder();
        // 根据文件夹id查找文件夹信息
        folderRecord = userLibraryFolderMapper.getFolderByFolderId(folderId);
        // 文件夹名称
        folderRecord.setFolderName(folderName);
        // 查找文件名是否已存在
        UserLibraryFolder folderRecordRepeat = userLibraryFolderMapper.selectFolderByUserIdAndName(userId, parentFolderId, folderName);
        // 更新文件夹成功flg
        int updateFlg = 0;
        if(folderRecordRepeat == null) {
            // 更新文件夹名称
            updateFlg = userLibraryFolderMapper.updateByPrimaryKeySelective(folderRecord);

            // 更新search表
            updateUserLibrarySearch(userId);
        }
        return updateFlg;
    }

    /**
     * 删除个人收藏文件夹
     * @param folderId
     * @param userId
     * @return
     */
    public int deleteUserLibraryFolder(Long folderId, Long userId) {
        // 数据登录
        Map<Long, UserLibraryFolderModel> userLibraryFolderMap = getUserLibraryFolderList(Integer.valueOf(userId + ""));
        List<Long> folderIds = new ArrayList<Long>();
        for(UserLibraryFolderModel userLibraryFolderModel : userLibraryFolderMap.values()) {
            // 查找当前选中文件夹及子文件夹
            if (folderId != null) {
                if(folderId.equals(userLibraryFolderModel.getFolderId())) {
                    List<UserLibraryFolderModel> allSublist = this.getUserChildFolder(userLibraryFolderModel);
                    for (UserLibraryFolderModel allSubUserLibraryFolderModel : allSublist) {
                        // 获取当前选中文件夹及子文件夹的folderId
                        folderIds.add(allSubUserLibraryFolderModel.getFolderId());
                    }
                }
            }

        }
        // 删除文件夹成功flg
        int deleteFlg = userLibraryFolderMapper.delUserLibraryFolder(folderId, userId);
        // 更新search表
        updateUserLibrarySearch(userId);
        // 循环当前选中文件夹及子文件夹的folderId
        for(Long folderIdOne : folderIds) {
            // 更新文件夹与文献关系表
            userLibraryFolderPublicationMapper.delUserFolderPublication(folderIdOne);
        }
        return deleteFlg;
    }

    /**
     * 更新search表
     * @param userId
     * @return
     */
    private void updateUserLibrarySearch(Long userId) {
        // search表当前user的数据全部删除
        UserLibraryFolderSearchExample userFolderSearchExample = new UserLibraryFolderSearchExample();
        UserLibraryFolderSearchExample.Criteria criteria = userFolderSearchExample.createCriteria();
        criteria.andUserIdEqualTo(userId);
        userLibraryFolderSearchMapper.deleteByExample(userFolderSearchExample);

        // 数据登录
        Map<Long, UserLibraryFolderModel> userLibraryFolderMap = getUserLibraryFolderList(Integer.valueOf(userId + ""));

        List<UserLibraryFolderSearch> userLibraryFolderSearchList = new ArrayList<UserLibraryFolderSearch>();
        for (UserLibraryFolderModel userLibraryFolderModel : userLibraryFolderMap.values()) {
            if (userLibraryFolderModel.getFolderId() == null) {
                continue;
            }
            // 获取所有子文件夹
            List<UserLibraryFolderModel> allSublist = this.getUserChildFolder(userLibraryFolderModel);
            for (UserLibraryFolderModel allSubUserLibraryFolderModel : allSublist) {
                UserLibraryFolderSearch userLibraryFolderSearch = new UserLibraryFolderSearch();
                userLibraryFolderSearch.setUserId(userId);
                userLibraryFolderSearch.setFolderId(userLibraryFolderModel.getFolderId());
                userLibraryFolderSearch.setAllSubFolderId(allSubUserLibraryFolderModel.getFolderId());

                userLibraryFolderSearchList.add(userLibraryFolderSearch);
            }
        }

        if (userLibraryFolderSearchList.size() > 0) {
            userLibraryFolderSearchMapper.insertAllSubFolder(userLibraryFolderSearchList);
        }

    }

    /**
     * 获取所有子文件夹
     * @param userLibraryFolderModel
     * @return
     */
    private List<UserLibraryFolderModel> getUserChildFolder(UserLibraryFolderModel userLibraryFolderModel) {
        List<UserLibraryFolderModel> list = new ArrayList<UserLibraryFolderModel>();
        list.add(userLibraryFolderModel);

        if (userLibraryFolderModel.getChildFolder() != null) {
            for (UserLibraryFolderModel childUserLibraryFolderModel : userLibraryFolderModel.getChildFolder()) {
                list.addAll(this.getUserChildFolder(childUserLibraryFolderModel));
            }
        }

        return list;
    }

    /**
     * 修改星级
     * @param userId
     * @param publicationId
     * @param publicationGrade
     * @return
     */
    public int updateUserLibraryGrade(Long userId, Long publicationId, String publicationGrade) {
        UserLibraryExample userLibraryExample = new UserLibraryExample();
        UserLibraryExample.Criteria criteria = userLibraryExample.createCriteria();
        criteria.andUserIdEqualTo(userId);
        criteria.andPublicationIdEqualTo(publicationId);

        UserLibrary record = new UserLibrary();
        record.setPublicationGrade(publicationGrade);
        // 更新文件夹名称
        int updateFlg = userLibraryMapper.updateByExampleSelective(record, userLibraryExample);
        return updateFlg;
    }

    /**
     * 新增文献标签
     * @param userId
     * @param publicationId
     * @param labelName
     * @return
     */
    public UserLibraryLabel addLabel(Long userId, Long publicationId, String labelName) {
        UserLibraryLabel userLibraryLabel = new UserLibraryLabel();
        userLibraryLabel.setUserId(userId);
        userLibraryLabel.setLabelName(labelName);
        userLibraryLabel.setDeleted(false);
        userLibraryLabel.setCreatedAt(systemTime);
        // 查找文件名是否已存在
        UserLibraryLabel userLibraryLabelRepeat = userLibraryLabelMapper.selectLabelByUserIdAndName(userId, labelName);
        // 更新文件夹成功flg
        int result = 0;
        if(userLibraryLabelRepeat == null) {
            result = userLibraryLabelMapper.insert(userLibraryLabel);
            UserLibraryLabelPublication userLibraryLabelPublication = new UserLibraryLabelPublication();
            userLibraryLabelPublication.setLabelId(userLibraryLabel.getLabelId());
            userLibraryLabelPublication.setPublicationId(publicationId);
            result += userLibraryLabelPublicationMapper.insert(userLibraryLabelPublication);
        } else {
            UserLibraryLabelPublication userLibraryLabelPublication = new UserLibraryLabelPublication();
            userLibraryLabelPublication.setLabelId(userLibraryLabelRepeat.getLabelId());
            userLibraryLabelPublication.setPublicationId(publicationId);
            result = userLibraryLabelPublicationMapper.insert(userLibraryLabelPublication);
        }
        if(result > 0){
            return userLibraryLabel;
        } else {
            return null;
        }
    }

    /**
     * 删除文献标签
     * @param labelId
     * @param publicationId
     * @return
     */
    public int delLabel(Long labelId, Long publicationId) {
        UserLibraryLabelPublicationExample userLibraryLabelPublicationExample = new UserLibraryLabelPublicationExample();
        UserLibraryLabelPublicationExample.Criteria criteria = userLibraryLabelPublicationExample.createCriteria();
        criteria.andLabelIdEqualTo(labelId);
        criteria.andPublicationIdEqualTo(publicationId);
        int result = userLibraryLabelPublicationMapper.deleteByExample(userLibraryLabelPublicationExample);
        return result;
    }

    /**
     * 标签自动补全
     * @param userId
     * @param labelName
     * @return
     */
    public List<UserLibraryLabel> autocompeleteLabelName(Long userId, String labelName) {
        UserLibraryLabelExample userLibraryLabelExample = new UserLibraryLabelExample();
        UserLibraryLabelExample.Criteria criteria = userLibraryLabelExample.createCriteria();
        criteria.andUserIdEqualTo(userId);
        criteria.andLabelNameILike(labelName+'%');
        userLibraryLabelExample.setOrderByClause("label_name asc");
        List<UserLibraryLabel> labelList = userLibraryLabelMapper.selectByExample(userLibraryLabelExample);
        return labelList;
    }
}
