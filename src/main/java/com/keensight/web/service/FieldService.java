package com.keensight.web.service;

import com.keensight.web.db.entity.Field;
import com.keensight.web.db.entity.UserFieldFollowing;
import com.keensight.web.db.mapper.FieldMapper;
import com.keensight.web.db.mapper.UserFieldFollowingMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 研究领域Service
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/17] 新建
 */
@Service
public class FieldService {

    @Resource
    private FieldMapper fieldMapper;
    @Resource
    private UserFieldFollowingMapper userFieldFollowingMapper;

    /**
     * 根据研究领域ID获取研究领域和相关研究领域信息
     *
     * @param fieldId 研究领域ID
     * @param userId  用户ID
     * @return
     */
    public Field getFieldAndRelatedFieldListByFieldId(Long fieldId, Long userId) {
        return fieldMapper.getFieldAndRelatedFieldListByFieldId(fieldId, userId);
    }

    /**
     * 根据研究领域ID集合检索研究领域信息
     *
     * @param fieldIdList 研究领域ID集合
     * @return
     */
    public List<Field> getFieldInfoByFieldIdList(List<Long> fieldIdList) {
        List<Field> fieldList = fieldMapper.getFieldInfoByFieldIdList(fieldIdList);
        if (fieldList == null) {
            fieldList = new ArrayList<>();
        }
        return fieldList;
    }

    /**
     * 根据研究领域ID更新用户关注的研究方向
     *
     * @param userId          用户ID
     * @param fieldId         研究领域ID
     * @param followingBtnFlg 关注按钮区分「1:关注」/「2:取消关注」
     * @return
     */
    public int updUserFieldFollowingByFieldId(Long userId, Long fieldId, String followingBtnFlg) {
        Date sysTime = new Date();
        UserFieldFollowing userFieldFollowing = new UserFieldFollowing();
        int updateNum;
        // 点击关注时
        if ("1".equals(followingBtnFlg)) {
            // 判断是否已关注该研究方向
            int count = userFieldFollowingMapper.getCount(userId, fieldId);
            if (count == 0) {
                // 追加用户关注的研究方向
                userFieldFollowing.setUserId(userId);
                userFieldFollowing.setFieldId(fieldId);
                userFieldFollowing.setDeleted(false);
                userFieldFollowing.setCreatedAt(sysTime);
                userFieldFollowing.setUpdatedAt(sysTime);
                updateNum = userFieldFollowingMapper.insert(userFieldFollowing);
            } else {
                // 更新记录更新时间
                userFieldFollowing.setUserId(userId);
                userFieldFollowing.setFieldId(fieldId);
                userFieldFollowing.setUpdatedAt(sysTime);
                updateNum = userFieldFollowingMapper.updateStatsByPrimaryKey(userFieldFollowing);
            }
        } else {
            // 取消关注时
            userFieldFollowing.setUserId(userId);
            userFieldFollowing.setFieldId(fieldId);
            userFieldFollowing.setDeleted(true);
            userFieldFollowing.setUpdatedAt(sysTime);
            updateNum = userFieldFollowingMapper.updateStatsByPrimaryKey(userFieldFollowing);
        }
        return updateNum;
    }
}
