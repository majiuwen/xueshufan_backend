package com.keensight.web.service;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.keensight.web.db.entity.SuggestedPublicationToClaim;
import com.keensight.web.db.mapper.SuggestedPublicationToClaimMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SuggestedPublicationToClaimService {

    @Resource
    private SuggestedPublicationToClaimMapper suggestedPublicationToClaimMapper;

    /**
     * 用户认领文章
     *
     * @param userId
     * @param index
     * @param rows
     * @return
     */

    public PageInfo<SuggestedPublicationToClaim> getSuggestedPublicationToClaimByPublicationIds(Long userId, int index, int rows) {

        int pageNum = index;
        int pageSize = rows;
        PageHelper.startPage(pageNum, pageSize);
        List<SuggestedPublicationToClaim> suggestedPublicationToClaim = suggestedPublicationToClaimMapper.getSuggestedPublicationToClaimByPublicationIds(userId);
        return new PageInfo<SuggestedPublicationToClaim>(suggestedPublicationToClaim);
    }

    /**
     * 用户操作
     *
     * @param userId
     * @param publicationId
     * @param status
     */
    public void updateSuggestedPublicationToClaim(Long userId, Long publicationId,String status) {
        upSuggestedPublicationToClaim(userId, publicationId,status);
    }

    /**
     * 用户推荐的文献表更新
     *
     * @param userId
     * @param publicationId
     * @param status
     */
    private void upSuggestedPublicationToClaim(Long userId, Long publicationId,String status) {
        SuggestedPublicationToClaim suggestedPublicationToClaim = new SuggestedPublicationToClaim();
        suggestedPublicationToClaim.setPublicationId(publicationId);
        suggestedPublicationToClaim.setUserId(userId);
        suggestedPublicationToClaim.setStatus(status);
        suggestedPublicationToClaimMapper.upSuggestedPublicationToClaim(suggestedPublicationToClaim);
    }
}
