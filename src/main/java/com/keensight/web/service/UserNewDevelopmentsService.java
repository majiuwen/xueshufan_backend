package com.keensight.web.service;

import cn.newtouch.dl.turbocloud.helper.FileHelper;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.keensight.web.constants.enums.UploadFolder;
import com.keensight.web.controller.UserNewDevelopmentsController;
import com.keensight.web.db.entity.UserActionPublication;
import com.keensight.web.db.entity.UserNews;
import com.keensight.web.db.entity.UserNewsFiles;
import com.keensight.web.db.entity.UserNewsLike;
import com.keensight.web.db.mapper.*;
import com.keensight.web.helper.FileServerHelper;
import com.keensight.web.model.NewsFileModel;
import com.keensight.web.model.PublicationModel;
import com.keensight.web.model.UserNewsCommentModel;
import com.keensight.web.model.UserNewsModel;
import com.keensight.web.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.*;

@Service
public class UserNewDevelopmentsService {
    @Autowired
    private FileHelper fileHelper;
    @Resource
    private UserNewsMapper userNewsMapper;
    @Resource
    private UserNewsFilesMapper userNewsFilesMapper;
    @Resource
    private UserNewsLikeMapper userNewsLikeMapper;
    @Resource
    private UserAuthorFollowingMapper userAuthorFollowingMapper;
    @Resource
    private UserActionPublicationMapper userActionPublicationMapper;
    @Resource
    private UserNewsCommentLikeMapper userNewsCommentLikeMapper;
    @Resource
    private UserNewsCommentMapper userNewsCommentMapper;
    @Resource
    private FileServerHelper fileServerHelper;
    @Resource
    private UserNewsCommentService userNewsCommentService;




    /**
     * 最新动态保存方法
     * @param userNewsModel
     * @return
     * @throws IOException
     */
    public Long saveNewDevelopments(UserNewsModel userNewsModel) throws IOException {
        // 实例化最新动态保存结果
        int result = 0;
        // 实例化最新动态文件保存结果
        int fileResult = 0;
        // 最新动态实例化
        UserNews userNews = new UserNews();
        // 最新动态文件实例化
        UserNewsFiles userNewsFiles = new UserNewsFiles();
        // 用户文献操作实例化
        UserActionPublication userActionPublication = new UserActionPublication();
        // 获取当前系统时间
        Date sysDate = new Date();
        // 获取当前时间时间戳
        String timeStamp = DateUtil.getSysDate("yyyyMMddHHmmssSSS");
        // 获取学者id
        Long userId = userNewsModel.getUserId();
        //创建Properties对象
        Properties prop = new Properties();
        //读取classPath中的properties文件
        prop.load(UserNewDevelopmentsService.class.getClassLoader().getResourceAsStream("application.properties"));
        String serverPath = prop.getProperty("keensight.file.server.path");
        // 文件服务器正式路径
        String filePath = prop.getProperty("news.upload.path");
        // 文件服务器临时路径
        String fileTempPath = prop.getProperty("news.upload.path") + prop.getProperty("news.temp.path");
        // 图片上传路径
        String imgPath = prop.getProperty("news.image.path");
        // 视频上传路径
        String videoPath = prop.getProperty("news.video.path");
        // 图片临时文件路径
        String imgTempFilePath = fileTempPath + imgPath;
        // 视频临时文件路径
        String videoTempFilePath = fileTempPath + videoPath;
        // 图片正式文件路径
        String imgFormalFilePath = filePath + imgPath;
        // 视频正式文件路径
        String videoFormalFilePath = filePath + videoPath;
        // 链接识别正则
        String findLink = "(https?:\\/\\/|www\\.)[a-zA-Z_0-9\\-@]+(\\.\\w[a-zA-Z_0-9\\-:]+)+(\\/[\\(\\)~#&\\-=?\\+\\%/\\.\\w]+)?";
        // 去除动态内容链接Str
        String newsContentReplaceLink = ((userNewsModel.getNewsContent().replaceAll(findLink,"")).replace("\\s","")).replace("\n","");
        // 学者id
        userNews.setUserId(userNewsModel.getUserId());
        // 最新动态标题
        userNews.setNewsTitle(userNewsModel.getNewsTitle());
        // 最新动态内容
        userNews.setNewsContent(userNewsModel.getNewsContent());
        // 链接
        if(!"".equals(userNewsModel.getNewsExternalLink())){
            userNews.setNewsExternalLink(userNewsModel.getNewsExternalLink());
        }
        // 评论权限
        userNews.setIsComment(userNewsModel.getIsComment());
        // 可见权限
        userNews.setVisibility(userNewsModel.getVisibility());
        // 动态上传时间
        userNews.setNewsPublishTime(sysDate);
        // 点赞数
        userNews.setLikeNum(0);
        // 评论数
        userNews.setCommentNum(0);

        // 最新动态标题是否违规
        // 非空判断
        if(!"".equals(userNewsModel.getNewsTitle())){
            List<ContentSafetyResult> contentSafetyResultList = ExamineUtil.textSafety(userNewsModel.getNewsTitle(), ExamineUtil.SCENE_ANTISPAM);
            if (contentSafetyResultList.size() > 0) {
                if (contentSafetyResultList.size() > 0) {
                    if (!"pass".equals(contentSafetyResultList.get(0).getSuggestion())) {
                        result = -1;
                    }
                }
            }
            // 最新动态内容
            if(!"".equals(newsContentReplaceLink)){
                contentSafetyResultList = ExamineUtil.textSafety(newsContentReplaceLink, ExamineUtil.SCENE_ANTISPAM);
                if (contentSafetyResultList.size() > 0) {
                    if (!"pass".equals(contentSafetyResultList.get(0).getSuggestion())) {
                        result = -1;
                    }
                }
            }

        }else{
            // 最新动态内容
            if(!"".equals(newsContentReplaceLink)) {
                List<ContentSafetyResult> contentSafetyResultList = ExamineUtil.textSafety(newsContentReplaceLink, ExamineUtil.SCENE_ANTISPAM);
                if (contentSafetyResultList.size() > 0) {
                    if (!"pass".equals(contentSafetyResultList.get(0).getSuggestion())) {
                        result = -1;
                    }
                }
            }
        }

        if (result == -1) {
            return -1L;
        }

        // 保存最新动态（标题及内容不存在违规的情况下）
        result = userNewsMapper.insertSelective(userNews);
        if(result > 0 && !("2").equals(userNewsModel.getVisibility())){
            userActionPublication.setUserId(userNewsModel.getUserId());
            userActionPublication.setUserAction("7");
            userActionPublication.setPublicationId(userNews.getNewsId());
            userActionPublication.setCreatedAt(sysDate);
            int userAction = userActionPublicationMapper.insertSelective(userActionPublication);
        }
        // 获取已保存的最新动态Id
        Long userNewsId = userNews.getNewsId();
        // 获取上传文件个数
        int uploadFileNum = userNewsModel.getFileNum();
        // 当文件数大于0时，执行文件数据保存
        if(uploadFileNum > 0 && result > 0){
            // 获取文件List
            List<NewsFileModel> fileNameList = userNewsModel.getFileNameList();
            // 获取上传的文件类型(视频/图片)
            String uploadFileType = userNewsModel.getUploadType();
            // 上传文件判断
            if("1".equals(uploadFileType)){
                String fileName = userId + "_" + userNewsId + "_" + timeStamp + fileNameList.get(0).getName().substring(fileNameList.get(0).getName().lastIndexOf('.'));
                // 视频文件处理
                userNewsFiles.setNewsId(userNewsId);
                userNewsFiles.setNewsFileUrl(videoFormalFilePath + "/" + userId + "/" + timeStamp + "/" + fileName);
                // 0为图片，1为视频
                userNewsFiles.setNewsFileType("1");
                userNewsFiles.setDelFlg("0");
                // 视频数据保存
                fileResult = userNewsFilesMapper.insertSelective(userNewsFiles);
                if(fileResult > 0){
                    // 视频截图数据获取
                    File fileOImg = new File(serverPath + videoTempFilePath + File.separator + userId + File.separator +  fileNameList.get(0).getName());
                    File fileTImg = new File(serverPath + videoTempFilePath + File.separator + userId + File.separator + fileNameList.get(0).getName().substring(0,fileName.lastIndexOf(".")) + ".jpg");
                    AppMediaTask appMediaTask1 = new AppMediaTask(fileOImg, fileTImg);
                    appMediaTask1.outThumnailFile();
                    // 视频文件服务器端处理
                    if(fileHelper.mkDir(serverPath + videoFormalFilePath + File.separator + userId + File.separator + timeStamp + File.separator)) {
                        File f = new File(serverPath + imgTempFilePath + File.separator + userId + File.separator + fileNameList.get(0).getName());
                        InputStream inputStream = new FileInputStream(f);
                        File f1 = new File(serverPath + imgTempFilePath + File.separator + userId + File.separator + fileNameList.get(0).getName().substring(0,fileName.lastIndexOf(".")));
                        InputStream inputStream1 = new FileInputStream(f1);
                        // 上传到s3储存桶中

                        AwsS3Utils.upload(videoFormalFilePath + "/" + userId + "/" + timeStamp + "/" + fileName, inputStream);
                        AwsS3Utils.upload(videoTempFilePath + "/" + userId + "/" + fileName.substring(0,fileName.lastIndexOf(".")) + ".jpg", inputStream1);
//                        // 将临时文件夹视频剪切到正式文件夹
//                        fileHelper.cutGeneralFile(serverPath + videoTempFilePath + File.separator + userId + File.separator + fileName, serverPath + videoFormalFilePath + File.separator + userId + File.separator + timeStamp + File.separator);
//                        // 将视频文件截图从临时文件夹转到正式文件夹
//                        fileHelper.cutGeneralFile(serverPath + videoTempFilePath + File.separator + userId + File.separator + fileName.substring(0,fileName.lastIndexOf(".")) + ".jpg", serverPath + videoFormalFilePath + File.separator + userId + File.separator + timeStamp + File.separator);
                    }
                    // 处理完成后删除临时文件夹
                    fileHelper.deleteGeneralFile(serverPath + videoTempFilePath + File.separator + userId + File.separator);
                }
            }else{
                //图片文件处理
                for(NewsFileModel newFile : fileNameList){
                    userNewsFiles.setNewsId(userNewsId);
                    // 0为图片，1为视频
                    userNewsFiles.setNewsFileType("0");
                    userNewsFiles.setDelFlg("0");
                    // 获取文件名
                    String fileName = userId + "_" + userNewsId + "_" + timeStamp + newFile.getName().substring(newFile.getName().lastIndexOf('.'));
                    userNewsFiles.setNewsFileUrl(imgFormalFilePath + "/" + userId + "/" + userNewsId + "/" + timeStamp + "/" + fileName);
                    // 图片数据保存
                    fileResult = userNewsFilesMapper.insertSelective(userNewsFiles);
                    if(fileResult > 0){
                        // 将临时文件夹图片剪切到正式文件夹
                        if(fileHelper.mkDir(serverPath + imgFormalFilePath + File.separator + userId + File.separator + userNewsId + File.separator + timeStamp + File.separator)) {
                            File f = new File(serverPath + imgTempFilePath + File.separator + userId + File.separator + newFile.getName());
                            InputStream inputStream = new FileInputStream(f);

                            // 上传到s3储存桶中
                            PutObjectResult putObjectResult = AwsS3Utils.upload(imgFormalFilePath + "/" + userId + "/" + userNewsId + "/" + timeStamp + "/" + fileName, inputStream);
//                            fileHelper.cutGeneralFile(serverPath + imgTempFilePath + File.separator + userId + File.separator + fileName,serverPath + imgFormalFilePath + File.separator + userId + File.separator + userNewsId + File.separator + timeStamp + File.separator);
                        }
                    }
                }
                // 处理完成后删除临时文件夹
                fileHelper.deleteGeneralFile(serverPath + imgTempFilePath + File.separator + userId + File.separator);
            }
        }

        return userNews.getNewsId();
    }

    /**
     * 最新动态获取方法
     * @param userId
     * @param index
     * @param rows
     * @return pagesInfo
     */
    public PageInfo<UserNewsModel> getNewDevelopments(Long loginUserId,Long userId, Integer index, Integer rows) throws ParseException {
        List<UserNewsModel> userNewsList = new ArrayList<>();
        // 获取最新动态总数
        int listCount = userNewsMapper.getNewsCount(userId);
        PageInfo<UserNewsModel> pagesInfo= new PageInfo<UserNewsModel>();
        pagesInfo.setTotal(listCount);
        pagesInfo.setPageNum(index);
        pagesInfo.setPageSize(rows);
        pagesInfo.setHasNextPage(listCount > 0 && listCount > index * rows);
        PageHelper.startPage(index, rows, true, null, true);
        List<Long> newIds = new ArrayList<>();
        userNewsList = userNewsMapper.getNewDevelopments(loginUserId, userId);
        if (userNewsList.size() > 0) {
            for(UserNewsModel userNews : userNewsList){
                newIds.add(userNews.getNewsId());
                // 动态文件list查询
                List<UserNewsFiles> filesList = userNewsFilesMapper.getUserNewsFiles(userNews.getNewsId());

                userNews.setUserNewsFilesList(filesList);
                if(filesList.size() > 0){
                    if(("1").equals(filesList.get(0).getNewsFileType())){
                        userNews.setShowFileType("1");
                        userNews.setVideoCut(filesList.get(0).getNewsFileUrl().substring(0,filesList.get(0).getNewsFileUrl().lastIndexOf(".")) + ".jpg");
                    }else{
                        userNews.setShowFileType("0");
                    }
                }
                // 时间差
                Date createdAt = userNews.getNewsPublishTime();
                userNews.setPublishTime(DateUtil.validateTime(new Date(), createdAt));
            }

            List<UserNewsCommentModel> firstLevelComments = new ArrayList<>();
            firstLevelComments = userNewsCommentMapper.getFirstLevelComments(loginUserId, newIds);
            for (UserNewsModel userNews : userNewsList) {
                for (UserNewsCommentModel userNewsCommentModel : firstLevelComments) {
                    if (userNews.getNewsId().equals(userNewsCommentModel.getNewsId())) {
                        userNews.setCommentNum(userNews.getCommentNum() +  userNewsCommentModel.getLevel2CommentNum());
                    }
                }
            }
        }

        pagesInfo.setList(userNewsList);
        return pagesInfo;
    }

    /**
     * 最新动态点赞数更新方法
     * @param newsId
     * @param userId
     * @param updFlg
     * @return
     */
    public int updLikeNums(Long newsId, Long userId, String updFlg) {
        int result = 0;
        UserNewsLike userNewsLike = new UserNewsLike();
        // 获取系统时间
        Date sysDate = new Date();
        // 查询最新动态点赞数
        // int oldLikeNum = userNewsMapper.se
        // 判断点赞/取消点赞
        if("1".equals(updFlg)){
            userNewsLike.setUserId(userId);
            userNewsLike.setNewsId(newsId);
            userNewsLike.setLikeTime(sysDate);
            // 点赞
            int addLikeNum = userNewsLikeMapper.insertSelective(userNewsLike);
            // 更新最新动态点赞数
            int updNewsLikeNum = userNewsMapper.updNewsLikeNum(1,newsId);
            if(addLikeNum > 0 && updNewsLikeNum > 0){
                result = 1;
            }
        }else{
            // 取消点赞
            int delLikeNum = userNewsLikeMapper.deleteLikeNum(userId,newsId);
            // 更新最新动态点赞数
            int updNewsLikeNum = userNewsMapper.updNewsLikeNum(0,newsId);
            if(delLikeNum > 0 && updNewsLikeNum > 0){
                result = 1;
            }
        }
        return result;
    }

    /**
     * 最新动态删除方法
     * @param visibility
     * @param newsId
     * @param commentNum
     * @param fileNum
     * @param likeNum
     * @return
     */
    public int delNewsData(String visibility, Long newsId, int commentNum, int fileNum, int likeNum) {
        int result = 0;
        UserNews userNews = new UserNews();
        // 判断可见权限
        if("2".equals(visibility)){
            // 仅自己可见
            if(commentNum > 0){
                // 删除评论点赞
                int delCommentsLike = userNewsCommentLikeMapper.deleteByNewsId(newsId);
                // 删除评论
                int delComments = userNewsCommentMapper.delByNewsId(newsId);
            }
            if(fileNum > 0){
                // 删除动态文件
                int delNewsFile = userNewsFilesMapper.deleteByNewsId(newsId);
            }
            if(likeNum > 0){
                // 删除动态点赞数据
                int delNewsLike = userNewsLikeMapper.deleteByNewsId(newsId);
            }
            // 删除动态
            userNews.setNewsId(newsId);
            int delNewsData = userNewsMapper.deleteByPrimaryKey(userNews);
            if(delNewsData > 0){
                result = 1;
            }
        }else{
            // 所有人可见，关注的人可见
            // 删除评论点赞
            int delCommentsLike = userNewsCommentLikeMapper.deleteByNewsId(newsId);
            // 删除评论
            int delComments = userNewsCommentMapper.delByNewsId(newsId);
            // 删除publication表数据
            int delPublication = userActionPublicationMapper.deleteByPublicationId(newsId);
            // 删除动态文件
            int delNewsFile = userNewsFilesMapper.deleteByNewsId(newsId);
            // 删除动态点赞数据
            int delNewsLike = userNewsLikeMapper.deleteByNewsId(newsId);
            // 删除动态
            userNews.setNewsId(newsId);
            int delNewsData = userNewsMapper.deleteByPrimaryKey(userNews);
            if(delNewsData > 0){
                result = 1;
            }
        }
        return result;
    }

    /**
     * 学者页推荐动态获取方法
     * @param loginUserId
     * @return
     */
    public Map<String, PageInfo> getAllNewsList(Long loginUserId, Integer index, Integer rows, Long userId, Long newsId) throws ParseException {
        List<PublicationModel> publicationModelList = new ArrayList<PublicationModel>();
        int pageNum = index;
        int pageSize = rows;
        PageHelper.startPage(pageNum, pageSize);
        List<Long> newIds = new ArrayList<>();
        List<UserNewsModel> userNewsModelList = userNewsMapper.getAllNewsList(loginUserId, userId, newsId);
        if (userNewsModelList.size() > 0) {
            for (UserNewsModel userNews : userNewsModelList) {
                newIds.add(userNews.getNewsId());
                PublicationModel publicationModel = new PublicationModel();
                List<UserNewsFiles> filesList = userNewsFilesMapper.getUserNewsFiles(userNews.getNewsId());
                userNews.setUserNewsFilesList(filesList);
                if (filesList.size() > 0) {
                    if (("1").equals(filesList.get(0).getNewsFileType())) {
                        userNews.setShowFileType("1");
                        userNews.setVideoCut(filesList.get(0).getNewsFileUrl().substring(0, filesList.get(0).getNewsFileUrl().lastIndexOf(".")) + ".jpg");
                    } else {
                        userNews.setShowFileType("0");
                    }
                }
                publicationModel.setPublicationId(userNews.getNewsId());
                publicationModel.setUserAction("7");
                publicationModel.setUserNewsModel(userNews);
                publicationModelList.add(publicationModel);
                // 动态发布时间格式化
                userNews.setPublishTime(cn.newtouch.dl.turbocloud.utils.DateUtil.convDate2Str(userNews.getNewsPublishTime(), "yy-MM-dd HH:mm"));
            }

            List<UserNewsCommentModel> firstLevelComments = new ArrayList<>();
            firstLevelComments = userNewsCommentMapper.getFirstLevelComments(loginUserId, newIds);
            for (UserNewsModel userNews : userNewsModelList) {
                for (UserNewsCommentModel userNewsCommentModel : firstLevelComments) {
                    if (userNews.getNewsId().equals(userNewsCommentModel.getNewsId())) {
                        userNews.setCommentNum(userNews.getCommentNum() +  userNewsCommentModel.getLevel2CommentNum());
                    }
                }
            }
        }

        Map<String, PageInfo> result = new HashMap<>();
        result.put("userNewsModelList",new PageInfo<UserNewsModel>(userNewsModelList));
        result.put("publicationModelList",new PageInfo<PublicationModel>(publicationModelList));
        return result;
    }
}
