package com.keensight.web.service;

import com.alibaba.druid.util.StringUtils;
import com.keensight.web.db.entity.UserDiscipline;
import com.keensight.web.db.entity.UserDisciplineExample;
import com.keensight.web.db.entity.UserDisciplineExample.Criteria;
import com.keensight.web.db.mapper.UserDisciplineMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class UserDisciplineService {

    @Resource
    private UserDisciplineMapper userDisciplineMapper;

    /**
     * 注册研究领域插入
     * 用户注册时对于用户的“研究领域”进行全删全查处理
     *
     * @param userId
     * @param fieldIds
     * @return
     */
    public int insertUserDiscipline(Long userId, String fieldIds) {
        int rec = 0;
        Date sysDate = new Date();
        if (!StringUtils.isEmpty(fieldIds)) {
            // 删除已经存在的研究领域
            UserDisciplineExample userDisciplineExample = new UserDisciplineExample();
            Criteria criteria = userDisciplineExample.createCriteria();
            criteria.andUserIdEqualTo(userId);
            userDisciplineMapper.deleteByExample(userDisciplineExample);

            rec = 0;
            String[] arrField = fieldIds.split(",");
            // 将数据插入用户研究领域表
            for (String fieldId : arrField) {
                if (!StringUtils.isEmpty(fieldId) && StringUtils.isNumber(fieldId)) {
                    UserDiscipline userDiscipline = new UserDiscipline();
                    userDiscipline.setUserId(userId);
                    userDiscipline.setFieldId(Long.valueOf(fieldId));
                    userDiscipline.setPublications(0);
                    userDiscipline.setCreatedAt(sysDate);
                    userDiscipline.setUpdatedAt(sysDate);
                    rec += userDisciplineMapper.insert(userDiscipline);
                }
            }
        }
        return rec;
    }

    /**
     * 获取用户已有的研究领域
     *
     * @param userId
     * @return
     */
    public List<UserDiscipline> getDisciplineByUserId(Long userId) {
        return userDisciplineMapper.getDisciplineByUserId(userId);
    }
}
