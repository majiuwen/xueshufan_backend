package com.keensight.web.service;

import com.keensight.web.db.entity.StatsUserMonthly;
import com.keensight.web.db.mapper.StatsUserMonthlyMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 作者月份文献统计Service
 *
 * @author : newtouch
 * @version : v1.0.0 [2021/10/22] 新建
 */
@Service
public class StatsUserMonthlyService {
    @Resource
    private StatsUserMonthlyMapper statsUserMonthlyMapper;

    /**
     * 根据作者ID获取作者月份文献统计信息
     * @param userId
     * @return
     */
    public List<StatsUserMonthly> getStatsUserMonthlyList(int userId) {
        List<StatsUserMonthly> statsUserMonthlyInfo = statsUserMonthlyMapper.getStatsUserMonthlyLists(userId);
        if(statsUserMonthlyInfo == null) {
            statsUserMonthlyInfo = new ArrayList<>();
        }
        return statsUserMonthlyInfo;
    }
}
