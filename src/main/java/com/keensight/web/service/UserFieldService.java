package com.keensight.web.service;

import com.keensight.web.db.entity.UserDiscipline;
import com.keensight.web.db.entity.UserField;
import com.keensight.web.db.mapper.UserDisciplineMapper;
import com.keensight.web.db.mapper.UserFieldMapper;
import com.keensight.web.model.UserFieldModel;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserFieldService {

    @Resource
    private UserFieldMapper userFieldMapper;
    @Resource
    private UserDisciplineMapper userDisciplineMapper;

    /**
     * 获取用户的研究领域及学科
     * @param userId
     * @return
     */
    public UserFieldModel getUserField(Long userId) {

        // 获取用户的研究领域
        List<UserField> userFields = userFieldMapper.getUserField(userId);
        // 获取用户的学科
        List<UserDiscipline> userDisciplines = userDisciplineMapper.getUserDisciplineList(userId);

        UserFieldModel userFieldModel = new UserFieldModel();
        //显示修改按钮
        userFieldModel.setUpdateBtnFlg("1");
        userFieldModel.setUserDisciplines(userDisciplines);
        userFieldModel.setUserFields(userFields);

        return userFieldModel;
    }
}
