package com.keensight.web.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.keensight.web.db.entity.Document;
import com.keensight.web.db.mapper.DocumentMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author : newtouch
 * @version : v1.0.0 [2020/11/05] 新建
 */
@Service
public class DocumentService {
    @Resource
    private DocumentMapper documentMapper;

    /**
     * 文档统计结果取得
     *
     * @param userId 用户ID
     * @return 文档统计结果集合
     */
    public List<Document> getDocumentStatisticsByUserId(Long userId) {
        return documentMapper.getDocumentStatisticsByUserId(userId);
    }

    /**
     * 根据检索条件查询文献ID集合
     *
     * @param selAllFlg   检索所有Flg
     * @param userId      用户ID
     * @param docTypeList 文献类型集合
     * @param sortColumns 排序列
     * @param pageNum     开始页数
     * @param pageSize    显示件数
     * @return 文献ID集合
     */
    public PageInfo<Long> getPublicationIdList(boolean selAllFlg, Long userId, List<String> docTypeList, String sortColumns, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize, true, null, true);
        List<Long> publicationIdList = documentMapper.getPublicationIdList(selAllFlg, userId, docTypeList, sortColumns);
        return new PageInfo<Long>(publicationIdList);
    }
}
