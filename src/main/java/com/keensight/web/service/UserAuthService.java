package com.keensight.web.service;

import com.alibaba.druid.util.StringUtils;
import com.keensight.web.db.entity.UserAuth;
import com.keensight.web.db.entity.UserAuthExample;
import com.keensight.web.db.mapper.UserAuthMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class UserAuthService {

    @Resource
    private UserAuthMapper userAuthMapper;

    /**
     * 登录
     * @param identityType
     * @param identifier
     * @param credential
     * @param userId
     * @return
     */
    public UserAuth login(String identityType, String identifier, String credential, Long userId) {
        return userAuthMapper.login(identityType, identifier, credential, userId);
    }

    /**
     * 注册插入邮箱和电话
     * @param userId
     * @param emailOrTel
     * @param password
     * @return
     */
    public int insertRegistEmailOrTel(Long userId, String type, String emailOrTel, String password) {
        UserAuth userAuth = new UserAuth();
        userAuth.setUserId(userId);
        userAuth.setIdentityType(type);
        userAuth.setIdentifier(emailOrTel);
        userAuth.setCredential(password);
        userAuth.setVerified(false);
        userAuth.setCreatedAt(new Date());
        userAuth.setUpdatedAt(new Date());
        return userAuthMapper.insert(userAuth);
    }

    /**
     * 注册更新信息
     * @param userId
     * @param type
     * @param emailOrTel
     * @param password
     * @return
     */
    public int updateRegistEmailOrTel(Long userId, String type, String emailOrTel, String password) {
        UserAuth userAuth = new UserAuth();
        userAuth.setUserId(userId);
        userAuth.setIdentityType(type);
        userAuth.setIdentifier(emailOrTel);
        userAuth.setCredential(password);
        userAuth.setVerified(false);
        userAuth.setCreatedAt(new Date());
        userAuth.setUpdatedAt(new Date());
        return userAuthMapper.updateByPrimaryKey(userAuth);
    }

    /**
     * 注册信息查重
     * @param emial
     * @return
     */
    public UserAuth checkIdentifier(String emial) {
        return userAuthMapper.checkIdentifier(emial);
    }

    /**
     * 设置密码/重置密码
     * @param userId
     * @param password
     * @return
     */
    public int updUserPassword(Long userId, String password) {
        UserAuth user = new UserAuth();
        user.setUserId(userId);
        user.setIdentityType("phone");
        user.setCredential(password);
        user.setUpdatedAt(new Date());
        int resCount = userAuthMapper.updateByPrimaryKeySelective(user);
        return resCount;
    }

    /**
     * 根据微信授权用户唯一标识获取用户验证信息
     *
     * @param openId 微信授权用户唯一标识
     * @return 用户验证信息
     */
    public UserAuth getUserAuthInfoByOpenId(String openId) {
        return userAuthMapper.getUserAuthInfoByOpenId(openId);
    }

    /**
     * 根据userId获取用户验证信息
     *
     * @param userId userId
     * @return
     */
    public List<UserAuth> getUserAuthInfoByUserId(Long userId) {
        UserAuthExample userAuthExample = new UserAuthExample();
        userAuthExample.createCriteria().andUserIdEqualTo(userId).andIdentityTypeEqualTo("phone");
        return userAuthMapper.selectByExample(userAuthExample);
    }

    /**
     * 根据微信授权用户唯一标识更新用户验证信息
     *
     * @param userId 用户Id
     * @param openId 微信授权用户唯一标识
     * @return 用户验证信息
     */
    public int updUserIdByOpenId(Long userId, String openId) {
        return userAuthMapper.updUserIdByOpenId(userId, openId, new Date());
    }

    /**
     * 查询用户基本信息是否存在
     * @return
     */
    public int selectCountByUserId(Long userId, String identutyType){
        int resCount = 0;
        UserAuthExample userAuthExample = new UserAuthExample();
        UserAuthExample.Criteria criteria =  userAuthExample.createCriteria();
        if (userId != null) {
            criteria.andUserIdEqualTo(userId);
            if (!StringUtils.isEmpty(identutyType)) {
                criteria.andIdentityTypeEqualTo(identutyType);
            }
            resCount = userAuthMapper.countByExample(userAuthExample);
        }

        return resCount;
    }
}
