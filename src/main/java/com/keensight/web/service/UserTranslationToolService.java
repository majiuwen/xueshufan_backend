package com.keensight.web.service;

import com.keensight.web.db.entity.UserAnnotation;
import com.keensight.web.db.entity.UserTranslationTool;
import com.keensight.web.db.mapper.UserTranslationToolMapper;
import com.keensight.web.model.UserAnnotationModel;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserTranslationToolService {
    @Resource
    private UserTranslationToolMapper userTranslationToolMapper;

    /**
     * 获取翻译工具信息
     * @return
     */
    public List<UserTranslationTool> getUserTranslationTool4List() {

        // 用户批注信息Listu
        List<UserTranslationTool> userTranslationToolList = new ArrayList<UserTranslationTool>();
        userTranslationToolList = userTranslationToolMapper.selectUserTranslationToolList();

        return userTranslationToolList;
    }
}
