package com.keensight.web.service;

import cn.newtouch.dl.turbocloud.helper.FileHelper;
import com.keensight.web.controller.UserAchievementsController;
import com.keensight.web.db.entity.*;
import com.keensight.web.db.mapper.*;
import com.keensight.web.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@Service
public class UserActionPublicationService {

    // 用户的文献操作
    @Resource
    private UserActionPublicationMapper userActionPublicationMapper;
    // 用户的个人收藏
    @Resource
    private UserLibraryMapper userLibraryMapper;
    // 用户推荐的文献
    @Resource
    private UserPublicationRecommendationMapper userPublicationRecommendationMapper;
    // 用户关注的文献
    @Resource
    private UserPublicationFollowingMapper userPublicationFollowingMapper;
    // 用户的下载文献
    @Resource
    private UserPublicationDownloadingMapper userPublicationDownloadingMapper;
    // 用户在读的文章
    @Resource
    private UserPublicationBeingReadMapper userPublicationBeingReadMapper;
    // 文献统计数字
    @Resource
    private StatsPublicationTotalMapper statsPublicationTotalMapper;
    @Resource
    private StatsPublicationYearlyMapper statsPublicationYearlyMapper;
    @Resource
    private StatsPublicationMonthlyMapper statsPublicationMonthlyMapper;
    @Resource
    private StatsPublicationWeeklyMapper statsPublicationWeeklyMapper;
    // 作者文献统计总数
    @Resource
    private StatsUserTotalMapper statsUserTotalMapper;
    @Resource
    private StatsUserYearlyMapper statsUserYearlyMapper;
    @Resource
    private StatsUserMonthlyMapper statsUserMonthlyMapper;
    @Resource
    private StatsUserWeeklyMapper statsUserWeeklyMapper;
    @Resource
    private UserLibraryFolderPublicationMapper userLibraryFolderPublicationMapper;
    @Resource
    private PublicationMapper publicationMapper;
    @Resource
    private PublicationUserInstitutionMapper publicationUserInstitutionMapper;
    @Resource
    private DocumentMapper documentMapper;
    @Autowired
    private FileHelper fileHelper;

    /**
     * 用户操作
     * 定义Flg 0:引用 1:阅读 2:推荐 3:分享 4:收藏 5:下载 6:关注 7:举报
     *
     * @param userId
     * @param publicationId
     * @param authorIds
     * @param actionFlg
     * @param operationalBehaviorFlg
     */
    public void updUserAction(Long userId, Long publicationId, String authorIds, String actionFlg, Boolean operationalBehaviorFlg, Boolean deletePrivateFlg) throws ParseException {
        updUserAction(userId, publicationId, authorIds, actionFlg, operationalBehaviorFlg, null, false, deletePrivateFlg, null);
    }
    public void updUserAction(Long userId, Long publicationId, String authorIds, String actionFlg, Boolean operationalBehaviorFlg, String folderIds) throws ParseException {
        updUserAction(userId, publicationId, authorIds, actionFlg, operationalBehaviorFlg, folderIds, false, false,null);
    }
    public void updUserAction(Long userId, Long publicationId, String authorIds, String actionFlg, Boolean operationalBehaviorFlg, String folderIds, boolean hasFolder, Boolean deletePrivateFlg, Long docId) throws ParseException {
        // 作者ID转LIST
        List<Long> authorIdsL = new ArrayList<>();
        boolean authIdFlg = false;
        if (!StringUtils.isEmpty(authorIds)) {
            String[] authorArry = authorIds.split(",");
            for (String authorIdsS : authorArry) {
                authorIdsL.add(Long.valueOf(authorIdsS));
            }
            authIdFlg = true;
        }

        if(deletePrivateFlg == null){
            deletePrivateFlg = false;
        }

        // 当前系统时间
        Date systemTime = new Date();
        // 获取当前年最后一天
        Date year = DateUtil.getCurrYearLast();
        // 获取本月最后一天
        Date month = DateUtil.getLastDayOfMonth(systemTime);
        // 获取当前周周日
        Date week = DateUtil.getLastDayOfWeek(systemTime);

        switch(actionFlg){
            // 引用
            case "0" :
                break;
            // 阅读
            case "1" :
                if (!StringUtils.isEmpty(userId)) {
                    upUserPublicationBeingRead(userId, publicationId, systemTime);
                }
                break;
            // 推荐
            case "2" :
                upUserPublicationRecommendation(userId, publicationId, operationalBehaviorFlg, systemTime);
                break;
            // 分享
            case "3" :
                break;
            // 收藏
            case "4" :
                upUserLibrary(userId, publicationId, operationalBehaviorFlg, systemTime, folderIds, hasFolder, docId);
                break;
            // 下载
            case "5" :
                upUserPublicationDownloading(userId, publicationId, systemTime);
                break;
            // 关注
            case "6" :
                upUserPublicationFollowing(userId, publicationId, operationalBehaviorFlg, systemTime);
                break;
            // 举报
            default :
                break;
        }

        if (!deletePrivateFlg) {
            if (operationalBehaviorFlg) {
                // 文献统计数字的文献-相关操作次数增加
                int publicationTotalNum = statsPublicationTotalMapper.updateStatsPublicationTotalIncrease(publicationId, actionFlg, systemTime);
                if (publicationTotalNum == 0) {
                    updateDbPublicationTotal(publicationId, systemTime);
                    statsPublicationTotalMapper.updateStatsPublicationTotalIncrease(publicationId, actionFlg, systemTime);
                }
                int publicationYearlyNum = statsPublicationYearlyMapper.updateStatsPublicationYearlyIncrease(publicationId, actionFlg, year, systemTime);
                if (publicationYearlyNum == 0) {
                    updateDbPublicationYearly(publicationId, year, systemTime);
                    statsPublicationYearlyMapper.updateStatsPublicationYearlyIncrease(publicationId, actionFlg, year, systemTime);
                }
                int publicationMonthlyNum = statsPublicationMonthlyMapper.updateStatsPublicationMonthlyIncrease(publicationId, actionFlg, month, systemTime);
                if (publicationMonthlyNum == 0) {
                    updateDbPublicationMonth(publicationId, month, systemTime);
                    statsPublicationMonthlyMapper.updateStatsPublicationMonthlyIncrease(publicationId, actionFlg, month, systemTime);
                }
                int publicationWeeklyNum = statsPublicationWeeklyMapper.updateStatsPublicationWeeklyIncrease(publicationId, actionFlg, week, systemTime);
                if (publicationWeeklyNum == 0) {
                    updateDbPublicationWeekly(publicationId, week, systemTime);
                    statsPublicationWeeklyMapper.updateStatsPublicationWeeklyIncrease(publicationId, actionFlg, week, systemTime);
                }
                if (authIdFlg) {
                    // 作者文献统计总数-相关操作次数增加
                    updateDbUserTotal(authorIdsL, actionFlg, operationalBehaviorFlg, systemTime);
                    updateDbUserYearly(authorIdsL, year, actionFlg, operationalBehaviorFlg, systemTime);
                    updateDbUserMonthly(authorIdsL, month, actionFlg, operationalBehaviorFlg, systemTime);
                    updateDbUserWeekly(authorIdsL, week, actionFlg, operationalBehaviorFlg, systemTime);
                }
            } else {
                // 文献统计数字的文献-相关操作次数减少
                int statsPublicationTotal = statsPublicationTotalMapper.updateStatsPublicationTotalReduce(publicationId, actionFlg, systemTime);
                if (statsPublicationTotal == 0) {
                    updateDbPublicationTotal(publicationId, systemTime);
                }
                int statsPublicationYearly = statsPublicationYearlyMapper.updateStatsPublicationYearlyReduce(publicationId, actionFlg, year, systemTime);
                if (statsPublicationYearly == 0) {
                    updateDbPublicationYearly(publicationId, year, systemTime);
                }
                int statsPublicationMonthly = statsPublicationMonthlyMapper.updateStatsPublicationMonthlyReduce(publicationId, actionFlg, month, systemTime);
                if (statsPublicationMonthly == 0) {
                    updateDbPublicationMonth(publicationId, month, systemTime);
                }
                int statsPublicationWeekly = statsPublicationWeeklyMapper.updateStatsPublicationWeeklyReduce(publicationId, actionFlg, week, systemTime);
                if (statsPublicationWeekly == 0) {
                    updateDbPublicationWeekly(publicationId, week, systemTime);
                }
                if (authIdFlg) {
                    // 作者文献统计总数-相关操作次数减少
                    updateDbUserTotal(authorIdsL, actionFlg, operationalBehaviorFlg, systemTime);
                    updateDbUserYearly(authorIdsL, year, actionFlg, operationalBehaviorFlg, systemTime);
                    updateDbUserMonthly(authorIdsL, month, actionFlg, operationalBehaviorFlg, systemTime);
                    updateDbUserWeekly(authorIdsL, week, actionFlg, operationalBehaviorFlg, systemTime);
                }
            }

            // 用户的文献操作追加
            String userAction = getUserAction(actionFlg);
            if (!"".equals(userAction) && operationalBehaviorFlg){
                UserActionPublication userActionPublication = new UserActionPublication();
                // 判断用户是否已操作过该文献
                int count = userActionPublicationMapper.getCount(userId, publicationId, userAction);
                if (count == 0) {
                    userActionPublication.setUserId(userId);
                    userActionPublication.setPublicationId(publicationId);
                    userActionPublication.setUserAction(userAction);
                    userActionPublication.setCreatedAt(systemTime);
                    userActionPublicationMapper.insert(userActionPublication);
                } else {
                    // 更新创建时间
                    userActionPublication.setUserId(userId);
                    userActionPublication.setPublicationId(publicationId);
                    userActionPublication.setUserAction(userAction);
                    userActionPublication.setCreatedAt(systemTime);
                    userActionPublicationMapper.updateCreatedAt(userActionPublication);
                }
            }
        }

    }

    /**
     * 文献统计数字不存在时，插入数据
     *
     * @param publicationId
     * @param systemTime
     */
    private void updateDbPublicationTotal(Long publicationId, Date systemTime) {
        StatsPublicationTotal spt = new StatsPublicationTotal();
        spt.setPublicationId(publicationId);
        spt.setUpdatedDate(systemTime);
        spt.setCitations(0);
        spt.setReads(0);
        spt.setRecommendations(0);
        spt.setSharings(0);
        spt.setSavings(0);
        spt.setDownloads(0);
        spt.setFollowings(0);
        spt.setReported(0);
        spt.setUpdatedAt(systemTime);
        statsPublicationTotalMapper.insert(spt);
    }

    /**
     * 文献年度统计不存在时，插入数据
     *
     * @param publicationId
     * @param year
     * @param systemTime
     */
    private void updateDbPublicationYearly(Long publicationId, Date year, Date systemTime) {
        StatsPublicationYearly spy = new StatsPublicationYearly();
        spy.setPublicationId(publicationId);
        spy.setYear(year);
        spy.setCitations(0);
        spy.setReads(0);
        spy.setRecommendations(0);
        spy.setSavings(0);
        spy.setSharings(0);
        spy.setDownloads(0);
        spy.setFollowings(0);
        spy.setReported(0);
        spy.setUpdatedAt(systemTime);
        statsPublicationYearlyMapper.insert(spy);
    }

    /**
     * 文献月统计不存在时，插入数据
     *
     * @param publicationId
     * @param month
     * @param systemTime
     */
    private void updateDbPublicationMonth(Long publicationId, Date month, Date systemTime) {
        StatsPublicationMonthly spm = new StatsPublicationMonthly();
        spm.setPublicationId(publicationId);
        spm.setMonth(month);
        spm.setCitations(0);
        spm.setReads(0);
        spm.setRecommendations(0);
        spm.setSavings(0);
        spm.setSharings(0);
        spm.setDownloads(0);
        spm.setFollowings(0);
        spm.setReported(0);
        spm.setUpdatedAt(systemTime);
        statsPublicationMonthlyMapper.insert(spm);
    }

    /**
     * 文献周统计不存在时，插入数据
     *
     * @param publicationId
     * @param week
     * @param systemTime
     */
    private void updateDbPublicationWeekly(Long publicationId, Date week, Date systemTime) {
        StatsPublicationWeekly spw = new StatsPublicationWeekly();
        spw.setPublicationId(publicationId);
        spw.setWeek(week);
        spw.setCitations(0);
        spw.setReads(0);
        spw.setRecommendations(0);
        spw.setSharings(0);
        spw.setSavings(0);
        spw.setDownloads(0);
        spw.setFollowings(0);
        spw.setReported(0);
        spw.setUpdatedAt(systemTime);
        statsPublicationWeeklyMapper.insert(spw);
    }

    /**
     * 作者文献统计总数不存在时，插入数据
     *
     * @param authorIdsL
     * @param actionFlg
     * @param operationalBehaviorFlg
     * @param systemTime
     */
    private void updateDbUserTotal(List<Long> authorIdsL, String actionFlg, Boolean operationalBehaviorFlg, Date systemTime) {
        // 获取DB中已有的统计数据
        List<Long> authorIds = statsUserTotalMapper.getStatsUserTotal(authorIdsL);
        List<StatsUserTotal> statsUserTotals = new ArrayList<>();
        // 如果DB不存在该条作者的记录，新增一条初始数据
        for (Long authorId : authorIdsL) {
            if (!authorIds.contains(authorId)) {
                StatsUserTotal sut = new StatsUserTotal();
                sut.setUserId(authorId);
                sut.setUpdatedDate(systemTime);
                sut.setPublications(0);
                sut.setCitations(0);
                sut.setReads(0);
                sut.setRecommendations(0);
                sut.setSharings(0);
                sut.setSavings(0);
                sut.setDownloads(0);
                sut.setFollowings(0);
                sut.setReported(0);
                sut.setUpdatedAt(systemTime);
                statsUserTotals.add(sut);
            }
        }
        // 批量插入数据
        if (statsUserTotals != null && statsUserTotals.size() > 0) {
            if(statsUserTotals.size() > 2700){
                List<StatsUserTotal> statsUserTotalsHead = statsUserTotals.subList(0,2700);
                List<StatsUserTotal> statsUserTotalsFoot = statsUserTotals.subList(2700,statsUserTotals.size());
                statsUserTotalMapper.insertStatsUsersTotal(statsUserTotalsHead);
                statsUserTotalMapper.insertStatsUsersTotal(statsUserTotalsFoot);
            } else {
                statsUserTotalMapper.insertStatsUsersTotal(statsUserTotals);
            }
        }

        if (operationalBehaviorFlg == true) {
            statsUserTotalMapper.updateStatsUserTotalIncrease(authorIdsL, actionFlg, systemTime);
        } else {
            statsUserTotalMapper.updateStatsUserTotalReduce(authorIdsL, actionFlg, systemTime);
        }
    }

    /**
     * 作者年度文献统计不存在时，插入数据
     *
     * @param authorIdsL
     * @param year
     * @param actionFlg
     * @param operationalBehaviorFlg
     * @param systemTime
     */
    private void updateDbUserYearly(List<Long> authorIdsL, Date year, String actionFlg, Boolean operationalBehaviorFlg, Date systemTime) {
        // 获取DB中已有的统计数据
        List<Long> authorIds = statsUserYearlyMapper.getStatsUserYearly(authorIdsL, year);
        List<StatsUserYearly> statsUserYearlies = new ArrayList<>();
        // 如果DB不存在该条作者的记录，新增一条初始数据
        for (Long authorId : authorIdsL) {
            if (!authorIds.contains(authorId)) {
                StatsUserYearly suy = new StatsUserYearly();
                suy.setUserId(authorId);
                suy.setYear(year);
                suy.setPublications(0);
                suy.setCitations(0);
                suy.setReads(0);
                suy.setRecommendations(0);
                suy.setSharings(0);
                suy.setSavings(0);
                suy.setDownloads(0);
                suy.setFollowings(0);
                suy.setReported(0);
                suy.setUpdatedAt(systemTime);
                statsUserYearlies.add(suy);
            }
        }
        // 批量插入数据
        if (statsUserYearlies != null && statsUserYearlies.size() > 0) {
            if(statsUserYearlies.size() > 2700){
                List<StatsUserYearly> statsUserYearliesHead = statsUserYearlies.subList(0,2700);
                List<StatsUserYearly> statsUserYearliesFoot = statsUserYearlies.subList(2700,statsUserYearlies.size());
                statsUserYearlyMapper.insertStatsUsersYearly(statsUserYearliesHead);
                statsUserYearlyMapper.insertStatsUsersYearly(statsUserYearliesFoot);
            } else {
                statsUserYearlyMapper.insertStatsUsersYearly(statsUserYearlies);
            }
        }

        if (operationalBehaviorFlg == true) {
            statsUserYearlyMapper.updateStatsUserYearlyIncrease(authorIdsL, actionFlg, year, systemTime);
        } else {
            statsUserYearlyMapper.updateStatsUserYearlyReduce(authorIdsL, actionFlg, year, systemTime);
        }
    }

    /**
     * 作者月文献统计不存在时，插入数据
     *
     * @param authorIdsL
     * @param month
     * @param actionFlg
     * @param operationalBehaviorFlg
     * @param systemTime
     */
    private void updateDbUserMonthly(List<Long> authorIdsL, Date month, String actionFlg, Boolean operationalBehaviorFlg, Date systemTime) {
        // 获取DB中已有的统计数据
        List<Long> authorIds = statsUserMonthlyMapper.getStatsUserMonthly(authorIdsL, month);
        List<StatsUserMonthly> statsUserMonthlies = new ArrayList<>();
        // 如果DB不存在该条作者的记录，新增一条初始数据
        for (Long authorId : authorIdsL) {
            if (!authorIds.contains(authorId)) {
                StatsUserMonthly sum = new StatsUserMonthly();
                sum.setUserId(authorId);
                sum.setMonth(month);
                sum.setPublications(0);
                sum.setCitations(0);
                sum.setReads(0);
                sum.setRecommendations(0);
                sum.setSharings(0);
                sum.setSavings(0);
                sum.setDownloads(0);
                sum.setFollowings(0);
                sum.setReported(0);
                sum.setUpdatedAt(systemTime);
                statsUserMonthlies.add(sum);
            }
        }
        // 批量插入数据
        if (statsUserMonthlies != null && statsUserMonthlies.size() > 0) {
            if(statsUserMonthlies.size() > 2700){
                List<StatsUserMonthly> statsUserMonthliesHead = statsUserMonthlies.subList(0,2700);
                List<StatsUserMonthly> statsUserMonthliesFoot = statsUserMonthlies.subList(2700,statsUserMonthlies.size());
                statsUserMonthlyMapper.insertStatsUsersMonthly(statsUserMonthliesHead);
                statsUserMonthlyMapper.insertStatsUsersMonthly(statsUserMonthliesFoot);
            } else {
                statsUserMonthlyMapper.insertStatsUsersMonthly(statsUserMonthlies);
            }
        }

        if (operationalBehaviorFlg == true) {
            statsUserMonthlyMapper.updateStatsUserMonthlyIncrease(authorIdsL, actionFlg, month, systemTime);
        } else {
            statsUserMonthlyMapper.updateStatsUserMonthlyReduce(authorIdsL, actionFlg, month, systemTime);
        }
    }

    /**
     * 作者周文献统计不存在时，插入数据
     *
     * @param authorIdsL
     * @param week
     * @param actionFlg
     * @param operationalBehaviorFlg
     * @param systemTime
     */
    private void updateDbUserWeekly(List<Long> authorIdsL, Date week, String actionFlg, Boolean operationalBehaviorFlg, Date systemTime) {
        // 获取DB中已有的统计数据
        List<Long> authorIds = statsUserWeeklyMapper.getStatsUserWeekly(authorIdsL, week);
        List<StatsUserWeekly> statsUserWeeklies = new ArrayList<>();
        // 如果DB不存在该条作者的记录，新增一条初始数据
        for (Long authorId : authorIdsL) {
            if (!authorIds.contains(authorId)) {
                StatsUserWeekly suw = new StatsUserWeekly();
                suw.setUserId(authorId);
                suw.setWeek(week);
                suw.setPublications(0);
                suw.setCitations(0);
                suw.setReads(0);
                suw.setRecommendations(0);
                suw.setSharings(0);
                suw.setSavings(0);
                suw.setDownloads(0);
                suw.setFollowings(0);
                suw.setReported(0);
                suw.setUpdatedAt(systemTime);
                statsUserWeeklies.add(suw);
            }
        }
        // 批量插入数据
        if (statsUserWeeklies != null && statsUserWeeklies.size() > 0) {
            if(statsUserWeeklies.size() > 2700){
                List<StatsUserWeekly> statsUserWeekliesHead = statsUserWeeklies.subList(0,2700);
                List<StatsUserWeekly> statsUserWeekliesFoot = statsUserWeeklies.subList(2700,statsUserWeeklies.size());
                statsUserWeeklyMapper.insertStatsUsersWeekly(statsUserWeekliesHead);
                statsUserWeeklyMapper.insertStatsUsersWeekly(statsUserWeekliesFoot);
            } else {
                statsUserWeeklyMapper.insertStatsUsersWeekly(statsUserWeeklies);
            }
        }

        if (operationalBehaviorFlg == true) {
            statsUserWeeklyMapper.updateStatsUserWeeklyIncrease(authorIdsL, actionFlg, week, systemTime);
        } else {
            statsUserWeeklyMapper.updateStatsUserWeeklyReduce(authorIdsL, actionFlg, week, systemTime);
        }
    }

    /**
     * 用户关注的文献表更新
     *
     * @param userId
     * @param publicationId
     * @param operationalBehaviorFlg
     * @param systemTime
     */
    private void upUserPublicationFollowing(Long userId, Long publicationId, Boolean operationalBehaviorFlg, Date systemTime) {
        UserPublicationFollowing userPublicationFollowing = new UserPublicationFollowing();
        if (operationalBehaviorFlg) {
            // 判断用户是否已关注该文献
            int count = userPublicationFollowingMapper.getCount(userId, publicationId, false);
            if (count == 0) {
                // 追加用户关注的文献
                userPublicationFollowing.setUserId(userId);
                userPublicationFollowing.setPublicationId(publicationId);
                userPublicationFollowing.setDeleted(false);
                userPublicationFollowing.setCreatedAt(systemTime);
                userPublicationFollowing.setUpdatedAt(systemTime);
                userPublicationFollowingMapper.insert(userPublicationFollowing);
            } else {
                // 更新创建时间
                userPublicationFollowing.setUserId(userId);
                userPublicationFollowing.setPublicationId(publicationId);
                userPublicationFollowing.setUpdatedAt(systemTime);
                userPublicationFollowingMapper.updateStatsByPrimaryKey(userPublicationFollowing);
            }
        } else {
            // 取消用户关注的文献
            userPublicationFollowing.setUserId(userId);
            userPublicationFollowing.setPublicationId(publicationId);
            userPublicationFollowing.setDeleted(true);
            userPublicationFollowing.setUpdatedAt(systemTime);
            userPublicationFollowingMapper.updateStatsByPrimaryKey(userPublicationFollowing);
        }
    }

    /**
     * 更新用户在读的文章
     *
     * @param userId
     * @param publicationId
     * @param systemTime
     */
    private void upUserPublicationBeingRead(Long userId, Long publicationId, Date systemTime) {
        // 更新用户在读的文章
        // 判断用户是否已阅读过该文章
        int count = userPublicationBeingReadMapper.getCount(userId, publicationId);
        if (count == 0) {
            // 新增阅读记录
            UserPublicationBeingRead userPublicationBeingRead = new UserPublicationBeingRead();
            userPublicationBeingRead.setUserId(userId);
            userPublicationBeingRead.setPublicationId(publicationId);
            userPublicationBeingRead.setCurrentPageNumber(null);
            userPublicationBeingRead.setCreatedAt(systemTime);
            userPublicationBeingRead.setUpdatedAt(systemTime);
            userPublicationBeingReadMapper.insert(userPublicationBeingRead);
        } else {
            // 更新创建时间
            UserPublicationBeingRead userPublicationBeingRead = new UserPublicationBeingRead();
            userPublicationBeingRead.setUserId(userId);
            userPublicationBeingRead.setPublicationId(publicationId);
            userPublicationBeingRead.setUpdatedAt(systemTime);
            userPublicationBeingReadMapper.updateByPrimaryKeySelective(userPublicationBeingRead);
        }
    }

    /**
     * 用户推荐的文献表更新
     *
     * @param userId
     * @param publicationId
     * @param operationalBehaviorFlg
     * @param systemTime
     */
    private void upUserPublicationRecommendation(Long userId, Long publicationId, Boolean operationalBehaviorFlg, Date systemTime) {
        UserPublicationRecommendation userPublicationRecommendation = new UserPublicationRecommendation();
        if (operationalBehaviorFlg) {
            // 判断用户是否已推荐过此文章
            int count = userPublicationRecommendationMapper.getCount(userId, publicationId, false);
            if (count == 0) {
                // 追加用户推荐的文献
                userPublicationRecommendation.setUserId(userId);
                userPublicationRecommendation.setPublicationId(publicationId);
                userPublicationRecommendation.setDeleted(false);
                userPublicationRecommendation.setCreatedAt(systemTime);
                userPublicationRecommendation.setUpdatedAt(systemTime);
                userPublicationRecommendationMapper.insert(userPublicationRecommendation);
            } else {
                // 更新创建时间
                userPublicationRecommendation.setUserId(userId);
                userPublicationRecommendation.setPublicationId(publicationId);
                userPublicationRecommendation.setUpdatedAt(systemTime);
                userPublicationRecommendationMapper.updateStatsByPrimaryKey(userPublicationRecommendation);
            }
        } else {
            // 取消用户推荐的文献
            userPublicationRecommendation.setUserId(userId);
            userPublicationRecommendation.setPublicationId(publicationId);
            userPublicationRecommendation.setDeleted(true);
            userPublicationRecommendation.setUpdatedAt(systemTime);
            userPublicationRecommendationMapper.updateStatsByPrimaryKey(userPublicationRecommendation);
        }
    }

    /**
     * 用户的个人收藏表更新
     *
     * @param userId
     * @param publicationId
     * @param operationalBehaviorFlg
     * @param systemTime
     */
    private void upUserLibrary(Long userId, Long publicationId, Boolean operationalBehaviorFlg, Date systemTime, String folderIds, boolean hasFolder, Long docId) {
        UserLibrary userLibrary = new UserLibrary();
        UserLibraryFolderPublication userLibraryFolderPublication = new UserLibraryFolderPublication();
        if (operationalBehaviorFlg) {
            // 判断该用户是否已收藏该文献
            int count = userLibraryMapper.getCount(userId, publicationId, false);
            if (count == 0) {
                int countDeleted = userLibraryMapper.getCount(userId, publicationId, true);
                if (countDeleted == 0) {
                    // 追加用户的个人收藏
                    userLibrary.setUserId(userId);
                    userLibrary.setPublicationId(publicationId);
                    userLibrary.setDeleted(false);
                    userLibrary.setPublicationGrade("5");
                    userLibrary.setCreatedAt(systemTime);
                    userLibrary.setUpdatedAt(systemTime);
                    userLibraryMapper.insert(userLibrary);
                } else {
                    UserLibraryExample example = new UserLibraryExample();
                    example.createCriteria().andUserIdEqualTo(userId).andPublicationIdEqualTo(publicationId);
                    userLibrary.setDeleted(false);
                    userLibrary.setUpdatedAt(systemTime);
                    userLibraryMapper.updateByExampleSelective(userLibrary,example);
                }
            } else {
                // 更新创建时间
                userLibrary.setUserId(userId);
                userLibrary.setPublicationId(publicationId);
                userLibrary.setUpdatedAt(systemTime);
                userLibraryMapper.updateStatsByPrimaryKey(userLibrary);
                // 删除文章与收藏文件夹关联信息
                userLibraryFolderPublicationMapper.deleteByKey(publicationId);
            }
            if(!StringUtils.isEmpty(folderIds)) {
                String[] folderIdArry = folderIds.split(",");
                for (String folderIdsS : folderIdArry) {
                    // 收藏文件夹id
                    userLibraryFolderPublication.setFolderId(Long.valueOf(folderIdsS));
                    // 文章id
                    userLibraryFolderPublication.setPublicationId(publicationId);
                    // 插入文章与收藏文件夹的关联表
                    userLibraryFolderPublicationMapper.insert(userLibraryFolderPublication);
                }
            }
        } else {
            if(hasFolder) {
                UserLibraryFolderPublicationExample example = new UserLibraryFolderPublicationExample();
                example.createCriteria().andPublicationIdEqualTo(publicationId);
                userLibraryFolderPublicationMapper.deleteByExample(example);
            }
            UserLibraryExample example = new UserLibraryExample();
            example.createCriteria().andUserIdEqualTo(userId).andPublicationIdEqualTo(publicationId);
            userLibraryMapper.deleteByExample(example);

            delPrivatePublication(publicationId, docId);
        }
    }

    /**
     * 用户下载的文献表更新
     *
     * @param userId
     * @param publicationId
     * @param systemTime
     */
    private void upUserPublicationDownloading(Long userId, Long publicationId, Date systemTime) {
        // 判断用户是否已下载该文献
        int count = userPublicationDownloadingMapper.getCount(userId, publicationId);
        UserPublicationDownloadingKey userPublicationDownloading = new UserPublicationDownloadingKey();
        userPublicationDownloading.setUserId(userId);
        userPublicationDownloading.setPublicationId(publicationId);
        userPublicationDownloading.setCreatedAt(systemTime);
        if (count == 0) {
            userPublicationDownloadingMapper.insert(userPublicationDownloading);
        } else {
            // 更新记录创建时间
            userPublicationDownloadingMapper.updateCreatAt(userPublicationDownloading);
        }
    }

    /**
     * 设置用户的文档操作
     * 定义Flg 0:引用 1:阅读 2:推荐 3:分享 4:收藏 5:下载 6:关注 7:举报
     *
     * @param actionFlg
     * @return
     */
    private static String getUserAction(String actionFlg) {
        String userAction = "";
        switch(actionFlg){
            case "2" :
                // 5: recommended publication 用户推荐文献
                userAction = "5";
                break;
            case "4" :
                // 3: saved publication 用户保存文献
                userAction = "3";
                break;
            case "5" :
                // 6: download publication 用户下载文献
                userAction = "6";
                break;
            case "6" :
                // 1: new publication 关注用户的新文献
                userAction = "1";
                break;
            default :
                userAction = "";
                break;
        }
        return userAction;
    }

    /**
     * 删除个人上传私有文献
     *
     * @param publicationId
     * @return
     */
    private void delPrivatePublication(Long publicationId, Long docId) {

        PublicationKey publicationKey = new PublicationKey();
        publicationKey.setPublicationId(publicationId);
        Publication publication = publicationMapper.selectByPrimaryKey(publicationKey);

        if (publicationId != null && !ObjectUtils.isEmpty(publication) && ("3".equals(publication.getStatus())) || "4".equals(publication.getStatus())) {
            if ("3".equals(publication.getStatus())) {
                // 删除私人上传文献作者关系
                PublicationUserInstitutionExample publicationUserInstitutionExample = new PublicationUserInstitutionExample();
                PublicationUserInstitutionExample.Criteria publicationUserInstitutionExampleCriteria = publicationUserInstitutionExample.createCriteria();
                publicationUserInstitutionExampleCriteria.andPublicationIdEqualTo(publicationId);
                publicationUserInstitutionMapper.deleteByExample(publicationUserInstitutionExample);
            }
            // 删除私人上传文献统计字数表
            StatsPublicationTotalExample statsPublicationTotalExample = new StatsPublicationTotalExample();
            StatsPublicationTotalExample.Criteria statsPublicationTotalExampleExampleCriteria = statsPublicationTotalExample.createCriteria();
            statsPublicationTotalExampleExampleCriteria.andPublicationIdEqualTo(publicationId);
            statsPublicationTotalMapper.deleteByExample(statsPublicationTotalExample);

            // 删除私人上传作者年度文献统计
            StatsPublicationYearlyExample statsPublicationYearlExample = new StatsPublicationYearlyExample();
            StatsPublicationYearlyExample.Criteria statsPublicationYearlExampleCriteria = statsPublicationYearlExample.createCriteria();
            statsPublicationYearlExampleCriteria.andPublicationIdEqualTo(publicationId);
            statsPublicationYearlyMapper.deleteByExample(statsPublicationYearlExample);

            // 删除私人上传作者月文献统计
            StatsPublicationMonthlyExample statsPublicationMonthlyExample = new StatsPublicationMonthlyExample();
            StatsPublicationMonthlyExample.Criteria statsPublicationMonthlyExampleExampleCriteria = statsPublicationMonthlyExample.createCriteria();
            statsPublicationMonthlyExampleExampleCriteria.andPublicationIdEqualTo(publicationId);
            statsPublicationMonthlyMapper.deleteByExample(statsPublicationMonthlyExample);

            // 删除私人上传作者周文献统计
            StatsPublicationWeeklyExample statsPublicationWeeklyExample = new StatsPublicationWeeklyExample();
            StatsPublicationWeeklyExample.Criteria statsPublicationWeeklyExampleleCriteria = statsPublicationWeeklyExample.createCriteria();
            statsPublicationWeeklyExampleleCriteria.andPublicationIdEqualTo(publicationId);
            statsPublicationWeeklyMapper.deleteByExample(statsPublicationWeeklyExample);


            // 删除私人上传用户在读的文章
            UserPublicationBeingReadExample userPublicationBeingReadExample = new UserPublicationBeingReadExample();
            UserPublicationBeingReadExample.Criteria userPublicationBeingReadExampleCriteria = userPublicationBeingReadExample.createCriteria();
            userPublicationBeingReadExampleCriteria.andPublicationIdEqualTo(publicationId);
            userPublicationBeingReadMapper.deleteByExample(userPublicationBeingReadExample);

            if (docId != null) {
                DocumentKey documentKey = new DocumentKey();
                documentKey.setDocId(docId);
                Document document = documentMapper.selectByPrimaryKey(documentKey);

                String docPath = document.getDocPath();
                try {
                    // 删除服务器pdf
                    //创建Properties对象
                    Properties prop = new Properties();
                    //读取classPath中的properties文件
                    prop.load(UserAchievementsController.class.getClassLoader().getResourceAsStream("application.properties"));
                    // 文件服务器总路径
                    String filePath = prop.getProperty("keensight.file.server.path");
                    fileHelper.deleteGeneralFile( filePath + docPath );
                } catch (IOException e) {
                    e.printStackTrace();
                }

                // 删除私人上传作者周文献统计
                documentMapper.deleteByPrimaryKey(documentKey);
            }

            // 删除私人上传文献
            publicationMapper.deleteByPrimaryKey(publicationKey);

        }
    }
}
