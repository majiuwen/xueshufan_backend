package com.keensight.web.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.keensight.web.db.entity.SuggestedAnnouncementToView;
import com.keensight.web.db.mapper.SuggestedAnnouncementToViewMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SuggestedAnnouncementToViewService {

    @Resource
    private SuggestedAnnouncementToViewMapper suggestedAnnouncementToViewMapper;

    /**
     * 获取您感兴趣的工作机会
     * @param userId
     * @param index
     * @param rows
     * @return
     */
    public PageInfo<SuggestedAnnouncementToView> getSuggestedAnnouncementToViewList(Long userId, int index, int rows) {
        int pageNum = index;
        int pageSize = rows;
        PageHelper.startPage(pageNum, pageSize);
        List<SuggestedAnnouncementToView> suggestedAnnouncementToViews = suggestedAnnouncementToViewMapper.getSuggestedAnnouncementToViewList(userId);
        return new PageInfo<SuggestedAnnouncementToView>(suggestedAnnouncementToViews);
    }

    /**
     * 获取各类通知，包括广告
     * @param userId
     * @param type
     * @param index
     * @param rows
     * @return
     */
    public PageInfo<SuggestedAnnouncementToView> getAnnouncementList(Long userId, String type, int index, int rows) {
        PageHelper.startPage(index, rows);
        List<SuggestedAnnouncementToView> suggestedAnnouncementToViews = suggestedAnnouncementToViewMapper.getUserAnnouncementList(userId, type);
        return new PageInfo<SuggestedAnnouncementToView>(suggestedAnnouncementToViews);
    }
}
