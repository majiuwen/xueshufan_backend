package com.keensight.web.service;

import com.keensight.web.db.entity.Journal;
import com.keensight.web.db.entity.JournalKey;
import com.keensight.web.db.mapper.JournalMapper;
import com.keensight.web.utils.NormalizedUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 期刊Service
 *
 * @author : newtouch
 * @version : v1.0.0 [2022/01/21] 新建
 */

@Service
public class JournalService {

    @Resource
    private JournalMapper journalMapper;

    /**
     * 根据会议系列Id检索会议实例信息
     *
     * @param JournalId
     * @return
     */
    public Journal getJournalById(Long JournalId) {
        JournalKey conferenceSerieKey = new JournalKey();
        conferenceSerieKey.setJournalId(JournalId);
        Journal journal = journalMapper.selectByPrimaryKey(conferenceSerieKey);
        if (journal == null) {
            journal = new Journal();
        }
        return journal;
    }

    /**
     * 根据期刊Name获取会议实例Id
     * @param journalName
     * @param publisherUrl
     * @return
     */
    public Long getJournalId(String journalName, String publisherUrl) {

        // 获取normalizedName
        String normalizedName = NormalizedUtil.getNormalizedTitle(journalName);

        Long journalId = journalMapper.getJournalId(normalizedName);
        if (journalId == null) {
            journalId = journalMapper.getMaxJournalId() + 1;
            Journal journal = new Journal();
            journal.setJournalId(journalId);
            journal.setNormalizedName(normalizedName);
            journal.setDisplayName(journalName);
            journal.setPublisherUrl(publisherUrl);
            // 新增期刊
            int resCont = journalMapper.insertSelective(journal);
            if (resCont == 0) {
                journalId = null;
            }
        }

        return journalId;
    }

    /**
     * 自动补全期刊名称
     *
     * @param journalName
     * @return
     */
    public List<Journal> getJournalList(String journalName) {

        List<Journal> journalNameList = journalMapper.selectJournalNameList(journalName);

        return journalNameList;
    }
}
