package com.keensight.web.service;

import com.keensight.web.db.entity.StatsPublicationYearly;
import com.keensight.web.db.mapper.StatsPublicationYearlyMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 文献年度统计Service
 *
 * @author : newtouch
 * @version : v1.0.0 [2021/10/26] 新建
 */
@Service
public class StatsPublicationYearlyService {
    @Resource
    private StatsPublicationYearlyMapper statsPublicationYearlyMapper;

    /**
     * 根据文献ID获取文献年度统计信息
     * @param publicationId
     * @return
     */
    public List<StatsPublicationYearly> getStatsPublicationYearlyList(int publicationId) {
        List<StatsPublicationYearly> statsPublicationYearlyInfo = statsPublicationYearlyMapper.getStatsPublicationYearlyList(publicationId);
        if(statsPublicationYearlyInfo == null) {
            statsPublicationYearlyInfo = new ArrayList<>();
        }
        return statsPublicationYearlyInfo;
    }
}
