package com.keensight.web.constants.enums;

/**
 * 常用文件的文件头枚举类
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/23] 新建
 */
public enum TopItemType {
    /** 作者 */
    AUTHOR("author"),
    /** 期刊 */
    JOURNAL("journal"),
    /** 会议 */
    CONFERENCE("conference"),
    /** 研究领域 */
    FIELD("field"),
    /** 文献类型 */
    DOCTYPE("doctype"),
    /** 机构 */
    INSTITUTION("institution");

    /** 文件类型 */
    private final String type;

    TopItemType(String type) {
        this.type = type;
    }

    /**
     * 获取 类型
     *
     * @return type 类型
     */
    public String getType() {
        return type;
    }
}
