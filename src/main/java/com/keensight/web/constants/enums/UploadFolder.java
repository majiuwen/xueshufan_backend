package com.keensight.web.constants.enums;

/**
 * 上传文件夹举类
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/23] 新建
 */
public enum UploadFolder {
    /** PDF */
    UPLOAD("upload"),
    /** PDF */
    PDF("pdf_fulltext"),
    /** PDF缩略图 */
    PDF_THUMBNAIL("pdf_thumbnail"),
    /** 图片 */
    JPEG("jpg"),
    /** 头像 */
    PROFILE_PHOTO("profile_photo");

    /** 文件夹 */
    private final String folderPath;

    UploadFolder(String folderPath) {
        this.folderPath = folderPath;
    }

    /**
     * 获取 文件夹
     *
     * @return folderPath 文件夹
     */
    public String getFolderPath() {
        return folderPath;
    }
}
