package com.keensight.web.constants.enums;

/**
 * 常用文件的文件头枚举类
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/23] 新建
 */
public enum FileType {
    /**
     * 常用文件的文件头枚举类
     */
    PDF("pdf", "255044462D312E"),
    JPEG("jpg", "FFD8FF"),
    PNG("png", "89504E47"),
    GIF("gif", "47494638"),
    XML("xml", "3C3F786D6C"),
    HTML("html", "68746D6C3E"),
    ZIP("zip", "504B0304"),
    RAR("rar", "52617221"),
    AVI("avi", "41564920");

    /** 文件头 */
    private final String fileHeader;
    /** 文件类型 */
    private final String fileType;

    /**
     *
     * @param fileType
     * @param fileHeader
     */
    FileType(String fileType, String fileHeader) {
        this.fileType = fileType;
        this.fileHeader = fileHeader;
    }

    /**
     * 获取 文件头
     *
     * @return fileHeader 文件头
     */
    public String getFileHeader() {
        return fileHeader;
    }

    /**
     * 获取 文件类型
     *
     * @return fileType 文件类型
     */
    public String getFileType() {
        return fileType;
    }
}
