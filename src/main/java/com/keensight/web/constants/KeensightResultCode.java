package com.keensight.web.constants;

public enum KeensightResultCode {
    /** 自定义错误(9000X) */

    // 身份验证用
    System_90001("90001"),
    // 关注错误
    System_90002("90002"),
    // 下载失败
    System_90003("90003");

    public final String code;

    private KeensightResultCode(String code) {
        this.code = code;
    }

    public String code() {
        return code;
    }
}
