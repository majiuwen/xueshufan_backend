package com.keensight.web.constants;

public class WebConst {
    /** 【代码区分】文献类型 */
    public static final String GROUP_CD_DOC_TYPE = "doc_type";
    /** 【代码区分】文档类型 */
    public static final String GROUP_CD_DOCUMENT_TYPE = "document_type";
    /** 文献类型所属 论文 */
    public static final String DOC_TYPE_BELONG = "paper";
    /** 上传全文按钮显示Flg 不显示 */
    public static final String UPLOADPDF_BTN_FLG_HIDE = "0";
    /** 上传全文按钮显示Flg 显示上传全文 */
    public static final String UPLOADPDF_BTN_FLG_UPLOAD_PDF = "1";
    /** 上传全文按钮显示Flg 显示有全文 */
    public static final String UPLOADPDF_BTN_FLG_EXIST_PDF = "2";
    /** 文档公开 */
    public static final String PUBLIC_DOC = "1";
    /** 时间格式 年月 */
    public static final String DATE_FORMAT_YEAR_MONTH = "yyyyMM";
    /** 文件后缀 pdf */
    public static final String FILE_SUFFIX_PDF = ".pdf";
    /** 文件后缀 jpg */
    public static final String FILE_SUFFIX_JPEG = ".jpg";
}
