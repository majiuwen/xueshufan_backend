package com.keensight.web.utils;

public class ContentSafetyResult {

    /**
     * 图片
     */
    public static final int TYPE_PHOTO=1;
     /*
     * 文字
     */
    public static final int TYPE_TEXT=2;

    /**
     * 文本正常，可以直接放行
     */
    public static final String SUGGESTION_PASS="pass";
    /**
     * 文本需要进一步人工审核
     */
    public static final String SUGGESTION_REVIEW="review";
    /**
     * 文本违规，可以直接删除或者限制公开
     */
    public static final String SUGGESTION_BLOCK="block";

    private Integer type;
    private String  content;
    private String  scene;
    private String  suggestion;
    private String  label;
    public Integer getType() {
        return type;
    }
    public void setType(Integer type) {
        this.type = type;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public String getScene() {
        return scene;
    }
    public void setScene(String scene) {
        this.scene = scene;
    }
    public String getSuggestion() {
        return suggestion;
    }
    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "ContentSafetyResult [type=" + type + ", content=" + content + ", scene=" + scene + ", suggestion="
                + suggestion + ", label=" + label + "]";
    }
}
