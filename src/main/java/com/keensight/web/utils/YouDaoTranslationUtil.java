package com.keensight.web.utils;

import com.alibaba.fastjson.JSONObject;
import com.keensight.web.model.SummaryTranslationModel;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class YouDaoTranslationUtil {
    private static Logger logger = LoggerFactory.getLogger(YouDaoTranslationUtil.class);
    // 文本翻译API HTTPS地址
    private static final String YOUDAO_URL = "https://openapi.youdao.com/api";
    // 应用ID
    private static final String APP_KEY = "2f0f9c8b00d3fcec";
    // 应用密钥
    private static final String APP_SECRET = "6ip2XsEqvlb1j2IrwHu2YSsAwdoVWu0z";
    // 源语言
    private static final String FROM = "EN";
    // 目标语言
    private static final String TO = "zh-CHS";
    // 签名类型
    private static final String SIGN_TYPE = "v3";

    public static String getTranslationText(String translationText) throws IOException {
        Map<String,String> params = new HashMap<String,String>();
        // 待翻译文本
        String q = translationText;
        // UUID
        String salt = String.valueOf(System.currentTimeMillis());
        params.put("from", FROM);
        params.put("to", TO);
        params.put("signType", SIGN_TYPE);
        // 当前UTC时间戳(秒)
        String curtime = String.valueOf(System.currentTimeMillis() / 1000);
        params.put("curtime", curtime);
        String signStr = APP_KEY + truncate(q) + salt + curtime + APP_SECRET;
        // 签名
        String sign = getDigest(signStr);
        params.put("appKey", APP_KEY);
        params.put("q", q);
        params.put("salt", salt);
        params.put("sign", sign);
        params.put("strict", "true");
        //** 处理结果 *//*
        return requestForHttp(YOUDAO_URL,params);
    }

    public static String requestForHttp(String url,Map<String,String> params) throws IOException {
        String description = "";
        /** 创建HttpClient */
        CloseableHttpClient httpClient = HttpClients.createDefault();

        /** httpPost */
        HttpPost httpPost = new HttpPost(url);
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
        Iterator<Map.Entry<String,String>> it = params.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry<String,String> en = it.next();
            String key = en.getKey();
            String value = en.getValue();
            paramsList.add(new BasicNameValuePair(key,value));
        }
        httpPost.setEntity(new UrlEncodedFormEntity(paramsList,"UTF-8"));
        CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
        try{
            Header[] contentType = httpResponse.getHeaders("Content-Type");
            logger.info("Content-Type:" + contentType[0].getValue());
            HttpEntity httpEntity = httpResponse.getEntity();
            String json = EntityUtils.toString(httpEntity,"UTF-8");
            SummaryTranslationModel summaryTranslationModel = JSONObject.parseObject(json, SummaryTranslationModel.class);
            if (summaryTranslationModel != null) {
                List<String> translationList = summaryTranslationModel.getTranslation();
                if (translationList.size() > 0) {
                    description = summaryTranslationModel.getTranslation().get(0);
                }
            }
        }finally {
            try{
                if(httpResponse!=null){
                    httpResponse.close();
                }
            }catch(IOException e){
                logger.info("## release resouce error ##" + e);
            }
        }
        return description;
    }

    /**
     * 生成加密字段
     */
    public static String getDigest(String string) {
        if (string == null) {
            return null;
        }
        char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        byte[] btInput = string.getBytes(StandardCharsets.UTF_8);
        try {
            MessageDigest mdInst = MessageDigest.getInstance("SHA-256");
            mdInst.update(btInput);
            byte[] md = mdInst.digest();
            int j = md.length;
            char[] str = new char[j * 2];
            int k = 0;
            for (byte byte0 : md) {
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    public static String truncate(String q) {
        if (q == null) {
            return null;
        }
        int len = q.length();
        String result;
        return len <= 20 ? q : (q.substring(0, 10) + len + q.substring(len - 10, len));
    }
}
