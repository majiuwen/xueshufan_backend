/**
 *
 */
package com.keensight.web.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;


/**
 * @author m-shichi@SYNC Co., Ltd.
 *
 */
public class AppMediaTask

{

	private File mOrigin = null;
	private File mThumnail = null;

	private MediaInfo mediaInfo = null;

	public AppMediaTask(File origin, File thumnail)
	{
		this.mOrigin = origin;
		this.mThumnail = thumnail;
	}

	/**
	 *
	 * @return
	 */
	public boolean outThumnailFile()
	{
		boolean ret = false;
		try
		{

			Ffmpeg4J ffmpeg = new Ffmpeg4J();

			mediaInfo = ffmpeg.getMediaInfo(mOrigin.getAbsolutePath());

			int start = mediaInfo.getDuration() != 0 ? mediaInfo.getDuration() / 3 : 0;

			ffmpeg.addParameter("-i");
			ffmpeg.addParameter(mOrigin.getAbsolutePath());
			ffmpeg.addParameter("-vframes");
			ffmpeg.addParameter("1");
			ffmpeg.addParameter("-an");
			ffmpeg.addParameter("-ss");
			ffmpeg.addParameter(String.valueOf(start));
			ffmpeg.addParameter("-s");
			if(mediaInfo.getRotate() == 90 || mediaInfo.getRotate() == 270){
				ffmpeg.addParameter(String.format("%dx%d", mediaInfo.getHeight() / 2, mediaInfo.getWidth() / 2));
			}else {
				ffmpeg.addParameter(String.format("%dx%d", mediaInfo.getWidth() / 2, mediaInfo.getHeight() / 2));
			}

			ffmpeg.addParameter("-f");
			ffmpeg.addParameter("image2");
			ffmpeg.addParameter(mThumnail.getAbsolutePath());

			ffmpeg.execute();

			if (mThumnail.exists())
			{
				ret = true;
			}
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return ret;
	}

	/**
	 *
	 * @return
	 */
	public boolean outFileToMp4()
	{
		boolean ret = false;
		try
		{

			Ffmpeg4J ffmpeg = new Ffmpeg4J();

			mediaInfo = ffmpeg.getMediaInfo(mOrigin.getAbsolutePath());

			ffmpeg.addParameter("-y");
			ffmpeg.addParameter("-i");
			ffmpeg.addParameter(mOrigin.getAbsolutePath());
			ffmpeg.addParameter("-ar");
			ffmpeg.addParameter("44100");
			ffmpeg.addParameter("-ac");
			ffmpeg.addParameter("2");
			ffmpeg.addParameter("-acodec");
			ffmpeg.addParameter("mp3");
			ffmpeg.addParameter("-ab");
			ffmpeg.addParameter("128k");
			ffmpeg.addParameter("-q:a");
			ffmpeg.addParameter("4");
			ffmpeg.addParameter("-qcomp");
			ffmpeg.addParameter("1");
			ffmpeg.addParameter("-bufsize");
			ffmpeg.addParameter("2000k");
			ffmpeg.addParameter("-maxrate");
			ffmpeg.addParameter("2000k");
			ffmpeg.addParameter("-s");
			ffmpeg.addParameter(mediaInfo.getWidth() + "x" + mediaInfo.getHeight());
			ffmpeg.addParameter(mThumnail.getAbsolutePath());

			ffmpeg.execute();

			if (mThumnail.exists())
			{
				ret = true;
			}
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return ret;
	}

	public MediaInfo getMediaInfo() {
		return mediaInfo;
	}

	public static String getCreationTime(File file) {
		if (file == null) {
			return null;
		}

		BasicFileAttributes attr = null;
		try {
			Path path =  file.toPath();
			attr = Files.readAttributes(path, BasicFileAttributes.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		// 创建时间
		Instant createInstant = attr.creationTime().toInstant();
		Instant updateInstant = attr.lastModifiedTime().toInstant();
		String format = "";
		if(createInstant.compareTo(updateInstant) < 0){
			format = DateTimeFormatter.ofPattern("yyyyMMdd HHmm").withZone(ZoneId.systemDefault()).format(createInstant);
		}else {
			format = DateTimeFormatter.ofPattern("yyyyMMdd HHmm").withZone(ZoneId.systemDefault()).format(updateInstant);
		}
		return format;
	}

	public static void main(String[] args) {
		File fileO = new File("D:\\uploadTest\\test.mp4");
		File fileT = new File("D:\\uploadTest\\111.mp4");
		AppMediaTask appMediaTask = new AppMediaTask(fileO, fileT);
		appMediaTask.outFileToMp4();

//		File fileO = new File("C:\\nmFiles\\files\\video\\20210818094147.mp4");
//		File fileT = new File("C:\\nmFiles\\files\\video\\20210818094147.jpg");
//		AppMediaTask appMediaTask = new AppMediaTask(fileO, fileT);
//		appMediaTask.outThumnailFile();
	}
}
