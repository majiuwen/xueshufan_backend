package com.keensight.web.utils;

import com.amazonaws.*;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.s3.transfer.MultipleFileUpload;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;

import java.io.*;
import java.net.URI;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Properties;

public class AwsS3Utils {
    static{
        System.setProperty(SDKGlobalConfiguration.ENABLE_S3_SIGV4_SYSTEM_PROPERTY, "true");
    }

    /**
     * 获取s3预签名url
     *
     * @param objectKey   文件key
     * @return s3预签名url
     */
    public static String signUrl(String objectKey) {
        String s3Url = null;
        try {
            //test
            Properties prop = new Properties();
            prop.load(PDFUtil.class.getClassLoader().getResourceAsStream("application.properties"));
            String accessKey = prop.getProperty("aws_s3.api.accessKey");
            String secretKey = prop.getProperty("aws_s3.api.secretKey");
            String region = prop.getProperty("aws_s3.api.region");
            //桶名称
            String bucketName = prop.getProperty("aws_s3.api.bucketName");

            try {
                AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);

                ClientConfiguration config = new ClientConfiguration();
                String proxyHost = System.getProperty("http.proxyHost");
                String proxyPort = System.getProperty("http.proxyPort");
                if (proxyHost != null && proxyPort != null) {
                    config.setProxyHost(proxyHost);
                    config.setProxyPort(Integer.valueOf(proxyPort));
                }
                AmazonS3 s3 = new AmazonS3Client(credentials, config);

//            s3.setRegion(com.amazonaws.regions.Region.getRegion(Regions.CN_NORTH_1));
                s3.setRegion(com.amazonaws.regions.Region.getRegion(Regions.fromName(region)));

                // 设置过期时间为10分钟
                java.util.Date expiration = new java.util.Date();
                long expTimeMillis = expiration.getTime();
                expTimeMillis += 1000 * 60 * 10;
                expiration.setTime(expTimeMillis);

                //生成预签名URL
                System.out.println("生成预签名URL");
                GeneratePresignedUrlRequest generatePresignedUrlRequest =
                        new GeneratePresignedUrlRequest(bucketName, objectKey)
                                .withMethod(HttpMethod.GET)
                                .withExpiration(expiration);
                URL url = s3.generatePresignedUrl(generatePresignedUrlRequest);

                System.out.println("生成的签名的URL: " + url.toString());
                s3Url = url.toString();
            }
            catch(AmazonServiceException e) {
                //调用成功传输，但Amazon S3无法处理
                //它返回了一个错误响应。
                e.printStackTrace();
            }
            catch(SdkClientException e) {
                //无法联系到Amazon S3以获得响应或客户端
                //无法解析来自Amazon S3的响应。
                e.printStackTrace();
            };
        } catch (IOException e) {
            e.printStackTrace();
        }

        return s3Url;
    }

    private static AmazonS3 amazonS3() {
        AmazonS3 s3Client = null;
        try {
            //test
            Properties prop = new Properties();
            prop.load(PDFUtil.class.getClassLoader().getResourceAsStream("application.properties"));
            String accessKey = prop.getProperty("aws_s3.api.accessKey");
            String secretKey = prop.getProperty("aws_s3.api.secretKey");
            String region = prop.getProperty("aws_s3.api.region");
            //桶名称
            String bucketName = prop.getProperty("aws_s3.api.bucketName");

            AWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
            AmazonS3ClientBuilder builder = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(awsCredentials));
            //设置S3的地区
            builder.setRegion(region);
            s3Client = builder.build();
        } catch (IOException e) {
            e.printStackTrace();
            s3Client = null;
        }

        return s3Client;
    }

    /**
     * s3服务器上传
     *
     * @param filePathName   文件路径名
     */
    public static PutObjectResult upload(String filePathName, InputStream inputStream) throws IOException {
        PutObjectResult putObjectRequest = null;
        try {
            //test
            Properties prop = new Properties();
            prop.load(PDFUtil.class.getClassLoader().getResourceAsStream("application.properties"));
            String accessKey = prop.getProperty("aws_s3.api.accessKey");
            String secretKey = prop.getProperty("aws_s3.api.secretKey");
            String region = prop.getProperty("aws_s3.api.region");
            //桶名称
            String bucketName = prop.getProperty("aws_s3.api.bucketName");

            AmazonS3 amazonS3Client =  amazonS3();
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentType("plain/text");
            objectMetadata.setContentLength(Long.valueOf(inputStream.available()));
            putObjectRequest = amazonS3Client.putObject(new PutObjectRequest(bucketName, filePathName, inputStream, objectMetadata));
        } catch (IOException e) {
            e.printStackTrace();
            putObjectRequest = null;
        }

        return putObjectRequest ;
    }

    /**
     * 获取s3服务器文件路径
     *
     * @param key_name   获取文件key
     */
    public static String getS3FileUrl (String key_name) {
        String url = null;
        try {
            //test
            Properties prop = new Properties();
            prop.load(PDFUtil.class.getClassLoader().getResourceAsStream("application.properties"));
            String accessKey = prop.getProperty("aws_s3.api.accessKey");
            String secretKey = prop.getProperty("aws_s3.api.secretKey");
            String region = prop.getProperty("aws_s3.api.region");
            //桶名称
            String bucketName = prop.getProperty("aws_s3.api.bucketName");
            key_name = key_name.replaceAll("%", "%25");
            key_name = key_name.replaceAll("=", "%3D");
            url = "https://" + bucketName + ".s3." + region + ".amazonaws.com.cn/" + key_name;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return url;
    }
}
