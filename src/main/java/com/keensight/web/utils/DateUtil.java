package com.keensight.web.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    /**
     * 时间差的计算
     *
     * @param createdAt
     * @param nowDate
     * @return
     * @throws ParseException
     */
    private static String getDistanceTime(String nowDate, String createdAt) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date one;
        Date two;
        long day = 0;
        long hour = 0;
        long min = 0;
        long sec = 0;
        one = df.parse(nowDate);
        two = df.parse(createdAt);
        long time1 = one.getTime();
        long time2 = two.getTime();
        long diff = time1 - time2;
        day = diff / (24 * 60 * 60 * 1000);
        hour = (diff / (60 * 60 * 1000) - day * 24);
        min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
        sec = (diff / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        return day + "," + hour + "," + min + "," + sec + ",";
    }

    /**
     * 经过时间
     *
     * @param createdAt
     * @param nowDate
     * @return
     */
    public static String validateTime(Date nowDate, Date createdAt) throws ParseException {
        String validateTime = null;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String nowTime = formatter.format(nowDate);
        String createdTime = formatter.format(createdAt);
        String time = getDistanceTime(nowTime, createdTime);
        String[] arrTime = time.split(",");
        int day = Integer.parseInt(arrTime[0]);
        int hour = Integer.parseInt(arrTime[1]);
        int min = Integer.parseInt(arrTime[2]);
        int sec = Integer.parseInt(arrTime[3]);
        if (day == 0) {
            if (hour == 0) {
                if (min <= 5) {
                    validateTime = "刚刚";
                } else {
                    validateTime = min + "分钟前";
                }
            } else {
                if (hour < 12) {
                    validateTime = hour + "小时前";
                } else {
                    validateTime = "今天";
                }
            }
        } else {
            if (day == 1) {
                validateTime = "昨天";
            } else if (day <= 7){
                validateTime = day + "天前";
            } else {
                int mon = day / 31;
                if (mon > 12) {
                    SimpleDateFormat formatter1 = new SimpleDateFormat("yy-MM-dd");
                    validateTime = formatter1.format(createdAt);
                } else {
                    SimpleDateFormat formatter1 = new SimpleDateFormat("MM-dd");
                    validateTime = formatter1.format(createdAt);
                }
            }
        }
        return validateTime;
    }

    /**
     * 获取当前年
     *
     * @return
     */
    public static Integer getYear() {
        Calendar cal = Calendar.getInstance();
        Integer year = cal.get(Calendar.YEAR);
        return year;
    }

    /**
     * 返回当月最后一天的日期
     *
     * @param date
     * @return
     */
    public static Date getLastDayOfMonth(Date date) {
        Calendar calendar = convert(date);
        calendar.set(Calendar.DATE, 1);
        calendar.roll(Calendar.DATE, -1);
        return calendar.getTime();
    }

    /**
     * 返回当周周日
     *
     * @param date
     * @return
     */
    public static Date getLastDayOfWeek(Date date) {
        Calendar calendar = convert(date);
        // 如果是周日直接返回
        if (calendar.get(Calendar.DAY_OF_WEEK) == 1) {
            return date;
        }
        calendar.add(Calendar.DATE, 7 - calendar.get(Calendar.DAY_OF_WEEK) + 1);
        return calendar.getTime();
    }

    /**
     * 获取上周日时间
     *
     * @param date
     * @return
     */
    public static Date getLastMonday(Date date) {
        Calendar c = Calendar.getInstance();
        // 减去一个星期
        c.add(Calendar.WEEK_OF_MONTH, -1);
        // 上个星期的今天是第几天,星期天是1,所以要减去1
        int d = c.get(Calendar.DAY_OF_WEEK) - 1;
        // 添加余下的天数
        c.add(Calendar.DAY_OF_WEEK, 7 - d);
        return c.getTime();
    }

    /**
     * 将日期转换为日历
     *
     * @param date 日期
     * @return 日历
     */
    private static Calendar convert(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    /**
     * 格式化日期
     *
     * @param date
     * @return
     */
    public static String dateFormat(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        return sdf.format(date);
    }


    /**
     * 系统时间取得
     *
     * @return
     */
    public static Date getSysDate() {
        Calendar calDefault = Calendar.getInstance();
        Date dateToday = calDefault.getTime();
        return dateToday;
    }

    /**
     * 指定格式的系统时间取得
     *
     * @param strDateFormat
     * @return
     */
    public static String getSysDate(String strDateFormat) {
        SimpleDateFormat sdf = null;
        String strNowDate = "";
        if (strDateFormat == null || strDateFormat.trim().length() == 0) {
            return strNowDate;
        }
        try {
            Date dateToday = getSysDate();
            sdf = new SimpleDateFormat(strDateFormat);
            strNowDate = sdf.format(dateToday);
        } catch (Exception e) {
            strNowDate = "";
            e.printStackTrace();
        } finally {
            if (sdf != null) {
                sdf = null;
            }
        }
        return strNowDate;
    }

    /**
     * 获取当前年最后一天
     *
     * @return
     */
    public static Date getCurrYearLast(){
        Calendar currCal= Calendar.getInstance();
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR,currCal.get(Calendar.YEAR));
        calendar.roll(Calendar.DAY_OF_YEAR,-1);
        return calendar.getTime();
    }

    /**
     * 格式化日期
     *
     * @param date
     * @return
     */
    public static String dateFormat(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }
}
