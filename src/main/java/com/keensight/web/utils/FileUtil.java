package com.keensight.web.utils;

import com.alibaba.druid.util.StringUtils;
import com.keensight.web.constants.enums.FileType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * 文件处理工具类
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/09] 新建
 */
public class FileUtil {
    /** logger */
    private static final Logger logger = LoggerFactory.getLogger(FileUtil.class);

    /**
     * 文件上传操作
     *
     * @param inputStream 上传的文件流
     * @param fileName    上传的文件名
     * @param filePath    上传的路径
     * @return 上传结果
     */
    public static boolean fileUpload(InputStream inputStream, String fileName, String filePath) {
        boolean uploadResult = true;
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        try {
            logger.debug("----------文件上传操作开始----------");

            File folder = new File(filePath);
            // 创建目录
            if (!folder.exists() || !folder.isDirectory()) {
                if (!folder.mkdirs()) {
                    return false;
                }
            }
            // 创建文件
            File file = new File(filePath + fileName);
            // 给文件写入内容
            bis = new BufferedInputStream(inputStream);
            byte[] buf = new byte[4096];
            bos = new BufferedOutputStream(new FileOutputStream(file));
            int bytesIn;
            while ((bytesIn = bis.read(buf, 0, buf.length)) != -1) {
                bos.write(buf, 0, bytesIn);
            }
            bos.flush();

            logger.debug("----------文件上传操作结束----------");
        } catch (Exception e) {
            uploadResult = false;
            logger.error("----------文件上传操作出错----------", e);
        } finally {
            try {
                if (bos != null) {
                    bos.close();
                }
                if (bis != null) {
                    bis.close();
                }
            } catch (Exception ex) {
            }
        }
        return uploadResult;
    }

    /**
     * 是否是指定类型的文件（多种文件类型的场合）
     *
     * @param inputStream 要判断的文件流
     * @param fileTypes   指定的文件类型数组
     * @return
     */
    public static boolean isSpecifyTypesFile(InputStream inputStream, FileType[] fileTypes) {
        boolean isSpecifyTypeFile = false;
        String hexString4FileHeader = getHexString4FileHeader(inputStream);
        for (FileType fileType : fileTypes) {
            if (!StringUtils.isEmpty(hexString4FileHeader) && hexString4FileHeader.toUpperCase().startsWith(fileType.getFileHeader().toUpperCase())) {
                isSpecifyTypeFile = true;
                break;
            }
        }
        return isSpecifyTypeFile;
    }

    /**
     * 是否是指定类型的文件（一种文件类型的场合）
     *
     * @param inputStream 要判断的文件流
     * @param fileType    指定的文件类型
     * @return
     */
    public static boolean isSpecifyTypeFile(InputStream inputStream, FileType fileType) {
        boolean isSpecifyTypeFile = false;
        String hexString4FileHeader = getHexString4FileHeader(inputStream);
        if (!StringUtils.isEmpty(hexString4FileHeader) && hexString4FileHeader.toUpperCase().startsWith(fileType.getFileHeader().toUpperCase())) {
            isSpecifyTypeFile = true;
        }
        return isSpecifyTypeFile;
    }

    /**
     * 获取文件的文件头对应的16进制字符串
     *
     * @param inputStream 文件流
     * @return
     */
    public static String getHexString4FileHeader(InputStream inputStream) {
        StringBuffer hexString4fileHeader = null;
        if (inputStream != null) {
            hexString4fileHeader = new StringBuffer();
            byte[] readByte = new byte[28];
            try {
                // 获取流中的文件头
                inputStream.read(readByte, 0, readByte.length);
                // 将文件头转换为16进制字符串
                for (byte binaryHeader : readByte) {
                    int hexHeader = binaryHeader & 0xFF;
                    String strHeader = Integer.toHexString(hexHeader);
                    if (strHeader.length() < 2) {
                        hexString4fileHeader.append(0);
                    }
                    hexString4fileHeader.append(strHeader);
                }
            } catch (IOException e) {
                logger.error("获取文件的文件头对应的16进制字符串出错", e);
            } finally {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
        return hexString4fileHeader != null ? hexString4fileHeader.toString() : null;
    }

    /**
     * 判断文件大小
     *
     * @param len
     *            文件长度
     * @param size
     *            限制大小
     * @param unit
     *            限制单位（B,K,M,G）
     * @return
     */
    public static boolean checkFileSize(Long len, int size, String unit) {
//        long len = file.length();
        double fileSize = 0;
        if ("B".equals(unit.toUpperCase())) {
            fileSize = (double) len;
        } else if ("K".equals(unit.toUpperCase())) {
            fileSize = (double) len / 1024;
        } else if ("M".equals(unit.toUpperCase())) {
            fileSize = (double) len / 1048576;
        } else if ("G".equals(unit.toUpperCase())) {
            fileSize = (double) len / 1073741824;
        }
        if (fileSize > size) {
            return false;
        }
        return true;
    }
}
