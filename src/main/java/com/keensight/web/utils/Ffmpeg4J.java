/**
 *
 */
package com.keensight.web.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SystemUtils;



/**
 * @author m-shichi@SYNC Co., Ltd.
 *
 */
public class Ffmpeg4J
{
	private List<String> cmdList = null;

	public Ffmpeg4J()
	{
		cmdList = new ArrayList<String>();
		cmdList.add(getAppPath());
	}

	/**
	 *
	 * @return
	 */
	public String getAppPath()
	{
		String path = null;
		try {
			if (SystemUtils.IS_OS_WINDOWS)
			{
				path = "D:\\ffmpeg\\bin\\ffmpeg.exe";
			}
			else
			{
				path = "ffmpeg";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return path;
	}

	/**
	 *
	 * @param param
	 */
	public void addParameter(String param)
	{
		cmdList.add(param);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	public MediaInfo getMediaInfo(String path)
	{
		BufferedReader br = null;
		Process process = null;
		MediaInfo mediaInfo = new MediaInfo();
		try
		{
			List<String> params = new ArrayList<String>();
			params.add(getAppPath());
			params.add("-i");
			params.add(path);

			ProcessBuilder pbuilder = new ProcessBuilder(params);
			pbuilder.redirectErrorStream(true);

			process = pbuilder.start();

			br = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line;
			while ((line = br.readLine()) != null)
			{
				if (!"".equals(line))
				{

					if (line.indexOf("Stream") != -1 && line.indexOf("Video:") != -1)
					{
						String[] args = line.split(",");
						for (String arg : args)
						{
							if (arg.indexOf("x") != -1)
							{
								String[] a1 = arg.trim().split(" ");
								String[] wh = a1[0].split("x");
								if (wh.length == 2)
								{
									System.out.println(String.format("幅:[%s] 高さ;[%s]", wh[0], wh[1]));
									mediaInfo.setWidth(Integer.parseInt(wh[0]));
									mediaInfo.setHeight(Integer.parseInt(wh[1]));
								}
							}
						}
					}
					else if (line.indexOf("Duration:") != -1)
					{
						String[] args = line.split(",");
						for (String arg : args)
						{
							if (arg.trim().startsWith("Duration:"))
							{
								int idx = arg.indexOf(":", 0);
								String time = arg.substring(idx + 1).trim();

								String[] hms = time.split(":");
								int duration = Integer.parseInt(hms[0]) * 60 * 60 + Integer.parseInt(hms[1]) * 60 + Integer.parseInt(hms[2].substring(0, 2));
								System.out.println(String.format("時間:[%d]秒", duration));
								mediaInfo.setDuration(duration);
							}
						}
					}
					else if (line.indexOf("rotate") != -1)
					{
						String[] args = line.split(",");
						for (String arg : args)
						{
							if (arg.trim().startsWith("rotate"))
							{
								int idx = arg.indexOf(":", 0);
								int rotate = Integer.parseInt(arg.substring(idx + 1).trim());

								System.out.println(String.format("ビデオの向き:[%d]°", rotate));
								mediaInfo.setRotate(rotate);
							}
						}
					}
					else
					{
					}
				}
			}
			process.waitFor();
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
		catch (InterruptedException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			if (null != br)
			{
				try
				{
					br.close();
				}
				catch (IOException ex)
				{}
			}
			if (null != process)
			{
				process.destroy();
			}
		}

		return mediaInfo;
	}
	/**
	 *
	 * @return
	 * @throws Exception
	 */
	public int execute() throws Exception
	{
		// 実行結果
		int iValue = 0;
		BufferedReader br = null;
		Process process = null;
		try
		{
			ProcessBuilder pbuilder = new ProcessBuilder(cmdList);
			pbuilder.redirectErrorStream(true);

			process = pbuilder.start();

			br = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line;
			while ((line = br.readLine()) != null)
			{
				if (!"".equals(line))
				{
					System.out.println(line);
				}
			}
			process.waitFor();

			// 実行結果
			iValue = process.exitValue();

			if (iValue > 1)
			{
				throw new Exception(String.valueOf(iValue) + ":" + line);
			}

		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			if (null != br)
			{
				try
				{
					br.close();
				}
				catch (IOException ex)
				{}
			}
			if (null != process)
			{
				process.destroy();
			}
		}

		return iValue;
	}
}
