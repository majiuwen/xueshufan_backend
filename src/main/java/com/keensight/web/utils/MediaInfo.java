/**
 *
 */
package com.keensight.web.utils;

/**
 * @author m-shichi@SYNC Co., Ltd.
 *
 */
public class MediaInfo
{
	private int width = 0;
	private int height = 0;
	private int duration = 0;
	private int rotate = 0;
	/**
	 * @return width
	 */
	public int getWidth()
	{
		return width;
	}
	/**
	 * @return height
	 */
	public int getHeight()
	{
		return height;
	}
	/**
	 * @return duration
	 */
	public int getDuration()
	{
		return duration;
	}
	/**
	 * @return rotate
	 */
	public int getRotate()
	{
		return rotate;
	}
	/**
	 * @param width
	 *            セットする width
	 */
	public void setWidth(int width)
	{
		this.width = width;
	}
	/**
	 * @param height
	 *            セットする height
	 */
	public void setHeight(int height)
	{
		this.height = height;
	}
	/**
	 * @param duration
	 *            セットする duration
	 */
	public void setDuration(int duration)
	{
		this.duration = duration;
	}
	/**
	 * @param rotate
	 *            セットする rotate
	 */
	public void setRotate(int rotate)
	{
		this.rotate = rotate;
	}

}
