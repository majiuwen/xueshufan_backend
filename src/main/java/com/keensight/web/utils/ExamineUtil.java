package com.keensight.web.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.green.model.v20180509.ImageSyncScanRequest;
import com.aliyuncs.green.model.v20180509.TextScanRequest;
import com.aliyuncs.http.FormatType;
import com.aliyuncs.http.HttpResponse;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.http.ProtocolType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.Normalizer;
import java.util.*;

/**
 *"porn","terrorism","ad","live","qrcode"
 */
public class ExamineUtil {
    static String ALIYUN_ACCESS_KEY_ID = "LTAI5tBevJ9kyjKAgkjQWFhR";
    static String ALIYUN_ACCESS_KEY_SECRET = "mTd3qKVnfVj2oZFsz1Mb5oDuPcsC4t";
    static String REGION_ID = "cn-shanghai";
    /**
     * 图片鉴黄
     */
    public static final String SCENE_PORN="porn";
    /**
     * 图片暴恐涉政
     */
    public static final String SCENE_TERRORISM="terrorism";
    /**
     * 图文违规
     */
    public static final String SCENE_AD="ad";
    /**
     * 图片二维码
     */
    public static final String SCENE_QRCODE="qrcode";
    /**
     * 图片不良场景
     */
    public static final String SCENE_LIVE="live";
    /**
     * 图片logo
     */
    public static final String SCENE_LOGO="logo";
    /**
     * 文本内容检测  文本检测不可和图片检测一起使用
     */
    public static final String SCENE_ANTISPAM="antispam";

    public static void main(String[] args) throws Exception {

        String string = " único  Cars-fds fútbol ";
        // 将有重音的字符转换成ascii字符：'único  Cars-fds fútbol ' -> 'unico  Cars-fds futbol '
        string = Normalizer.normalize(string, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
        System.out.println(string);
        // 变小写:'unico  Cars-fds futbol ' - > 'unico  cars-fds futbol '
        string = string.toLowerCase();
        System.out.println(string);
        // 将特殊字符转换为空格：'unico  cars-fds futbol ' -> 'unico  cars fds futbol '
        string = string.replaceAll("[^a-z]", " ");
        System.out.println(string);
        // 去掉字符间空格：'unico  cars fds futbol ' -> 'unico cars fds futbol '
        string = string.replaceAll("  ", " ");
        System.out.println(string);
        // 去掉首尾空格：'unico cars fds futbol ' -> 'unico  cars fds futbol'
        string = string.trim();
        System.out.println(string);
    }

    /**
     * 检测图片
     * @param scenes 官方检测场景
     * @param bizType 自定义业务检测场景
     * @param photos 需要检测的图片地址 非本地
     * @return
     */
    public static List<ContentSafetyResult> photoSafety(List<String> scenes,String bizType,List<byte[]> photos) {
        if(photos == null || photos.isEmpty()) {
            System.out.println("photos is empty");
            return null;
        }
        IClientProfile profile = DefaultProfile
                .getProfile(REGION_ID, ALIYUN_ACCESS_KEY_ID, ALIYUN_ACCESS_KEY_SECRET);
        IAcsClient client = new DefaultAcsClient(profile);

        ImageSyncScanRequest imageSyncScanRequest = new ImageSyncScanRequest();
        // 指定API返回格式。
        imageSyncScanRequest.setAcceptFormat(FormatType.JSON);
        // 指定请求方法。
        imageSyncScanRequest.setMethod(MethodType.POST);
        imageSyncScanRequest.setEncoding("utf-8");
        // 支持HTTP和HTTPS。
        imageSyncScanRequest.setProtocol(ProtocolType.HTTP);


        JSONObject httpBody = new JSONObject();
        /**
         * 设置要检测的风险场景。计费依据此处传递的场景计算。
         * 一次请求中可以同时检测多张图片，每张图片可以同时检测多个风险场景，计费按照场景计算。
         * 例如，检测2张图片，场景传递porn和terrorism，计费会按照2张图片鉴黄，2张图片暴恐检测计算。
         * scenes：表示鉴黄场景。
         */
        httpBody.put("scenes", scenes);

        ClientUploader clientUploader = ClientUploader.getImageClientUploader(profile, false);
        byte[] imageBytes = null;
        String url = null;

        if(StringUtils.isBlank(bizType)) {
            httpBody.put("bizType", Arrays.asList(bizType));
        }

        List<JSONObject> tasks=new ArrayList<JSONObject>();
        for(byte[] photo:photos) {
            /**
             * 设置待检测图片。一张图片对应一个task。
             * 多张图片同时检测时，处理的时间由最后一个处理完的图片决定。
             * 通常情况下批量检测的平均响应时间比单张检测的要长。一次批量提交的图片数越多，响应时间被拉长的概率越高。
             * 这里以单张图片检测作为示例, 如果是批量图片检测，请自行构建多个task。
             */
            try{
                // 上传到服务端。
//                imageBytes = photo.getBytes();
                url = clientUploader.uploadBytes(photo);
            }catch (Exception e){
                e.printStackTrace();
            }
            JSONObject task = new JSONObject();
            task.put("dataId", UUID.randomUUID().toString());

            // 设置图片链接。
            task.put("url", url);
            task.put("time", new Date());

            tasks.add(task);
        }
        httpBody.put("tasks", tasks);

        imageSyncScanRequest.setHttpContent(org.apache.commons.codec.binary.StringUtils.getBytesUtf8(httpBody.toJSONString()),
                "UTF-8", FormatType.JSON);

        /**
         * 请设置超时时间。服务端全链路处理超时时间为10秒，请做相应设置。
         * 如果您设置的ReadTimeout小于服务端处理的时间，程序中会获得一个read timeout异常。
         */
        imageSyncScanRequest.setConnectTimeout(3000);
        imageSyncScanRequest.setReadTimeout(10000);
        HttpResponse httpResponse = null;
        try {
            httpResponse = client.doAction(imageSyncScanRequest);
            System.out.println("send photoSafety request");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        List<ContentSafetyResult> results=new ArrayList<ContentSafetyResult>();
        // 服务端接收到请求，完成处理后返回的结果。
        if (httpResponse != null && httpResponse.isSuccess()) {
            JSONObject scrResponse = JSON.parseObject(org.apache.commons.codec.binary.StringUtils.newStringUtf8(httpResponse.getHttpContent()));
            System.out.println(JSON.toJSONString(scrResponse, true));
            int requestCode = scrResponse.getIntValue("code");
            // 每一张图片的检测结果。
            JSONArray taskResults = scrResponse.getJSONArray("data");

            if (200 == requestCode) {
                for (Object taskResult : taskResults) {
                    // 单张图片的处理结果。
                    int taskCode = ((JSONObject) taskResult).getIntValue("code");
                    // 图片对应检测场景的处理结果。如果是多个场景，则会有每个场景的结果。
                    JSONArray sceneResults = ((JSONObject) taskResult).getJSONArray("results");
                    if (200 == taskCode) {
                        for (Object sceneResult : sceneResults) {
                            ContentSafetyResult result=new ContentSafetyResult();
                            result.setScene(((JSONObject) sceneResult).getString("scene"));
                            result.setSuggestion(((JSONObject) sceneResult).getString("suggestion"));
                            result.setLabel(((JSONObject) sceneResult).getString("label"));
                            result.setContent(((JSONObject) taskResult).getString("url"));
                            result.setType(ContentSafetyResult.TYPE_PHOTO);
                            results.add(result);
                        }
                    } else {
                        // 单张图片处理失败, 原因视具体的情况详细分析。
                        System.out.println("task process fail. task response:" + JSON.toJSONString(taskResult));
                        return null;
                    }
                }
            } else {
                /**
                 * 表明请求整体处理失败，原因视具体的情况详细分析。
                 */
                System.out.println("the whole image scan request failed. response:" + JSON.toJSONString(scrResponse));
                return null;
            }
        }
        return results;
    }

    /**
     * 检测文字
     * @param content 要检测的文本
     * @param scene 官方检测场景
     * @return
     */
    public static List<ContentSafetyResult> textSafety(String content,String scene){
        if(StringUtils.isBlank(content)) {
            return null;
        }
        IClientProfile profile = DefaultProfile
                .getProfile(REGION_ID, ALIYUN_ACCESS_KEY_ID, ALIYUN_ACCESS_KEY_SECRET);
        IAcsClient client = new DefaultAcsClient(profile);
        TextScanRequest textScanRequest = new TextScanRequest();
        textScanRequest.setAcceptFormat(FormatType.JSON); // 指定API返回格式。
        textScanRequest.setHttpContentType(FormatType.JSON);
        textScanRequest.setMethod(com.aliyuncs.http.MethodType.POST); // 指定请求方法。
        textScanRequest.setEncoding("UTF-8");
        textScanRequest.setRegionId("cn-beijing");
        List<Map<String, Object>> tasks = new ArrayList<Map<String, Object>>();
        Map<String, Object> task1 = new LinkedHashMap<String, Object>();
        task1.put("dataId", UUID.randomUUID().toString());
        /**
         * 待检测的文本，长度不超过10000个字符。
         */
        task1.put("content", content);
        tasks.add(task1);
        JSONObject data = new JSONObject();

        /**
         * 检测场景。文本垃圾检测请传递antispam。
         **/
        data.put("scenes", Arrays.asList(scene));
        data.put("tasks", tasks);

        try {
            textScanRequest.setHttpContent(data.toJSONString().getBytes("UTF-8"), "UTF-8", FormatType.JSON);
        } catch (UnsupportedEncodingException e) {
            System.out.println(e.getMessage());
            return null;
        }
        // 请务必设置超时时间。
        textScanRequest.setConnectTimeout(3000);
        textScanRequest.setReadTimeout(6000);
        List<ContentSafetyResult> results=new ArrayList<ContentSafetyResult>();
        try {
            HttpResponse httpResponse = client.doAction(textScanRequest);
            if(httpResponse.isSuccess()){
                JSONObject scrResponse = JSON.parseObject(new String(httpResponse.getHttpContent(), "UTF-8"));
                System.out.println(JSON.toJSONString(scrResponse, true));
                if (200 == scrResponse.getInteger("code")) {
                    JSONArray taskResults = scrResponse.getJSONArray("data");
                    for (Object taskResult : taskResults) {
                        if(200 == ((JSONObject)taskResult).getInteger("code")){
                            JSONArray sceneResults = ((JSONObject)taskResult).getJSONArray("results");
                            for (Object sceneResult : sceneResults) {
                                ContentSafetyResult result=new ContentSafetyResult();
                                result.setType(ContentSafetyResult.TYPE_TEXT);
                                result.setContent(content);
                                result.setScene(((JSONObject)sceneResult).getString("scene"));
                                result.setSuggestion(((JSONObject)sceneResult).getString("suggestion"));
                                result.setLabel(((JSONObject)sceneResult).getString("label"));
                                results.add(result);
                            }
                        }else{
                            System.out.println("task process fail:" + ((JSONObject)taskResult).getInteger("code"));
                        }
                    }
                } else {
                    System.out.println("detect not success. code:" + scrResponse.getInteger("code"));
                }
            }else{
                System.out.println("response not success. status:" + httpResponse.getStatus());
            }
        } catch (ServerException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } catch (ClientException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return results;
    }

    /**
     * 查找预警的数据
     * @param results
     * @return
     */
    public static List<ContentSafetyResult> findWarningData(List<ContentSafetyResult> results){
        if(results == null || results.isEmpty()) {
            return Collections.emptyList();
        }
        List<ContentSafetyResult> warningResults=new ArrayList<ContentSafetyResult>();
        for (ContentSafetyResult result : results) {
            if(result.getSuggestion() != ContentSafetyResult.SUGGESTION_PASS) {
                warningResults.add(result);
            }
        }
        return warningResults;
    }
}
