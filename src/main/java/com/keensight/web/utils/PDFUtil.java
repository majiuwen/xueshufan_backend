package com.keensight.web.utils;

import com.alibaba.fastjson.JSON;
import com.keensight.web.model.PublicationModel;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.grobid.core.engines.config.GrobidAnalysisConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.grobid.core.*;
import org.grobid.core.data.*;
import org.grobid.core.factory.*;
import org.grobid.core.utilities.*;
import org.grobid.core.engines.Engine;
import org.grobid.core.main.GrobidHomeFinder;
import org.springframework.security.core.parameters.P;

/**
 * PDF文件处理工具类
 *
 * @author : newtouch
 * @version : v1.0.0 [2020/11/18] 新建
 */
public class PDFUtil {
    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(PDFUtil.class);

    /**
     * 根据页码创建PDF文件缩略图
     *
     * @param inputStream   要生成缩略图的PDF文件流
     * @param pdfPage       要生成缩略图的PDF页码（从0开始）
     * @param dpi           缩略图DPI
     * @param thumbnailPath 缩略图文件路径
     * @param thumbnailName 缩略图文件名称
     * @param thumbnailType 缩略图文件类型（jpg、png）
     */
    public static boolean createPDFThumbnail(InputStream inputStream, Integer pdfPage, Integer dpi, String thumbnailPath, String thumbnailName, String thumbnailType) {
        boolean result = false;
        PDDocument pdDocument = null;
        if (pdfPage == null) {
            pdfPage = 0;
        }
        if (dpi == null) {
            dpi = 32;
        }
        try {
            logger.debug("----------PDF文件缩略图创建开始----------");
            pdDocument = PDDocument.load(inputStream);
            PDFRenderer renderer = new PDFRenderer(pdDocument);
            int pages = pdDocument.getNumberOfPages();
            if (pages > 0 && pdfPage <= pages && pdfPage >= 0) {
                File folder = new File(thumbnailPath);
                // 创建目录
                if (!folder.exists() || !folder.isDirectory()) {
                    if (!folder.mkdirs()) {
                        return false;
                    }
                }
                // 创建文件
                File thumbnailFile = new File(thumbnailPath + thumbnailName);
                BufferedImage image = renderer.renderImageWithDPI(pdfPage, dpi);
                ImageIO.write(image, thumbnailType, thumbnailFile);

                result = true;
                logger.debug("----------PDF文件缩略图创建成功----------");
            } else {
                throw new Exception("PDF文件页数为0");
            }
        } catch (Exception e) {
            logger.error("----------PDF文件缩略图创建出错----------", e);
        } finally {

            try {
                if (pdDocument != null) {
                    pdDocument.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 读取PDF文件
     *
     * @param file
     * @param response
     * @return
     */
    public static boolean readPDFFile(File file, HttpServletResponse response) {
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        if (file == null || !file.exists() || response == null) {
            logger.error("----------PDF文件不存在----------");
            return false;
        }

        boolean readResult = false;
        logger.debug("----------PDF文件读取开始----------");
        response.setContentType("application/pdf");
        response.setHeader("content-type", "application/pdf");
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(file.getName(), StandardCharsets.UTF_8));
        try {
            bis = new BufferedInputStream(new FileInputStream(file));
            byte[] buf = new byte[4096];
            bos = new BufferedOutputStream(response.getOutputStream());
            int bytesIn;
            while ((bytesIn = bis.read(buf, 0, buf.length)) != -1) {
                bos.write(buf, 0, bytesIn);
            }
            bos.flush();
            logger.debug("----------PDF文件读取结束----------");
            readResult = true;
        } catch (Exception e) {
            logger.error("----------PDF文件读取出错----------", e);
        } finally {
            try {
                if (bos != null) {
                    bos.close();
                }
                if (bis != null) {
                    bis.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return readResult;
    }

    public static PublicationModel extractPDFInfo(String pdfPath) {
        String pdfInfo = "";
        PublicationModel publicationModel = new PublicationModel();
        try {
            Properties prop = new Properties();
            prop.load(PDFUtil.class.getClassLoader().getResourceAsStream("grobid.properties"));
            String pGrobidHome = prop.getProperty("grobid.pGrobidHome");

            // The GrobidHomeFinder can be instantiate without parameters to verify the grobid home in the standard
            // location (classpath, ../grobid-home, ../../grobid-home)

            // If the location is customised:
            GrobidHomeFinder grobidHomeFinder = new GrobidHomeFinder(Arrays.asList(pGrobidHome));

            //The grobid yaml config file needs to be instantiate using the correct grobidHomeFinder or it will use the default
            //locations
            GrobidProperties.getInstance(grobidHomeFinder);

            Engine engine = GrobidFactory.getInstance().createEngine();

            GrobidAnalysisConfig config = (new GrobidAnalysisConfig.GrobidAnalysisConfigBuilder()).startPage(0).endPage(2).build();

            // Biblio object for the result
            BiblioItem resHeader = new BiblioItem();
            pdfInfo = engine.processHeader(pdfPath, config, resHeader);

            // DOI
            String doi = "";
            Pattern p = Pattern.compile("<idno type=\"DOI\">(.*)</idno>");
            Matcher m = p.matcher(pdfInfo);
            while(m.find()){
                doi = m.group(1);
            }

            publicationModel.setDoi(doi);

            // 论文标题
            String title = "";
            p = Pattern.compile("<title level=\"a\" type=\"main\">(.*)</title>");
            m = p.matcher(pdfInfo);
            while(m.find()){
                title = m.group(1);
            }

            publicationModel.setDisplayTitle(title);
            publicationModel.setNormalizedTitle(title);

            // 出版社
            String publisher = "";
            p = Pattern.compile("<publisher>(.*)</publisher>");
            m = p.matcher(pdfInfo);
            while(m.find()){
                publisher = m.group(1);
            }

            publicationModel.setPublisher(publisher);

            // 期刊名
            String journalName = "";
            p = Pattern.compile("<title level=\"j\" type=\"main\">(.*)</title>");
            m = p.matcher(pdfInfo);
            while(m.find()){
                journalName = m.group(1);
            }

            publicationModel.setJournalName(journalName);

            // 期刊序列号
            String issn = "";
            p = Pattern.compile("<idno type=\"ISSN\">(.*)</idno>");
            m = p.matcher(pdfInfo);
            while(m.find()){
                issn = m.group(1);
            }

            publicationModel.setIssn(issn);

            // 卷数
            String volume = "";
            p = Pattern.compile("<biblScope unit=\"volume\">(.*)</biblScope>");
            m = p.matcher(pdfInfo);
            while(m.find()){
                volume = m.group(1);
            }

            publicationModel.setVolume(volume);

            // 期数
            String issue = "";
            p = Pattern.compile("<biblScope unit=\"issue\">(.*)</biblScope>");
            m = p.matcher(pdfInfo);
            while(m.find()){
                issue = m.group(1);
            }

            publicationModel.setIssue(issue);

            // 发表日期
            String dateStr = "";
            Date date = null;
            int year = 0;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy");
            p = Pattern.compile("<date type=\"published\" when=\"(.*)\">");
            m = p.matcher(pdfInfo);
            while(m.find()){
                dateStr = m.group(1);
                year = Integer.parseInt(m.group(1).substring(0,4));
                try {
                    date = simpleDateFormat.parse(m.group(1));
                } catch (ParseException e) {
                    try {
                        date = simpleDateFormat1.parse(m.group(1));
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                }
            }
            publicationModel.setDateStr(dateStr);
            if(year != 0 ){
                publicationModel.setYear(year);
            }
            if(date != null){

                publicationModel.setDate(date);
            }

            // 摘要
            String summary = "";
            pdfInfo = pdfInfo.replaceAll("\n","");
            p = Pattern.compile("<abstract>(.*)</abstract>");
            m = p.matcher(pdfInfo);
            while(m.find()){
                summary = m.group(1).trim().replaceAll("<p>","").replaceAll("</p>","");
            }

            publicationModel.setSummary(summary);

            System.out.println(JSON.toJSON(publicationModel).toString().replaceAll(",",",\n"));

        } catch (Exception e) {
            // If an exception is generated, print a stack trace
            e.printStackTrace();
        }
        return publicationModel;
    }
}
