package com.keensight.web.utils;

import java.io.UnsupportedEncodingException;
import java.text.Normalizer;

public class NormalizedUtil {
    /**
     * NormalizedTitle转换规则
     * @param content
     */
    public static String getNormalizedTitle (String content) {
            // 将有重音的字符转换成ascii字符：'único  Cars-fds fútbol ' -> 'unico  Cars-fds futbol '
            String decomposed = Normalizer.normalize(content, Normalizer.Form.NFKD);
            String regex = "[\\p{InCombiningDiacriticalMarks}\\p{IsLm}\\p{IsSk}]+";
            try {
                content = new String(decomposed.replaceAll(regex, "").getBytes("ascii"), "ascii");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            // 变小写:'unico  Cars-fds futbol ' - > 'unico  cars-fds futbol '
            content = content.toLowerCase();
            System.out.println(content);
            // 将特殊字符转换为空格：'unico  cars-fds futbol ' -> 'unico  cars fds futbol '
            content = content.replaceAll("[^a-z]", " ");
            System.out.println(content);
            // 去掉字符间空格：'unico  cars fds futbol ' -> 'unico cars fds futbol '
            content = content.replaceAll("  ", " ");
            System.out.println(content);
            // 去掉首尾空格：'unico cars fds futbol ' -> 'unico  cars fds futbol'
            content = content.trim();
            System.out.println(content);

        return content;
    }
}
