package com.keensight.web.db.entity;

public class RelatedPublication extends RelatedPublicationKey {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_related_publication.score
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private Double score;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_related_publication.score
     *
     * @return the value of t_m_related_publication.score
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public Double getScore() {
        return score;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_related_publication.score
     *
     * @param score the value for t_m_related_publication.score
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setScore(Double score) {
        this.score = score;
    }
}