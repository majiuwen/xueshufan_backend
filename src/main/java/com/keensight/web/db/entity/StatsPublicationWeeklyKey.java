package com.keensight.web.db.entity;

import java.util.Date;

public class StatsPublicationWeeklyKey {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_stats_publication_weekly.publication_id
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    private Long publicationId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_stats_publication_weekly.week
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    private Date week;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_stats_publication_weekly.publication_id
     *
     * @return the value of t_stats_publication_weekly.publication_id
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public Long getPublicationId() {
        return publicationId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_stats_publication_weekly.publication_id
     *
     * @param publicationId the value for t_stats_publication_weekly.publication_id
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public void setPublicationId(Long publicationId) {
        this.publicationId = publicationId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_stats_publication_weekly.week
     *
     * @return the value of t_stats_publication_weekly.week
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public Date getWeek() {
        return week;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_stats_publication_weekly.week
     *
     * @param week the value for t_stats_publication_weekly.week
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public void setWeek(Date week) {
        this.week = week;
    }
}
