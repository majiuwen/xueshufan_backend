package com.keensight.web.db.entity;

public class DisciplineKey {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_discipline.field_id
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private Long fieldId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_discipline.subfield_id
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private Long subfieldId;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_discipline.field_id
     *
     * @return the value of t_m_discipline.field_id
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public Long getFieldId() {
        return fieldId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_discipline.field_id
     *
     * @param fieldId the value for t_m_discipline.field_id
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setFieldId(Long fieldId) {
        this.fieldId = fieldId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_discipline.subfield_id
     *
     * @return the value of t_m_discipline.subfield_id
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public Long getSubfieldId() {
        return subfieldId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_discipline.subfield_id
     *
     * @param subfieldId the value for t_m_discipline.subfield_id
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setSubfieldId(Long subfieldId) {
        this.subfieldId = subfieldId;
    }
}