package com.keensight.web.db.entity;

public class DocumentKey {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_document.doc_id
     *
     * @mbggenerated Wed Nov 18 14:30:08 CST 2020
     */
    private Long docId;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_document.doc_id
     *
     * @return the value of t_m_document.doc_id
     *
     * @mbggenerated Wed Nov 18 14:30:08 CST 2020
     */
    public Long getDocId() {
        return docId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_document.doc_id
     *
     * @param docId the value for t_m_document.doc_id
     *
     * @mbggenerated Wed Nov 18 14:30:08 CST 2020
     */
    public void setDocId(Long docId) {
        this.docId = docId;
    }
}