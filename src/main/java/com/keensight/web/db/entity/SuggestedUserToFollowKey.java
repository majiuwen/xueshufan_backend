package com.keensight.web.db.entity;

public class SuggestedUserToFollowKey {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_suggested_user_to_follow.user_id
     *
     * @mbggenerated Wed Nov 04 18:34:58 CST 2020
     */
    private Long userId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_suggested_user_to_follow.user_id_to_follow
     *
     * @mbggenerated Wed Nov 04 18:34:58 CST 2020
     */
    private Long userIdToFollow;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_suggested_user_to_follow.user_id
     *
     * @return the value of t_suggested_user_to_follow.user_id
     *
     * @mbggenerated Wed Nov 04 18:34:58 CST 2020
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_suggested_user_to_follow.user_id
     *
     * @param userId the value for t_suggested_user_to_follow.user_id
     *
     * @mbggenerated Wed Nov 04 18:34:58 CST 2020
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_suggested_user_to_follow.user_id_to_follow
     *
     * @return the value of t_suggested_user_to_follow.user_id_to_follow
     *
     * @mbggenerated Wed Nov 04 18:34:58 CST 2020
     */
    public Long getUserIdToFollow() {
        return userIdToFollow;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_suggested_user_to_follow.user_id_to_follow
     *
     * @param userIdToFollow the value for t_suggested_user_to_follow.user_id_to_follow
     *
     * @mbggenerated Wed Nov 04 18:34:58 CST 2020
     */
    public void setUserIdToFollow(Long userIdToFollow) {
        this.userIdToFollow = userIdToFollow;
    }
}