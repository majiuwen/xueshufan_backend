package com.keensight.web.db.entity;

import java.util.Date;

public class UserNewsCommentLike {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_news_comment_like.news_comment_id
     *
     * @mbggenerated Fri Nov 26 10:28:24 CST 2021
     */
    private Long newsCommentId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_news_comment_like.user_id
     *
     * @mbggenerated Fri Nov 26 10:28:24 CST 2021
     */
    private Long userId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_news_comment_like.like_time
     *
     * @mbggenerated Fri Nov 26 10:28:24 CST 2021
     */
    private Date likeTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_news_comment_like.news_comment_id
     *
     * @return the value of t_m_user_news_comment_like.news_comment_id
     *
     * @mbggenerated Fri Nov 26 10:28:24 CST 2021
     */
    public Long getNewsCommentId() {
        return newsCommentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_news_comment_like.news_comment_id
     *
     * @param newsCommentId the value for t_m_user_news_comment_like.news_comment_id
     *
     * @mbggenerated Fri Nov 26 10:28:24 CST 2021
     */
    public void setNewsCommentId(Long newsCommentId) {
        this.newsCommentId = newsCommentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_news_comment_like.user_id
     *
     * @return the value of t_m_user_news_comment_like.user_id
     *
     * @mbggenerated Fri Nov 26 10:28:24 CST 2021
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_news_comment_like.user_id
     *
     * @param userId the value for t_m_user_news_comment_like.user_id
     *
     * @mbggenerated Fri Nov 26 10:28:24 CST 2021
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_news_comment_like.like_time
     *
     * @return the value of t_m_user_news_comment_like.like_time
     *
     * @mbggenerated Fri Nov 26 10:28:24 CST 2021
     */
    public Date getLikeTime() {
        return likeTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_news_comment_like.like_time
     *
     * @param likeTime the value for t_m_user_news_comment_like.like_time
     *
     * @mbggenerated Fri Nov 26 10:28:24 CST 2021
     */
    public void setLikeTime(Date likeTime) {
        this.likeTime = likeTime;
    }
}