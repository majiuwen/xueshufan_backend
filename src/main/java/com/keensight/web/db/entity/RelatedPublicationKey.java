package com.keensight.web.db.entity;

public class RelatedPublicationKey {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_related_publication.publication_id
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private Long publicationId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_related_publication.related_publication_id
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private Long relatedPublicationId;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_related_publication.publication_id
     *
     * @return the value of t_m_related_publication.publication_id
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public Long getPublicationId() {
        return publicationId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_related_publication.publication_id
     *
     * @param publicationId the value for t_m_related_publication.publication_id
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setPublicationId(Long publicationId) {
        this.publicationId = publicationId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_related_publication.related_publication_id
     *
     * @return the value of t_m_related_publication.related_publication_id
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public Long getRelatedPublicationId() {
        return relatedPublicationId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_related_publication.related_publication_id
     *
     * @param relatedPublicationId the value for t_m_related_publication.related_publication_id
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setRelatedPublicationId(Long relatedPublicationId) {
        this.relatedPublicationId = relatedPublicationId;
    }
}