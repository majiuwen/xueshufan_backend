package com.keensight.web.db.entity;

import java.util.Date;

public class ConferenceSerie extends ConferenceSerieKey {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_conference_serie.rank
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private Integer rank;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_conference_serie.normalized_name
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private String normalizedName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_conference_serie.display_name
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private String displayName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_conference_serie.publications
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private Integer publications;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_conference_serie.publication_families
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private Integer publicationFamilies;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_conference_serie.citations
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private Integer citations;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_conference_serie.created_at
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private Date createdAt;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_conference_serie.display_name_cn
     *
     * @mbggenerated Wed Nov 03 14:49:16 CST 2021
     */
    private String displayNameCn;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_conference_serie.rank
     *
     * @return the value of t_m_conference_serie.rank
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public Integer getRank() {
        return rank;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_conference_serie.rank
     *
     * @param rank the value for t_m_conference_serie.rank
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setRank(Integer rank) {
        this.rank = rank;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_conference_serie.normalized_name
     *
     * @return the value of t_m_conference_serie.normalized_name
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public String getNormalizedName() {
        return normalizedName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_conference_serie.normalized_name
     *
     * @param normalizedName the value for t_m_conference_serie.normalized_name
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setNormalizedName(String normalizedName) {
        this.normalizedName = normalizedName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_conference_serie.display_name
     *
     * @return the value of t_m_conference_serie.display_name
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_conference_serie.display_name
     *
     * @param displayName the value for t_m_conference_serie.display_name
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_conference_serie.publications
     *
     * @return the value of t_m_conference_serie.publications
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public Integer getPublications() {
        return publications;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_conference_serie.publications
     *
     * @param publications the value for t_m_conference_serie.publications
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setPublications(Integer publications) {
        this.publications = publications;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_conference_serie.publication_families
     *
     * @return the value of t_m_conference_serie.publication_families
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public Integer getPublicationFamilies() {
        return publicationFamilies;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_conference_serie.publication_families
     *
     * @param publicationFamilies the value for t_m_conference_serie.publication_families
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setPublicationFamilies(Integer publicationFamilies) {
        this.publicationFamilies = publicationFamilies;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_conference_serie.citations
     *
     * @return the value of t_m_conference_serie.citations
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public Integer getCitations() {
        return citations;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_conference_serie.citations
     *
     * @param citations the value for t_m_conference_serie.citations
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setCitations(Integer citations) {
        this.citations = citations;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_conference_serie.created_at
     *
     * @return the value of t_m_conference_serie.created_at
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_conference_serie.created_at
     *
     * @param createdAt the value for t_m_conference_serie.created_at
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_conference_serie.display_name_cn
     *
     * @return the value of t_m_conference_serie.display_name_cn
     *
     * @mbggenerated Wed Nov 03 14:49:16 CST 2021
     */
    public String getDisplayNameCn() {
        return displayNameCn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_conference_serie.display_name_cn
     *
     * @param displayNameCn the value for t_m_conference_serie.display_name_cn
     *
     * @mbggenerated Wed Nov 03 14:49:16 CST 2021
     */
    public void setDisplayNameCn(String displayNameCn) {
        this.displayNameCn = displayNameCn;
    }
}
