package com.keensight.web.db.entity;

public class UserAuthKey {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_auth.user_id
     *
     * @mbggenerated Tue Nov 17 10:59:25 JST 2020
     */
    private Long userId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_auth.identity_type
     *
     * @mbggenerated Tue Nov 17 10:59:25 JST 2020
     */
    private String identityType;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_auth.user_id
     *
     * @return the value of t_m_user_auth.user_id
     *
     * @mbggenerated Tue Nov 17 10:59:25 JST 2020
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_auth.user_id
     *
     * @param userId the value for t_m_user_auth.user_id
     *
     * @mbggenerated Tue Nov 17 10:59:25 JST 2020
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_auth.identity_type
     *
     * @return the value of t_m_user_auth.identity_type
     *
     * @mbggenerated Tue Nov 17 10:59:25 JST 2020
     */
    public String getIdentityType() {
        return identityType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_auth.identity_type
     *
     * @param identityType the value for t_m_user_auth.identity_type
     *
     * @mbggenerated Tue Nov 17 10:59:25 JST 2020
     */
    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }
}