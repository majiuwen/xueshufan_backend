package com.keensight.web.db.entity;

import java.util.Date;
import java.util.List;

public class Field extends FieldKey {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_field.rank
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private Integer rank;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_field.normalized_name
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private String normalizedName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_field.display_name
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private String displayName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_field.main_type
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private String mainType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_field.level
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private Integer level;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_field.publications
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private Integer publications;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_field.publication_families
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private Integer publicationFamilies;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_field.citations
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private Integer citations;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_field.created_at
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    private Date createdAt;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_field.display_name_cn
     *
     * @mbggenerated Wed Nov 03 14:49:16 CST 2021
     */
    private String displayNameCn;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_field.rank
     *
     * @return the value of t_m_field.rank
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public Integer getRank() {
        return rank;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_field.rank
     *
     * @param rank the value for t_m_field.rank
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setRank(Integer rank) {
        this.rank = rank;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_field.normalized_name
     *
     * @return the value of t_m_field.normalized_name
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public String getNormalizedName() {
        return normalizedName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_field.normalized_name
     *
     * @param normalizedName the value for t_m_field.normalized_name
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setNormalizedName(String normalizedName) {
        this.normalizedName = normalizedName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_field.display_name
     *
     * @return the value of t_m_field.display_name
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_field.display_name
     *
     * @param displayName the value for t_m_field.display_name
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_field.main_type
     *
     * @return the value of t_m_field.main_type
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public String getMainType() {
        return mainType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_field.main_type
     *
     * @param mainType the value for t_m_field.main_type
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setMainType(String mainType) {
        this.mainType = mainType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_field.level
     *
     * @return the value of t_m_field.level
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_field.level
     *
     * @param level the value for t_m_field.level
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_field.publications
     *
     * @return the value of t_m_field.publications
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public Integer getPublications() {
        return publications;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_field.publications
     *
     * @param publications the value for t_m_field.publications
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setPublications(Integer publications) {
        this.publications = publications;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_field.publication_families
     *
     * @return the value of t_m_field.publication_families
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public Integer getPublicationFamilies() {
        return publicationFamilies;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_field.publication_families
     *
     * @param publicationFamilies the value for t_m_field.publication_families
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setPublicationFamilies(Integer publicationFamilies) {
        this.publicationFamilies = publicationFamilies;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_field.citations
     *
     * @return the value of t_m_field.citations
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public Integer getCitations() {
        return citations;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_field.citations
     *
     * @param citations the value for t_m_field.citations
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setCitations(Integer citations) {
        this.citations = citations;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_field.created_at
     *
     * @return the value of t_m_field.created_at
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_field.created_at
     *
     * @param createdAt the value for t_m_field.created_at
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_field.display_name_cn
     *
     * @return the value of t_m_field.display_name_cn
     *
     * @mbggenerated Wed Nov 03 14:49:16 CST 2021
     */
    public String getDisplayNameCn() {
        return displayNameCn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_field.display_name_cn
     *
     * @param displayNameCn the value for t_m_field.display_name_cn
     *
     * @mbggenerated Wed Nov 03 14:49:16 CST 2021
     */
    public void setDisplayNameCn(String displayNameCn) {
        this.displayNameCn = displayNameCn;
    }

    /** 相关研究领域集合 */
    private List<RelatedField> relatedFieldList;
    /** 关注标志 */
    private String followingFlg;

    /**
     * 获取 相关研究领域集合
     *
     * @return relatedFieldList 相关研究领域集合
     */
    public List<RelatedField> getRelatedFieldList() {
        return relatedFieldList;
    }

    /**
     * 设置 相关研究领域集合
     *
     * @param relatedFieldList 相关研究领域集合
     */
    public void setRelatedFieldList(List<RelatedField> relatedFieldList) {
        this.relatedFieldList = relatedFieldList;
    }

    /**
     * 获取 关注标志
     *
     * @return followingFlg 关注标志
     */
    public String getFollowingFlg() {
        return followingFlg;
    }

    /**
     * 设置 关注标志
     *
     * @param followingFlg 关注标志
     */
    public void setFollowingFlg(String followingFlg) {
        this.followingFlg = followingFlg;
    }
}
