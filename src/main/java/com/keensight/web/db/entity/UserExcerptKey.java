package com.keensight.web.db.entity;

public class UserExcerptKey {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.excerpt_id
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private Long excerptId;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.excerpt_id
     *
     * @return the value of t_m_user_excerpt.excerpt_id
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public Long getExcerptId() {
        return excerptId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.excerpt_id
     *
     * @param excerptId the value for t_m_user_excerpt.excerpt_id
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setExcerptId(Long excerptId) {
        this.excerptId = excerptId;
    }
}