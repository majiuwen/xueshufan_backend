package com.keensight.web.db.entity;

import java.util.Date;

public class UserExcerpt extends UserExcerptKey {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.publication_id
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private Long publicationId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.doc_id
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private Long docId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.user_id
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private Long userId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.original_text
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String originalText;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.translation_text
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String translationText;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.translation_tool_id
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private Long translationToolId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.selected_content
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String selectedContent;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.action_type
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String actionType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.content_type
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private Integer contentType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.create_type
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private Integer createType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.is_data_collect
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String isDataCollect;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.name
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String name;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.origin_page
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private Integer originPage;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.page_num
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private Integer pageNum;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.pre_render
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private Boolean preRender;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.start_dom
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String startDom;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.end_dom
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String endDom;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.start_offset
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String startOffset;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.end_offset
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String endOffset;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.sub_action_type
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String subActionType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.uid
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String uid;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.type
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String type;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.page_top
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String pageTop;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.page_height
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String pageHeight;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.page_width
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String pageWidth;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.page_left
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String pageLeft;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.all_page_excerpt_flg
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String allPageExcerptFlg;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.color
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String color;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.size
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private Integer size;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.rect_mode
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private Boolean rectMode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.snapshot_url
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private String snapshotUrl;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_m_user_excerpt.excerpt_date
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    private Date excerptDate;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.publication_id
     *
     * @return the value of t_m_user_excerpt.publication_id
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public Long getPublicationId() {
        return publicationId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.publication_id
     *
     * @param publicationId the value for t_m_user_excerpt.publication_id
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setPublicationId(Long publicationId) {
        this.publicationId = publicationId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.doc_id
     *
     * @return the value of t_m_user_excerpt.doc_id
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public Long getDocId() {
        return docId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.doc_id
     *
     * @param docId the value for t_m_user_excerpt.doc_id
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setDocId(Long docId) {
        this.docId = docId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.user_id
     *
     * @return the value of t_m_user_excerpt.user_id
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.user_id
     *
     * @param userId the value for t_m_user_excerpt.user_id
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.original_text
     *
     * @return the value of t_m_user_excerpt.original_text
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getOriginalText() {
        return originalText;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.original_text
     *
     * @param originalText the value for t_m_user_excerpt.original_text
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setOriginalText(String originalText) {
        this.originalText = originalText;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.translation_text
     *
     * @return the value of t_m_user_excerpt.translation_text
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getTranslationText() {
        return translationText;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.translation_text
     *
     * @param translationText the value for t_m_user_excerpt.translation_text
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setTranslationText(String translationText) {
        this.translationText = translationText;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.translation_tool_id
     *
     * @return the value of t_m_user_excerpt.translation_tool_id
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public Long getTranslationToolId() {
        return translationToolId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.translation_tool_id
     *
     * @param translationToolId the value for t_m_user_excerpt.translation_tool_id
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setTranslationToolId(Long translationToolId) {
        this.translationToolId = translationToolId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.selected_content
     *
     * @return the value of t_m_user_excerpt.selected_content
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getSelectedContent() {
        return selectedContent;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.selected_content
     *
     * @param selectedContent the value for t_m_user_excerpt.selected_content
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setSelectedContent(String selectedContent) {
        this.selectedContent = selectedContent;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.action_type
     *
     * @return the value of t_m_user_excerpt.action_type
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.action_type
     *
     * @param actionType the value for t_m_user_excerpt.action_type
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.content_type
     *
     * @return the value of t_m_user_excerpt.content_type
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public Integer getContentType() {
        return contentType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.content_type
     *
     * @param contentType the value for t_m_user_excerpt.content_type
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setContentType(Integer contentType) {
        this.contentType = contentType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.create_type
     *
     * @return the value of t_m_user_excerpt.create_type
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public Integer getCreateType() {
        return createType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.create_type
     *
     * @param createType the value for t_m_user_excerpt.create_type
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setCreateType(Integer createType) {
        this.createType = createType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.is_data_collect
     *
     * @return the value of t_m_user_excerpt.is_data_collect
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getIsDataCollect() {
        return isDataCollect;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.is_data_collect
     *
     * @param isDataCollect the value for t_m_user_excerpt.is_data_collect
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setIsDataCollect(String isDataCollect) {
        this.isDataCollect = isDataCollect;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.name
     *
     * @return the value of t_m_user_excerpt.name
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.name
     *
     * @param name the value for t_m_user_excerpt.name
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.origin_page
     *
     * @return the value of t_m_user_excerpt.origin_page
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public Integer getOriginPage() {
        return originPage;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.origin_page
     *
     * @param originPage the value for t_m_user_excerpt.origin_page
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setOriginPage(Integer originPage) {
        this.originPage = originPage;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.page_num
     *
     * @return the value of t_m_user_excerpt.page_num
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public Integer getPageNum() {
        return pageNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.page_num
     *
     * @param pageNum the value for t_m_user_excerpt.page_num
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.pre_render
     *
     * @return the value of t_m_user_excerpt.pre_render
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public Boolean getPreRender() {
        return preRender;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.pre_render
     *
     * @param preRender the value for t_m_user_excerpt.pre_render
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setPreRender(Boolean preRender) {
        this.preRender = preRender;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.start_dom
     *
     * @return the value of t_m_user_excerpt.start_dom
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getStartDom() {
        return startDom;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.start_dom
     *
     * @param startDom the value for t_m_user_excerpt.start_dom
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setStartDom(String startDom) {
        this.startDom = startDom;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.end_dom
     *
     * @return the value of t_m_user_excerpt.end_dom
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getEndDom() {
        return endDom;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.end_dom
     *
     * @param endDom the value for t_m_user_excerpt.end_dom
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setEndDom(String endDom) {
        this.endDom = endDom;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.start_offset
     *
     * @return the value of t_m_user_excerpt.start_offset
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getStartOffset() {
        return startOffset;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.start_offset
     *
     * @param startOffset the value for t_m_user_excerpt.start_offset
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setStartOffset(String startOffset) {
        this.startOffset = startOffset;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.end_offset
     *
     * @return the value of t_m_user_excerpt.end_offset
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getEndOffset() {
        return endOffset;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.end_offset
     *
     * @param endOffset the value for t_m_user_excerpt.end_offset
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setEndOffset(String endOffset) {
        this.endOffset = endOffset;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.sub_action_type
     *
     * @return the value of t_m_user_excerpt.sub_action_type
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getSubActionType() {
        return subActionType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.sub_action_type
     *
     * @param subActionType the value for t_m_user_excerpt.sub_action_type
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setSubActionType(String subActionType) {
        this.subActionType = subActionType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.uid
     *
     * @return the value of t_m_user_excerpt.uid
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getUid() {
        return uid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.uid
     *
     * @param uid the value for t_m_user_excerpt.uid
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.type
     *
     * @return the value of t_m_user_excerpt.type
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getType() {
        return type;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.type
     *
     * @param type the value for t_m_user_excerpt.type
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.page_top
     *
     * @return the value of t_m_user_excerpt.page_top
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getPageTop() {
        return pageTop;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.page_top
     *
     * @param pageTop the value for t_m_user_excerpt.page_top
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setPageTop(String pageTop) {
        this.pageTop = pageTop;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.page_height
     *
     * @return the value of t_m_user_excerpt.page_height
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getPageHeight() {
        return pageHeight;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.page_height
     *
     * @param pageHeight the value for t_m_user_excerpt.page_height
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setPageHeight(String pageHeight) {
        this.pageHeight = pageHeight;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.page_width
     *
     * @return the value of t_m_user_excerpt.page_width
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getPageWidth() {
        return pageWidth;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.page_width
     *
     * @param pageWidth the value for t_m_user_excerpt.page_width
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setPageWidth(String pageWidth) {
        this.pageWidth = pageWidth;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.page_left
     *
     * @return the value of t_m_user_excerpt.page_left
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getPageLeft() {
        return pageLeft;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.page_left
     *
     * @param pageLeft the value for t_m_user_excerpt.page_left
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setPageLeft(String pageLeft) {
        this.pageLeft = pageLeft;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.all_page_excerpt_flg
     *
     * @return the value of t_m_user_excerpt.all_page_excerpt_flg
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getAllPageExcerptFlg() {
        return allPageExcerptFlg;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.all_page_excerpt_flg
     *
     * @param allPageExcerptFlg the value for t_m_user_excerpt.all_page_excerpt_flg
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setAllPageExcerptFlg(String allPageExcerptFlg) {
        this.allPageExcerptFlg = allPageExcerptFlg;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.color
     *
     * @return the value of t_m_user_excerpt.color
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getColor() {
        return color;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.color
     *
     * @param color the value for t_m_user_excerpt.color
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.size
     *
     * @return the value of t_m_user_excerpt.size
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public Integer getSize() {
        return size;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.size
     *
     * @param size the value for t_m_user_excerpt.size
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setSize(Integer size) {
        this.size = size;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.rect_mode
     *
     * @return the value of t_m_user_excerpt.rect_mode
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public Boolean getRectMode() {
        return rectMode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.rect_mode
     *
     * @param rectMode the value for t_m_user_excerpt.rect_mode
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setRectMode(Boolean rectMode) {
        this.rectMode = rectMode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.snapshot_url
     *
     * @return the value of t_m_user_excerpt.snapshot_url
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public String getSnapshotUrl() {
        return snapshotUrl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.snapshot_url
     *
     * @param snapshotUrl the value for t_m_user_excerpt.snapshot_url
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setSnapshotUrl(String snapshotUrl) {
        this.snapshotUrl = snapshotUrl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_m_user_excerpt.excerpt_date
     *
     * @return the value of t_m_user_excerpt.excerpt_date
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public Date getExcerptDate() {
        return excerptDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_m_user_excerpt.excerpt_date
     *
     * @param excerptDate the value for t_m_user_excerpt.excerpt_date
     *
     * @mbggenerated Thu Dec 16 10:53:50 CST 2021
     */
    public void setExcerptDate(Date excerptDate) {
        this.excerptDate = excerptDate;
    }
    /** 翻译工具code */
    private String translationToolCode;
    /** 翻译工具名字 */
    private String translationToolName;
    /** pdf路径 */
    private String fulltextPath;
    /** 原文Html */
    private String originalTextHtml;
    /** 译文Html */
    private String translationTextHtml;
    /** 文件名 */
    private String displayTitle;
    /** 出版社 */
    private String publisher;
    /** 私人上传文件路径 */
    private String privateDocpath;

    public String getTranslationToolCode() {
        return translationToolCode;
    }

    public void setTranslationToolCode(String translationToolCode) {
        this.translationToolCode = translationToolCode;
    }

    public String getTranslationToolName() {
        return translationToolName;
    }

    public void setTranslationToolName(String translationToolName) {
        this.translationToolName = translationToolName;
    }

    public String getFulltextPath() {
        return fulltextPath;
    }

    public void setFulltextPath(String fulltextPath) {
        this.fulltextPath = fulltextPath;
    }

    public String getOriginalTextHtml() {
        return originalTextHtml;
    }

    public void setOriginalTextHtml(String originalTextHtml) {
        this.originalTextHtml = originalTextHtml;
    }

    public String getTranslationTextHtml() {
        return translationTextHtml;
    }

    public void setTranslationTextHtml(String translationTextHtml) {
        this.translationTextHtml = translationTextHtml;
    }

    public String getDisplayTitle() {
        return displayTitle;
    }

    public void setDisplayTitle(String displayTitle) {
        this.displayTitle = displayTitle;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPrivateDocpath() {
        return privateDocpath;
    }

    public void setPrivateDocpath(String privateDocpath) {
        this.privateDocpath = privateDocpath;
    }
}
