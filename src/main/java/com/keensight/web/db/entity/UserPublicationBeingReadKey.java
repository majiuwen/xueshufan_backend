package com.keensight.web.db.entity;

public class UserPublicationBeingReadKey {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_user_publication_being_read.user_id
     *
     * @mbggenerated Wed Nov 04 18:34:58 CST 2020
     */
    private Long userId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_user_publication_being_read.publication_id
     *
     * @mbggenerated Wed Nov 04 18:34:58 CST 2020
     */
    private Long publicationId;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_user_publication_being_read.user_id
     *
     * @return the value of t_user_publication_being_read.user_id
     *
     * @mbggenerated Wed Nov 04 18:34:58 CST 2020
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_user_publication_being_read.user_id
     *
     * @param userId the value for t_user_publication_being_read.user_id
     *
     * @mbggenerated Wed Nov 04 18:34:58 CST 2020
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_user_publication_being_read.publication_id
     *
     * @return the value of t_user_publication_being_read.publication_id
     *
     * @mbggenerated Wed Nov 04 18:34:58 CST 2020
     */
    public Long getPublicationId() {
        return publicationId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_user_publication_being_read.publication_id
     *
     * @param publicationId the value for t_user_publication_being_read.publication_id
     *
     * @mbggenerated Wed Nov 04 18:34:58 CST 2020
     */
    public void setPublicationId(Long publicationId) {
        this.publicationId = publicationId;
    }
}