package com.keensight.web.db.entity;

import java.util.Date;

public class StatsPublicationWeekly extends StatsPublicationWeeklyKey {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_stats_publication_weekly.citations
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    private Integer citations;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_stats_publication_weekly.reads
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    private Integer reads;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_stats_publication_weekly.recommendations
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    private Integer recommendations;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_stats_publication_weekly.sharings
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    private Integer sharings;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_stats_publication_weekly.savings
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    private Integer savings;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_stats_publication_weekly.downloads
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    private Integer downloads;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_stats_publication_weekly.followings
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    private Integer followings;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_stats_publication_weekly.reported
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    private Integer reported;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_stats_publication_weekly.updated_at
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    private Date updatedAt;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_stats_publication_weekly.citations
     *
     * @return the value of t_stats_publication_weekly.citations
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public Integer getCitations() {
        return citations;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_stats_publication_weekly.citations
     *
     * @param citations the value for t_stats_publication_weekly.citations
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public void setCitations(Integer citations) {
        this.citations = citations;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_stats_publication_weekly.reads
     *
     * @return the value of t_stats_publication_weekly.reads
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public Integer getReads() {
        return reads;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_stats_publication_weekly.reads
     *
     * @param reads the value for t_stats_publication_weekly.reads
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public void setReads(Integer reads) {
        this.reads = reads;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_stats_publication_weekly.recommendations
     *
     * @return the value of t_stats_publication_weekly.recommendations
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public Integer getRecommendations() {
        return recommendations;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_stats_publication_weekly.recommendations
     *
     * @param recommendations the value for t_stats_publication_weekly.recommendations
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public void setRecommendations(Integer recommendations) {
        this.recommendations = recommendations;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_stats_publication_weekly.sharings
     *
     * @return the value of t_stats_publication_weekly.sharings
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public Integer getSharings() {
        return sharings;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_stats_publication_weekly.sharings
     *
     * @param sharings the value for t_stats_publication_weekly.sharings
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public void setSharings(Integer sharings) {
        this.sharings = sharings;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_stats_publication_weekly.savings
     *
     * @return the value of t_stats_publication_weekly.savings
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public Integer getSavings() {
        return savings;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_stats_publication_weekly.savings
     *
     * @param savings the value for t_stats_publication_weekly.savings
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public void setSavings(Integer savings) {
        this.savings = savings;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_stats_publication_weekly.downloads
     *
     * @return the value of t_stats_publication_weekly.downloads
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public Integer getDownloads() {
        return downloads;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_stats_publication_weekly.downloads
     *
     * @param downloads the value for t_stats_publication_weekly.downloads
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public void setDownloads(Integer downloads) {
        this.downloads = downloads;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_stats_publication_weekly.followings
     *
     * @return the value of t_stats_publication_weekly.followings
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public Integer getFollowings() {
        return followings;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_stats_publication_weekly.followings
     *
     * @param followings the value for t_stats_publication_weekly.followings
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public void setFollowings(Integer followings) {
        this.followings = followings;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_stats_publication_weekly.reported
     *
     * @return the value of t_stats_publication_weekly.reported
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public Integer getReported() {
        return reported;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_stats_publication_weekly.reported
     *
     * @param reported the value for t_stats_publication_weekly.reported
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public void setReported(Integer reported) {
        this.reported = reported;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_stats_publication_weekly.updated_at
     *
     * @return the value of t_stats_publication_weekly.updated_at
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_stats_publication_weekly.updated_at
     *
     * @param updatedAt the value for t_stats_publication_weekly.updated_at
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
