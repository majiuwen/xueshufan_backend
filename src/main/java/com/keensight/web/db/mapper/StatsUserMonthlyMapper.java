package com.keensight.web.db.mapper;

import com.keensight.web.db.entity.StatsUserMonthly;
import com.keensight.web.db.entity.StatsUserMonthlyExample;
import com.keensight.web.db.entity.StatsUserMonthlyKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface StatsUserMonthlyMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_stats_user_monthly
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    int countByExample(StatsUserMonthlyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_stats_user_monthly
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    int deleteByExample(StatsUserMonthlyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_stats_user_monthly
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    int deleteByPrimaryKey(StatsUserMonthlyKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_stats_user_monthly
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    int insert(StatsUserMonthly record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_stats_user_monthly
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    int insertSelective(StatsUserMonthly record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_stats_user_monthly
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    List<StatsUserMonthly> selectByExample(StatsUserMonthlyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_stats_user_monthly
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    StatsUserMonthly selectByPrimaryKey(StatsUserMonthlyKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_stats_user_monthly
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    int updateByExampleSelective(@Param("record") StatsUserMonthly record, @Param("example") StatsUserMonthlyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_stats_user_monthly
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    int updateByExample(@Param("record") StatsUserMonthly record, @Param("example") StatsUserMonthlyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_stats_user_monthly
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    int updateByPrimaryKeySelective(StatsUserMonthly record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_stats_user_monthly
     *
     * @mbggenerated Thu Nov 26 10:45:05 CST 2020
     */
    int updateByPrimaryKey(StatsUserMonthly record);

    /**
     * 旧作者文献月度统计总数减少更新
     * @param record
     * @return
     */
    int updateOldStatsUserMonth(StatsUserMonthly record);

    /**
     * 新作者文献月度统计总数增加更新
     * @param record
     * @return
     */
    int updateNewStatsUserMonth(StatsUserMonthly record);

    /**
     * 根据作者ID获取作者月文献统计
     * @param authorIds
     * @return
     */
    List<Long> getStatsUserMonthly(@Param("authorIds") List<Long> authorIds, @Param("month")Date month);

    /**
     * 批量插入
     * @param statsUserMonthlies
     * @return
     */
    int insertStatsUsersMonthly(List<StatsUserMonthly> statsUserMonthlies);

    /**
     * 作者月文献统计-相关操作次数增加
     * 定义Flg 0:引用 1:阅读 2:推荐 3:分享 4:收藏 5:下载 6:关注 7:举报
     * @param authorIds
     * @param actionFlg
     */
    int updateStatsUserMonthlyIncrease(@Param("authorIds")List<Long> authorIds, @Param("actionFlg")String actionFlg,
                                       @Param("month")Date month, @Param("updatedAt")Date updatedAt);

    /**
     * 作者月文献统计-相关操作减少
     * 定义Flg 2:推荐 4:收藏 6:关注
     * @param authorIds
     * @param actionFlg
     */
    int updateStatsUserMonthlyReduce(@Param("authorIds")List<Long> authorIds, @Param("actionFlg")String actionFlg,
                                     @Param("month")Date month, @Param("updatedAt")Date updatedAt);

    /**
     * 旧作者文献月度统计总数的作者发文数减少更新
     * @param record
     * @return
     */
    int updateOldStatsUserMonthPublications(StatsUserMonthly record);

    /**
     * 新作者文献月度统计总数的作者发文数增加更新
     * @param record
     * @return
     */
    int updateNewStatsUserMonthPublications(StatsUserMonthly record);

    /**
     * 作者月文献统计查询
     * @param userId
     * @return
     */
    List<StatsUserMonthly> getStatsUserMonthlyLists(@Param("userId")Integer userId);
}
