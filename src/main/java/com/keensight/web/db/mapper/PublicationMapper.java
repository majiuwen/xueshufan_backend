package com.keensight.web.db.mapper;

import com.keensight.web.db.entity.Publication;
import com.keensight.web.db.entity.PublicationExample;
import com.keensight.web.db.entity.PublicationKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PublicationMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_publication
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int countByExample(PublicationExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_publication
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int deleteByExample(PublicationExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_publication
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int deleteByPrimaryKey(PublicationKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_publication
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int insert(Publication record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_publication
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int insertSelective(Publication record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_publication
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    List<Publication> selectByExample(PublicationExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_publication
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    Publication selectByPrimaryKey(PublicationKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_publication
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int updateByExampleSelective(@Param("record") Publication record, @Param("example") PublicationExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_publication
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int updateByExample(@Param("record") Publication record, @Param("example") PublicationExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_publication
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int updateByPrimaryKeySelective(Publication record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_publication
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int updateByPrimaryKey(Publication record);

    /**
     * 文献详细信息一览取得(共通取得用)
     * @param publicationIds 文献ID
     * @param userId 用户ID
     * @return
     */
    List<Publication> getPublicationDetailList(@Param("publicationIds")List<Long> publicationIds, @Param("userId")Long userId);

    /**
     * 文献统计结果取得方法
     *
     * @param userId 用户ID
     * @return 文献统计结果集合
     */
    List<Publication> getPublicationStatisticsByUserId(@Param("userId") Long userId);

    /**
     * 根据检索条件查询文献ID集合
     *
     * @param userId      用户ID
     * @param docTypeList 文献类型集合
     * @param sortColumns 排序列
     * @return 文献ID集合
     */
    List<Long> getPublicationIdList(@Param("userId") Long userId, @Param("docTypeList") List<String> docTypeList, @Param("sortColumns") String sortColumns);

    /**
     * 根据父文献ID检索参考文献ID集合 文献的参考文献使用
     * @param publicationId 父文献ID
     * @param sortColumns   排序列
     * @return 参考文献ID集合
     */
    List<Long> getArticlesReferenceIdList(@Param("publicationId") Long publicationId,@Param("sortColumns") String sortColumns);

    /**
     * 根据被引用文献ID检索引文ID集合 文献的引文使用
     * @param publicationId 父文献ID
     * @param sortColumns   排序列
     * @return 参考文献ID集合
     */
    List<Long> getArticlesCitationIdList(@Param("publicationId") Long publicationId,@Param("sortColumns") String sortColumns);

    /**
     * 获取用户管理全文列表
     * @param userId
     * @param sortColumns
     * @return
     */
    List<Publication> getUserManageAccessList(@Param("userId") Long userId, @Param("sortColumns") String sortColumns);

    /**
     * 更新文献表的文档公开字段
     * @param publicationId
     * @param publicDoc
     * @return
     */
    int updatePublicDoc(@Param("publicationId")List<Long> publicationId,@Param("publicDoc") Boolean publicDoc);

    /**
     * 用户新增文献时publicationId最大值查询
     * @return
     */
    Long getMaxPublicationId();

    /**
     * 根据doi查询文献信息
     * @return
     */
    List<Publication> selPublicationByILikeDoi(@Param("doi") String doi);
}
