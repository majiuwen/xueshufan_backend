package com.keensight.web.db.mapper;

import com.keensight.web.db.entity.UserEducation;
import com.keensight.web.db.entity.UserEducationExample;
import java.util.List;

import com.keensight.web.db.entity.UserEducationalBackground;
import org.apache.ibatis.annotations.Param;

public interface UserEducationMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_user_education
     *
     * @mbggenerated Thu Nov 11 11:24:46 CST 2021
     */
    int countByExample(UserEducationExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_user_education
     *
     * @mbggenerated Thu Nov 11 11:24:46 CST 2021
     */
    int deleteByExample(UserEducationExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_user_education
     *
     * @mbggenerated Thu Nov 11 11:24:46 CST 2021
     */
    int insert(UserEducation record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_user_education
     *
     * @mbggenerated Thu Nov 11 11:24:46 CST 2021
     */
    int insertSelective(UserEducation record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_user_education
     *
     * @mbggenerated Thu Nov 11 11:24:46 CST 2021
     */
    List<UserEducation> selectByExample(UserEducationExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_user_education
     *
     * @mbggenerated Thu Nov 11 11:24:46 CST 2021
     */
    int updateByExampleSelective(@Param("record") UserEducation record, @Param("example") UserEducationExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_user_education
     *
     * @mbggenerated Thu Nov 11 11:24:46 CST 2021
     */
    int updateByExample(@Param("record") UserEducation record, @Param("example") UserEducationExample example);

    /**
     * 获取作者教育背景信息
     * @param userId
     * @return
     */
    List<UserEducation> getUserEducationList(@Param("userId") Integer userId);
}