package com.keensight.web.db.mapper;

import com.keensight.web.db.entity.Announcement;
import com.keensight.web.db.entity.AnnouncementExample;
import com.keensight.web.db.entity.AnnouncementKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AnnouncementMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_announcement
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int countByExample(AnnouncementExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_announcement
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int deleteByExample(AnnouncementExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_announcement
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int deleteByPrimaryKey(AnnouncementKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_announcement
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int insert(Announcement record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_announcement
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int insertSelective(Announcement record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_announcement
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    List<Announcement> selectByExample(AnnouncementExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_announcement
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    Announcement selectByPrimaryKey(AnnouncementKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_announcement
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int updateByExampleSelective(@Param("record") Announcement record, @Param("example") AnnouncementExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_announcement
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int updateByExample(@Param("record") Announcement record, @Param("example") AnnouncementExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_announcement
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int updateByPrimaryKeySelective(Announcement record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_announcement
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int updateByPrimaryKey(Announcement record);
}