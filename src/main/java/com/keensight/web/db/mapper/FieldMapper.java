package com.keensight.web.db.mapper;

import com.keensight.web.db.entity.Field;
import com.keensight.web.db.entity.FieldExample;
import com.keensight.web.db.entity.FieldKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FieldMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_field
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int countByExample(FieldExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_field
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int deleteByExample(FieldExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_field
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int deleteByPrimaryKey(FieldKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_field
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int insert(Field record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_field
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int insertSelective(Field record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_field
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    List<Field> selectByExample(FieldExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_field
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    Field selectByPrimaryKey(FieldKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_field
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int updateByExampleSelective(@Param("record") Field record, @Param("example") FieldExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_field
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int updateByExample(@Param("record") Field record, @Param("example") FieldExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_field
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int updateByPrimaryKeySelective(Field record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_field
     *
     * @mbggenerated Wed Nov 04 18:29:30 CST 2020
     */
    int updateByPrimaryKey(Field record);

    /**
     * 根据研究领域ID获取研究领域和相关研究领域信息
     *
     * @param fieldId 研究领域ID
     * @param userId  用户ID
     * @return
     */
    Field getFieldAndRelatedFieldListByFieldId(@Param("fieldId") Long fieldId, @Param("userId") Long userId);

    /**
     * 根据研究领域ID集合检索研究领域信息
     *
     * @param fieldIdList
     * @return
     */
    List<Field> getFieldInfoByFieldIdList(@Param("fieldIdList") List<Long> fieldIdList);

    /**
     * 获取地图显示用户的领域信息
     * @param userId
     * @return
     */
    List<Field> getUserMakerFieldList(@Param("userId") Long userId);
}
