package com.keensight.web.db.mapper;

import com.keensight.web.db.entity.Field;
import com.keensight.web.db.entity.User;
import com.keensight.web.db.entity.UserExample;
import com.keensight.web.db.entity.UserKey;
import com.keensight.web.model.UserModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface UserMapper {

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_user
     *
     * @mbggenerated Wed Nov 04 18:56:05 CST 2020
     */
    int countByExample(UserExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_user
     *
     * @mbggenerated Wed Nov 04 18:56:05 CST 2020
     */
    int deleteByExample(UserExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_user
     *
     * @mbggenerated Wed Nov 04 18:56:05 CST 2020
     */
    int deleteByPrimaryKey(UserKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_user
     *
     * @mbggenerated Wed Nov 04 18:56:05 CST 2020
     */
    int insert(User record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_user
     *
     * @mbggenerated Wed Nov 04 18:56:05 CST 2020
     */
    int insertSelective(User record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_user
     *
     * @mbggenerated Wed Nov 04 18:56:05 CST 2020
     */
    List<User> selectByExample(UserExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_user
     *
     * @mbggenerated Wed Nov 04 18:56:05 CST 2020
     */
    User selectByPrimaryKey(UserKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_user
     *
     * @mbggenerated Wed Nov 04 18:56:05 CST 2020
     */
    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_user
     *
     * @mbggenerated Wed Nov 04 18:56:05 CST 2020
     */
    int updateByExample(@Param("record") User record, @Param("example") UserExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_user
     *
     * @mbggenerated Wed Nov 04 18:56:05 CST 2020
     */
    int updateByPrimaryKeySelective(User record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_m_user
     *
     * @mbggenerated Wed Nov 04 18:56:05 CST 2020
     */
    int updateByPrimaryKey(User record);

    /**
     * 根据用户Id获取用户详细信息
     * @param userId
     * @param longUserId
     * @return
     */
    User selectUserInfoByUserId(@Param("userId") Long userId, @Param("loginUserId") Long longUserId);

    /**
     * 现在文献用户作者名取得
     * @param publicationId
     * @return
     */
    List<User> getDisplayNames(@Param("publicationId") Long publicationId);

    /**
     * 根据文献ID获取共同作者(文献全文)
     * @param publicationId
     * @return
     */
    List<Long> getAuthorsByPublicationId(@Param("publicationId") Long publicationId);

    /**
     * 用户注册 插入
     * @param userId
     * @param normalizedName
     * @param displayName
     * @param profilePhotoPath
     * @param userCategory
     * @param currentPosition
     * @param institutionName
     * @param departmentName
     * @param programDegree
     * @param yearStartedProgram
     * @return
     */
    int insertRegist(@Param("userId") Long userId,@Param("normalizedName") String normalizedName,@Param("displayName") String displayName,
                     @Param("profilePhotoPath") String profilePhotoPath,@Param("userCategory") String userCategory,@Param("currentPosition") String currentPosition,
                     @Param("institutionName") String institutionName,@Param("departmentName") String departmentName,@Param("programDegree") String programDegree,
                     @Param("yearStartedProgram") Integer yearStartedProgram,@Param("createdAt") Date createdAt,@Param("updatedAt") Date updatedAt);

    /**
     * 用户注册 更新
     * @param userId
     * @param normalizedName
     * @param displayName
     * @param profilePhotoPath
     * @param userCategory
     * @param currentPosition
     * @param institutionName
     * @param departmentName
     * @param programDegree
     * @param yearStartedProgram
     * @param updatedAt
     * @return
     */
    int updateRegist(@Param("userId") Long userId,@Param("normalizedName") String normalizedName,@Param("displayName") String displayName,
                     @Param("profilePhotoPath") String profilePhotoPath,@Param("userCategory") String userCategory,@Param("currentPosition") String currentPosition,
                     @Param("institutionName") String institutionName,@Param("departmentName") String departmentName,@Param("programDegree") String programDegree,
                     @Param("yearStartedProgram") Integer yearStartedProgram, @Param("updatedAt") Date updatedAt);

    /**
     * 用户注册时userId最大值查询
     * @return
     */
    Long getMaxUserId();

    /**
     * 用户注册时userAuthId最大值查询
     * @return
     */
    Long getMaxUserAuthId();

    /**
     * 根据用户Id获取该用户关注的其他用户的详细信息
     *
     * @param userId
     * @param userIds
     * @return
     */
    List<User> getUserInfoList(@Param("userId") Long userId, @Param("userIds") List<Long> userIds);

    /**
     * 获取用户详细信息
     *
     * @param userIds
     * @return
     */
    List<User> getUserDetailInfo(@Param("userIds") List<Long> userIds);

    /**
     * 获取账户状态
     *
     * @param userId
     * @return
     */
    String getAccountStatus(@Param("userId") Long userId);

    /**
     * 获取人才地图maker方法
     * @param nLng
     * @param nLat
     * @param sLng
     * @param sLat
     * @param code
     * @param isethniccn
     * @param fieldId
     * @return
     */
    List<UserModel> getUserMaker(@Param("nLng") Double nLng, @Param("nLat") Double nLat, @Param("sLng") Double sLng, @Param("sLat") Double sLat, @Param("code") String code, @Param("isethniccn") String isethniccn, @Param("fieldId") Long fieldId);
}
