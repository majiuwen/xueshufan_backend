package com.keensight.web.helper;

import com.keensight.web.constants.enums.UploadFolder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class FileServerHelper {
    @Value("${keensight.file.server.url}")
    private String fileServerUrl;

    public String getFileServerUrl(String filePath, UploadFolder uploadFolder) {
        String fileUrl = "";

        if (StringUtils.isEmpty(filePath)) {
            return fileUrl;
        }

        fileUrl = filePath;

        return fileUrl;
    }
}
