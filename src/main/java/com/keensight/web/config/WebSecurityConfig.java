package com.keensight.web.config;

import cn.newtouch.dl.turbocloud.config.TurboSecurityConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends TurboSecurityConfig {

    /**
     * 设置请求(各个系统可以覆写该方法，增加自己的规则)
     *
     */
    @Override
    protected void antMatchers(HttpSecurity httpSecurity) throws Exception {
        super.antMatchers(httpSecurity);

        // 推荐学者一览点击「关注」/「取消关注」
        httpSecurity.authorizeRequests().antMatchers("/userHome/addRecommendedClickToFollow").authenticated();

        // 推荐学者一览点击「关注」/「取消关注」
        httpSecurity.authorizeRequests().antMatchers("/userSummary/updateDeletedByPrimaryKey").authenticated();

        // 推荐学者一览点击「关注」/「取消关注」
        httpSecurity.authorizeRequests().antMatchers("/articlesFullText/updateDeletedByPrimaryKey").authenticated();

        // 研究方向详细关注「关注」/「取消关注」
        httpSecurity.authorizeRequests().antMatchers("/searchResultsDirection/updUserFieldFollowingByFieldId").authenticated();

        // 下载
        httpSecurity.authorizeRequests().antMatchers("/articlesFullText/fileDownload").authenticated();

        // 按钮事件，针对文献的用户操作 0:引用 1:阅读 2:推荐 3:分享 4:收藏 5:下载 6:关注 7:举报
        httpSecurity.authorizeRequests().antMatchers("/userHome/updUserAction").authenticated();

        // 最新动态点赞
        httpSecurity.authorizeRequests().antMatchers("/userSummary/updLikeNums").authenticated();

        // 最新动态点赞
        httpSecurity.authorizeRequests().antMatchers("/userAnnotation/*").authenticated();

        // 最新动态点赞
        httpSecurity.authorizeRequests().antMatchers("/userExcerpt/*").authenticated();

        // 除上面外的所有请求全部都可以用
        httpSecurity.authorizeRequests().anyRequest().permitAll();

        httpSecurity.headers().frameOptions().sameOrigin();
    }
}
