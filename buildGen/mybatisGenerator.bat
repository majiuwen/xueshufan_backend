@echo off

rem ====================================
set JAVA_HOME=D:\java\8


rem ====================================
set path=.;%path%
set path=%JAVA_HOME%\bin;%path%


rem ====================================
set classpath=buildGen/lib/mybatis-generator-core-1.3.2.jar;%classpath%
set classpath=buildGen/lib/postgresql-connector-jdbc4.jar;%classpath%

@echo on

rem ====================================
rem MyBatisGenerator
rem java -jar mybatis-generator-core-1.3.2.jar -configfile genconfig_postgresql.xml -overwrite

java -cp lib/mybatis-generator-core-1.3.2.jar org.mybatis.generator.api.ShellRunner -configfile genconfig_postgresql.xml
